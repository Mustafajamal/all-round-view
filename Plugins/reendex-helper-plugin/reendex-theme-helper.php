<?php
/**
 * Plugin Name: Reendex Helper Plugin
 *
 * Plugin URI: http://via-theme.com
 * Version: 1.3
 * Description: After you install the Reendex Theme, you have to install this Helper Plugin in order to get all the functions of our theme.
 * Author: Via-Theme
 * Author URI: http://via-theme.com
 * Text Domain: reendex
 *
 * @package Reendex
 * License:
 * Copyright (c) 2018, reendex
 */

// define.
define( 'PLG_URL', plugins_url() );
define( 'PLG_DIR', dirname( __FILE__ ) );
$do_not_duplicate = array();
/**
 * Reendex all custom post types
 */
include_once( PLG_DIR . '/reendex-custom-post.php' );
include_once( PLG_DIR . '/includes/twitteroauth.php' );
// Reendex - Parallax.
// Usages:[reendex_parallax_shortcode].
if ( ! function_exists( 'reendex_parallax' ) ) {
	/**
	 * Parallax Shortcode
	 *
	 * Displays parallax.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of parallax.
	 */
	function reendex_parallax( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'slider_image'          => '',
			'slider_title_one'      => '',
			'slider_title_two'      => '',
			'slider_title_three'    => '',
			'slider_title_four'     => '',
			'parallax'              => '',
			'padding'               => '',
		), $atts );
		ob_start();
		$image_url = wp_get_attachment_image_src( $atts['slider_image'], 'full' );
		$padding = '';
		?>
		<?php
		$args = array(
			'numberposts' => 10,
			'offset' => 0,
			'category' => 0,
			'orderby' => 'post_date',
			'order' => 'DESC',
			'include' => '',
			'exclude' => '',
			'meta_key' => '',
			'meta_value' =>'',
			'post_type' => 'post',
			'post_status' => 'publish',
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field'    => 'slug',
					'terms'    => 'articles'
				)
			),
			'suppress_filters' => true
		);

		$recent_posts = wp_get_recent_posts( $args, ARRAY_A );
		$latest_1 = $recent_posts[0];
		$latest_2 = $recent_posts[1];
		$latest_3 = $recent_posts[2];
		$latest_4 = $recent_posts[3];
		$latest_5 = $recent_posts[4];
		?>
		<div id="<?php if ( 1 == $atts['parallax'] ) { echo 'parallax-section'; } ?>" class="slider_area"> 
			<div class="image1 img-overlay1" style="<?php if ( '' != $image_url ) { echo 'background-image:url( ' . esc_url( $image_url[0] ) . ' )'; } ?>"> 
				<div class="container"> 
					<div class="caption text-center" style="<?php if ( '' != $padding ) { echo 'padding:' . esc_attr( $atts['padding'] ) . 'px 0'; } ?>;">
						<?php if ( '' != $atts['slider_title_one'] ) : ?> 
							<div class="color-white text-center weight-300 medium-caption"><?php print_r($latest_2['post_title']); ?></div>
						<?php endif; ?>
						<?php if ( '' != $atts['slider_title_two'] ) : ?> 
							<div class="color-white text-center weight-800 large-caption"><?php print_r ($latest_1['post_title']); ?></div>
						<?php endif; ?>
						<?php if ( '' != $atts['slider_title_three'] ) : ?> 
							<div class="color-white text-center weight-400 medium-caption"><?php print_r($latest_3['post_title']); ?></div>
						<?php endif; ?>
						<?php if ( '' != $atts['slider_title_four'] ) : ?>                                
							<h5><?php print_r ($latest_4['post_title']); ?></h5>
							<h5><?php print_r ($latest_5['post_title']); ?></h5>
						<?php endif; ?>
					</div><!-- /.caption text-center -->                             
				</div><!-- /.container -->                        
			</div><!-- /.image1 img-overlay1 -->                     
		</div>
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_parallax_shortcode', 'reendex_parallax' );

// Reendex - News Ticker.
// usages:[reendex_newsticker_shortcode].
if ( ! function_exists( 'reendex_newsticker' ) ) {
	/**
	 * Newsticker Shortcode
	 *
	 * Displays newsticker.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of newsticker.
	 */
	function reendex_newsticker( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                 => esc_html__( 'Breaking News', 'reendex' ),
			'ticker_title_length'   => 10,
			'per_page'              => 5,
			'taxonomies'            => '',
			'order'                 => '',
			'orderby'               => '',
			'offset'                => 0,
			'duplicate_posts'       => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="outer
			<?php
			if ( '' !== $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			}
			?>"> 
			<div class="breaking-ribbon"> 
				<h4><?php echo esc_html( $atts['title'] ); ?></h4> 
			</div><!-- /.breaking-ribbon -->                             
			<div class="newsticker"> 
				<ul>
					<?php
						$args = array(
							'post_type'         => 'post',
							'posts_per_page'    => $atts['per_page'],
							'order'             => $atts['order'],
							'orderby'           => $atts['orderby'],
							'offset'            => $atts['offset'],
							'post__not_in'      => $postids,
						);
					if ( ! empty( $atts['taxonomies'] ) ) {
						$vc_taxonomies_types = get_taxonomies(
							array(
								'public' => true,
							)
						);
						$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
							'hide_empty' => false,
							'include' => $atts['taxonomies'],
						) );
						$args['tax_query'] = array();
						$tax_queries = array(); // List of taxnonomies.
						foreach ( $terms as $t ) {
							if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
								$tax_queries[ $t->taxonomy ] = array(
									'taxonomy' => $t->taxonomy,
									'field' => 'id',
									'terms' => array( $t->term_id ),
									'include_children'  => false,
									'relation' => 'IN',
								);
							} else {
								$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
							}
						}
						$args['tax_query'] = array_values( $tax_queries );
						$args['tax_query']['relation'] = 'OR';
					}
					$query = new WP_Query( $args );
					while ( $query->have_posts() ) :
						$query->the_post();
						$do_not_duplicate[] = get_the_ID();
					?>
					<li> 
						<h4>
							<span class="category">
								<?php
									$i = 0;
								foreach ( ( get_the_category() ) as $category ) {
									$i++;
									if ( 1 == $i ) { echo esc_attr( $category->cat_name . ':' ); }
								}
							?>
							</span>
							<a href="<?php the_permalink(); ?>"> <?php reendex_short_title( $atts['ticker_title_length'] ); ?></a>
						</h4> 
					</li>
					<?php endwhile;
					wp_reset_postdata();
					?>
				</ul>                                 
				<div class="navi"> 
					<button class="up">
						<i class="fa fa-caret-left"></i>
					</button>                                     
					<button class="down">
						<i class="fa fa-caret-right"></i>
					</button>                                     
				</div><!-- /.navi -->               
			</div><!-- /.newsticker -->                             
		</div><!-- /.outer -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_newsticker_shortcode', 'reendex_newsticker' );

// Reendex - RSS News Ticker.
// usages:[reendex_news_ticker_rss].
if ( ! function_exists( 'reendex_news_ticker_rss' ) ) {
	/**
	 * News Ticker RSS Feed Shortcode
	 *
	 * Displays News Ticker RSS Feed.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Ticker RSS Feed.
	 */
	function reendex_news_ticker_rss( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'         => esc_html__( 'News RSS Feed', 'reendex' ),
			'urls'          => '',
			'count'         => 5,
			'show_date'     => 0,
			'show_desc'     => 0,
			'open_newtab'   => 1,
			'strip_desc'    => 100,
			'read_more'     => esc_html__( '...', 'reendex' ),
			'strip_title'   => 0,
			'ticker_speed'  => 4,
			'extra_class'   => '',
		), $atts );
		ob_start();
		?>
		<div class="outer-rss <?php if ( '' !== $atts['extra_class'] ) { echo esc_attr( $atts['extra_class'] ); } ?>"> 
			<div class="breaking-ribbon"> 
				<h4><?php echo esc_html( $atts['title'] ); ?></h4> 
			</div><!-- /.breaking-ribbon -->
			<div class="newsticker-rss">  
				<?php $args = array(
					'urls'          => $atts['urls'],
					'count'         => $atts['count'],
					'show_date'     => $atts['show_date'],
					'show_desc'     => $atts['show_desc'],
					'open_newtab'   => $atts['open_newtab'],
					'strip_desc'    => $atts['strip_desc'],
					'read_more'     => $atts['read_more'],
					'strip_title'   => $atts['strip_title'],
					'ticker_speed'  => $atts['ticker_speed'],
				);
				$ags = new WP_Query( $args );
				?>
				<?php echo "\n" . '
					<div class="news-ticker-rss">' . "\n";
						ticker_rss_parser( $atts );
						echo "\n" . '
						<div class="navi"> 
							<button class="up">
								<i class="fa fa-caret-left"></i>
							</button>                                     
							<button class="down">
								<i class="fa fa-caret-right"></i>
							</button>                                     
						</div>
					</div>
					' . "\n";
				?>					
			</div><!-- /.newsticker-rss -->
		</div><!-- /.outer-rss -->    
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_ticker_rss_shortcode', 'reendex_news_ticker_rss' );

// Reendex - On-Air Ticker.
// usages:[reendex_on_air_ticker_shortcode].
if ( ! function_exists( 'reendex_on_air_ticker' ) ) {
	/**
	 * On-air ticker Shortcode
	 *
	 * Displays on-air ticker.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of on-air ticker.
	 */
	function reendex_on_air_ticker( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'         => '',
			'ticker_text'   => '',
			'ticker_link'   => '',
			'extra_class'   => '',
		), $atts );
		ob_start();
		?>
		<div class="outer
			<?php
			if ( '' !== $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			}
			?>">
			<?php if ( '' !== $atts['title'] ) : ?> 
				<div class="breaking-ribbon"> 
					<h4><?php echo esc_html( $atts['title'] ); ?></h4> 
				</div><!-- /.breaking-ribbon -->
			<?php endif; ?>         
			<div class="news-on-air"> 
				<ul> 
					<li> 
						<h4><i class="fa fa-video-camera" aria-hidden="true"></i><a href="<?php echo esc_url( $atts['ticker_link'] ); ?>"><?php echo esc_html( $atts['ticker_text'] ); ?></a></h4> 
					</li>                                     
				</ul>                                 
			</div> <!-- /.news-on-air -->                            
		</div><!-- /.outer -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_on_air_ticker_shortcode', 'reendex_on_air_ticker' );

// Reendex - News Slider.
// usages:[reendex_news_slider_shortcode].
if ( ! function_exists( 'reendex_news_slider' ) ) {
	/**
	 * Slider Shortcode
	 *
	 * Displays slider.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of slider.
	 */
	function reendex_news_slider( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title_length' 	    => 10,
			'taxonomies'        => '',
			'order' 			=> 'ASC',
			'category_show'     => 0,
			'per_page' 			=> 8,
			'target'            => '_self',
			'order'             => '',
			'orderby'           => '',
			'offset'            => 0,
			'duplicate_posts'   => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div id="news-slider" class="owl-carousel <?php echo esc_attr( $atts['extra_class'] ); ?>"> 
			<?php $args = array(
				'post_type'             => 'post',
				'post_status'           => 'publish',
				'ignore_sticky_posts'   => 1,
				'posts_per_page'        => $atts['per_page'],
				'order'                 => $atts['order'],
				'orderby'               => $atts['orderby'],
				'offset'                => $atts['offset'],
				'post__not_in'          => $postids,
			);
			if ( ! empty( $atts['taxonomies'] ) ) {
				$vc_taxonomies_types = get_taxonomies(
					array(
						'public' => true,
					)
				);
				$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
					'hide_empty' => false,
					'include' => $atts['taxonomies'],
				) );
				$args['tax_query'] = array();
				$tax_queries = array(); // List of taxonomies.
				foreach ( $terms as $t ) {
					if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
						$tax_queries[ $t->taxonomy ] = array(
							'taxonomy' => $t->taxonomy,
							'field' => 'id',
							'terms' => array( $t->term_id ),
							'include_children'  => false,
							'relation' => 'IN',
						);
					} else {
						$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
					}
				}
				$args['tax_query'] = array_values( $tax_queries );
				$args['tax_query']['relation'] = 'OR';
			}
			global $post;
			$rows = 4;
			$count = 0;
			$classitem = 0;
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) :
				$query->the_post();
				$do_not_duplicate[] = get_the_ID();
				if ( $rows > 1 && 0 == $count % $rows ) {
					echo '<div class="news-slide">';
					$classitem = 0;
				}
				if ( 0 == $classitem ) {
					$itemclass = 'first';
				} elseif ( 1 == $classitem ) {
					$itemclass = 'second';
				} elseif ( 2 == $classitem ) {
					$itemclass = 'third';
				} elseif ( 3 == $classitem ) {
					$itemclass = 'fourth';
				}
				?>
				<div class="news-slider-layer <?php echo esc_attr( $itemclass ); ?>"> 
					<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"> 
						<div class="content">
							<?php
							$i = 0;
							$posttags = get_the_tags();
							$tag_count = 0;
							if ( 0 == $atts['category_show'] ) :
								foreach ( ( get_the_category() ) as $category ) {
									$i++;
									if ( 1 == $i ) {
										?>
											<span class="category-tag bg-<?php echo esc_attr( $count + 1 ); ?>"><?php echo esc_html( $category->cat_name ); ?></span>
										<?php
									}
								}
							else :
								if ( $posttags ) {
									foreach ( $posttags as $tag ) {
										$tag_count++;
										$i++;
										if ( 1 == $i ) { ?>
											<span class="category-tag bg-<?php echo esc_attr( $count + 1 ); ?>">
												<?php echo esc_html( $tag->name ); ?>
											</span>
										<?php }
										if ( $tag_count > 1 ) {
											break;
										}
									}
								}
							endif; ?>
							<p><?php reendex_short_title( $atts['title_length'] ); ?></p> 
						</div><!-- /.content -->                                         
						<?php
						if ( 0 == $classitem ) {
							the_post_thumbnail( 'reendex_slider1_thumb_1' );
						} elseif ( 1 == $classitem ) {
							the_post_thumbnail( 'reendex_slider1_thumb_2' );
						} elseif ( 2 == $classitem ) {
							the_post_thumbnail( 'reendex_slider1_thumb_3' );
						} elseif ( 3 == $classitem ) {
							the_post_thumbnail( 'reendex_slider1_thumb_3' );
						}
						?>
						<?php if ( has_post_format( 'video' ) ) : ?>
							<span class="video-icon-large">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-large.png" alt="video"/>
							</span>
						<?php endif; ?>
					</a>                                     
				</div><!-- /.news-slider-layer -->
				<?php
				if ( $rows > 1 && ( $count % $rows == $rows - 1 || $count == $query->post_count - 1 ) ) {
					echo '</div>';
				}
				$count++;
				$classitem++;
				?>
			<?php endwhile;
			wp_reset_postdata();
			?>
		</div><!-- /#news-slider -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_slider_shortcode', 'reendex_news_slider' );

// Reendex - World Slider.
// usages:[reendex_world_slider_shortcode].
if ( ! function_exists( 'reendex_world_slider' ) ) {
	/**
	 * World Slider Shortcode
	 *
	 * Displays World Slider.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of World Slider.
	 */
	function reendex_world_slider( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title_length'          => 15,
			'category_show'         => 1,
			'date_show'             => 0,
			'taxonomies'            => '',
			'per_page'              => 5,
			'target'                => '_self',
			'order'                 => '',
			'orderby'               => '',
			'offset'                => 0,
			'duplicate_posts'       => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="<?php if ( '' != $atts['extra_class'] ) { echo esc_attr( $atts['extra_class'] ); } ?>">
			<div class="flexslider">
				<ul class="slides">
				<?php
					$i = 0;
					$args = array(
						'post_type'             => 'post',
						'post_status'           => 'publish',
						'ignore_sticky_posts'   => 1,
						'posts_per_page'        => $atts['per_page'],
						'order'                 => $atts['order'],
						'orderby'               => $atts['orderby'],
						'offset'                => $atts['offset'],
						'post__not_in'          => $postids,
					);
				if ( ! empty( $atts['taxonomies'] ) ) {
					$vc_taxonomies_types = get_taxonomies(
						array(
							'public' => true,
						)
					);
					$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
						'hide_empty' => false,
						'include' => $atts['taxonomies'],
					) );
					$args['tax_query'] = array();
					$tax_queries = array(); // List of taxonomies.
					foreach ( $terms as $t ) {
						if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
							$tax_queries[ $t->taxonomy ] = array(
								'taxonomy' => $t->taxonomy,
								'field' => 'id',
								'terms' => array( $t->term_id ),
								'include_children'  => false,
								'relation' => 'IN',
							);
						} else {
							$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
						}
					}
					$args['tax_query'] = array_values( $tax_queries );
					$args['tax_query']['relation'] = 'OR';
				}
				$query = new WP_Query( $args );
				while ( $query->have_posts() ) :
					$query->the_post();
					$do_not_duplicate[] = get_the_ID();
					?>
						<li>
							<?php
								the_post_thumbnail( 'reendex_news6_thumb_1' );
							?>
							<?php if ( has_post_format( 'video' ) ) : ?>
								<span class="video-icon-large">
									<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-large.png" alt="video"/>
								</span>
							<?php endif; ?>
							<div class="meta">
							<h3><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?></a></h3>
							<?php
							if ( 1 == $atts['category_show'] ) {
								$count = 0;
								foreach ( ( get_the_category() ) as $category ) {
									$count++;
									$category_id = get_cat_ID( $category->cat_name );
									$category_link = get_category_link( $category_id );
									if ( 1 == $count ) {
										?>
											<h4 class="slider-category"><?php echo esc_html( $category->cat_name ); ?></h4>
										<?php
									}
								}
							}
							?>
							<?php if ( 1 == $atts['date_show'] ) : ?>
								<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
									<h4 class='slider-day'><?php the_time( esc_html__( 'Y-m-d g:iA', 'reendex' ) ); ?></h4>
								<?php } else { ?>
									<h4 class='slider-day'><?php echo esc_html( get_the_modified_date( 'Y-m-d g:iA' ) ); ?></h4>
								<?php } ?>	
							<?php endif; ?>
							</div><!-- /.meta -->
						</li>
					<?php endwhile;
					wp_reset_postdata();
					?>
				</ul><!-- /.slides -->
			</div><!-- /.flexslider -->
		</div> 
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_world_slider_shortcode','reendex_world_slider' );

// Reendex - Posts Big Gallery Slider 1.
// usages:[reendex_posts_big_gallery_slider_one_shortcode].
if ( ! function_exists( 'reendex_posts_big_gallery_slider_one' ) ) {
	/**
	 * Posts Big Gallery Slider 1 Shortcode
	 *
	 * Displays Posts Big Gallery Slider 1.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Posts Big Gallery Slider 1.
	 */
	function reendex_posts_big_gallery_slider_one( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                     => '',
			'title_color'               => '',
			'title_bg_color'            => '#2c2c34',
			'title_margin_bottom'       => '5',
			'per_page'                  => 6,
			'category_show'             => 0,
			'taxonomies'                => '',
			'label_show'                => 1,
			'target'                    => '_self',
			'number_of_visible_item'    => 'big-gallery-slider-3',
			'order'                     => '',
			'orderby'                   => '',
			'offset'                    => 0,
			'duplicate_posts'           => '',
			'extra_class'               => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="gallery-image <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<?php if ( '' !== $atts['title'] ) : ?>
				<div class="carousel-title" style="
					<?php
					if ( '' !== $atts['title_color'] ) {
						echo 'color:' . esc_attr( $atts['title_color'] ) . ';';
					}
					if ( '' !== $atts['title_bg_color'] ) {
						echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';';
					} ?>
					<?php
					if ( '' != $atts['title_margin_bottom'] ) {
						echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
					} ?>">
					<?php echo esc_html( $atts['title'] ); ?>
				</div><!-- /.carousel-title -->
			<?php endif; ?>
			<div id="<?php echo esc_attr( $atts['number_of_visible_item'] ); ?>" class="owl-carousel">
				<?php
					$args = array(
						'post_type'            => 'post',
						'post_status'          => 'publish',
						'ignore_sticky_posts'  => 1,
						'posts_per_page'       => $atts['per_page'],
						'order'                => $atts['order'],
						'orderby'              => $atts['orderby'],
						'offset'               => $atts['offset'],
						'post__not_in'         => $postids,
					);
				if ( ! empty( $atts['taxonomies'] ) ) {
					$vc_taxonomies_types = get_taxonomies(
						array(
							'public' => true,
						)
					);
					$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
						'hide_empty' => false,
						'include' => $atts['taxonomies'],
					) );
					$args['tax_query'] = array();
					$tax_queries = array(); // List of taxonomies.
					foreach ( $terms as $t ) {
						if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
							$tax_queries[ $t->taxonomy ] = array(
								'taxonomy' => $t->taxonomy,
								'field' => 'id',
								'terms' => array( $t->term_id ),
								'include_children'  => false,
								'relation' => 'IN',
							);
						} else {
							$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
						}
					}
					$args['tax_query'] = array_values( $tax_queries );
					$args['tax_query']['relation'] = 'OR';
				}
				$query = new WP_Query( $args );
				while ( $query->have_posts() ) :
					$query->the_post();
					$do_not_duplicate[] = get_the_ID();
				?>
					<div class="big-gallery">
						<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
						<?php
							the_post_thumbnail( 'reendex_video_gallery_thumb',
								array(
									'class' => 'img-not-lazy',
								)
							);
						?>
						</a>
						<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
							<?php
							if ( 1 === $atts['label_show'] ) :
								$posttags = get_the_tags();
								$tag_count = 0;
								$count = 0;
								if ( 0 == $atts['category_show'] ) {
									$post_cat = get_the_terms( get_the_id(), 'category' );

									if ( $post_cat ) {
										foreach ( $post_cat as $post_cats ) :
											$count++;
											$slug = $post_cats->slug;
											$name = $post_cats->name;
											if ( 1 == $count ) {
												echo '<span class="label-14">' . esc_html( $name ) . '</span>';
											}
										endforeach;
									}
								} elseif ( 1 == $atts['category_show'] ) {
									if ( $posttags ) {
										foreach ( $posttags as $tag ) {
											$tag_id = $tag->term_id;
											$tag_count++;
											$count++;
											if ( 1 == $count ) {
												?>
													<span class="label-14">
														<?php echo esc_html( $tag->name ); ?>
													</span>                                                 
												<?php
											}
											if ( $tag_count > 1 ) {
												break;
											}
										}
									}
								}
							endif;
							?>
							<?php if ( has_post_format( 'video' ) ) : ?>
								<span class="video-icon-large">
									<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-large.png" alt="video"/>
								</span>
							<?php endif; ?>
						</a> 
					</div><!-- /.big-gallery -->
				<?php endwhile;
				wp_reset_postdata();
				?>
			</div>
		</div><!-- /.gallery-image -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_posts_big_gallery_slider_one_shortcode','reendex_posts_big_gallery_slider_one' );

// Reendex - Posts Big Gallery Slider 2.
// usages:[reendex_posts_big_gallery_slider_two_shortcode].
if ( ! function_exists( 'reendex_posts_big_gallery_slider_two' ) ) {
	/**
	 * Posts Big Gallery Slider 2 Shortcode
	 *
	 * Displays Posts Big Gallery Slider 2.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Posts Big Gallery Slider 2.
	 */
	function reendex_posts_big_gallery_slider_two( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'             => '',
			'title_color'       => '',
			'title_bg_color'    => '',
			'title_length'      => '',
			'content_length'    => '',
			'taxonomies'        => '',
			'per_page'          => 9,
			'target'            => '_self',
			'order'             => '',
			'orderby'           => '',
			'offset'            => 0,
			'duplicate_posts'   => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="news-galler-slider-wrapper <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<?php
			if ( '' !== $atts['title'] ) : ?>
				<div class="carousel-title" style="
					<?php
					if ( '' !== $atts['title_color'] ) {
						echo 'color:' . esc_attr( $atts['title_color'] ) . ';';
					}
					if ( '' !== $atts['title_bg_color'] ) {
						echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';';
					}
					?>">
					<?php echo esc_html( $atts['title'] ); ?>
				</div><!-- /.carousel-title --> 
			<?php endif; ?>
			<div class="container">
				<div class="row gallery-wrapper">
					<div id="news-gallery-slider" class="owl-carousel">
						<?php
							$args = array(
								'post_type'            => 'post',
								'post_status'          => 'publish',
								'ignore_sticky_posts'  => 1,
								'posts_per_page'       => $atts['per_page'],
								'order'                => $atts['order'],
								'orderby'              => $atts['orderby'],
								'offset'               => $atts['offset'],
								'post__not_in'         => $postids,
							);
						if ( ! empty( $atts['taxonomies'] ) ) {
							$vc_taxonomies_types = get_taxonomies(
								array(
									'public' => true,
								)
							);
							$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
								'hide_empty' => false,
								'include'    => $atts['taxonomies'],
							) );
							$args['tax_query'] = array();
							$tax_queries = array(); // List of taxonomies.
							foreach ( $terms as $t ) {
								if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
									$tax_queries[ $t->taxonomy ] = array(
										'taxonomy'          => $t->taxonomy,
										'field'             => 'id',
										'terms'             => array( $t->term_id ),
										'include_children'  => false,
										'relation'          => 'IN',
									);
								} else {
									$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
								}
							}
							$args['tax_query'] = array_values( $tax_queries );
							$args['tax_query']['relation'] = 'OR';
						}
						$query = new WP_Query( $args );
						while ( $query->have_posts() ) :
							$query->the_post();
							$do_not_duplicate[] = get_the_ID();
						?>
							<div class="news-gallery-slider">
								<div class="image">
									<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
										<?php the_post_thumbnail( 'reendex_news1_thumb',
											array(
												'class' => 'img-responsive img-not-lazy',
											)
										);
										?>
										<?php if ( has_post_format( 'video' ) ) : ?>
											<span class="video-icon-small">
												<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-small.png" alt="video"/>
											</span>
										<?php endif; ?>
									</a>
								</div><!-- /.image --> 
								<div class="post-content">
									<i class="fa fa-clock-o"></i>
									<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
										<span class="day"><?php the_time( esc_html__( 'D, d M, Y', 'reendex' ) ); ?></span>
									<?php } else { ?>
										<span class="day"><?php echo esc_html( get_the_modified_date( 'D, d M, Y' ) ); ?></span>
									<?php } ?> 
									<h3 class="post-title"><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?></a></h3>
									<?php if ( '' !== $atts['content_length'] ) : ?>                                    
										<div class="content">
											<p class="single-post-content"><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['content_length'] ) ); ?></a></p>
										</div>
									<?php endif; ?>
									<?php
									$count = 0;
									foreach ( ( get_the_category() ) as $category ) {
										$count++;
										$category_id = get_cat_ID( $category->cat_name );
										$category_link = get_category_link( $category_id );
										if ( 1 === $count ) { ?>
											<a href="<?php echo esc_url( $category_link ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( $category->cat_name ); ?></a>
										<?php }
									}
									?>
								</div><!-- /.post-content -->                                 
							</div><!-- /.news-gallery-slider -->
						<?php endwhile;
						wp_reset_postdata();
						?>
					</div><!-- /#news-gallery-slider .owl-carousel-->
				</div><!-- /.row gallery-wrapper -->
			</div><!-- /.container -->
		</div><!-- /.news-galler-slider-wrapper -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_posts_big_gallery_slider_two_shortcode','reendex_posts_big_gallery_slider_two' );

// Reendex - Posts Small Gallery Slider.
// usages:[reendex_posts_small_gallery_slider_shortcode].
if ( ! function_exists( 'reendex_posts_small_gallery_slider' ) ) {
	/**
	 * Posts Small Gallery Slider Shortcode
	 *
	 * Displays Posts Small Gallery Slider.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Posts Small Gallery Slider.
	 */
	function reendex_posts_small_gallery_slider( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                 => '',
			'title_color'           => '',
			'title_bg_color'        => '',
			'title_length'          => '',
			'title_margin_bottom'   => '30',
			'category_show'         => 0,
			'taxonomies'            => '',
			'per_page'              => 6,
			'target'                => '_self',
			'order'                 => '',
			'orderby'               => '',
			'offset'                => 0,
			'duplicate_posts'       => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="<?php echo esc_attr( $atts['extra_class'] ); ?>">
			<?php if ( '' !== $atts['title'] ) : ?>
				<div class="carousel-title" style="
					<?php
					if ( '' !== $atts['title_color'] ) {
						echo 'color:' . esc_attr( $atts['title_color'] ) . ';';
					}
					if ( '' !== $atts['title_bg_color'] ) {
						echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';';
					}
					?>
					<?php
					if ( '' != $atts['title_margin_bottom'] ) {
						echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
					}
					?>">
					<?php echo esc_html( $atts['title'] ); ?>
				</div><!-- /.carousel-title -->
			<?php endif; ?>
			<div id="small-gallery-slider" class="owl-carousel">
				<?php
					$args = array(
						'post_type'            => 'post',
						'post_status'          => 'publish',
						'ignore_sticky_posts'  => 1,
						'posts_per_page'       => $atts['per_page'],
						'order'                => $atts['order'],
						'orderby'              => $atts['orderby'],
						'offset'               => $atts['offset'],
						'post__not_in'         => $postids,
					);
				if ( ! empty( $atts['taxonomies'] ) ) {
					$vc_taxonomies_types = get_taxonomies(
						array(
							'public' => true,
						)
					);
					$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
						'hide_empty' => false,
						'include' => $atts['taxonomies'],
					) );
					$args['tax_query'] = array();
					$tax_queries = array(); // List of taxonomies.
					foreach ( $terms as $t ) {
						if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
							$tax_queries[ $t->taxonomy ] = array(
								'taxonomy' => $t->taxonomy,
								'field' => 'id',
								'terms' => array( $t->term_id ),
								'include_children'  => false,
								'relation' => 'IN',
							);
						} else {
							$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
						}
					}
					$args['tax_query'] = array_values( $tax_queries );
					$args['tax_query']['relation'] = 'OR';
				}
				$query = new WP_Query( $args );
				while ( $query->have_posts() ) :
					$query->the_post();
					$do_not_duplicate[] = get_the_ID();
				?>
					<div class="small-gallery">
						<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
							<?php
								the_post_thumbnail( 'reendex_carousel_news_thumb',
									array(
										'class' => 'img-responsive',
									)
								);
							?>
						</a>
						<div class="post-content">
							<?php
							$count = 0;
							$posttags = get_the_tags();
							$tag_count = 0;
							if ( 0 == $atts['category_show'] ) :
								foreach ( ( get_the_category() ) as $category ) {
									$count++;
									if ( 1 == $count ) {
										?>
											<a href="<?php echo esc_url( $category_link ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( $category->cat_name ); ?></a>
										<?php
									}
								}
							else :
								if ( $posttags ) {
									foreach ( $posttags as $tag ) {
										$tag_id = $tag->term_id;
										$tag_count++;
										$count++;
										if ( 1 == $count ) {
											?>
												<a href="<?php echo esc_url( get_tag_link( $tag_id ) ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
													<?php echo esc_html( $tag->name ); ?>
												</a>
											<?php
										}
										if ( $tag_count > 1 ) {
											break;
										}
									}
								}
							endif; ?>							
							<p><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?></a></p>
							<i class="fa fa-clock-o"></i>
							<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
								<span class="day"><?php the_time( esc_html__( 'D, d M, Y', 'reendex' ) ); ?></span>
							<?php } else { ?>
								<span class="day"><?php echo esc_html( get_the_modified_date( 'D, d M, Y' ) ); ?></span>
							<?php } ?>
						</div><!-- /.post-content -->                                 
					</div><!-- /.small-gallery -->
				<?php endwhile;
				wp_reset_postdata();
				?>
			</div><!-- /.small-gallery-slider -->
		</div>
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_posts_small_gallery_slider_shortcode','reendex_posts_small_gallery_slider' );

// Reendex - Video Gallery Slider.
// usages:[reendex_video_gallery_slider_shortcode].
if ( ! function_exists( 'reendex_video_gallery_slider' ) ) {
	/**
	 * Video Gallery Slider Shortcode
	 *
	 * Displays Video Gallery Slider.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Video Gallery Slider.
	 */
	function reendex_video_gallery_slider( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                     => '',
			'title_color'               => '',
			'title_bg_color'            => '#2c2c34',
			'title_margin_bottom'       => '5',
			'per_page'                  => 6,
			'categories'                => '',
			'order'                     => 'DESC',
			'target'                    => '_self',
			'number_of_visible_items'   => 'big-gallery-slider-3',
			'extra_class'               => '',
		), $atts );
		ob_start();
		?>
		<div class="gallery-video">
			<?php if ( '' !== $atts['title'] ) : ?>
				<div class="carousel-title" style="
					<?php
					if ( '' !== $atts['title_color'] ) {
						echo 'color:' . esc_attr( $atts['title_color'] ) . ';';
					}
					if ( '' !== $atts['title_bg_color'] ) {
						echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';';
					} ?>
					<?php
					if ( '' != $atts['title_margin_bottom'] ) {
						echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
					} ?>">
					<?php echo esc_html( $atts['title'] ); ?>
				</div><!-- /.carousel-title -->
			<?php endif; ?>
			<div id="<?php echo esc_attr( $atts['number_of_visible_items'] ); ?>" class="owl-carousel">
				<?php
				$args = array(
					'post_type'            => 'our-video',
					'post_status'          => 'publish',
					'ignore_sticky_posts'  => 1,
					'posts_per_page'       => $atts['per_page'],
					'order'                => $atts['order'],
				);
				$categories = str_replace( ' ', '', $atts['categories'] );
				if ( strlen( $categories ) > 0 ) {
					$ar_categories = explode( ',', $categories );
					if ( is_array( $ar_categories ) && count( $ar_categories ) > 0 ) {
						$field_name = is_numeric( $ar_categories[0] )?'term_id':'slug';
						$args['tax_query'] = array(
							array(
								'taxonomy'          => 'thcat_taxonomy',
								'terms'             => $ar_categories,
								'field'             => $field_name,
								'include_children'  => false,
							),
						);
					}
				}
				$query = new WP_Query( $args );
				while ( $query->have_posts() ) :
					$query->the_post();
				?>
					<div class="big-gallery">
						<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
							<?php the_post_thumbnail( 'reendex_video_gallery_thumb',
								array(
									'class' => 'img-not-lazy',
								)
							);
							?>
						</a>
						<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
							<span class="play-icon"></span>
						</a> 
					</div><!-- /.big-gallery -->
				<?php endwhile;
				wp_reset_postdata();
				?>
			</div>
		</div><!-- /.gallery-video -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_video_gallery_slider_shortcode','reendex_video_gallery_slider' );

// Reendex - Logo Showcase Slider.
// usages:[reendex_logo_showcase_slider_shortcode].
if ( ! function_exists( 'reendex_logo_showcase_slider' ) ) {
	/**
	 * Logo Showcase Slider Shortcode
	 *
	 * Displays Logo Showcase Slider.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Logo Showcase Slider.
	 */
	function reendex_logo_showcase_slider( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'         => '',
			'partner_logo'  => '',
			'logo_link'     => '',
			'target'        => '_self',
			'extra_class'   => '',
		), $atts );
		ob_start();
		?>
		<div class="<?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div class="logo-gallery-header">
				<h3 class="logo-gallery-title"><?php echo esc_html( $atts['title'] ); ?></h3>
			</div><!-- /.logo-gallery-header -->
				<?php $partner_logo = $atts['partner_logo']; ?>
			<div id="logo-gallery-slider" class="owl-carousel">
				<?php $partner_logo = vc_param_group_parse_atts( $atts['partner_logo'] );
				foreach ( (array) $partner_logo as $logo ) :
					$thumbnail = wp_get_attachment_image_src( $logo['partner_logo'], 'full' );
					$image_alt = get_post_meta( $logo['partner_logo'], '_wp_attachment_image_alt', true );
					$image_title = get_the_title( $logo['partner_logo'] );
				?>
					<div class="partners">                   
						<a href="<?php echo esc_url( $logo['logo_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
							<?php
							if ( '' != $thumbnail ) {
								echo '<img src=" ' . esc_url( $thumbnail[0] ) . ' " alt=" ' . esc_attr( $image_alt ) . ' " title=" ' . esc_attr( $image_title ) . ' ">';
							}
							?>
						</a>
					</div><!-- /.partners -->
				<?php endforeach;?>
			</div><!-- /#logo-gallery-slider -->
		</div>
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_logo_showcase_slider_shortcode','reendex_logo_showcase_slider' );

// Reendex - RSS Feed.
// usages:[reendex_rss_feed_tabs].
if ( ! function_exists( 'reendex_rss_feed_tabs' ) ) {
	/**
	 * RSS Feed Shortcode
	 *
	 * Displays RSS Feed.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of RSS Feed.
	 */
	function reendex_rss_feed_tabs( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                 => '',
			'title_style'           => '1',
			'title_margin_bottom'   => '',
			'urls'                  => '',
			'count'                 => 9,
			'show_date'             => 0,
			'show_desc'             => 0,
			'open_newtab'           => 0,
			'strip_desc'            => 100,
			'read_more'             => esc_html__( '...', 'reendex' ),
			'enable_ticker'         => 0,
			'strip_title'           => 0,
			'ticker_speed'          => '',
			'visible_items'         => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		?>
		<div class="
			<?php
			if ( '' !== $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			} ?>"> 
			<?php if ( '' != $atts['title'] ) : ?>
				<div class="
					<?php
					if ( 2 == $atts['title_style'] ) {
						echo 'title-style02';
					} elseif ( 3 == $atts['title_style'] ) {
						echo 'title-style01';
					} else {
						echo 'block-title-1';
					} ?>"
					style="
					<?php
					if ( '' != $atts['title_margin_bottom'] ) {
						echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
					} ?>"> 
					<h3><?php if ( '' != $atts['title'] ) { echo esc_attr( $atts['title'] ); } ?></h3> 
				</div>
			<?php endif; ?>
			<div class="rss-feed-tabs">  
				<?php
					$args = array(
						'urls'          => $atts['urls'],
						'count'         => $atts['count'],
						'show_date'     => $atts['show_date'],
						'show_desc'     => $atts['show_desc'],
						'open_newtab'   => $atts['open_newtab'],
						'strip_desc'    => $atts['strip_desc'],
						'read_more'     => $atts['read_more'],
						'enable_ticker' => $atts['enable_ticker'],
						'strip_title'   => $atts['strip_title'],
						'ticker_speed'  => $atts['ticker_speed'],
						'visible_items' => $atts['visible_items'],
					);
					$ags = new WP_Query( $args );
				?>
				<?php
				echo "\n" . '<div class="news-ticker-rss-widget ticker-style-smodern">' . "\n";
						ticker_rss_parser( $atts );
				echo "\n" . '</div>
				' . "\n";
				?>
			</div><!-- /.rss-feed-tabs -->
		</div>    
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_rss_feed_tabs_shortcode', 'reendex_rss_feed_tabs' );

// Reendex - News Feed.
// usages:[reendex_news_feed_shortcode].
if ( ! function_exists( 'reendex_news_feed' ) ) {
	/**
	 * News Feed Shortcode
	 *
	 * Displays News Feed.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Feed.
	 */
	function reendex_news_feed( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'feed_title_style'   => '1',
			'feed_title'         => '',
			'feed_title_length'  => 3,
			'read_more_text'     => esc_html__( 'Read more...', 'reendex' ),
			'taxonomies'         => '',
			'per_page'           => 5,
			'title_show'         => 1,
			'date_show'          => 0,
			'number_of_visible'  => '',
			'target'             => '_self',
			'scroll_off'         => 1,
			'margin_bottom'      => '',
			'order'              => '',
			'orderby'            => '',
			'offset'             => 0,
			'duplicate_posts'    => '',
			'extra_class'        => '',
		),$atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<?php
		if ( '' != $atts['feed_title'] ) : ?>
			<div class="
				<?php
				if ( 2 == $atts['feed_title_style'] ) {
					echo 'title-style02';
				} elseif ( 3 == $atts['feed_title_style'] ) {
					echo 'title-style01';
				} else {
					echo 'block-title-1';
				}
				?>"
				style="
					<?php
					if ( '' != $atts['margin_bottom'] ) {
						echo 'margin-bottom:' . esc_attr( $atts['margin_bottom'] ) . 'px;';
					} ?>"> 
				<h3><?php if ( '' != $atts['feed_title'] ) { echo esc_attr( $atts['feed_title'] ); } ?></h3> 
			</div>
		<?php endif; ?>
		<div class="sidebar-newsfeed
			<?php
			if ( '' !== $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			}
			?>"> 
			<div class="
				<?php
				if ( 0 == $atts['scroll_off'] ) {
					echo 'sidebar-newspost';
				} else {
					if ( '' != $atts['number_of_visible'] ) {
						echo esc_attr( $atts['number_of_visible'] );
					} else {
						echo 'newsfeed-2';
					}
				}
				?>">
				<?php
				$args = array(
					'post_type'            => 'post',
					'post_status'          => 'publish',
					'ignore_sticky_posts'  => 1,
					'posts_per_page'       => $atts['per_page'],
					'order'                => $atts['order'],
					'orderby'              => $atts['orderby'],
					'offset'               => $atts['offset'],
					'post__not_in'         => $postids,
				);
				if ( ! empty( $atts['taxonomies'] ) ) {
					$vc_taxonomies_types = get_taxonomies(
						array(
							'public' => true,
						)
					);
					$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
						'hide_empty' => false,
						'include' => $atts['taxonomies'],
					) );
					$args['tax_query'] = array();
					$tax_queries = array(); // List of taxonomies.
					foreach ( $terms as $t ) {
						if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
							$tax_queries[ $t->taxonomy ] = array(
								'taxonomy'          => $t->taxonomy,
								'field'             => 'id',
								'terms'             => array( $t->term_id ),
								'include_children'  => false,
								'relation'          => 'IN',
							);
						} else {
							$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
						}
					}
					$args['tax_query'] = array_values( $tax_queries );
					$args['tax_query']['relation'] = 'OR';
				}
				$query = new WP_Query( $args );
				?> 
				<ul class="flex-module"> 
					<?php
					while ( $query->have_posts() ) :
						$query->the_post();
						$do_not_duplicate[] = get_the_ID();
					?>
						<li> 
							<div class="item">
								<?php if ( has_post_thumbnail() ) : ?> 
									<div class="item-image">
										<a class="img-link" href="<?php the_permalink(); ?>">
											<?php
												the_post_thumbnail( 'reendex_feed_thumb',
													array(
														'class' => 'img-responsive img-full',
													)
												);
											?>
											<?php if ( has_post_format( 'video' ) ) : ?>
											<span class="video-icon-small">
												<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-small.png" alt="video"/>
											</span>
											<?php endif; ?>
										</a>
									</div><!-- /.item-image -->
								<?php endif; ?> 
								<div class="item-content text">
									<?php if ( ( 1 == $atts['title_show'] ) || ( 1 == $atts['date_show'] ) ) : ?> 
										<h4 class="ellipsis">
											<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
												<?php
												if ( 1 == $atts['title_show'] ) {
													reendex_short_title( $atts['feed_title_length'] );
												} ?>
												<?php if ( 1 == $atts['date_show'] ) : ?>
													<span class="post-meta-elements">
														<?php
														if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
															<span class="post-meta-date"> <i class="fa fa-calendar"></i><?php the_time( esc_html__( 'F d, Y', 'reendex' ) ); ?></span>
														<?php } else { ?>
															<span class="post-meta-date"> <i class="fa fa-calendar"></i><?php echo esc_html( get_the_modified_date( 'F d, Y' ) ); ?></span>
														<?php } ?>
													</span>
												<?php endif; ?>											
											</a>
										</h4>
									<?php endif; ?>
									<?php if ( 0 == $atts['title_show'] ) : ?>
										<p><a class="text-concat" href="<?php the_permalink(); ?>"><?php echo esc_html( get_the_excerpt() ); ?></a><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><span><?php if ( isset( $atts['read_more_text'] ) ) { echo esc_attr( $atts['read_more_text'] ); } ?></span></a></p>
									<?php else : ?> 
										<p><a class="text-concat" href="<?php the_permalink(); ?>"><?php echo esc_html( get_the_excerpt() ); ?></a><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><span><?php if ( isset( $atts['read_more_text'] ) ) { echo esc_attr( $atts['read_more_text'] ); } ?></span></a></p>
									<?php endif; ?>
								</div><!-- /.item-content text -->                                                    
							</div><!-- /.item -->                                                
						</li>
					<?php endwhile;
					wp_reset_postdata();
					?>
				</ul><!-- /.flex-module -->                                         
			</div>                                     
		</div><!-- /.sidebar-newsfeed -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_feed_shortcode','reendex_news_feed' );

// Reendex - News Feed Light.
// usages:[reendex_news_feed_light_shortcode].
if ( ! function_exists( 'reendex_news_feed_light' ) ) {
	/**
	 * News Feed Light Shortcode
	 *
	 * Displays News Feed Light.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Feed Light.
	 */
	function reendex_news_feed_light( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title_style'           => '1',
			'title'                 => '',
			'title_show'            => 1,
			'title_length'          => 5,
			'date_show'             => 0,
			'content_length'        => 7,
			'title_margin_bottom'   => '',
			'taxonomies'            => '',
			'per_page'              => 9,
			'target'                => '_self',
			'text_numbering'        => 1,
			'order'                 => '',
			'orderby'               => '',
			'offset'                => 0,
			'duplicate_posts'       => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<?php if ( '' != $atts['title'] ) : ?>
			<div class="
				<?php
				if ( 2 == $atts['title_style'] ) {
					echo 'title-style02';
				} elseif ( 3 == $atts['title_style'] ) {
					echo 'title-style01';
				} else {
					echo 'block-title-1'; } ?>"
				style="
				<?php
				if ( '' != $atts['title_margin_bottom'] ) {
					echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
				} ?>"> 
				<h3><?php if ( '' != $atts['title'] ) { echo esc_attr( $atts['title'] ); } ?></h3> 
			</div>
		<?php endif; ?>
		<div class="sidebar-post light
			<?php
			if ( '' !== $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			} ?>"> 
			<ul class="flex-module"> 
				<?php
					$i = 0;
					$args = array(
						'post_type'            => 'post',
						'post_status'          => 'publish',
						'ignore_sticky_posts'  => 1,
						'posts_per_page'       => $atts['per_page'],
						'order'                => $atts['order'],
						'orderby'              => $atts['orderby'],
						'offset'               => $atts['offset'],
						'post__not_in'         => $postids,
					);
				if ( ! empty( $atts['taxonomies'] ) ) {
					$vc_taxonomies_types = get_taxonomies(
						array(
							'public' => true,
						)
					);
					$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
						'hide_empty' => false,
						'include' => $atts['taxonomies'],
					) );
					$args['tax_query'] = array();
					$tax_queries = array(); // List of taxonomies.
					foreach ( $terms as $t ) {
						if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
							$tax_queries[ $t->taxonomy ] = array(
								'taxonomy'          => $t->taxonomy,
								'field'             => 'id',
								'terms'             => array( $t->term_id ),
								'include_children'  => false,
								'relation'          => 'IN',
							);
						} else {
							$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
						}
					}
					$args['tax_query'] = array_values( $tax_queries );
					$args['tax_query']['relation'] = 'OR';
				}
				$query = new WP_Query( $args );
				while ( $query->have_posts() ) :
					$query->the_post();
					$do_not_duplicate[] = get_the_ID();
					$i++;
				?>
					<li>
						<div class="item">
							<?php if ( has_post_thumbnail() ) : ?> 
								<div class="item-image">
									<a class="img-link" href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
										<?php
											the_post_thumbnail( 'reendex_feed_thumb',
												array(
													'class' => 'img-responsive img-full',
												)
											);
										?>
										<?php if ( has_post_format( 'video' ) ) : ?>
											<span class="video-icon-small">
												<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-small.png" alt="video"/>
											</span>
										<?php endif; ?>
									</a>
								</div><!-- /.item -->
							<?php endif; ?>
							<div class="item-content">
								<?php if ( 1 === $atts['text_numbering'] ) : ?> 
								<h3><?php echo esc_html( str_pad( $i, 2, '0', STR_PAD_LEFT ) ); ?></h3>
								<?php endif; ?>								    
								<?php if ( ( 1 == $atts['title_show'] ) || ( 1 == $atts['date_show'] ) ) : ?> 
									<h4 class="ellipsis">
										<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
											<?php
											if ( 1 == $atts['title_show'] ) {
												reendex_short_title( $atts['title_length'] );
											} ?>
											<?php if ( 1 == $atts['date_show'] ) : ?>
												<span class="post-meta-elements">
													<?php
													if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
														<span class="post-meta-date"> <i class="fa fa-calendar"></i><?php the_time( esc_html__( 'F d, Y', 'reendex' ) ); ?></span>
													<?php } else { ?>
														<span class="post-meta-date"> <i class="fa fa-calendar"></i><?php echo esc_html( get_the_modified_date( 'F d, Y' ) ); ?></span>
													<?php } ?>
												</span>
											<?php endif; ?>											
										</a>
									</h4>
								<?php endif; ?>
								<p class="ellipsis"><a class="external-link" target="<?php echo esc_attr( $atts['target'] ); ?>" href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['content_length'] ) ); ?></a></p>
							</div><!-- /.item-content -->                                                 
						</div><!-- /.item -->
					</li>
				<?php endwhile;
				wp_reset_postdata();
				?>                   
			</ul><!-- /.flex-module -->                                     
		</div><!-- /.sidebar-post light -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_feed_light_shortcode','reendex_news_feed_light' );

// Reendex - Small Sidebar Ad Banner.
// usages:[reendex_small_sidebar_ad_banner_shortcode].
if ( ! function_exists( 'reendex_small_sidebar_ad_banner' ) ) {
	/**
	 * Small Sidebar Ad Banner Shortcode
	 *
	 * Displays Small Sidebar Ad Banner.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Small Sidebar Ad Banner.
	 */
	function reendex_small_sidebar_ad_banner( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'ad_title'              => '',
			'title_text_color'      => '',
			'title_bg_color'        => '',
			'ad_image'              => '',
			'ad_image_link'         => esc_html__( '#', 'reendex' ),
			'target'                => '_self',
			'title_margin_bottom'   => '',
			'ad_type'               => 'image',
			'ad_image_alt'          => '',
			'content'               => '',
			'google_publisher_id'   => '',
			'google_slot_id'        => '',
			'google_desktop'        => '',
			'google_tablet'         => '',
			'google_phone'          => '',
			'ad_bottom_text'        => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		$image_url = wp_get_attachment_image_src( $atts['ad_image'], 'full' );
		$bottom_text = $atts['ad_bottom_text'];
		$render   = '';
		?>
		<div class="ad-wrapper <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<?php if ( '' != $atts['ad_title'] ) : ?>
				<div class="sponsored" style="
					<?php
					if ( '' != $atts['title_bg_color'] ) {
						echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';color:' . esc_attr( $atts['title_text_color'] ) . ';';
					}
					if ( '' != $atts['title_margin_bottom'] ) {
						echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
					} ?>">
					<h3><?php echo esc_attr( $atts['ad_title'] ); ?></h3>
				</div><!-- /.sponsored -->
			<?php endif; ?>
			<div class="sidebar-ad-place">
				<?php
				if ( 'image' == $atts['ad_type'] ) {
					if ( '' != $image_url ) {
						$render = "<a href='" . esc_url( $atts['ad_image_link'] ) . "' target='" . esc_attr( $atts['target'] ) . "'><img class='ad-image' src='" . esc_url( $image_url[0] ) . "' alt='" . esc_attr( $atts['ad_image_alt'] ) . "'></a>";
					}
				} elseif ( 'code' == $atts['ad_type'] ) {
					$code = empty( $atts['content'] ) && ! empty( $atts['content'] ) ? $atts['content'] : $content;
					$render = "<div class='ad-image'>" . $code . '</div>';
				} elseif ( 'googleads' == $atts['ad_type'] && ! empty( $atts['google_publisher_id'] ) && ! empty( $atts['google_slot_id'] ) ) {
					$google_publisher_id = $atts['google_publisher_id'];
					$google_slot_id   = $atts['google_slot_id'];
					$desktop_ad_size = array( '300','250' );
					$tablet_ad_size = array( '300','250' );
					$phone_ad_size = array( '300','250' );
					$ad_style = '';
					$random_string = reendex_get_random_string( 6 );

					if ( 'auto' !== $atts['google_desktop'] ) {
						$desktop_ad_size = explode( 'x', $atts['google_desktop'] );
					}
					if ( 'auto' !== $atts['google_tablet'] ) {
						$tablet_ad_size = explode( 'x', $atts['google_tablet'] );
					}
					if ( 'auto' !== $atts['google_phone'] ) {
						$phone_ad_size = explode( 'x', $atts['google_phone'] );
					}
					if ( 'hide' !== $atts['google_desktop'] && is_array( $desktop_ad_size ) && isset( $desktop_ad_size['0'] ) && isset( $desktop_ad_size['1'] ) ) {
						$ad_style .= ".ad_{$random_string}{ width:{$desktop_ad_size[0]}px !important; height:{$desktop_ad_size[1]}px !important; }\n";
					}
					if ( 'hide' !== $atts['google_tablet'] && is_array( $tablet_ad_size ) && isset( $tablet_ad_size['0'] ) && isset( $tablet_ad_size['1'] ) ) {
						$ad_style .= "@media (max-width:1199px) { .ad_{$random_string}{ width:{$tablet_ad_size[0]}px !important; height:{$tablet_ad_size[1]}px !important; } }\n";
					}
					if ( 'hide' !== $atts['google_phone'] && is_array( $phone_ad_size ) && isset( $phone_ad_size['0'] ) && isset( $phone_ad_size['1'] ) ) {
						$ad_style .= "@media (max-width:767px) { .ad_{$random_string}{ width:{$phone_ad_size[0]}px !important; height:{$phone_ad_size[1]}px !important; } }\n";
					}
					$render = "
					<style type='text/css'>{$ad_style}</style>
					<div class=\"ad-inner-wrapper\">
						<ins class=\"adsbygoogle ad_{$random_string}\"
						style=\"display:inline-block\"
						data-ad-client=\"$google_publisher_id\"
						data-ad-slot=\"$google_slot_id\"
						></ins>   
					</div>";
				} // End if().
				add_filter( 'safe_style_css', 'reendex_modify_safe_css' );
				echo wp_kses( $render, reendex_banner_allowed_html() );
				if ( '' != isset( $atts['ad_bottom_text'] ) && $atts['ad_bottom_text'] ) { ?>
					<div class='ad-bottom-text'>
						<?php echo esc_html( $atts['ad_bottom_text'] ); ?>
					</div>
				<?php }
				?>                                    
			</div><!-- /.sidebar-ad-place -->
		</div>	
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_small_sidebar_ad_banner_shortcode', 'reendex_small_sidebar_ad_banner' );

// Reendex - Big Sidebar Ad Banner.
// usages:[reendex_big_sidebar_ad_banner_shortcode].
if ( ! function_exists( 'reendex_big_sidebar_ad_banner' ) ) {
	/**
	 * Big Sidebar Ad Banner Shortcode
	 *
	 * Displays Big Sidebar Ad Banner.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Big Sidebar Ad Banner.
	 */
	function reendex_big_sidebar_ad_banner( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'ad_title'              => '',
			'title_text_color'      => '',
			'title_bg_color'        => '',
			'ad_image'              => '',
			'ad_image_link'         => esc_html__( '#', 'reendex' ),
			'target'                => '_self',
			'title_margin_bottom'   => '',
			'ad_type'               => 'image',
			'ad_image_alt'          => '',
			'content'               => '',
			'google_publisher_id'   => '',
			'google_slot_id'        => '',
			'google_desktop'        => '',
			'google_tablet'         => '',
			'google_phone'          => '',
			'ad_bottom_text'        => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		$image_url = wp_get_attachment_image_src( $atts['ad_image'], 'full' );
		$bottom_text = $atts['ad_bottom_text'];
		$render   = '';
		?>
		<div class="ad-wrapper <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<?php if ( '' != $atts['ad_title'] ) : ?>
				<div class="sponsored" style="
					<?php
					if ( '' != $atts['title_bg_color'] ) {
						echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';color:' . esc_attr( $atts['title_text_color'] ) . ';';
					}
					if ( '' != $atts['title_margin_bottom'] ) {
						echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
					} ?>">
					<h3><?php echo esc_attr( $atts['ad_title'] ); ?></h3>
				</div><!-- /.sponsored -->
			<?php endif; ?>
			<div class="big-sidebar-ad-place">
				<?php
				if ( 'image' == $atts['ad_type'] ) {
					if ( '' != $image_url ) {
						$render = "<a href='" . esc_url( $atts['ad_image_link'] ) . "' target='" . esc_attr( $atts['target'] ) . "'><img class='ad-image' src='" . esc_url( $image_url[0] ) . "' alt='" . esc_attr( $atts['ad_image_alt'] ) . "'></a>";
					}
				} elseif ( 'code' == $atts['ad_type'] ) {
					$code = empty( $atts['content'] ) && ! empty( $atts['content'] ) ? $atts['content'] : $content;
					$render = "<div class='ad-image'>" . $code . '</div>';
				} elseif ( 'googleads' == $atts['ad_type'] && ! empty( $atts['google_publisher_id'] ) && ! empty( $atts['google_slot_id'] ) ) {
					$google_publisher_id = $atts['google_publisher_id'];
					$google_slot_id   = $atts['google_slot_id'];
					$desktop_ad_size = array( '300','600' );
					$tablet_ad_size = array( '300','600' );
					$phone_ad_size = array( '300','600' );
					$ad_style = '';
					$random_string = reendex_get_random_string( 6 );

					if ( 'auto' !== $atts['google_desktop'] ) {
						$desktop_ad_size = explode( 'x', $atts['google_desktop'] );
					}
					if ( 'auto' !== $atts['google_tablet'] ) {
						$tablet_ad_size = explode( 'x', $atts['google_tablet'] );
					}
					if ( 'auto' !== $atts['google_phone'] ) {
						$phone_ad_size = explode( 'x', $atts['google_phone'] );
					}
					if ( 'hide' !== $atts['google_desktop'] && is_array( $desktop_ad_size ) && isset( $desktop_ad_size['0'] ) && isset( $desktop_ad_size['1'] ) ) {
						$ad_style .= ".ad_{$random_string}{ width:{$desktop_ad_size[0]}px !important; height:{$desktop_ad_size[1]}px !important; }\n";
					}
					if ( 'hide' !== $atts['google_tablet'] && is_array( $tablet_ad_size ) && isset( $tablet_ad_size['0'] ) && isset( $tablet_ad_size['1'] ) ) {
						$ad_style .= "@media (max-width:1199px) { .ad_{$random_string}{ width:{$tablet_ad_size[0]}px !important; height:{$tablet_ad_size[1]}px !important; } }\n";
					}
					if ( 'hide' !== $atts['google_phone'] && is_array( $phone_ad_size ) && isset( $phone_ad_size['0'] ) && isset( $phone_ad_size['1'] ) ) {
						$ad_style .= "@media (max-width:767px) { .ad_{$random_string}{ width:{$phone_ad_size[0]}px !important; height:{$phone_ad_size[1]}px !important; } }\n";
					}
					$render = "
					<style type='text/css'>{$ad_style}</style>
					<div class=\"ad-inner-wrapper\">
						<ins class=\"adsbygoogle ad_{$random_string}\"
						style=\"display:inline-block\"
						data-ad-client=\"$google_publisher_id\"
						data-ad-slot=\"$google_slot_id\"
						></ins>   
					</div>";
				} // End if().
				add_filter( 'safe_style_css', 'reendex_modify_safe_css' );
				echo wp_kses( $render, reendex_banner_allowed_html() );
				if ( '' != isset( $atts['ad_bottom_text'] ) && $atts['ad_bottom_text'] ) { ?>
					<div class='ad-bottom-text'>
						<?php echo esc_html( $atts['ad_bottom_text'] ); ?>
					</div>
				<?php }
				?>                                    
			</div><!-- /.sidebar-ad-place -->
		</div>	
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_big_sidebar_ad_banner_shortcode', 'reendex_big_sidebar_ad_banner' );

// Reendex - Center Ad Banner.
// usages:[reendex_center_ad_banner_shortcode].
if ( ! function_exists( 'reendex_center_ad_banner' ) ) {
	/**
	 * Center Ad Banner Shortcode
	 *
	 * Displays Center Ad Banner.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Center Ad Banner.
	 */
	function reendex_center_ad_banner( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'ad_title'              => '',
			'title_text_color'      => '',
			'title_bg_color'        => '',
			'ad_image'              => '',
			'ad_image_link'         => esc_html__( '#', 'reendex' ),
			'target'                => '_self',
			'title_margin_bottom'   => '',
			'ad_type'               => 'image',
			'ad_image_alt'          => '',
			'content'               => '',
			'google_publisher_id'   => '',
			'google_slot_id'        => '',
			'google_desktop'        => '',
			'google_tablet'         => '',
			'google_phone'          => '',
			'ad_bottom_text'        => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		$image_url = wp_get_attachment_image_src( $atts['ad_image'], 'full' );
		$bottom_text = $atts['ad_bottom_text'];
		$render   = '';
		?>
		<div class="ad-wrapper <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<?php if ( '' != $atts['ad_title'] ) : ?>
				<div class="sponsored" style="
					<?php
					if ( '' != $atts['title_bg_color'] ) {
						echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';color:' . esc_attr( $atts['title_text_color'] ) . ';';
					}
					if ( '' != $atts['title_margin_bottom'] ) {
						echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
					} ?>">
					<h3><?php echo esc_attr( $atts['ad_title'] ); ?></h3>
				</div><!-- /.sponsored -->
			<?php endif; ?>
			<div class="ad-place">
				<?php
				if ( 'image' == $atts['ad_type'] ) {
					if ( '' != $image_url ) {
						$render = "<a href='" . esc_url( $atts['ad_image_link'] ) . "' target='" . esc_attr( $atts['target'] ) . "'><img class='ad-image' src='" . esc_url( $image_url[0] ) . "' alt='" . esc_attr( $atts['ad_image_alt'] ) . "'></a>";
					}
				} elseif ( 'code' == $atts['ad_type'] ) {
					$code = empty( $atts['content'] ) && ! empty( $atts['content'] ) ? $atts['content'] : $content;
					$render = "<div class='ad-image'>" . $code . '</div>';
				} elseif ( 'googleads' == $atts['ad_type'] && ! empty( $atts['google_publisher_id'] ) && ! empty( $atts['google_slot_id'] ) ) {
					$google_publisher_id = $atts['google_publisher_id'];
					$google_slot_id   = $atts['google_slot_id'];
					$desktop_ad_size = array( '728','90' );
					$tablet_ad_size = array( '468','60' );
					$phone_ad_size = array( '320','50' );
					$ad_style = '';
					$random_string = reendex_get_random_string( 6 );

					if ( 'auto' !== $atts['google_desktop'] ) {
						$desktop_ad_size = explode( 'x', $atts['google_desktop'] );
					}
					if ( 'auto' !== $atts['google_tablet'] ) {
						$tablet_ad_size = explode( 'x', $atts['google_tablet'] );
					}
					if ( 'auto' !== $atts['google_phone'] ) {
						$phone_ad_size = explode( 'x', $atts['google_phone'] );
					}
					if ( 'hide' !== $atts['google_desktop'] && is_array( $desktop_ad_size ) && isset( $desktop_ad_size['0'] ) && isset( $desktop_ad_size['1'] ) ) {
						$ad_style .= ".ad_{$random_string}{ width:{$desktop_ad_size[0]}px !important; height:{$desktop_ad_size[1]}px !important; }\n";
					}
					if ( 'hide' !== $atts['google_tablet'] && is_array( $tablet_ad_size ) && isset( $tablet_ad_size['0'] ) && isset( $tablet_ad_size['1'] ) ) {
						$ad_style .= "@media (max-width:1199px) { .ad_{$random_string}{ width:{$tablet_ad_size[0]}px !important; height:{$tablet_ad_size[1]}px !important; } }\n";
					}
					if ( 'hide' !== $atts['google_phone'] && is_array( $phone_ad_size ) && isset( $phone_ad_size['0'] ) && isset( $phone_ad_size['1'] ) ) {
						$ad_style .= "@media (max-width:767px) { .ad_{$random_string}{ width:{$phone_ad_size[0]}px !important; height:{$phone_ad_size[1]}px !important; } }\n";
					}
					$render = "
					<style type='text/css'>{$ad_style}</style>
					<div class=\"ad-inner-wrapper\">
						<ins class=\"adsbygoogle ad_{$random_string}\"
						style=\"display:inline-block\"
						data-ad-client=\"$google_publisher_id\"
						data-ad-slot=\"$google_slot_id\"
						></ins>   
					</div>";
				} // End if().
				add_filter( 'safe_style_css', 'reendex_modify_safe_css' );
				echo wp_kses( $render, reendex_banner_allowed_html() );
				if ( '' != isset( $atts['ad_bottom_text'] ) && $atts['ad_bottom_text'] ) { ?>
					<div class='ad-bottom-text'>
						<?php echo esc_html( $atts['ad_bottom_text'] ); ?>
					</div>
				<?php }
				?>                                    
			</div><!-- /.sidebar-ad-place -->
		</div>	
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_center_ad_banner_shortcode', 'reendex_center_ad_banner' );

// Reendex - Section Title.
// usages:[reendex_section_title_shortcode].
if ( ! function_exists( 'reendex_section_title' ) ) {
	/**
	 * Section title Shortcode
	 *
	 * Displays Section title.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Section title.
	 */
	function reendex_section_title( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'section_title'         => '',
			'section_sub_title'     => '',
			'title_bg_color'        => '',
			'title_style'           => 1,
			'extra_class'           => '',
		), $atts );
		ob_start();
		?>
		<?php if ( ( 2 == $atts['title_style'] ) && ( '' != $atts['section_title'] ) ) : ?>
			<div class="title-style01
				<?php
				if ( '' != $atts['extra_class'] ) {
					echo esc_attr( $atts['extra_class'] );
				} ?>">
				<h3><?php echo esc_html( $atts['section_title'] ); ?></h3>
			</div>
		<?php elseif ( ( 3 == $atts['title_style'] ) && ( '' != $atts['section_title'] ) ) : ?>
			<div class="title-style02
				<?php
				if ( '' != $atts['extra_class'] ) {
					echo esc_attr( $atts['extra_class'] );
				} ?>">
				<h3><?php echo esc_html( $atts['section_title'] ); ?></h3>
			</div>
		<?php else : ?>
			<?php if ( ( '' != $atts['section_title'] ) || ( '' != $atts['section_sub_title'] ) ) : ?> 
				<div class="module-title
					<?php
					if ( '' != $atts['extra_class'] ) {
						echo esc_attr( $atts['extra_class'] );
					} ?>">
					<?php if ( '' != $atts['section_title'] ) : ?> 
						<h3 class="title">
							<span class="bg-1" style="
								<?php
								if ( '' != $atts['title_bg_color'] ) {
									echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';';
								} ?>">
								<?php echo esc_html( $atts['section_title'] ); ?>
							</span>
						</h3>
					<?php endif; ?>
					<?php if ( '' != $atts['section_sub_title'] ) : ?>  
						<h3 class="subtitle"><?php echo esc_html( $atts['section_sub_title'] ); ?></h3>
					<?php endif; ?> 
				</div><!-- /.module-title -->
			<?php endif;
		endif; ?>
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_section_title_shortcode', 'reendex_section_title' );

// Reendex - Module Title 1.
// usages:[reendex_module_title_one_shortcode].
if ( ! function_exists( 'reendex_module_title_one' ) ) {
	/**
	 * Module Title 1 Shortcode
	 *
	 * Displays Title Module 1.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Module Title 1.
	 */
	function reendex_module_title_one( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'             => esc_html__( 'More headlines in our', 'reendex' ),
			'highlighted_title' => 'item sections',
			'subtitle'          => esc_html__( 'latest # news', 'reendex' ),
			'title_link'        => '',
			'target'            => '_self',
			'extra_class'       => '',
		), $atts );
		ob_start();
		?>
		<div class="title-area <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<h2 class="title-style05 style-02">
				<?php echo esc_html( $atts['title'] ); ?>
				<span><a href="<?php echo esc_url( $atts['title_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_attr( $atts['highlighted_title'] ); ?></a></span>
			</h2>
			<?php if ( '' !== $atts['subtitle'] ) : ?>
				<div class="center-title">
					<span class="title-line-left"></span>
					<h4 class="title-style05 style-01"><?php echo esc_attr( $atts['subtitle'] ); ?></h4>
					<span class="title-line-right"></span>
				</div>
			<?php endif; ?>
		</div><!-- /.title-area -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_module_title_one_shortcode', 'reendex_module_title_one' );

// Reendex - Module Title 2.
// usages:[reendex_module_title_two_shortcode].
if ( ! function_exists( 'reendex_module_title_two' ) ) {
	/**
	 * Module Title 2 Shortcode
	 *
	 * Displays Module Title 2.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Module Title 2.
	 */
	function reendex_module_title_two( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'             => esc_html__( 'Analysis & Comment', 'reendex' ),
			'subtitle'          => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		?>
		<div class="section-header <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<h2 class="section-title"><?php echo esc_html( $atts['title'] ); ?><span class="dot">.</span></h2>
			<?php if ( '' !== $atts['subtitle'] ) : ?>
				<h4 class="section-subtitle"><?php echo esc_attr( $atts['subtitle'] ); ?></h4>
			<?php endif; ?>
		</div><!-- /.section-header -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_module_title_two_shortcode', 'reendex_module_title_two' );

// Reendex - Module Title 3.
// usages:[reendex_module_title_three_shortcode].
if ( ! function_exists( 'reendex_module_title_three' ) ) {
	/**
	 * Module Title 3 Shortcode
	 *
	 * Displays Module Title 3.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Module Title 3.
	 */
	function reendex_module_title_three( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'             => esc_html__( 'Analysis & Comment', 'reendex' ),
			'subtitle'          => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		?>
		<div class="section-header-left <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<h2 class="section-title-left"><?php echo esc_html( $atts['title'] ); ?><span class="dot">.</span></h2>
			<?php if ( '' !== $atts['subtitle'] ) : ?>
				<h4 class="section-subtitle"><?php echo esc_attr( $atts['subtitle'] ); ?></h4>
			<?php endif; ?>
		</div><!-- /.section-header-left -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_module_title_three_shortcode', 'reendex_module_title_three' );

// Reendex - Section Highlights.
// usages:[reendex_section_highlights_shortcode].
if ( ! function_exists( 'reendex_section_highlights' ) ) {
	/**
	 * Section Highlights Shortcode
	 *
	 * Displays Section Highlights.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Section Highlights.
	 */
	function reendex_section_highlights( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'section_highlights'    => '',
			'button_link'           => '',
			'target'                => '_self',
			'button_text'           => esc_html__( 'Story highlights', 'reendex' ),
			'extra_class'           => '',
		), $atts );
		ob_start();
		?>
		<div class="section-highlighs-wrapper <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<span class="section-highlight-inner">
				<a href="<?php echo esc_url( $atts['button_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>" class="section-highlight-link"><?php echo esc_attr( $atts['button_text'] ); ?></a>
			</span>
			<?php if ( '' !== $atts['section_highlights'] ) : ?>
				<h4 class="section-highlighs"><?php echo esc_attr( $atts['section_highlights'] ); ?></h4>
			<?php endif; ?>
		</div><!-- /.section-highlighs-wrapper -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_section_highlights_shortcode', 'reendex_section_highlights' );

// Reendex - News Module 1.
// usages:[reendex_news_module_one_shortcode].
if ( ! function_exists( 'reendex_news_module_one' ) ) {
	/**
	 * News Module 1 Shortcode
	 *
	 * Displays News Module 1.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Module 1.
	 */
	function reendex_news_module_one( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title_style'           => 1,
			'section_title'         => '',
			'section_sub_title'     => '',
			'title_bg_color'        => '',
			'title_length'          => 5,
			'content_show'          => 1,
			'content_length'        => 15,
			'label_show'            => 1,
			'category_show'         => 0,
			'readmore_show'         => 1,
			'read_more'             => '',
			'date_show'             => 0,
			'author_meta'           => 0,
			'taxonomies'            => '',
			'per_page'              => '',
			'rows'                  => 2,
			'column'                => 2,
			'target'                => '_self',
			'item_image'            => 'item-image-1',
			'order'                 => '',
			'orderby'               => '',
			'offset'                => 0,
			'duplicate_posts'       => '',
			'extra_class'           => '',

		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="news1
			<?php
			if ( '' !== $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			} ?>"> 
			<?php if ( ( 2 == $atts['title_style'] ) && ( '' != $atts['section_title'] ) ) : ?>
				<div class="title-style01">
					<h3><?php echo esc_html( $atts['section_title'] ); ?></h3>
				</div>
			<?php elseif ( ( 3 == $atts['title_style'] ) && ( '' != $atts['section_title'] ) ) : ?>
				<div class="title-style02">
					<h3><?php echo esc_html( $atts['section_title'] ); ?></h3>
				</div>
			<?php else : ?>
				<?php if ( ( '' != $atts['section_title'] ) || ( '' != $atts['section_sub_title'] ) ) : ?> 
					<div class="module-title">
						<?php if ( '' != $atts['section_title'] ) : ?> 
							<h3 class="title">
								<span class="bg-1" style="
									<?php
									if ( '' != $atts['title_bg_color'] ) {
										echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';';
									} ?>">
									<?php echo esc_html( $atts['section_title'] ); ?>
								</span>
							</h3>
						<?php endif; ?>
						<?php if ( '' != $atts['section_sub_title'] ) : ?>  
							<h3 class="subtitle"><?php echo esc_html( $atts['section_sub_title'] ); ?></h3>
						<?php endif; ?> 
					</div>
				<?php endif;
			endif; ?>                       
			<?php
			if ( $atts['column'] >= 1 ) {
				echo '<div class="row no-gutter">';
			}
			?>
			<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					'post_type'            => 'post',
					'post_status'          => 'publish',
					'ignore_sticky_posts'  => 1,
					'posts_per_page'       => $atts['per_page'],
					'paged' => $paged,
					'order'                => $atts['order'],
					'orderby'              => $atts['orderby']
				);
			if ( ! empty( $atts['taxonomies'] ) ) {
				$vc_taxonomies_types = get_taxonomies(
					array(
						'public' => true,
					)
				);
				$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
					'hide_empty' => false,
					'include' => $atts['taxonomies'],
				) );
				$args['tax_query'] = array();
				$tax_queries = array(); // List of taxonomies.
				foreach ( $terms as $t ) {
					if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
						$tax_queries[ $t->taxonomy ] = array(
							'taxonomy'          => $t->taxonomy,
							'field'             => 'id',
							'terms'             => array( $t->term_id ),
							'include_children'  => false,
							'relation'          => 'IN',
						);
					} else {
						$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
					}
				}
				$args['tax_query'] = array_values( $tax_queries );
				$args['tax_query']['relation'] = 'OR';
			}

			$count = 0;
			$columns = 0;
			//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			//$args = array('posts_per_page' => $atts['per_page'], 'paged' => $paged );
			/* ?><pre><?php print_r($args); ?></pre><?php */
			query_posts($args); 
			$query = new WP_Query( $args );
			if(have_posts()) : while ( $query->have_posts() ) :
				$query->the_post();
				$do_not_duplicate[] = get_the_ID();
				if ( $atts['column'] > 1 ) {
					$columns = 12 / $atts['column'];
				}
				if ( $atts['rows'] >= 1 && 0 == $count % $atts['rows'] ) {
					echo '<div class="news"><div class="news-outer-wrapper"> ';
				}
			?>
				<div class="item-wrapper col-sm-<?php echo absint( $columns ); ?> col-md-<?php echo absint( $columns ); ?>">                                     
					<div class="item">
						<?php if ( has_post_thumbnail() ) : ?> 
							<div class="<?php echo esc_attr( $atts['item_image'] ); ?>">
								<a class="img-link" href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
									<?php the_post_thumbnail( 'full',
										array(
											'class' => 'img-responsive img-full',
										)
									);
									?>
									<?php if ( has_post_format( 'video' ) ) : ?>
										<span class="video-icon-large">
											<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-large.png" alt="video"/>
										</span>
									<?php endif; ?>
								</a>
								<?php if ( 1 === $atts['label_show'] ) :
									$i = 0;
									$posttags = get_the_tags();
									$tag_count = 0;
									if ( 0 == $atts['category_show'] ) :
										foreach ( ( get_the_category() ) as $category ) {
											$i++;
											$category_id = get_cat_ID( $category->cat_name );
											$category_link = get_category_link( $category_id );
											if ( 1 == $i ) {
												?>
												<span>
													<a class="label-14" href="<?php echo esc_url( $category_link ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( $category->cat_name ); ?></a>
												</span>
												<?php
											}
										}
									else :
										if ( $posttags ) {
											foreach ( $posttags as $tag ) {
												$tag_id = $tag->term_id;
												$tag_count++;
												$i++;
												if ( 1 == $i ) {
													?>
													<span>
														<a class="label-14" href="<?php echo esc_url( get_tag_link( $tag_id ) ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
															<?php echo esc_html( $tag->name ); ?>
														</a>
													</span>
													<?php
												}
												if ( $tag_count > 1 ) {
													break;
												}
											}
										}
									endif;
								endif; ?>
							</div>
						<?php endif; ?>                                        
						<div class="item-content"> 
							<div class="title-left title-style04 underline04"> 
								<h3>
									<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?></a>
								</h3> 
							<div class="post-meta-elements">
							<div class="post-meta-author"> 
								<?php $written_by = get_field( "written_by" );
									if( $written_by ) { 
										?><i class="fa fa-user"></i> <?php esc_html_e( 'By: ', 'reendex' ); echo $written_by; }?> 
							<!-- <a href="<?php //echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ) ); ?>"><?php //the_author(); ?></a>-->
							</div>
							<div class="post-meta-date">
								<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
									<i class="fa fa-calendar"></i><a href="<?php echo esc_url( get_day_link( get_post_time( 'Y' ), get_post_time( 'm' ), get_post_time( 'd' ) ) ); ?>"><?php the_time( 'F d, Y','reendex' ); ?></a>
								<?php } else { ?>
									<i class="fa fa-calendar"></i><span class="date updated"><a href="<?php echo esc_url( get_day_link( get_the_date( 'Y' ), get_the_date( 'm' ), get_the_date( 'd' ) ) ) ?>"><?php echo esc_html( get_the_date( 'F j, Y' ) ); ?></a></span>
								<?php } ?>
							</div><!-- /.post-meta-date -->
							<span class="archive_view">
						
							  <?php 
							  $post_id_get = $post->ID; ?>
							  <i class="fa fa-eye"></i>
							  <?php
							  echo do_shortcode( '[views id="'.$post_id_get.'"]' ); ?>
							  
							</span>
							<?php if ( is_sticky() ) : ?>
								<span class="sticky-post"><?php esc_html_e( 'Featured', 'reendex' ); ?></span>
							<?php endif; ?>				
						</div><!-- /.post-meta-elements -->
							</div><!-- /.title-left -->
			
							<?php if ( 1 == $atts['date_show'] ) : ?>
								<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
									<div class="date">
										<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
											<i class="fa fa-clock-o"></i>
											<span class="day"><strong><?php the_time( esc_html__( 'M d, Y', 'reendex' ) ); ?></strong></span>
										</a>
									</div>
								<?php } else { ?>
									<div class="date">
										<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
											<i class="fa fa-clock-o"></i>
											<span class="day"><strong><?php echo esc_html( get_the_modified_date( 'M d, Y' ) ); ?></strong></span>
										</a>
									</div>
								<?php } ?>
							<?php endif; ?>
							<?php if ( 1 == $atts['content_show'] ) : ?>
								<p class="external-link"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( reendex_read_more( get_the_content(), $atts['content_length'] ) ); ?></a></p>
							<?php elseif ( has_excerpt() ) : ?>
								<a class="external-link" target="<?php echo esc_attr( $atts['target'] ); ?>" href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
							<?php endif; ?>
							<?php if ( 1 == $atts['readmore_show'] ) : ?>
								<div>
									<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
										<span class="read-more">
											<?php
											if ( '' !== $atts['read_more'] ) {
												echo esc_attr( $atts['read_more'] );
											} else {
												esc_html_e( 'Read more', 'reendex' );
											}
											?>
										</span>
									</a>
								</div>
						<footer class="entry-footer">
							<?php reendex_entry_footer(); ?>
						</footer>
							<?php endif; ?>                                          
						</div><!-- /.item-content -->                                         
					</div><!-- /.item -->                                     
				</div><!-- /.item-wrapper col-sm -->                                     
				<?php
				if ( $atts['rows'] >= 1 && ( $count % $atts['rows'] == $atts['rows'] - 1 || $count == $query->post_count - 1) ) {
					echo '</div></div>';
				}
				$count++;
			endwhile;
			?>
			<?php else : ?>
			    <!-- Display "Posts not found" message here -->
			    <div class="no_post_found"> <p>No data Found..</p> </div>
			<?php endif; ?>

					<div class="pagination after_content">
				    <?php 
				        echo paginate_links( array(
				            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
				            'total'        => $query->max_num_pages,
				            'current'      => max( 1, get_query_var( 'paged' ) ),
				            'format'       => '?paged=%#%',
				            'show_all'     => false,
				            'type'         => 'plain',
				            'end_size'     => 5,
				            'mid_size'     => 1,
				            'prev_next'    => true,
				            'prev_text'    => sprintf( '<i></i> %1$s', __( 'Previous', 'text-domain' ) ),
				            'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
				            'add_args'     => false,
				            'add_fragment' => '',
				        ) );			     
 
				    ?>
				</div><?php
			wp_reset_postdata();
			?>
			<?php
			if ( $atts['column'] >= 1 ) {
				echo '</div>';
			}
			?>
		</div><!-- /.section-highlighs-wrapper -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_module_one_shortcode','reendex_news_module_one' );

// Reendex - News Module 2.
// usages:[reendex_news_module_two_shortcode].
if ( ! function_exists( 'reendex_news_module_two' ) ) {
	/**
	 * News Module 2 Shortcode
	 *
	 * Displays News Module 2.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Module 2.
	 */
	function reendex_news_module_two( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'taxonomies'        => '',
			'content_show'      => 1,
			'content_length'    => '6',
			'per_page'          => 3,
			'target'            => '_self',
			'category_show'     => 0,
			'order'             => '',
			'orderby'           => '',
			'offset'            => 0,
			'duplicate_posts'   => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="news entry-post
			<?php
			if ( '' !== $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			} ?>"> 
			<?php
				$i = 0;
				$args = array(
					'post_type'            => 'post',
					'post_status'          => 'publish',
					'ignore_sticky_posts'  => 1,
					'posts_per_page'       => $atts['per_page'],
					'order'                => $atts['order'],
					'orderby'              => $atts['orderby'],
					'offset'               => $atts['offset'],
					'post__not_in'         => $postids,
				);
			if ( ! empty( $atts['taxonomies'] ) ) {
				$vc_taxonomies_types = get_taxonomies(
					array(
						'public' => true,
					)
				);
				$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
					'hide_empty' => false,
					'include' => $atts['taxonomies'],
				) );
				$args['tax_query'] = array();
				$tax_queries = array(); // List of taxonomies.
				foreach ( $terms as $t ) {
					if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
						$tax_queries[ $t->taxonomy ] = array(
							'taxonomy'          => $t->taxonomy,
							'field'             => 'id',
							'terms'             => array( $t->term_id ),
							'include_children'  => false,
							'relation'          => 'IN',
						);
					} else {
						$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
					}
				}
				$args['tax_query'] = array_values( $tax_queries );
				$args['tax_query']['relation'] = 'OR';
			}
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) :
				$query->the_post();
				$do_not_duplicate[] = get_the_ID();
				$i++;
			?>
				<div class="item"> 
					<div class="item-image">
						<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
							<?php the_post_thumbnail( 'full',
								array(
									'class' => 'img-responsive img-full',
								)
							); ?>
							<?php if ( has_post_format( 'video' ) ) : ?>
								<span class="video-icon-small">
									<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-small.png" alt="video"/>
								</span>
							<?php endif; ?>
						</a>
					</div><!-- /.item-image -->                                         
					<div class="item-content">
						<?php
							$count = 0;
							$posttags = get_the_tags();
							$tag_count = 0;
						if ( 0 == $atts['category_show'] ) :
							foreach ( ( get_the_category() ) as $category ) {
								$count++;
								$category_id = get_cat_ID( $category->cat_name );
								$category_link = get_category_link( $category_id );
								if ( 1 == $count ) {
									?>
										<div class="entry-meta bg-<?php echo esc_attr( $i + 1 ); ?>"><a href="<?php echo esc_url( $category_link ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( $category->cat_name ); ?></a></div>
									<?php
								}
							}
						else :
							if ( $posttags ) {
								foreach ( $posttags as $tag ) {
									$tag_id = $tag->term_id;
									$tag_count++;
									$count++;
									if ( 1 == $count ) {
										?>
										<div class="entry-meta bg-<?php echo esc_attr( $i + 1 ); ?>">
											<a href="<?php echo esc_url( get_tag_link( $tag_id ) ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
												<?php echo esc_html( $tag->name ); ?>
											</a>													    
										</div>
									<?php
									}
									if ( $tag_count > 1 ) {
										break;
									}
								}
							}
						endif;
						?>							
						<?php if ( 1 == $atts['content_show'] ) : ?>
							<p class="external-link"><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_content(), $atts['content_length'] ) ); ?></a></p>
						<?php elseif ( has_excerpt() ) : ?>
							<a class="external-link" target="<?php echo esc_attr( $atts['target'] ); ?>" href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
						<?php endif; ?>
					</div><!-- /.item-content -->                                         
				</div><!-- /.item -->
			<?php endwhile;
			wp_reset_postdata();
			?>
		</div><!-- /.news entry-post--> 
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_module_two_shortcode','reendex_news_module_two' );

// Reendex - News Module 3.
// usages:[reendex_news_module_three_shortcode].
if ( ! function_exists( 'reendex_news_module_three' ) ) {
	/**
	 * News Module 3 Shortcode
	 *
	 * Displays News Module 3.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Module 3.
	 */
	function reendex_news_module_three( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title_length'      => 5,
			'content_length'    => 8,
			'title_show'        => 1,
			'content_show'      => 1,
			'date_show'         => 0,
			'taxonomies'        => '',
			'per_page'          => 1,
			'target'            => '_self',
			'order'             => '',
			'orderby'           => '',
			'offset'            => 0,
			'duplicate_posts'   => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="article">
			<div class="container-half container-text
				<?php
				if ( '' != $atts['extra_class'] ) {
					echo esc_attr( $atts['extra_class'] );
				} ?>"> 
				<?php
					$args = array(
						'post_type'             => 'post',
						'post_status'           => 'publish',
						'ignore_sticky_posts'   => 1,
						'posts_per_page'        => $atts['per_page'],
						'order'                 => $atts['order'],
						'orderby'               => $atts['orderby'],
						'offset'                => $atts['offset'],
						'post__not_in'          => $postids,
					);
				if ( ! empty( $atts['taxonomies'] ) ) {
					$vc_taxonomies_types = get_taxonomies(
						array(
							'public' => true,
						)
					);
					$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
						'hide_empty' => false,
						'include' => $atts['taxonomies'],
					) );
					$args['tax_query'] = array();
					$tax_queries = array(); // List of taxonomies.
					foreach ( $terms as $t ) {
						if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
							$tax_queries[ $t->taxonomy ] = array(
								'taxonomy' => $t->taxonomy,
								'field' => 'id',
								'terms' => array( $t->term_id ),
								'include_children'  => false,
								'relation' => 'IN',
							);
						} else {
							$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
						}
					}
					$args['tax_query'] = array_values( $tax_queries );
					$args['tax_query']['relation'] = 'OR';
				}
				$query = new WP_Query( $args );
				while ( $query->have_posts() ) :
					$query->the_post();
					$do_not_duplicate[] = get_the_ID();
				?>
					<div class="entry-content">
						<?php if ( 1 == $atts['date_show'] ) : ?>
							<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
								<h3>
									<span class="day"><?php the_time( esc_html__( 'd M, Y', 'reendex' ) ); ?></span>
								</h3>
							<?php } else { ?>
								<h3>
									<span class="day"><?php echo esc_html( get_the_modified_date( 'd M, Y' ) ); ?></span>
								</h3>
							<?php } ?>
						<?php endif; ?>                                
						<?php if ( 1 == $atts['title_show'] ) : ?> 
							<div class="title-left title-style04 underline04"> 
								<h4><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?>...</a></h4> 
							</div>
						<?php endif; ?>
						<?php if ( 1 == $atts['content_show'] ) : ?>
							<p class="external-link"><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_content(), $atts['content_length'] ) ); ?></a></p>
						<?php elseif ( has_excerpt() ) : ?>
							<a class="external-link" target="<?php echo esc_attr( $atts['target'] ); ?>" href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
						<?php endif; ?>
					</div><!-- /.entry-content -->
				<?php endwhile;
				wp_reset_postdata();
				?>
			</div><!-- /.container-half -->
		</div><!-- /.article -->			
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_module_three_shortcode','reendex_news_module_three' );

// Reendex - News Module 4.
// usages:[reendex_news_module_four_shortcode].
if ( ! function_exists( 'reendex_news_module_four' ) ) {
	/**
	 * News Module 4 Shortcode
	 *
	 * Displays News Module 4.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Module 4.
	 */
	function reendex_news_module_four( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'taxonomies'        => '',
			'label_show'        => 1,
			'category_show'     => 0,
			'title_show'        => 1,
			'title_style'       => 1,
			'title_length'      => 5,
			'content_length'    => 9,
			'label_color'       => '',
			'content_show'      => 1,
			'date_show'         => 0,
			'per_page'          => 2,
			'target'            => '_self',
			'order'             => '',
			'orderby'           => '',
			'offset'            => 0,
			'duplicate_posts'   => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="article
			<?php
			if ( '' != $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			} ?>"> 
			<?php
				$args = array(
					'post_type'             => 'post',
					'post_status'           => 'publish',
					'ignore_sticky_posts'   => 1,
					'posts_per_page'        => $atts['per_page'],
					'order'                 => $atts['order'],
					'orderby'               => $atts['orderby'],
					'offset'                => $atts['offset'],
					'post__not_in'          => $postids,
				);
			if ( ! empty( $atts['taxonomies'] ) ) {
				$vc_taxonomies_types = get_taxonomies(
					array(
						'public' => true,
					)
				);
				$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
					'hide_empty' => false,
					'include' => $atts['taxonomies'],
				) );
				$args['tax_query'] = array();
				$tax_queries = array(); // List of taxonomies.
				foreach ( $terms as $t ) {
					if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
						$tax_queries[ $t->taxonomy ] = array(
							'taxonomy' => $t->taxonomy,
							'field' => 'id',
							'terms' => array( $t->term_id ),
							'include_children'  => false,
							'relation' => 'IN',
						);
					} else {
						$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
					}
				}
				$args['tax_query'] = array_values( $tax_queries );
				$args['tax_query']['relation'] = 'OR';
			}
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) :
				$query->the_post();
				$do_not_duplicate[] = get_the_ID();
			?>
				<div class="container-half">
					<div class="image">
						<?php if ( has_post_thumbnail() ) { ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
								<?php the_post_thumbnail( 'full' ); ?>
								<?php if ( 1 === $atts['label_show'] ) :
									$i = 0;
									$posttags = get_the_tags();
									$tag_count = 0;
									if ( 0 == $atts['category_show'] ) :
										foreach ( ( get_the_category() ) as $category ) {
											$i++;
											$category_id = get_cat_ID( $category->cat_name );
											$category_link = get_category_link( $category_id );
											if ( 1 == $i ) {
												?>
													<span class="label-1" style="<?php if ( '' != $atts['label_color'] ) { echo 'background-color:' . esc_attr( $atts['label_color'] ) . ';'; }?>"><?php echo esc_html( $category->cat_name ); ?></span>
												<?php
											}
										}
									else :
										if ( $posttags ) {
											foreach ( $posttags as $tag ) {
												$tag_id = $tag->term_id;
												$tag_count++;
												$i++;
												if ( 1 == $i ) {
													?>
														<span class="label-1" style="<?php if ( '' != $atts['label_color'] ) { echo 'background-color:' . esc_attr( $atts['label_color'] ) . ';'; }?>">
															<?php echo esc_html( $tag->name ); ?>
														</span>
													<?php
												} // End if().
												if ( $tag_count > 1 ) {
													break;
												}
											}
										}
									endif;
								endif;
								?>
								<?php if ( has_post_format( 'video' ) ) : ?>
									<span class="video-icon-small">
										<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-small.png" alt="video"/>
									</span>
								<?php endif; ?>
							</a>
						<?php } // End if(). ?>
					</div><!-- /.image -->
					<div class="item-content">
						<?php if ( '0' !== $atts['title_show'] ) : ?>
							<?php if ( 1 == $atts['title_style'] ) : ?>
								<h3><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?>...</a></h3>
							<?php else : ?>
								<div class="title-left title-style04
									<?php
									if ( 2 == $atts['title_style'] ) {
										   echo 'underline04';
									} ?>">
									<h3>
										<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?>...</a>
									</h3>
								</div>
							<?php endif; ?>
						<?php endif; ?>
						<?php if ( 1 == $atts['date_show'] ) : ?>
							<div class="date">
								<i class="fa fa-clock-o"></i>
								<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
									<span class="day"><?php the_time( esc_html__( 'M d, Y', 'reendex' ) ); ?></span>
									<span class="hour"><?php the_time( 'g:i a' ); ?></span>
								<?php } else { ?>
									<span class="day"><?php echo esc_html( get_the_modified_date( 'M d, Y' ) ); ?></span>
									<span class="hour"><?php echo esc_html( get_post_modified_time( 'g:i a' ) ); ?></span>
								<?php } ?>
							</div>									
						<?php endif; ?>
						<?php if ( 1 == $atts['content_show'] ) : ?>
							<p class="external-link"><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_content(), $atts['content_length'] ) ); ?></a></p>
						<?php elseif ( has_excerpt() ) : ?>
							<a class="external-link" target="<?php echo esc_attr( $atts['target'] ); ?>" href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
						<?php endif; ?>
					</div><!-- /.item-content -->
				</div><!-- /.container-half -->
			<?php endwhile;
			wp_reset_postdata();
			?>
		</div><!-- /.article --> 
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_module_four_shortcode', 'reendex_news_module_four' );

// Reendex - News Module 5.
// usages:[reendex_news_module_five_shortcode].
if ( ! function_exists( 'reendex_news_module_five' ) ) {
	/**
	 * News Module 5 Shortcode
	 *
	 * Displays News Module 5.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Module 5.
	 */
	function reendex_news_module_five( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'module_title_style'    => 1,
			'section_title'         => '',
			'section_sub_title'     => '',
			'title_bg_color'        => '',
			'title_length'          => 5,
			'content_length'        => 15,
			'date_show'             => 1,
			'readmore_show'         => 1,
			'title_show'            => 1,
			'content_show'          => 1,
			'label_show'            => 1,
			'category_show'         => 0,
			'style_blog'            => 1,
			'taxonomies'            => '',
			'per_page'              => 6,
			'target'                => '_self',
			'read_more'             => '',
			'order'                 => '',
			'orderby'               => '',
			'offset'                => 0,
			'duplicate_posts'       => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="
			<?php
			if ( '' != $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			} ?>">
			<?php if ( ( 2 == $atts['module_title_style'] ) && ( '' != $atts['section_title'] ) ) : ?>
				<div class="title-style01">
					<h3><?php echo esc_html( $atts['section_title'] ); ?></h3>
				</div>
			<?php elseif ( ( 3 == $atts['module_title_style'] ) && ( '' != $atts['section_title'] ) ) : ?>
				<div class="title-style02">
					<h3><?php echo esc_html( $atts['section_title'] ); ?></h3>
				</div>
			<?php else : ?>
				<?php if ( ( '' != $atts['section_title'] ) || ( '' != $atts['section_sub_title'] ) ) : ?> 
					<div class="module-title">
						<?php if ( '' != $atts['section_title'] ) : ?> 
							<h3 class="title">
								<span class="bg-1" style="
									<?php
									if ( '' != $atts['title_bg_color'] ) {
										echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';';
									} ?>">
									<?php echo esc_html( $atts['section_title'] ); ?>
								</span>
							</h3>
						<?php endif; ?>
						<?php if ( '' != $atts['section_sub_title'] ) : ?>  
							<h3 class="subtitle"><?php echo esc_html( $atts['section_sub_title'] ); ?></h3>
						<?php endif; ?> 
					</div>
				<?php endif;
			endif; ?>
			<div class="article">
				<?php
					$i = 4;
					$args = array(
						'post_type'             => 'post',
						'post_status'           => 'publish',
						'ignore_sticky_posts'   => 1,
						'posts_per_page'        => $atts['per_page'],
						'order'                 => $atts['order'],
						'orderby'               => $atts['orderby'],
						'offset'                => $atts['offset'],
						'post__not_in'          => $postids,
					);
				if ( ! empty( $atts['taxonomies'] ) ) {
					$vc_taxonomies_types = get_taxonomies(
						array(
							'public' => true,
						)
					);
					$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
						'hide_empty' => false,
						'include' => $atts['taxonomies'],
					) );
					$args['tax_query'] = array();
					$tax_queries = array(); // List of taxonomies.
					foreach ( $terms as $t ) {
						if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
							$tax_queries[ $t->taxonomy ] = array(
								'taxonomy' => $t->taxonomy,
								'field' => 'id',
								'terms' => array( $t->term_id ),
								'include_children'  => false,
								'relation' => 'IN',
							);
						} else {
							$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
						}
					}
					$args['tax_query'] = array_values( $tax_queries );
					$args['tax_query']['relation'] = 'OR';
				}
				$query = new WP_Query( $args );
				while ( $query->have_posts() ) :
					$query->the_post();
					$do_not_duplicate[] = get_the_ID();
					$i++;
				?>
					<div class="entry-block">
						<div class="entry-image">
							<a href="<?php the_permalink(); ?>" class="img-link" target="<?php echo esc_attr( $atts['target'] ); ?>">
								<?php the_post_thumbnail( 'full',
									array(
										'class' => 'img-responsive img-full',
									)
								);
								?>
								<?php if ( has_post_format( 'video' ) ) : ?>
									<span class="video-icon-large">
										<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-large.png" alt="video"/>
									</span>
								<?php endif; ?>
							</a>
							<?php
							if ( 1 === $atts['label_show'] ) :
								$count = 0;
								$posttags = get_the_tags();
								$tag_count = 0;
								if ( 0 == $atts['category_show'] ) :
									foreach ( ( get_the_category() ) as $category ) {
										$count++;
										$category_id = get_cat_ID( $category->cat_name );
										$category_link = get_category_link( $category_id );
										if ( 1 == $count ) {
											?>
												<span><a class="label-<?php echo esc_attr( $i ); ?>" href="<?php echo esc_url( $category_link ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( $category->cat_name ); ?></a></span>
											<?php
										}
									}
								else :
									if ( $posttags ) {
										foreach ( $posttags as $tag ) {
											$tag_id = $tag->term_id;
											$tag_count++;
											$count++;
											if ( 1 == $count ) {
												?>
													<span>
														<a class="label-<?php echo esc_attr( $i ); ?>" href="<?php echo esc_url( get_tag_link( $tag_id ) ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
															<?php echo esc_html( $tag->name ); ?>
														</a>
													</span>
												<?php
											}
											if ( $tag_count > 1 ) {
												break;
											}
										}
									}
								endif;
							endif;
							?>									
						</div><!-- /.image -->						
						<div class="entry-content">
							<?php if ( 1 == $atts['title_show'] ) : ?>
								<div class="title-left title-style04 underline04"> 
									<h3>
										<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?>...</a>
									</h3> 
								</div>
							<?php endif; ?>						        
							<?php if ( 1 == $atts['date_show'] ) : ?>
								<p class="meta-date">
									<i class="fa fa-clock-o"></i>
									<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
										<span class="day"><?php the_time( esc_html__( 'M d, Y', 'reendex' ) ); ?></span>
										<span class="hour"><?php the_time( 'g:i a' ); ?></span>
									<?php } else { ?>
										<span class="day"><?php echo esc_html( get_the_modified_date( 'M d, Y' ) ); ?></span>
										<span class="hour"><?php echo esc_html( get_post_modified_time( 'g:i a' ) ); ?></span>
									<?php } ?>
								</p>
							<?php endif; ?>
							<?php if ( 1 == $atts['content_show'] ) : ?>
								<p class="external-link"><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_content(), $atts['content_length'] ) ); ?></a></p>
							<?php elseif ( has_excerpt() ) : ?>
								<a class="external-link" target="<?php echo esc_attr( $atts['target'] ); ?>" href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
							<?php endif; ?>								
							<div>
								<?php if ( 1 == $atts['readmore_show'] ) : ?>
									<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
										<span class="read-more">
											<?php
											if ( '' != $atts['read_more'] ) {
												echo esc_attr( $atts['read_more'] );
											} else {
												esc_html_e( 'Read more', 'reendex' );
											} ?>
										</span>
									</a>
								<?php endif; ?>
							</div>
						</div><!-- /.entry-content -->
					</div><!-- /.entry-block -->
				<?php endwhile;
				wp_reset_postdata();
				?>
			</div><!-- /.article --> 
		</div> 
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_module_five_shortcode', 'reendex_news_module_five' );

// Reendex - News Module 6.
// usages:[reendex_news_module_six_shortcode].
if ( ! function_exists( 'reendex_news_module_six' ) ) {
	/**
	 * News Module 6 Shortcode
	 *
	 * Displays News Module 6.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Module 6.
	 */
	function reendex_news_module_six( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title_length'          => 5,
			'content_length'        => 15,
			'title_show'            => 0,
			'title_style'           => 1,
			'title_length'          => 5,
			'content_show'          => 1,
			'label_show'            => 1,
			'category_show'         => 0,
			'label_color'           => '',
			'date_show'             => 0,
			'taxonomies'            => '',
			'style_blog'            => 1,
			'target'                => '_self',
			'per_page'              => 1,
			'order'                 => '',
			'orderby'               => '',
			'offset'                => 0,
			'duplicate_posts'       => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="news
			<?php
			if ( '' != $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			} ?>">
			<?php
				$i = 4;
				$args = array(
					'post_type'             => 'post',
					'post_status'           => 'publish',
					'ignore_sticky_posts'   => 1,
					'posts_per_page'        => $atts['per_page'],
					'order'                 => $atts['order'],
					'orderby'               => $atts['orderby'],
					'offset'                => $atts['offset'],
					'post__not_in'          => $postids,
				);
			if ( ! empty( $atts['taxonomies'] ) ) {
				$vc_taxonomies_types = get_taxonomies(
					array(
						'public' => true,
					)
				);
				$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
					'hide_empty' => false,
					'include' => $atts['taxonomies'],
				) );
				$args['tax_query'] = array();
				$tax_queries = array(); // List of taxonomies.
				foreach ( $terms as $t ) {
					if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
						$tax_queries[ $t->taxonomy ] = array(
							'taxonomy' => $t->taxonomy,
							'field' => 'id',
							'terms' => array( $t->term_id ),
							'include_children'  => false,
							'relation' => 'IN',
						);
					} else {
						$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
					}
				}
				$args['tax_query'] = array_values( $tax_queries );
				$args['tax_query']['relation'] = 'OR';
			}
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) :
				$query->the_post();
				$do_not_duplicate[] = get_the_ID();
				$i++;
			?>	
				<div class="entry-block-full big-video">
					<?php if ( '2' !== $atts['style_blog'] ) : ?>
						<div class="entry-media">
							<a href="<?php the_permalink(); ?>" class="img-link" target="<?php echo esc_attr( $atts['target'] ); ?>">
								<?php the_post_thumbnail( 'reendex_news6_thumb_2',
									array(
										'class' => 'img-responsive img-full',
									)
								);
								?>
								<?php if ( has_post_format( 'video' ) ) : ?>
									<span class="video-icon-large">
										<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-large.png" alt="video"/>
									</span>
								<?php endif; ?>
							</a>
							<div class="image">
								<?php if ( 1 === $atts['label_show'] ) :
									$count = 0;
									$posttags = get_the_tags();
									$tag_count = 0;
									if ( 0 == $atts['category_show'] ) :
										foreach ( ( get_the_category() ) as $category ) {
											$count++;
											$category_id = get_cat_ID( $category->cat_name );
											$category_link = get_category_link( $category_id );
											if ( 1 == $count ) {
												?>
													<span class="label-1" style="<?php if ( '' != $atts['label_color'] ) { echo 'background-color:' . esc_attr( $atts['label_color'] ) . ';'; }?>"><?php echo esc_html( $category->cat_name ); ?></span>
												<?php
											}
										}
									else :
										if ( $posttags ) {
											foreach ( $posttags as $tag ) {
												$tag_id = $tag->term_id;
												$tag_count++;
												$count++;
												if ( 1 == $count ) { ?>
													<span class="label-1" style="
														<?php
														if ( '' != $atts['label_color'] ) {
															echo 'background-color:' . esc_attr( $atts['label_color'] ) . ';';
														} ?>">
														<?php echo esc_html( $tag->name ); ?>
													</span>
												<?php }
												if ( $tag_count > 1 ) {
													break;
												}
											}
										}
									endif;
								endif; ?>										
							</div><!-- /.image -->
						</div><!-- /.entry-media -->
					<?php else : ?> 
						<div class="image-full">
							<a href="<?php the_permalink(); ?>" class="img-link" target="<?php echo esc_attr( $atts['target'] ); ?>">
								<?php
								if ( '1' !== $atts['style_blog'] ) {
									the_post_thumbnail( 'reendex_news6_thumb_1',
										array(
											'class' => 'img-responsive img-full',
										)
									);
								}
								?>
								<?php if ( has_post_format( 'video' ) ) : ?>
									<span class="video-icon-large">
										<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-large.png" alt="video"/>
									</span>
								<?php endif; ?>
							</a>
						</div><!-- /.image-full -->
					<?php endif; ?>
					<div class="content">
						<?php if ( '0' !== $atts['title_show'] ) : ?>
							<?php if ( 1 == $atts['title_style'] ) : ?>
								<div class="title-left title-style04 underline04">
									<h2><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?>...</a></h2>
								</div>
							<?php else : ?>
								<div class="title-left title-style04 <?php if ( 2 == $atts['title_style'] ) { echo 'underline04'; } ?>">
									<h2><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?>...</a></h2>
								</div>
							<?php endif; ?>
						<?php endif; ?>
						<?php if ( 1 == $atts['date_show'] ) : ?>
							<div class="meta-date">
								<i class="fa fa-clock-o"></i>
								<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
									<span class="day"><?php the_time( esc_html__( 'M d, Y', 'reendex' ) ); ?></span>
									<span class="hour"><?php the_time( 'g:i a' ); ?></span>
								<?php } else { ?>
									<span class="day"><?php echo esc_html( get_the_modified_date( 'M d, Y' ) ); ?></span>
									<span class="hour"><?php echo esc_html( get_post_modified_time( 'g:i a' ) ); ?></span>
								<?php } ?>
							</div>									
						<?php endif; ?>
						<?php if ( 1 == $atts['content_show'] ) : ?>
							<p class="external-link"><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( wp_strip_all_tags( reendex_read_more( get_the_content(), $atts['content_length'] ) ) ); ?></a></p>
						<?php elseif ( has_excerpt() ) : ?>
							<a class="external-link" target="<?php echo esc_attr( $atts['target'] ); ?>" href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
						<?php endif; ?>
					</div><!-- /.content -->
				</div><!-- /.entry-block-full -->	
			<?php endwhile;
			wp_reset_postdata();
			?>
		</div> 
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_module_six_shortcode', 'reendex_news_module_six' );

// Reendex - News Module 7.
// usages:[reendex_news_module_seven_shortcode].
if ( ! function_exists( 'reendex_news_module_seven' ) ) {
	/**
	 * News Module 7 Shortcode
	 *
	 * Displays News Module 7.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Module 7.
	 */
	function reendex_news_module_seven( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'bg_image'  => 'bg_image',
			'title'     => '',
			'subtitle'  => '',
			'link'      => 'link',

		), $atts );
		$href = vc_build_link( $atts['link'] );
		$image_path = wp_get_attachment_image_src( $atts['bg_image'], 'full', false, array(
			'class'    => 'img-responsive img-full',
		) );
		$content = '<a class="container-full" href="' . $href['url'] . '" title="' . $href['title'] . '" target="' . $href['target'] . '">
			<div class="entry-media">
				<div class="image" style="background-image: url(' . esc_attr( $image_path[0] ) . ')">
					<div class="container-full-box-overlay">
						<div class="content">
							<h2>' . $atts['title'] . '</h2>
							<h4 class="external-link">' . $atts['subtitle'] . '</h4>
						</div>
					</div>
				</div>
			</div>
		</a>';
		return $content;
	}
	add_shortcode( 'reendex_news_module_seven_shortcode', 'reendex_news_module_seven' );
} // End if().

// Reendex - News Module 8.
// usages:[reendex_news_module_eight_shortcode].
if ( ! function_exists( 'reendex_news_module_eight' ) ) {
	/**
	 * News Module 8 Shortcode
	 *
	 * Displays News Module 8.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Module 8.
	 */
	function reendex_news_module_eight( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'bg_image'  => 'bg_image',
			'title'     => '',
			'subtitle'  => '',
			'date'      => '',
			'link'      => 'link',
		), $atts );
		$href = vc_build_link( $atts['link'] );
		$image_path = wp_get_attachment_image_src( $atts['bg_image'], 'full', false, array(
			'class'    => 'img-responsive img-full',
			'itemprop' => 'logo',
		) );
		$content = '<a class="container-full light" href="' . $href['url'] . '" title="' . $href['title'] . '" target="' . $href['target'] . '">
			<div class="entry-media">
				<div class="image" style="background-image: url(' . esc_attr( $image_path[0] ) . ')">
					<div class="item-content">
						<h3 class="day">' . $atts['date'] . '</h3>
						<h2>' . $atts['title'] . '</h2>
						<h4 class="external-link">' . $atts['subtitle'] . '</h4>
					</div>
				</div>
			</div>
		</a>';
		return $content;
	}
	add_shortcode( 'reendex_news_module_eight_shortcode', 'reendex_news_module_eight' );
} // End if().

// Reendex - News Headlines.
// usages:[reendex_news_headlines_shortcode].
if ( ! function_exists( 'reendex_news_headlines' ) ) {
	/**
	 * News Headlines Shortcode
	 *
	 * Displays News Headlines.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Headlines.
	 */
	function reendex_news_headlines( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title_style'           => '1',
			'title'                 => '',
			'title_show'            => 1,
			'title_length'          => 5,
			'date_show'             => 0,
			'content_length'        => 7,
			'title_margin_bottom'   => '',
			'taxonomies'            => '',
			'per_page'              => 9,
			'target'                => '_self',
			'text_numbering'        => 1,
			'order'                 => '',
			'orderby'               => '',
			'offset'                => 0,
			'duplicate_posts'       => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<?php if ( '' != $atts['title'] ) : ?>
			<div class="
				<?php
				if ( 2 == $atts['title_style'] ) {
					echo 'title-style02';
				} elseif ( 3 == $atts['title_style'] ) {
					echo 'title-style01';
				} else {
					echo 'block-title-1';
				} ?>"
				style="
					<?php
					if ( '' != $atts['title_margin_bottom'] ) {
						echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
					} ?>"> 
				<h3><?php if ( '' != $atts['title'] ) { echo esc_attr( $atts['title'] ); } ?></h3> 
			</div>
		<?php endif; ?>
		<div class="sidebar-post
			<?php
			if ( '' !== $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			} ?>"> 
			<ul class="flex-module"> 
				<?php
					$i = 0;
					$args = array(
						'post_type'             => 'post',
						'post_status'           => 'publish',
						'ignore_sticky_posts'   => 1,
						'posts_per_page'        => $atts['per_page'],
						'order'                 => $atts['order'],
						'orderby'               => $atts['orderby'],
						'offset'                => $atts['offset'],
						'post__not_in'          => $postids,
					);
				if ( ! empty( $atts['taxonomies'] ) ) {
					$vc_taxonomies_types = get_taxonomies(
						array(
							'public' => true,
						)
					);
					$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
						'hide_empty' => false,
						'include' => $atts['taxonomies'],
					) );
					$args['tax_query'] = array();
					$tax_queries = array(); // List of taxonomies.
					foreach ( $terms as $t ) {
						if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
							$tax_queries[ $t->taxonomy ] = array(
								'taxonomy'          => $t->taxonomy,
								'field'             => 'id',
								'terms'             => array( $t->term_id ),
								'include_children'  => false,
								'relation'          => 'IN',
							);
						} else {
							$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
						}
					}
					$args['tax_query'] = array_values( $tax_queries );
					$args['tax_query']['relation'] = 'OR';
				}
				$query = new WP_Query( $args );
				while ( $query->have_posts() ) :
					$query->the_post();
					$do_not_duplicate[] = get_the_ID();
					$i++;
				?>
					<li>
						<div class="item">
							<?php if ( has_post_thumbnail() ) : ?>
								<div class="item-image">
									<a class="img-link" href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
										<?php
											the_post_thumbnail( 'reendex_feed_thumb',
												array(
													'class' => 'img-responsive img-full',
												)
											);
										?>
										<?php if ( has_post_format( 'video' ) ) : ?>
											<span class="video-icon-small">
												<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-small.png" alt="video"/>
											</span>
										<?php endif; ?>
									</a>
								</div><!-- /.item-image -->
							<?php endif; ?>
							<div class="item-content text">
								<?php if ( 1 === $atts['text_numbering'] ) : ?> 
									<h3><?php echo esc_html( str_pad( $i, 2, '0', STR_PAD_LEFT ) ); ?></h3>
								<?php endif; ?>								    
								<?php if ( ( 1 == $atts['title_show'] ) || ( 1 == $atts['date_show'] ) ) : ?> 
									<h4 class="ellipsis">
										<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
											<?php
											if ( 1 == $atts['title_show'] ) {
												reendex_short_title( $atts['title_length'] );
											} ?>
											<?php if ( 1 == $atts['date_show'] ) : ?>
												<span class="post-meta-elements">
													<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
														<span class="post-meta-date"> <i class="fa fa-calendar"></i><?php the_time( esc_html__( 'F d, Y', 'reendex' ) ); ?> </span>
													<?php } else { ?>
														<span class="post-meta-date"> <i class="fa fa-calendar"></i><?php echo esc_html( get_the_modified_date( 'F d, Y' ) ); ?></span>
													<?php } ?>
												</span>
											<?php endif; ?>											
										</a>
									</h4>
								<?php endif; ?>
								<p class="ellipsis"><a class="external-link" target="<?php echo esc_attr( $atts['target'] ); ?>" href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['content_length'] ) ); ?></a></p>
							</div><!-- /.item-content text -->                                                 
						</div><!-- /.item -->
					</li>
				<?php endwhile;
				wp_reset_postdata();
				?>                   
			</ul><!-- /.flex-module -->                                     
		</div><!-- /.sidebar-post -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_headlines_shortcode', 'reendex_news_headlines' );

// Reendex News - Latest Events.
// usages:[reendex_latest_events].
if ( ! function_exists( 'reendex_latest_events' ) ) {
	/**
	 * Latest Events Shortcode
	 *
	 * Displays Latest Events.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Latest Events.
	 */
	function reendex_latest_events( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'             => esc_html__( 'Latest Events', 'reendex' ),
			'title_text_color'  => '',
			'title_bg_color'    => '',
			'content_length'    => '',
			'per_page'          => 6,
			'target'            => '_self',
			'extra_class'       => '',
		), $atts );
		ob_start();
		?>
		<div class="<?php echo esc_attr( $atts['extra_class'] ); ?>">
			<?php if ( '' !== $atts['title'] ) : ?>                                 
				<div class="block-title-3" style="
					<?php
					if ( '' !== $atts['title_bg_color'] ) {
						echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';';
					} ?>"> 
					<h3 class="currency-title" style="
						<?php
						if ( '' !== $atts['title_text_color'] ) {
							echo 'color:' . esc_attr( $atts['title_text_color'] ) . ';';
						} ?>">
						<?php echo esc_html( $atts['title'] ); ?>
					</h3> 
				</div>
			<?php endif; ?>            
			<div class="latest-events">
				<?php
					$args = array(
						'post_type'            => 'post',
						'post_status'          => 'publish',
						'ignore_sticky_posts'  => 1,
						'posts_per_page'       => $atts['per_page'],
						'tax_query' => array(
							array(
								'taxonomy'  => 'post_format',
								'field'     => 'slug',
								'terms'     => 'post-format-video',
								'operator'  => 'NOT IN',
							),
						),
					);
					$query = new WP_Query( $args );
					$permalink = get_permalink();
				while ( $query->have_posts() ) :
					$query->the_post();
				?>
					<div class="events">
						<div class="events-date">	
							<a href="<?php echo esc_html( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
								<?php
									$day = 'd';
									$month = 'M';
								?>
								<span class="events-day"><?php the_time( $day ); ?></span>
								<span class="events-month"><?php the_time( $month ); ?></span>
							</a>
						</div><!-- /.events-date -->
						<?php if ( '' !== $atts['content_length'] ) : ?>                                    
							<div class="content">
								<p><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['content_length'] ) ); ?></a></p>
							</div>
						<?php endif; ?>
					</div><!-- /.events -->
				<?php endwhile;
				wp_reset_postdata(); ?>
			</div><!-- /.latest-events -->
		</div>
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_latest_events_shortcode', 'reendex_latest_events' );

// Reendex - News Single Post.
// usages:[reendex_news_single_post_shortcode].
if ( ! function_exists( 'reendex_news_single_post' ) ) {
	/**
	 * News Single Post Shortcode
	 *
	 * Displays News Single Post.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of News Single Post.
	 */
	function reendex_news_single_post( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title_style'           => 1,
			'title'                 => '',
			'title_margin_bottom'   => '',
			'content_length'        => 10,
			'title_show'            => 1,
			'title_length'          => 5,
			'date_show'             => 1,
			'taxonomies'            => '',
			'per_page'              => 1,
			'target'                => '_self',
			'order'                 => '',
			'orderby'               => '',
			'offset'                => 0,
			'duplicate_posts'       => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="sidebar-block sidebar-wrapper
			<?php
			if ( '' != $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			} ?>">
			<?php if ( '' != $atts['title'] ) : ?>
				<div class="
					<?php
					if ( 2 == $atts['title_style'] ) {
						echo 'title-style02';
					} elseif ( 3 == $atts['title_style'] ) {
						echo 'title-style01';
					} else {
						echo 'block-title-2';
					} ?>"
					style="
						<?php
						if ( '' != $atts['title_margin_bottom'] ) {
							echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
						} ?>"> 
					<h3><?php if ( '' != $atts['title'] ) { echo esc_attr( $atts['title'] ); } ?></h3> 
				</div>
			<?php endif; ?>
			<?php
				$i = 0;
				$args = array(
					'post_type'             => 'post',
					'post_status'           => 'publish',
					'ignore_sticky_posts'   => 1,
					'posts_per_page'        => $atts['per_page'],
					'order'                 => $atts['order'],
					'orderby'               => $atts['orderby'],
					'offset'                => $atts['offset'],
					'post__not_in'          => $postids,
				);
			if ( ! empty( $atts['taxonomies'] ) ) {
				$vc_taxonomies_types = get_taxonomies(
					array(
						'public' => true,
					)
				);
				$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
					'hide_empty' => false,
					'include' => $atts['taxonomies'],
				) );
				$args['tax_query'] = array();
				$tax_queries = array(); // List of taxonomies.
				foreach ( $terms as $t ) {
					if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
						$tax_queries[ $t->taxonomy ] = array(
							'taxonomy' => $t->taxonomy,
							'field' => 'id',
							'terms' => array( $t->term_id ),
							'include_children'  => false,
							'relation' => 'IN',
						);
					} else {
						$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
					}
				}
				$args['tax_query'] = array_values( $tax_queries );
				$args['tax_query']['relation'] = 'OR';
			}
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) :
				$query->the_post();
				$do_not_duplicate[] = get_the_ID();
			?>
				<div class="sidebar-image"> 
					<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
						<?php
							the_post_thumbnail( 'reendex_news_gallery_thumb' );
						?>
						<?php if ( has_post_format( 'video' ) ) : ?>
							<span class="video-icon-large">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-large.png" alt="video"/>
							</span>
						<?php endif; ?>
					</a>
					<div class="sidebar-content">
						<?php if ( 1 == $atts['title_show'] ) : ?> 
							<div class="title-style04 underline04">
								<h3><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?></a></h3>
							</div>
						<?php endif; ?>								    
						<?php if ( 1 == $atts['date_show'] ) : ?>
							<div class="date">
								<i class="fa fa-clock-o"></i>
								<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
									<span class="day"><?php the_time( esc_html__( 'M d, Y', 'reendex' ) ); ?></span>
									<span class="hour"><?php the_time( 'g:i a' ); ?></span>
								<?php } else { ?>
									<span class="day"><?php echo esc_html( get_the_modified_date( 'M d, Y' ) ); ?></span>
									<span class="hour"><?php echo esc_html( get_post_modified_time( 'g:i a' ) ); ?></span>
								<?php } ?>
							</div><!-- /.date -->										
						<?php endif; ?>
						<p><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['content_length'] ) ); ?></a></p>
					</div><!-- /.sidebar-content -->
				</div><!-- /.sidebar-image -->
			<?php endwhile;
			wp_reset_postdata();
			?>
		</div><!-- /.sidebar-block --> 
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_news_single_post_shortcode', 'reendex_news_single_post' );

// Reendex - Thumbnails Gallery.
// usages:[reendex_thumbnails_gallery_shortcode].
if ( ! function_exists( 'reendex_thumbnails_gallery' ) ) {
	/**
	 * Thumbnails Gallery Shortcode
	 *
	 * Displays Thumbnails Gallery.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Thumbnails Gallery.
	 */
	function reendex_thumbnails_gallery( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'show_title'        => '',
			'title_length'      => '',
			'label_show'        => 0,
			'category_show'     => 0,
			'show_date'         => 1,
			'date_format'       => '1',
			'taxonomies'        => '',
			'per_page'          => 4,
			'target'            => '_self',
			'order'             => '',
			'orderby'           => '',
			'offset'            => 0,
			'duplicate_posts'   => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		?>
		<div class="<?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div class="news-block">
				<?php

					$args = array(
						'post_type'             => 'post',
						'post_status'           => 'publish',
						'ignore_sticky_posts'   => 1,
						'posts_per_page'        => $atts['per_page'],
						'paged' 				=> $paged,
						'order'                 => '',
						'orderby'               => '',
						'offset'                => $atts['offset'],
				
					);
				if ( ! empty( $atts['taxonomies'] ) ) {
					$vc_taxonomies_types = get_taxonomies(
						array(
							'public' => true,
						)
					);
					$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
						'hide_empty' => false,
						'include' => $atts['taxonomies'],
					) );
					$args['tax_query'] = array();
					$tax_queries = array(); // List of taxonomies.
					foreach ( $terms as $t ) {
						if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
							$tax_queries[ $t->taxonomy ] = array(
								'taxonomy' => $t->taxonomy,
								'field' => 'id',
								'terms' => array( $t->term_id ),
								'include_children'  => false,
								'relation' => 'IN',
							);
						} else {
							$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
						}
					}
					$args['tax_query'] = array_values( $tax_queries );
					$args['tax_query']['relation'] = 'OR';
				}
				

				// the query to set the posts per page to 3
				/* $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array('posts_per_page' => $atts['per_page'], 'paged' => $paged ); 
				
				?><pre><?php print_r($args); ?></pre><?php
				*/


				query_posts($args); 
				// the loop 

				$query = new WP_Query( $args );
				if(have_posts()) : while ( $query->have_posts() ) :
					$query->the_post();
					$do_not_duplicate[] = get_the_ID();
				?>
					<div class="item-block"> 
						<div class="item-image">
							<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
								<?php
									the_post_thumbnail( 'reendex_news4_thumb',
										array(
											'class' => 'img-responsive img-full',
										)
									);
								?>
								<?php if ( has_post_format( 'video' ) ) : ?>
									<span class="video-icon-small">
										<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-small.png" alt="video"/>
									</span>
								<?php endif; ?>
							</a>
							<?php if ( 1 == $atts['label_show'] ) :
								$count = 0;
								$posttags = get_the_tags();
								$tag_count = 0;
								if ( 0 == $atts['category_show'] ) :
									foreach ( ( get_the_category() ) as $category ) {
										$count++;
										$category_id = get_cat_ID( $category->cat_name );
										$category_link = get_category_link( $category_id );
										if ( 1 == $count ) {
											?>
												<a href="<?php echo esc_url( $category_link ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><span class="label-14"><?php echo esc_html( $category->cat_name ); ?></span></a>
											<?php
										}
									}
								else :
									if ( $posttags ) {
										foreach ( $posttags as $tag ) {
											$tag_id = $tag->term_id;
											$tag_count++;
											$count++;
											if ( 1 == $count ) {
												?>
													<a href="<?php echo esc_url( get_tag_link( $tag_id ) ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
														<span class="label-14"><?php echo esc_html( $tag->name ); ?></span>
													</a>
												<?php
											}
											if ( $tag_count > 1 ) {
												break;
											}
										}
									}
								endif;
							endif; ?>						
						</div><!-- /.item-image -->                                             
						<div class="item-content">
							<?php if ( 1 !== $atts['show_title'] ) : ?>
								<p><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'],'...' ); ?></a></p>
							<?php endif; ?>
							<div class="post-meta-author">
								<?php
							$current_id = get_the_ID();
							$author = get_field( 'hide_author',$current_id);
							$author = $author[0];
							if( $author == 'yes' ) {
									
								}else{
								?>
								<i class="fa fa-user"></i>
								<?php esc_html_e( 'By: ', 'reendex' ); ?> <?php the_author(); ?>
								<?php
							}
								?>
							</div>
							<?php if ( 1 == $atts['show_date'] ) : ?>
								<i class="fa fa-clock-o"></i>
								<?php //if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
									<span class="day">
										<?php
										the_time( esc_html__( 'D, M d, Y', 'reendex' ) );
										?>
									</span>
								<?php ///} ?>
								<span class="view_eye">
								<i class="fa fa-eye"></i> 
										<?php
										$current_id = get_the_ID();
										echo do_shortcode("[views id='".$current_id."']"); ?> Views
										</span>
							<?php endif; ?>
							
						</div><!-- /.item-content -->                                              
					</div><!-- /.item-block --> 
				<?php endwhile; ?>
				<?php else : ?>
			        <!-- Display "Posts not found" message here -->
			        <div class="no_post_found"> <p>No data Found..</p> </div>
			    <?php endif; ?>
				<div class="pagination after_content">
				    <?php 
				        echo paginate_links( array(
				            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
				            'total'        => $query->max_num_pages,
				            'current'      => max( 1, get_query_var( 'paged' ) ),
				            'format'       => '?paged=%#%',
				            'show_all'     => false,
				            'type'         => 'plain',
				            'end_size'     => 5,
				            'mid_size'     => 1,
				            'prev_next'    => true,
				            'prev_text'    => sprintf( '<i></i> %1$s', __( 'Previous', 'text-domain' ) ),
				            'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
				            'add_args'     => false,
				            'add_fragment' => '',
				        ) );			     
 
				    ?>
				</div><?php
				wp_reset_postdata();
				?>
			</div><!-- /.news-block -->     	
		</div>
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_thumbnails_gallery_shortcode', 'reendex_thumbnails_gallery' );

// Reendex - Lightbox Gallery.
// usages:[reendex_lighbox_gallery_shortcode].
if ( ! function_exists( 'reendex_lighbox_gallery' ) ) {
	/**
	 * Lightbox Gallery Shortcode
	 *
	 * Displays Lightbox Gallery.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Lightbox Gallery.
	 */
	function reendex_lighbox_gallery( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                 => '',
			'title_style'           => 1,
			'title_margin_bottom'   => '',
			'taxonomies'            => '',
			'per_page'              => 8,
			'title_show'            => 0,
			'show_content'          => 0,
			'title_length'          => '5',
			'content_show'          => 1,
			'content_length'        => 15,
			'date_show'             => 0,
			'target'                => '_self',
			'order'                 => '',
			'orderby'               => '',
			'offset'                => 0,
			'duplicate_posts'       => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="news
			<?php
			if ( '' != $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			} ?>">
			<?php if ( '' != $atts['title'] ) : ?>
				<div class="
					<?php
					if ( 2 == $atts['title_style'] ) {
						echo 'title-style02';
					} elseif ( 3 == $atts['title_style'] ) {
						echo 'title-style01';
					} else {
						echo 'block-title-2';
					} ?>"
					style="
						<?php
						if ( '' != $atts['title_margin_bottom'] ) {
							echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
						} ?>"> 
					<h3><?php if ( '' != $atts['title'] ) { echo esc_attr( $atts['title'] ); } ?></h3> 
				</div>
			<?php endif; ?>	
			<ul class="rollover-thumbnail">
				<?php
					$args = array(
						'post_type'             => 'post',
						'post_status'           => 'publish',
						'ignore_sticky_posts'   => 1,
						'posts_per_page'        => $atts['per_page'],
						'order'                 => $atts['order'],
						'orderby'               => $atts['orderby'],
						'offset'                => $atts['offset'],
						'post__not_in'          => $postids,
					);
				if ( ! empty( $atts['taxonomies'] ) ) {
					$vc_taxonomies_types = get_taxonomies(
						array(
							'public' => true,
						)
					);
					$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
						'hide_empty' => false,
						'include' => $atts['taxonomies'],
					) );
					$args['tax_query'] = array();
					$tax_queries = array(); // List of taxonomies.
					foreach ( $terms as $t ) {
						if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
							$tax_queries[ $t->taxonomy ] = array(
								'taxonomy'          => $t->taxonomy,
								'field'             => 'id',
								'terms'             => array( $t->term_id ),
								'include_children'  => false,
								'relation'          => 'IN',
							);
						} else {
							$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
						}
					}
					$args['tax_query'] = array_values( $tax_queries );
					$args['tax_query']['relation'] = 'OR';
				}


				$query = new WP_Query( $args );
				while ( $query->have_posts() ) :
					$query->the_post();
					$do_not_duplicate[] = get_the_ID();
				?>
					<li>
						<div class="rollover-thumbnail"> 
							<a href="<?php the_post_thumbnail_url( 'full' ); ?>" rel="lightbox" data-lightbox="rollover" title="<a href= ''><?php the_title(); ?></a>"> 
								<span class="rollover"></span> 
								<?php
									the_post_thumbnail( 'reendex_news_gallery_thumb' );
								?> 
							</a> 
						</div><!-- /.rollover-thumbnail -->
						<?php if ( 1 == $atts['title_show'] ) : ?> 
							<h4><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?>...</a></h4>
						<?php endif; ?>
						<?php if ( 1 == $atts['show_content'] ) :
							if ( 1 == $atts['content_show'] ) : ?>
								<p class="external-link"><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_content(), $atts['content_length'] ) ); ?></a></p>
							<?php elseif ( has_excerpt() ) : ?>
								<a class="external-link" target="<?php echo esc_attr( $atts['target'] ); ?>" href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
							<?php endif;
						endif; ?>									
						<?php if ( 1 == $atts['date_show'] ) : ?>
							<div class="date">
								<i class="fa fa-calendar"></i>
								<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
									<span class="day"><?php the_time( esc_html__( 'M d, Y', 'reendex' ) ); ?></span>
								<?php } else { ?>
									<span class="day"><?php echo esc_html( get_the_modified_date( 'M d, Y' ) ); ?></span>
								<?php } ?>
							</div><!-- /.date -->									
						<?php endif; ?>
					</li>
				<?php endwhile;
				wp_reset_postdata();
				?>
			</ul><!-- /.rollover-thumbnail -->
		</div><!-- /.news --> 
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_lighbox_gallery_shortcode', 'reendex_lighbox_gallery' );

// Reendex - Stay Connected.
// usages:[reendex_stay_connected_shortcode].
if ( ! function_exists( 'reendex_stay_connected' ) ) {
	/**
	 * Stay Connected Shortcode
	 *
	 * Displays Stay Connected.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Stay Connected.
	 */
	function reendex_stay_connected( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'         => '',
			'social_media'  => '',
			'target'        => '_self',
			'extra_class'   => '',
		), $atts );
		ob_start();
		?>
		<div class="sidebar-social-icons <?php echo esc_attr( $atts['extra_class'] ); ?>"> 
			<?php if ( '' !== $atts['title'] ) : ?>                                   
				<div class="sidebar-social-icons-inner"> 
					<h5><?php echo esc_html( $atts['title'] ); ?></h5> 
				</div>                                     
			<?php endif; ?>                                     
			<div class="sidebar-social-icons-wrapper">
				<ul>
					<?php
					$atts['social_media'] = vc_param_group_parse_atts( $atts['social_media'] );
					foreach ( $atts['social_media'] as $icons ) : ?> 
						<li> 
							<a class="<?php echo esc_html( $icons['social_icon'] ); ?>" href="<?php echo esc_url( $icons['social_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
								<i class="fa fa-<?php echo esc_html( $icons['social_icon'] ); ?>"></i>
							</a> 
						</li>                                         
					<?php endforeach; ?>                                        
				</ul>
			</div><!-- /.sidebar-social-icons-wrapper -->
		</div><!-- /.sidebar-social-icons -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_stay_connected_shortcode', 'reendex_stay_connected' );

// Reendex - Instagram.
// usages:[reendex_instagram_shortcode].
if ( ! function_exists( 'reendex_instagram' ) ) {
	/**
	 * Instagram Shortcode
	 *
	 * Displays Instagram.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Instagram.
	 */
	function reendex_instagram( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'             => esc_html__( 'Find us on Instagram!', 'reendex' ),
			'subtitle'          => '',
			'description'       => '',
			'username'          => '',
			'number'            => 6,
			'button_text'       => esc_html__( 'We are now on Instagram', 'reendex' ),
			'button_link'       => '',
			'button_target'     => '_self',
			'target'            => '_blank',
			'extra_class'       => '',
		), $atts );
		ob_start();
		?>
		<div class="instagram-wrapper <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div class="row">
				<div class="col-md-6"> 
					<div class="instagram-content">  
						<h2 class="instagram-title"><?php echo esc_html( $atts['title'] ); ?></h2>
						<h4 class="instagram-subtitle"><?php echo esc_html( $atts['subtitle'] ); ?></h4>
						<?php if ( '' !== $atts['description'] ) : ?>
							<p class="description"><?php echo esc_html( wp_strip_all_tags( $atts['description'] ) ); ?></p>
						<?php endif; ?>  
						<a href="<?php echo esc_url( $atts['button_link'] ); ?>" target="<?php echo esc_attr( $atts['button_target'] ); ?>" class="btn btn-default"><?php echo esc_attr( $atts['button_text'] ); ?></a>                             
					</div>
				</div><!-- /.col-md-6 -->
				<div class="col-md-6 reendex-instagram">
					<?php
						$args = array(
							'photos_per_page'   => $atts['number'],
						);
					?>
					<?php
					$number = $atts['number'];
					if ( '' != $atts['username'] ) {
						$media_array = reendex_scrape_instagram( $atts['username'], $atts['number'] );
						if ( is_wp_error( $media_array ) ) {
							echo wp_kses_post( $media_array->get_error_message() );
						} else {
							// filter for images only.
							$images_only = apply_filters( 'reendex_images_only', false );
							if ( $images_only ) {
								$media_array = array_filter( $media_array, array( $this, 'images_only' ) );
							}
							?>
							<ul class="instagram-photo-content">
								<?php
								$i = 0;
								foreach ( $media_array as $item ) :
									if ( $atts['number'] == $i ) {
										break;
									}
									$i++;
									$short_caption = wp_trim_words( $item['description'], 5, '' );
									$short_caption = preg_replace( '/[^A-Za-z0-9?! ]/','', $short_caption );
									?>
									<li>
										<a href="<?php if ( '' != $item['link'] ) { echo esc_url( $item['link'] ); } ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
											<img class="img-responsive" src="<?php echo esc_attr( $item['small'] ); ?>" alt="<?php echo esc_attr( $short_caption ); ?>" title="<?php echo esc_attr( $item['description'] ); ?>">
										</a>									
									</li>
								<?php endforeach; ?>
							</ul><!-- /.instagram-photo-content -->
							<?php
						}
					}
					?>               
				</div><!-- /.col-md-6 reendex-instagram -->
			</div><!-- /.row -->	
		</div><!-- /.instagram-wrapper -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_instagram_shortcode', 'reendex_instagram' );

// Reendex - Twitter Feed.
// usages:[reendex_twitter_shortcode].
if ( ! function_exists( 'reendex_twitter' ) ) {
	/**
	 * Twitter Feed Shortcode Shortcode
	 *
	 * Displays Twitter Feed Shortcode.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Twitter Feed.
	 */
	function reendex_twitter( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                 => '',
			'username'              => esc_html__( 'Envato', 'reendex' ),
			'limit'                 => 4,
			'exclude_replies'       => 'false',
			'cache_time'            => 12,
			'consumer_key'          => '',
			'consumer_secret'       => '',
			'access_token'          => '',
			'access_token_secret'   => '',
			'extra_class'           => '',
		), $atts );
		if ( '' === $atts['username'] || ! class_exists( 'Reendex\TwitterOAuth' ) ) {
			return;
		}
		$consumer_key = '';
		$consumer_secret = '';
		$access_token = '';
		$access_token_secret = '';
		if ( '' === $consumer_key || '' === $consumer_secret || '' === $access_token || '' === $access_token_secret ) {
			$consumer_key           = 'cLbGsjEKbb9ONH09lc64vIzlH';
			$consumer_secret        = 'X30NcLFUL39AsCJCgYjLum8DJcqxW9BSz8yE63R6FZJx7NjoHs';
			$access_token           = '883239810261012480-7apuXU4vAleAgHPPPOBRK6xzCHAxkHI';
			$access_token_secret    = 'kAR7VHj8dMPUhjVxh0rUw4WgWk9NlEjF9TOlTCVqBvMeW';
		}
		$atts['exclude_replies'] = ( 'false' == $atts['exclude_replies'] )? 1: 2;
		$transient_key = 'twitter_' . implode( '', $atts );
		$cache = get_transient( $transient_key );
		if ( 1 == $cache ) {
			return $cache;
		} else {
			$twitter_connection = new Reendex\TwitterOAuth( $atts['consumer_key'], $atts['consumer_secret'], $atts['access_token'], $atts['access_token_secret'] );
			$tweets = $twitter_connection->get( 'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=' . $atts['username'] . '&count=' . $atts['limit'] . '&exclude_replies=' . $atts['exclude_replies'] );
			if ( ! isset( $tweets->errors ) && is_array( $tweets ) ) {
				ob_start();
				?>
				<div class="twitter-feed-area <?php echo esc_attr( $atts['extra_class'] ); ?>">
					<?php if ( '' != $atts['title'] ) : ?>
						<div class="twitter-feed-inner">
							<h5><?php echo esc_html( $atts['title'] ); ?></h5>
						</div>
					<?php endif; ?>				    
					<div class="twitter-feed-wrapper">
						<div class="news-block">
							<?php
							foreach ( $tweets as $tweet ) {
								$tweet_link = 'https://twitter.com/' . $tweet->user->screen_name . '/statuses/' . $tweet->id;
								$user_link = 'https://twitter.com/' . $tweet->user->screen_name;
								?>
								<div class="item-block">
									<div class="item-image-1">
										<a href="<?php echo esc_url( $user_link ); ?>" class="img-link">
											<img src="<?php echo esc_attr( $tweet->user->profile_image_url ); ?>" title="<?php echo esc_attr( $tweet->user->name ); ?>" alt="<?php echo esc_attr( $tweet->user->name ); ?>">
										</a>
										<a href="<?php echo esc_url( $user_link ); ?>" class="label-13">
											<p><i class="fa fa-twitter"></i><?php echo '@' . esc_html( $tweet->user->name ); ?></p>
										</a>
									</div><!-- /.item-image-1 -->
									<div class="content">
										<?php echo esc_html( $tweet->text ); ?>
									</div>
								</div><!-- /.item-block -->
								<?php
							}
							?>
						</div><!-- /.news-block -->
					</div><!-- /.twitter-feed-wrapper -->
				</div><!-- /.twitter-feed-area -->
				<?php
				$output = ob_get_clean();
				set_transient( $transient_key, $output, $atts['cache_time'] * HOUR_IN_SECONDS );
				return $output;
			} // End if().
		} // End if().
	}
} // End if().
add_shortcode( 'reendex_twitter_shortcode', 'reendex_twitter' );

// Reendex - Weather Dark.
// usages:[reendex_weather_shortcode].
if ( ! function_exists( 'reendex_weather' ) ) {
	/**
	 * Weather Dark Shortcode
	 *
	 * Displays Weather Dark.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Weather Dark.
	 */
	function reendex_weather( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'extra_class'       => '',
		), $atts );
		ob_start();
		?>
		<div id="weather" class="sidebar-weather <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div class='panel'>
				<div class="block-title-1">
					<span class='city' id='city'></span>
				</div>
				<div class="weather-card">
					<div class='weather'>
						<div class='temperature' id='temperature'>
							<div class='temp' id='temp'>
								<i id='condition'></i>
								<span id='num'></span>
								<a class='fahrenheit active' id='fahrenheit' href="#">&deg;F</a>
								<span class='divider secondary'>|</span>
								<a class='celsius' id='celsius' href="#">&deg;C</a>
							</div>
						</div>
						<div class='group secondary'>
							<div id='haze' class="desc-text"></div>
							<div id='wind' class="desc-text"></div>
							<div id='humidity' class="desc-text"></div>
						</div>
						<div class='forecast' id='forecast'></div>					
					</div><!-- /.weather --> 
				</div><!-- /.weather-card --> 
			</div><!-- /.panel -->
		</div><!-- /#weather --> 
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_weather_shortcode', 'reendex_weather' );

// Reendex - Weather Light.
// usages:[reendex_weather_light_shortcode].
if ( ! function_exists( 'reendex_weather_light' ) ) {
	/**
	 * Weather Light Shortcode
	 *
	 * Displays Weather Light.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Weather Light.
	 */
	function reendex_weather_light( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'extra_class'       => '',
		), $atts );
		ob_start();
		?>
		<div id="weather" class="sidebar-weather <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div class='panel'>
				<div class="block-title-1">
					<span class='city' id='city'></span>
				</div>
				<div class="weather-card-light">
					<div class='weather'>
						<div class='temperature' id='temperature'>
							<div class='temp' id='temp'>
								<i id='condition'></i>
								<span id='num'></span>
								<a class='fahrenheit active' id='fahrenheit' href="#">&deg;F</a>
								<span class='divider secondary'>|</span>
								<a class='celsius' id='celsius' href="#">&deg;C</a>
							</div>
						</div>
						<div class='group secondary'>
							<div id='haze' class="desc-text"></div>
							<div id='wind' class="desc-text"></div>
							<div id='humidity' class="desc-text"></div>
						</div>
						<div class='forecast' id='forecast'></div>					
					</div>
				</div><!-- /.weather-card-light -->
			</div><!-- /.panel -->
		</div><!-- /#weather -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_weather_light_shortcode', 'reendex_weather_light' );

// Reendex - Small Weather.
// usages:[reendex_small_weather_shortcode].
if ( ! function_exists( 'reendex_small_weather' ) ) {
	/**
	 * Weather Shortcode
	 *
	 * Displays Weather.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Weather.
	 */
	function reendex_small_weather( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'         => '',
			'extra_class'   => '',
		), $atts );
		ob_start();
		?>
		<div id="weather" class="sidebar-weather <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div class="panel">
				<div class="small-weather-card">
					<div class='weather'>
						<h4 class="local-weather-title"><?php echo esc_html( $atts['title'] ); ?></h4> 
						<span class='city' id='city'></span>
						<div class='temperature' id='temperature'>
							<div class='temp' id='temp'>
								<i id='condition'></i>
								<span id='num'></span>
								<a class='fahrenheit active' id='fahrenheit' href="#">&deg;F</a>
								<span class='divider secondary'>|</span>
								<a class='celsius' id='celsius' href="#">&deg;C</a>
							</div>
						</div>
					</div><!-- /.weather -->
				</div><!-- /.small-weather-card -->
			</div><!-- /.panel -->
		</div><!-- /#weather -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_small_weather_shortcode', 'reendex_small_weather' );

// Reendex - Video Project.
// usages:[reendex_video_project_shortcode].
if ( ! function_exists( 'reendex_video_project' ) ) {
	/**
	 * Video project Shortcode
	 *
	 * Displays Video project.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Video project.
	 */
	function reendex_video_project( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                     => '',
			'post_type'                 => 'our-video',
			'categories'                => '',
			'per_page'                  => 1,
			'order'                     => 'DESC',
			'target'                    => '_self',
			'related_content_length'    => '100',
		), $atts );
		ob_start();
		?>
		<div class="row no-gutter project-video">
			<?php
				$args = array(
					'post_type'            => 'our-video',
					'post_status'          => 'publish',
					'ignore_sticky_posts'  => 1,
					'posts_per_page'       => 2,
					'order'                => $atts['order'],
					'orderby'         => 'rand',
				);
				$categories = str_replace( ' ', '', $atts['categories'] );
			if ( strlen( $categories ) > 0 ) {
				$ar_categories = explode( ',', $categories );
				if ( is_array( $ar_categories ) && count( $ar_categories ) > 0 ) {
					$field_name = is_numeric( $ar_categories[0] )?'term_id':'slug';
					$args['tax_query'] = array(
						array(
							'taxonomy'         => 'thcat_taxonomy',
							'terms'            => $ar_categories,
							'field'            => $field_name,
							'include_children' => false,
						),
					);
				}
			}
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) :
				$query->the_post();
				$reendex_metabox_id = '';
				$reendex_metabox_id = get_the_id();
			?>
				<div class="col-sm-8 col-md-8"> 
					<div class="video-full">
						<div class="fitvids-video">	        
							<iframe src="<?php echo esc_url( get_post_meta( $reendex_metabox_id, 'reendex_video_link', true ) ); ?>"></iframe>
						</div>						
					</div>                                 
				</div>                             
			<?php endwhile; ?>
			<div class="col-xs-12 col-sm-4 col-md-4"> 
				<div class="related-video-title"> 
					<h4><?php echo esc_html( $atts['title'] ); ?></h4> 
				</div>
				<?php
					$related_args = array(
						'post_type'       => 'our-video',
						'post_status'     => 'publish',
						'post__not_in'    => array( get_the_ID() ),
						'orderby'         => 'rand',
					);
					$related_video = new WP_Query( $related_args );
				if ( $related_video->have_posts() ) : ?>
					<div class="related video-full">
						<div class="fitvids-video">	        
							<iframe src="<?php echo esc_url( get_post_meta( $reendex_metabox_id, 'reendex_video_link', true ) ); ?>"></iframe>
						</div>
					</div><!-- /.related video-full -->
					<div class="content-wrap">
						<div class="video-post_content">
							<div class="title-left title-style03 underline03">
								<h4><a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php the_title(); ?></a></h4>
							</div>
							<div class="content">
								<p><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['related_content_length'] ) ); ?></a></p>
							</div>
							<ul class="social-links list-inline">
								<?php
									$social_media = get_post_meta( get_the_id(),'reendex_social_group',true );
								if ( is_array( $social_media ) ) {
									foreach ( $social_media as $icons ) :?>
										<li>
											<a class="<?php echo esc_html( $icons['reendex_social_icon'] );?>" href="<?php if ( isset( $icons['reendex_social_link'] ) ) { echo esc_url( $icons['reendex_social_link'] ); } ?>">
												<i class="fa fa-<?php echo esc_html( $icons['reendex_social_icon'] );?>"></i>
											</a>
										</li>
									<?php endforeach;
								}
								?>
							</ul><!-- /.social-links list-inline -->
						</div><!-- /.video-post_content -->
					</div><!-- /.content-wrap -->
				<?php endif;
				wp_reset_postdata(); ?>        
			</div><!-- /.col-xs-12 col-sm-4 col-md-4 -->
		</div><!-- /.row no-gutter project-video -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_video_project_shortcode', 'reendex_video_project' );

// Reendex - Video News.
// usages:[reendex_video_news_shortcode].
if ( ! function_exists( 'reendex_video_news' ) ) {
	/**
	 * Video News Shortcode
	 *
	 * Displays Video News.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Video News.
	 */
	function reendex_video_news( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                 => '',
			'title_style'           => 1,
			'title_margin_bottom'   => '30',
			'content_length'        => '',
			'taxonomies'            => '',
			'per_page'              => 1,
			'target'                => '_self',
			'order'                 => '',
			'orderby'               => '',
			'offset'                => 0,
			'duplicate_posts'       => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		global $do_not_duplicate;
		$postids = '';
		if ( 'true' == $atts['duplicate_posts'] ) {
			$postids = array_merge( array( get_the_ID() ), $do_not_duplicate );
		}
		?>
		<div class="video-news <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<?php if ( '' != $atts['title'] ) : ?>
				<div class="
					<?php
					if ( 2 == $atts['title_style'] ) {
						echo 'title-style02';
					} elseif ( 3 == $atts['title_style'] ) {
						echo 'title-style01';
					} else {
						echo 'block-title-2';
					} ?>"
					style="
						<?php
						if ( '' != $atts['title_margin_bottom'] ) {
							echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
						} ?>"> 
					<h3><?php if ( '' != $atts['title'] ) { echo esc_attr( $atts['title'] ); } ?></h3> 
				</div>
			<?php endif; ?>	
			<?php
				$args = array(
					'post_type'             => 'post',
					'post_status'           => 'publish',
					'ignore_sticky_posts'   => 1,
					'posts_per_page'        => $atts['per_page'],
					'order'                 => $atts['order'],
					'orderby'               => $atts['orderby'],
					'offset'                => $atts['offset'],
					'post__not_in'          => $postids,
				);
			if ( ! empty( $atts['taxonomies'] ) ) {
				$vc_taxonomies_types = get_taxonomies(
					array(
						'public' => true,
					)
				);
				$terms = get_terms( array_keys( $vc_taxonomies_types ), array(
					'hide_empty' => false,
					'include' => $atts['taxonomies'],
				) );
				$args['tax_query'] = array();
				$tax_queries = array(); // List of taxonomies.
				foreach ( $terms as $t ) {
					if ( ! isset( $tax_queries[ $t->taxonomy ] ) ) {
						$tax_queries[ $t->taxonomy ] = array(
							'taxonomy'          => $t->taxonomy,
							'field'             => 'id',
							'terms'             => array( $t->term_id ),
							'include_children'  => false,
							'relation'          => 'IN',
						);
					} else {
						$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
					}
				}
				$args['tax_query'] = array_values( $tax_queries );
				$args['tax_query']['relation'] = 'OR';
			}
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) :
				$query->the_post();
				$do_not_duplicate[] = get_the_ID();
			?>                                 
				<div class="sidebar-block">
					<div class="fitvids-video">	        
						<iframe src="<?php echo esc_url( get_post_meta( get_the_id(), 'reendex_post_formate_vdo', true ) ); ?>"></iframe>
					</div><!-- /.fitvids-video -->
					<?php if ( '' !== $atts['content_length'] ) : ?>                                    
						<div class="sidebar-content">
							<h3><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['content_length'] ) ); ?></a></h3>
						</div>
					<?php endif; ?>                                     
				</div><!-- /.sidebar-block -->                                 
			<?php endwhile;
			wp_reset_postdata();
			?>
		</div><!-- /.video-news -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_video_news_shortcode', 'reendex_video_news' );

// Reendex - Video Post Module.
// usages:[reendex_video_post_module_shortcode].
if ( ! function_exists( 'reendex_video_post_module' ) ) {
	/**
	 * Video Post Module Shortcode
	 *
	 * Displays Video Post Module.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Video Post Module.
	 */
	function reendex_video_post_module( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                 => '',
			'title_style'           => 1,
			'title_margin_bottom'   => '',
			'show_content'          => 0,
			'video_link'            => '',
			'extra_class'           => '',
			'link'                  => 'link',
			'link_text'         => '',
			'header'            => '',
			'programme_name'    => '',
			'button_link'       => '',
			'button_text'       => '',
			'line-height'       => '24',
			'header_color'      => '#ced2d9',
			'header_f_size'     => '18',
			'programme_color'   => '#fff',
			'programme_f_size'  => '28',
			'description_color' => '#ced2d9',
			'desc_f_size'       => '14',
			'read_more_color'   => '#ddd',
			'color'             => '#212126',
			'add_link'          => '',
		), $atts );
		$href = vc_build_link( $atts['link'] );
		$addlink = '';
		$addlink .= '<a class="container" href="' . $href['url'] . '" title="' . $href['title'] . '" target="' . $href['target'] . '">' . $atts['link_text'] . '</a>';
		ob_start();
		?>
		<div class="video-promo-item news camera-live <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div class="video-post_content">
				<?php if ( '' != $atts['title'] ) : ?>
					<div class="
						<?php
						if ( 2 == $atts['title_style'] ) {
							echo 'title-style02';
						} elseif ( 3 == $atts['title_style'] ) {
							echo 'title-style01';
						} else {
							echo 'block-title-2';
						}
						?>"
						style="
							<?php
							if ( '' != $atts['title_margin_bottom'] ) {
								echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
							} ?>"> 
						<h3><?php if ( '' != $atts['title'] ) { echo esc_attr( $atts['title'] ); } ?></h3> 
					</div>
				<?php endif; ?>
			</div><!-- /.video-post_content -->
			<div class="video-full">
				<div class="fitvids-video">	        
					<iframe src="<?php echo esc_url( $atts['video_link'] ); ?>"></iframe>
				</div><!-- /.fitvids-video -->
			</div><!-- /.video-full -->
			<?php if ( 1 == $atts['show_content'] ) : ?>
				<div class="video-promo-wrapper">
					<div class="video-promo-content" style="background:<?php echo esc_attr( $atts['color'] ); ?>">
						<span class="section-highlight-inner">
							<a href="<?php echo esc_url( $atts['button_link'] ); ?>" target="blank"><?php echo esc_html( $atts['button_text'] ); ?></a>
						</span>
						<div class="programme-header" style="color:<?php echo esc_attr( $atts['header_color'] ); ?>; font-size:<?php echo esc_attr( $atts['header_f_size'] ) ?>px;">
							<?php echo esc_html( $atts['header'] ); ?>
						</div>
						<div class="programme-name" style="color:<?php echo esc_attr( $atts['programme_color'] ); ?>; font-size:<?php echo esc_attr( $atts['programme_f_size'] ) ?>px;">
							<?php echo esc_html( $atts['programme_name'] ); ?>
						</div>
						<div class="content-wrapper">
							<p style="color:<?php echo esc_attr( $atts['description_color'] ); ?>; font-size:<?php echo esc_attr( $atts['desc_f_size'] ) ?>px; line-height:<?php echo esc_attr( $atts['line-height'] ) ?>px;">
								<?php echo esc_html( wp_strip_all_tags( $content ) ); ?>
							</p>
							<a class="read-more" href="<?php echo esc_url( $href['url'] ); ?>" style="color:<?php echo esc_attr( $atts['read_more_color'] ); ?>" target="<?php echo esc_attr( $href['target'] ); ?>"><?php echo esc_html( $href['title'] ); ?></a>
						</div>
					</div><!-- /.video-promo-content-->
				</div><!-- /.video-promo-wrapper-->
			<?php endif; ?>
		</div><!-- /.video-promo-item news camera-live-->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_video_post_module_shortcode', 'reendex_video_post_module' );

// Reendex - Big Video Module.
// usages:[reendex_big_video_module_shortcode].
if ( ! function_exists( 'reendex_reendex_big_video_module' ) ) {
	/**
	 * Big Video Module Shortcode
	 *
	 * Displays Big Video Module.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Big Video Module.
	 */
	function reendex_big_video_module( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                 => '',
			'title_style'           => 1,
			'title_margin_bottom'   => '',
			'video_link'            => '',
			'extra_class'           => '',
			'link'                  => 'link',
			'site'                  => 'youtube',
		), $atts );
		$href = vc_build_link( $atts['link'] );
		ob_start();
		?>
		<div class="news camera-live <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div class="video-post_content">
				<?php if ( '' != $atts['title'] ) : ?>
					<div class="
						<?php
						if ( 2 == $atts['title_style'] ) {
							echo 'title-style02';
						} elseif ( 3 == $atts['title_style'] ) {
							echo 'title-style01';
						} else {
							echo 'block-title-2';
						}
						?>"
						style="
							<?php
							if ( '' != $atts['title_margin_bottom'] ) {
								echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
							} ?>"> 
						<h3><?php if ( '' != $atts['title'] ) { echo esc_attr( $atts['title'] ); } ?></h3> 
					</div>
				<?php endif; ?>
			</div><!-- /.video-post_content -->
			<div class="video-full">
				<div class="video-container">
					<div class="fitvids-video">	        
						<iframe src="<?php echo esc_url( $atts['video_link'] ); ?>"></iframe>
					</div>					
				</div><!-- /.video-container -->
			</div><!-- /.video-full -->
			<div class="sidebar-content">
				<h3><a href="<?php echo esc_url( $href['url'] ); ?>" target="<?php echo esc_attr( $href['target'] ); ?>"><?php echo esc_html( $href['title'] ); ?></a></h3>
			</div>		
		</div><!-- /.news camera-live-->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_big_video_module_shortcode', 'reendex_big_video_module' );

// Reendex - TV Schedule Banner.
// usages:[reendex_tv_schedule_banner_shortcode].
if ( ! function_exists( 'reendex_tv_schedule_banner' ) ) {
	/**
	 * TV Schedule banner Shortcode
	 *
	 * Displays TV Schedule banner.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of TV Schedule banner.
	 */
	function reendex_tv_schedule_banner( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'schedule_title'    => '',
			'news_title'        => '',
			'host_title'        => esc_html__( 'Hosted by', 'reendex' ),
			'host_name'         => '',
			'description'       => '',
			'description_link'  => '',
			'target'            => '_self',
			'host_image'        => '',
			'box_animation'     => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		 $host_image_url = wp_get_attachment_image_src( $atts['host_image'],'full' );
		 $atts['host_name'] = $atts['host_title'] . ' ' . $atts['host_name'];
		?>
		<div class="tvbanner-area <?php echo esc_attr( $atts['extra_class'] ); ?>"> 
			<div class="show-info">
				<?php if ( '' !== $atts['schedule_title'] ) : ?>
					<h4 class="schedule-logo bg-1"><?php echo esc_attr( $atts['schedule_title'] ); ?></h4>
				<?php endif; ?>
				<div class="show-title">
					<?php if ( '' !== $atts['news_title'] ) : ?> 
						<h2><?php echo esc_attr( $atts['news_title'] ); ?></h2>
					<?php endif; ?> 
					<h3><?php echo esc_attr( $atts['host_name'] ); ?></h3> 
				</div>
				<?php if ( '' !== $atts['description'] ) : ?>                             
					<h4 class="show-info-button bg-1"><a href="<?php echo esc_url( $atts['description_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_attr( $atts['description'] ); ?></a></h4>
				<?php endif; ?> 
				<div class="figure">
					<?php
					if ( '' !== $host_image_url ) {
						echo '<img src=" ' . esc_url( $host_image_url[0] ) . ' " alt="' . esc_attr( $atts['host_name'] ) . '">';
					}
					?>
				</div><!-- /.figure -->                             
			</div><!-- /.show-info -->
			<?php if ( 1 == $atts['box_animation'] ) : ?>                         
				<div class="schedule-squares">
					<span class="square2"></span>
					<span class="square3"></span>
					<span class="square4"></span> 
					<span class="square5"></span>
					<span class="square6"></span>
					<span class="square7"></span>
					<span class="square8"></span>
					<span class="square9"></span>
					<span class="square10"></span>
					<span class="square11"></span>
				</div><!-- /.schedule-squares -->
			<?php endif; ?>                         
		</div><!-- /.tvbanner-area -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_tv_schedule_banner_shortcode', 'reendex_tv_schedule_banner' );

// Reendex - TV Schedule Slider.
// usages:[reendex_tv_schedule_shortcode].
if ( ! function_exists( 'reendex_tv_schedule' ) ) {
	/**
	 * TV Schedule Slider Shortcode
	 *
	 * Displays TV Schedule.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of TV Schedule Slider.
	 */
	function reendex_tv_schedule( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                 => '',
			'title_color'           => '',
			'title_bg_color'        => '',
			'title_margin_bottom'   => '30',
			'per_page'              => 5,
			'title_length'          => 5,
			'content_length'        => 8,
			'target'                => '_self',
			'extra_class'           => '',
		), $atts );
		ob_start();
		?>
		<div class="sidebar-block">
			<div class="sidebar-schedule <?php echo esc_attr( $atts['extra_class'] ); ?>">
				<?php if ( '' !== $atts['title'] ) : ?>                                 
					<div class="block-title-3" style="
						<?php
						if ( '' !== $atts['title_bg_color'] ) {
							echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';';
						} ?>
						<?php
						if ( '' != $atts['title_margin_bottom'] ) {
							echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
						} ?>"> 
						<h3 class="schedule-title" style="<?php if ( '' !== $atts['title_color'] ) { echo 'color:' . esc_attr( $atts['title_color'] ) . ';'; } ?>"><?php echo esc_html( $atts['title'] ); ?></h3> 
					</div>
				<?php endif; ?>            
				<div id="sidebar-schedule-slider" class="owl-carousel">
					<?php
						$args = array(
							'post_type'            => 'tv_schedule',
							'post_status'          => 'publish',
							'ignore_sticky_posts'  => 1,
							'posts_per_page'       => $atts['per_page'],
						);
						$query = new WP_Query( $args );
					while ( $query->have_posts() ) :
						$query->the_post();
						$tv_time = get_post_meta( get_the_id(),'reendex_tvschedule_time',true );
						$permalink = get_permalink();
					?>
						<div class="sidebar-schedule-slide"> 
							<div class="sidebar-schedule-slider-layer"> 
								<div class="content"> 
									<h3 class="hour-tag"><?php echo esc_attr( $tv_time ); ?></h3> 
									<h4 class="sidebar-show-title bg-1"><a href="<?php echo esc_url( $permalink ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title( $atts['title_length'] ); ?></a></h4>
									<p><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['content_length'] ) ); ?></a></p>
								</div>                                                     
								<?php
									the_post_thumbnail( 'reendex_news6_thumb_2',
										array(
											'class' => 'img-responsive img-not-lazy',
										)
									);
								?>
							</div><!-- /.sidebar-schedule-slider-layer -->                                             
						</div><!-- /.sidebar-schedule-slide -->
					<?php endwhile;
					wp_reset_postdata();
					?>
				</div><!-- /#sidebar-schedule-slider -->
			</div><!-- /.sidebar-schedule -->
		</div><!-- /.sidebar-block -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_tv_schedule_shortcode', 'reendex_tv_schedule' );

// Reendex - TV Schedule Tab.
// usages:[reendex_tv_schedule_tab_shortcode].
if ( ! function_exists( 'reendex_tv_schedule_tab' ) ) {
	/**
	 * TV Schedule Tab Shortcode
	 *
	 * Displays TV Schedule Tab.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of TV Schedule Tab.
	 */
	function reendex_tv_schedule_tab( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'number_of_show'            => -1,
			'time_of_the_day_first'     => esc_html__( 'Morning', 'reendex' ),
			'time_of_the_day_second'    => esc_html__( 'Afternoon', 'reendex' ),
			'time_of_the_day_third'     => esc_html__( 'Evening', 'reendex' ),
			'time_of_the_day_fourth'    => esc_html__( 'Late', 'reendex' ),
			'content_length'            => 100,
			'target'                    => '_self',
		), $atts );
		ob_start();
		?>
		<div class="panel panel-flat tv_schedule_list"> 
			<div class="panel-body"> 
				<div class="tabbable"> 
					<ul class="nav nav-tabs nav-tabs-solid nav-justified">
						<?php
							$args['orderby']    = 'slug';
							$args['order']      = 'ASC';
							$tv_cat = get_terms( 'tv_category', $args );
							$n = 0;
						foreach ( $tv_cat as $tv_cats ) :
							$n++;
							$name = $tv_cats->name;
							$slug = $tv_cats->slug;
							?>
							<li class="
								<?php
								if ( 1 === $n ) {
									echo 'active';
								} ?>">
								<a data-toggle="tab" href="#<?php echo esc_attr( $slug ); ?>" aria-expanded="true"><?php echo esc_attr( $name ); ?></a>
							</li>
						<?php endforeach;?>
					</ul><!-- /.nav nav-tabs nav-tabs-solid nav-justified -->
					<div class="tab-content"> 
						<?php
							$i = 0;
						foreach ( $tv_cat as $cats ) {
							$i++;
							$name = $cats->name;
							$slug = $cats->slug;
						?>
						<div id="<?php echo esc_attr( $slug ); ?>" class="tab-pane
							<?php
							if ( 1 === $i ) {
								echo 'active';
							} ?>"> 
							<div class="row no-gutter">
								<?php
								if ( is_array( $tv_cat ) && count( $tv_cat ) > 0 ) {
									$field_name = is_numeric( $tv_cat[0] )?'term_id':'slug';
									$args = array(
										'post_type'             => 'tv_schedule',
										'posts_per_page'        => $atts['number_of_show'],
										'order'                 => 'ASC',
										'tax_query' => array(
											array(
												'taxonomy'          => 'tv_category',
												'terms'             => $cats,
												'field'             => $field_name,
												'include_children'  => false,
											),
										),
									);
								}
								$query = new WP_Query( $args );
								$mc = 0;
								$fc = 0;
								$ec = 0;
								$lt = 0;
								while ( $query->have_posts() ) :
									$query->the_post();
									$tv_shift = get_post_meta( get_the_id(),'reendex_tvschedule_shift',true );
									$tv_date = get_post_meta( get_the_id(),'reendex_tvschedule_date',true );
									$tv_time = get_post_meta( get_the_id(),'reendex_tvschedule_time',true );
									$tv_subtitle = get_post_meta( get_the_id(),'reendex_tvschedule_subtitle',true );
								?>
									<?php if ( 'morning' === $tv_shift ) : $mc++ ?>
										<?php if ( 1 === $mc ) : ?>
											<div class="title-style01">
												<h2>
													<?php
													if ( isset( $atts['time_of_the_day_first'] ) ) {
														echo esc_attr( $atts['time_of_the_day_first'] );
													} ?>
												</h2>
											</div>
										<?php endif; ?>
										<div class="news">
											<div class="news-outer-wrapper"> 
												<div class="item-wrapper">
													<div class="item">
														<div class="item-image-2">
															<a class="img-link" href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
																<?php the_post_thumbnail( 'full',
																	array(
																		'class' => 'img-responsive img-full',
																	)
																);
																?>
															</a>
														</div><!-- /.item-image-2 -->
														<div class="item-content">
															<?php if ( '' !== $tv_time ) : ?>
																<h3 class="schedule-hour"><?php echo esc_attr( $tv_time ); ?></h3>
															<?php endif; ?>									    
															<div class="title-left title-style04 underline04">
																<h3 class="schedule-show"><a href="<?php the_permalink();?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php the_title(); ?></a></h3>
															</div>
															<?php if ( '' !== $tv_date ) : ?>
																<h4 class="date subtitle"><?php echo esc_attr( $tv_date ); ?></h4>
															<?php endif; ?>
															<?php if ( '' !== $tv_subtitle ) : ?>
																<h4 class="subtitle"><?php echo esc_attr( $tv_subtitle ); ?></h4>
															<?php endif; ?>
															<p class="schedule-content"><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['content_length'] ) ); ?></a></p>
														</div><!-- /.item-content -->
													</div><!-- /.item -->
												</div><!-- /.item-wrapper -->
											</div><!-- /.news-outer-wrapper -->
										</div><!-- /.news -->
									<?php elseif ( 'afternoon' === $tv_shift ) : $fc++ ?>
										<?php if ( 1 === $fc ) : ?>
											<div class="title-style01">
												<h2>
													<?php
													if ( isset( $atts['time_of_the_day_second'] ) ) {
														echo esc_attr( $atts['time_of_the_day_second'] );
													} ?>
												</h2>
											</div>
										<?php endif; ?>
										<div class="news">
											<div class="news-outer-wrapper"> 
												<div class="item-wrapper">
													<div class="item">
														<div class="item-image-2">
															<a class="img-link" href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
																<?php the_post_thumbnail( 'reendex_news1_thumb',
																	array(
																		'class' => 'img-responsive img-full',
																	)
																);
																?>
															</a>
														</div><!-- /.item-image-2 -->
														<div class="item-content">
															<?php if ( '' !== $tv_time ) : ?>
																<h3 class="schedule-hour"><?php echo esc_attr( $tv_time ); ?></h3>
															<?php endif; ?>									    
															<div class="title-left title-style04 underline04">
																<h3 class="schedule-show"><a href="<?php the_permalink();?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php the_title(); ?></a></h3>
															</div>
															<?php if ( '' !== $tv_date ) : ?>
																<h4 class="date subtitle"><?php echo esc_attr( $tv_date ); ?></h4>
															<?php endif; ?>
															<?php if ( '' !== $tv_subtitle ) : ?>
																<h4 class="subtitle"><?php echo esc_attr( $tv_subtitle ); ?></h4>
															<?php endif; ?>
															<p class="schedule-content"><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['content_length'] ) ); ?></a></p>
														</div><!-- /.item-content -->
													</div><!-- /.item -->
												</div><!-- /.item-wrapper -->
											</div><!-- /.news-outer-wrapper -->
										</div><!-- /.news -->
									<?php elseif ( 'evening' === $tv_shift ) : $ec++ ?>
										<?php if ( 1 === $ec ) : ?>
											<div class="title-style01">
												<h2>
													<?php
													if ( isset( $atts['time_of_the_day_third'] ) ) {
														echo esc_attr( $atts['time_of_the_day_third'] );
													} ?>
												</h2>
											</div>
										<?php endif; ?>
										<div class="news">
											<div class="news-outer-wrapper"> 
												<div class="item-wrapper">
													<div class="item">
														<div class="item-image-2">
															<a class="img-link" href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
																<?php the_post_thumbnail( 'reendex_news1_thumb',
																	array(
																		'class' => 'img-responsive img-full',
																	)
																); ?>
															</a>
														</div><!-- /.item-image-2 -->
														<div class="item-content">
															<?php if ( '' !== $tv_time ) : ?>
																<h3 class="schedule-hour"><?php echo esc_attr( $tv_time ); ?></h3>
															<?php endif; ?>									    
															<div class="title-left title-style04 underline04">
																<h3 class="schedule-show"><a href="<?php the_permalink();?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php the_title(); ?></a></h3>
															</div>
															<?php if ( '' !== $tv_date ) : ?>
																<h4 class="date subtitle"><?php echo esc_attr( $tv_date ); ?></h4>
															<?php endif; ?>
															<?php if ( '' !== $tv_subtitle ) : ?>
																<h4 class="subtitle"><?php echo esc_attr( $tv_subtitle ); ?></h4>
															<?php endif; ?>
															<p class="schedule-content"><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['content_length'] ) ); ?></a></p>
														</div><!-- /.item-content -->
													</div><!-- /.item -->
												</div><!-- /.item-wrapper -->
											</div><!-- /.news-outer-wrapper -->
										</div><!-- /.news -->
									<?php else : $lt++ ?>
										<?php if ( 1 === $lt ) : ?>
											<div class="title-style01">
												<h2>
													<?php
													if ( isset( $atts['time_of_the_day_fourth'] ) ) {
														echo esc_attr( $atts['time_of_the_day_fourth'] );
													} ?>
												</h2>
											</div>
										<?php endif; ?>
										<div class="news">
											<div class="news-outer-wrapper"> 
												<div class="item-wrapper">
													<div class="item">
														<div class="item-image-2">
															<a class="img-link" href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
																<?php the_post_thumbnail( 'reendex_news1_thumb',
																	array(
																		'class' => 'img-responsive img-full',
																	)
																); ?>
															</a>
														</div><!-- /.item-image-2 -->
														<div class="item-content">
															<?php if ( '' !== $tv_time ) : ?>
																<h3 class="schedule-hour"><?php echo esc_attr( $tv_time ); ?></h3>
															<?php endif; ?>									    
															<div class="title-left title-style04 underline04">
																<h3 class="schedule-show"><a href="<?php the_permalink();?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php the_title(); ?></a></h3>
															</div>
															<?php if ( '' !== $tv_date ) : ?>
																<h4 class="date subtitle"><?php echo esc_attr( $tv_date ); ?></h4>
															<?php endif; ?>
															<?php if ( '' !== $tv_subtitle ) : ?>
																<h4 class="subtitle"><?php echo esc_attr( $tv_subtitle ); ?></h4>
															<?php endif; ?>
															<p class="schedule-content"><a href="<?php echo esc_url( get_permalink() ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $atts['content_length'] ) ); ?></a></p>
														</div><!-- /.item-content -->
													</div><!-- /.item -->
												</div><!-- /.item-wrapper -->
											</div><!-- /.news-outer-wrapper -->
										</div><!-- /.news -->
									<?php endif;
								endwhile;
								wp_reset_postdata();
								?>
							</div><!-- /.row no-gutter-->
						</div>
						<?php } // End foreach().
						?>
					</div><!-- /.tab-content --> 
				</div><!-- /.tabbable --> 
			</div><!-- /.panel-body -->
		</div><!-- /.panel panel-flat tv_schedule_list -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_tv_schedule_tab_shortcode', 'reendex_tv_schedule_tab' );

// Reendex - Call To Action.
// usages:[reendex_call_to_action_shortcode].
if ( ! function_exists( 'reendex_call_to_action' ) ) {
	/**
	 * Call to Action Shortcode
	 *
	 * Displays Call to Action.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Call to Action.
	 */
	function reendex_call_to_action( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'             => '',
			'button_text'       => '',
			'button_link'       => '',
			'target'            => '_self',
			'background_image'  => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		$call_image_url = wp_get_attachment_image_src( $atts['background_image'],'full' );
		?>
		<div class="<?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div id="parallax-section1"> 
				<div class="image3 img-overlay1" style="
					<?php
					if ( '' !== $call_image_url ) {
						echo 'background-image:url( ' . esc_url( $call_image_url[0] ) . ' )';
					} ?>"> 
					<div class="container"> 
						<div class="caption text-center"> 
							<h2 class="color-white weight-300 small-caption"><?php echo esc_html( $atts['title'] ); ?></h2> 
							<a href="<?php echo esc_url( $atts['button_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>" class="btn btn-default"><?php echo esc_attr( $atts['button_text'] ); ?></a> 
						</div>                             
					</div><!-- /.container -->                         
				</div><!-- /.image3 img-overlay1 -->                     
			</div><!-- /#parallax-section1 -->
		</div>
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_call_to_action_shortcode', 'reendex_call_to_action' );

// Reendex - Our Team.
// usages:[reendex_our_team_shortcode].
if ( ! function_exists( 'reendex_our_team' ) ) {
	/**
	 * Our team Shortcode
	 *
	 * Displays Our team.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Our team.
	 */
	function reendex_our_team( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'per_page'      => 10,
			'order'         => 'DESC',
			'target'        => '_self',
			'extra_class'   => '',
		), $atts );
		ob_start();
		?>
		<div id="topPos" title="toTop" class="container"> 
			<div class="editorial-section <?php echo esc_attr( $atts['extra_class'] ); ?>"> 
				<div class="row no-gutter"> 
					<div class="hidden-xs hidden-sm col-md-3 editorial-sticky">
						<div class="panel panel-default" id="myScrollspy"> 
							<div class="panel-body">
								<ul class="nav nav-pills nav-stacked">
									<?php
										$args = array(
											'post_type'         => 'our_team',
											'post_status'       => 'publish',
											'posts_per_page'    => $atts['per_page'],
											'order'             => $atts['order'],
										);
										$i = 0;
										$query = new WP_Query( $args );
									while ( $query->have_posts() ) :
										$query->the_post();
										$i++;
										$destination = get_post_meta( get_the_id(),'reendex_destination', true );
									?>
										<li class="menuitem <?php if ( 1 === $i ) { echo 'active'; } ?>">
											<a href="#section<?php echo esc_attr( $i ); ?>"><strong><?php the_title(); ?></strong> <?php echo esc_attr( $destination ); ?></a>
										</li>
									<?php endwhile; ?>
								</ul><!-- /.nav nav-pills nav-stacked -->
							</div><!-- /.panel-body -->
						</div><!-- /.panel panel-default -->
					</div><!-- /.hidden-xs hidden-sm col-md-3 -->
					<div class="col-md-9 editorial-sticky"> 
						<div class="panel panel-default"> 
							<div class="panel-body"> <span></span>
								<?php
									$cargs = array(
										'post_type'       => 'our_team',
										'post_status'     => 'publish',
										'posts_per_page'  => $atts['per_page'],
									);
									$j = 0;
									$cquery = new WP_Query( $cargs );
								while ( $cquery->have_posts() ) :
									$cquery->the_post();
									$j++;
									$cdestination = get_post_meta( get_the_id(),'reendex_destination', true );
									$ctwiter_user = get_post_meta( get_the_id(),'reendex_twiter_user', true );
									$ctwiter_link = get_post_meta( get_the_id(),'reendex_twiter_link', true );
									$permalink = get_permalink();
								?>
									<section id="section<?php echo esc_attr( $j ); ?>">
										<div class="media">
											<div class="pull-left visible-sm visible-md visible-lg media-left"><?php the_post_thumbnail(); ?></div>
											<div class="media-body">
												<h2 class="media-heading"><a href="<?php echo esc_url( $permalink ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><strong><?php the_title(); ?></strong><?php echo', ' . esc_attr( $cdestination ); ?></a></h2>
												<?php if ( '' !== $ctwiter_user ) : ?>
													<a class="twitter" target="_blank" href="<?php echo esc_url( $ctwiter_link ); ?>"><i class="fa fa-twitter"></i><?php echo'@' . esc_attr( $ctwiter_user ); ?></a>
												<?php endif; ?>
												<?php the_content(); ?>
											</div>
										</div><!-- /.media -->
									</section>
								<?php endwhile; ?>
							</div><!-- /.panel-body --> 
						</div><!-- /.panel panel-default --> 
					</div><!-- /.col-md-9 --> 
				</div><!-- /.row --> 
			</div><!-- /..editorial-section --> 
		</div><!-- /#topPos--> 
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_our_team_shortcode', 'reendex_our_team' );

// Reendex - Sport Results.
// usages:[reendex_sport_results_shortcode].
if ( ! function_exists( 'reendex_sport_results' ) ) {
	/**
	 * Sport Results Shortcode
	 *
	 * Displays Sport Results.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Sport Results.
	 */
	function reendex_sport_results( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'result_style'      => 1,
			'racing_result'     => '',
			'tennis_result'     => '',
			'football_team'     => '',
			'up_date'           => '',
			'target'            => '_self',
			'extra_class'       => '',
		), $atts );
		ob_start();
		?>
		<div class="panel <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div class="table-responsive">
				<?php if ( 2 == $atts['result_style'] ) : ?>
					<table class="sport-results table table-bordered table-striped">
						<thead>
							<tr class="bg-red">
								<th><?php esc_html_e( '#', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Driver', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Nat', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Team', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Pts', 'reendex' ); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
								$atts['racing_result'] = vc_param_group_parse_atts( $atts['racing_result'] );
								$r = 0;
							foreach ( (array) $atts['racing_result'] as $rresults ) :
								$r++;
							?>
								<tr>
									<td><?php echo esc_attr( $r ); ?></td>
									<td>
										<a href="
											<?php
											if ( isset( $rresults['driv_det'] ) ) {
												echo esc_url( $rresults['driv_det'] );
											} ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
											<?php
											if ( isset( $rresults['driv_name'] ) ) {
												echo esc_attr( $rresults['driv_name'] );
											} ?>
										</a>
									</td>
									<td>
										<?php
										if ( isset( $rresults['nat'] ) ) {
											echo esc_attr( $rresults['nat'] );
										} ?>
									</td>
									<td>
										<?php
										if ( isset( $rresults['team_name'] ) ) {
											echo esc_attr( $rresults['team_name'] );
										} ?>
									</td>
									<td>
										<?php
										if ( isset( $rresults['pts'] ) ) {
											echo esc_attr( $rresults['pts'] );
										} ?>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table><!-- /.sport-results -->
				<?php elseif ( 3 == $atts['result_style'] ) : ?>
					<table class="table">
						<thead>
							<tr class="bg-red">
								<th><?php esc_html_e( 'Date', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Competitors', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Set 1', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Set 2', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Set 3', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Set 4', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Set 5', 'reendex' ); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
								$atts['tennis_result'] = vc_param_group_parse_atts( $atts['tennis_result'] );
								$r = 0;
							foreach ( (array) $atts['tennis_result'] as $tresults ) :
								$r++;
							?>
								<tr>
									<td>
										<span class="day">
											<?php
											if ( isset( $tresults['tdate'] ) ) {
												echo esc_attr( $tresults['tdate'] );
											} ?>
										</span>
										<?php
										if ( isset( $tresults['tround'] ) ) {
											echo esc_attr( $tresults['tround'] );
										} ?>
									</td>
									<td>
										<?php
										if ( isset( $tresults['competitor'] ) ) {
											echo esc_attr( $tresults['competitor'] );
										} else {
											echo '-';
										} ?>
									</td>
									<td>
										<?php
										if ( isset( $tresults['sets1'] ) ) {
											echo esc_attr( $tresults['sets1'] );
										} else {
											echo '-';
										} ?>
									</td>
									<td>
										<?php
										if ( isset( $tresults['sets2'] ) ) {
											echo esc_attr( $tresults['sets2'] );
										} else {
											echo '-';
										} ?>
									</td>
									<td>
										<?php
										if ( isset( $tresults['sets3'] ) ) {
											echo esc_attr( $tresults['sets3'] );
										} else {
											echo '-';
										} ?>
									</td>
									<td>
										<?php
										if ( isset( $tresults['sets4'] ) ) {
											echo esc_attr( $tresults['sets4'] );
										} else {
											echo '-';
										} ?>
									</td>
									<td>
										<?php
										if ( isset( $tresults['sets5'] ) ) {
											echo esc_attr( $tresults['sets5'] );
										} else {
											echo '-';
										} ?>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table><!-- /.table -->
				<?php else : ?>
					<table class="sport-results table">
						<thead>
							<tr class="bg-red">
								<th><?php esc_html_e( '#', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Team', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Won', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Lost', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Tied', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Pts For', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Pts Ag', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'PCT', 'reendex' ); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
								$football_team = vc_param_group_parse_atts( $atts['football_team'] );
								$fc = 0;
							foreach ( (array) $football_team as $football_results ) :
								$fc++;
							?> 
								<tr>
									<td><?php echo esc_attr( $fc ); ?></td>
									<td><a href="<?php echo esc_url( $football_results['fteam_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php if ( isset( $football_results['fteam_name'] ) ) { echo esc_attr( $football_results['fteam_name'] ); } ?></a></td>
									<td><?php if ( isset( $football_results['won_match'] ) ) { echo esc_attr( $football_results['won_match'] ); } ?></td>
									<td><?php if ( isset( $football_results['lost_match'] ) ) { echo esc_attr( $football_results['lost_match'] ); } ?></td>
									<td><?php if ( isset( $football_results['tied_match'] ) ) { echo esc_attr( $football_results['tied_match'] ); } ?></td>
									<td><?php if ( isset( $football_results['ptsfor'] ) ) { echo esc_attr( $football_results['ptsfor'] ); } ?></td>
									<td><?php if ( isset( $football_results['ptsag'] ) ) { echo esc_attr( $football_results['ptsag'] ); } ?></td>
									<td><?php if ( isset( $football_results['win_per'] ) ) { echo esc_attr( $football_results['win_per'] ); } ?></td>   
								</tr>                                      
							<?php endforeach;?>
						</tbody>
					</table><!-- /.sport-results table -->
				<?php endif; ?>
				<?php if ( '' != $atts['up_date'] ) : ?>
					<div class="table-footer"> 
						<span><?php esc_html_e( 'Last updated:', 'reendex' ); ?> <span class="date"><?php echo esc_attr( $atts['up_date'] ); ?></span></span> 
					</div>
				<?php endif; ?>				
			</div><!-- /.table-responsive -->
		</div><!-- /.panel-->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_sport_results_shortcode', 'reendex_sport_results' );

// Reendex - Sidebar Football Results.
// usages:[reendex_sidebar_football_results_shortcode].
if ( ! function_exists( 'reendex_sidebar_football_results_sidebar' ) ) {
	/**
	 * Sidebar Football Results Shortcode
	 *
	 * Displays Sidebar Football Results.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Sidebar Football Results.
	 */
	function reendex_sidebar_football_results( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'sfootball_team'    => '',
			'up_date'           => '',
			'target'            => '_self',
			'extra_class'       => '',
		), $atts );
		ob_start();
		?>
		<div class="panel <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div class="table-responsive">
				<table class="football-results table">
					<thead>
						<tr class="bg-red">
							<th><?php esc_html_e( '#', 'reendex' ); ?></th>
							<th><?php esc_html_e( 'Team', 'reendex' ); ?></th>
							<th><?php esc_html_e( 'Pl', 'reendex' ); ?></th>
							<th><?php esc_html_e( 'GD', 'reendex' ); ?></th>
							<th><?php esc_html_e( 'Pts', 'reendex' ); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
							$football_team = vc_param_group_parse_atts( $atts['sfootball_team'] );
							$fc = 0;
						foreach ( (array) $football_team as $football_results ) :
							$fc++;
						?> 
							<tr>
								<td><?php echo esc_attr( $fc ); ?></td>
								<td><a href="<?php echo esc_url( $football_results['fteam_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php if ( isset( $football_results['fteam_name'] ) ) { echo esc_attr( $football_results['fteam_name'] ); } ?></a></td>
								<td><?php if ( isset( $football_results['pl'] ) ) { echo esc_attr( $football_results['pl'] ); } ?></td>
								<td><?php if ( isset( $football_results['gd'] ) ) { echo esc_attr( $football_results['gd'] ); } ?></td>
								<td><?php if ( isset( $football_results['pts'] ) ) { echo esc_attr( $football_results['pts'] ); } ?></td>   
							</tr>                                      
						<?php endforeach;?>
					</tbody>
				</table><!-- /.football-results table -->
				<?php if ( '' != $atts['up_date'] ) : ?>
					<div class="table-footer"> 
						<span><?php esc_html_e( 'Last updated:', 'reendex' ); ?> <span class="date"><?php echo esc_attr( $atts['up_date'] ); ?></span></span> 
					</div>
				<?php endif; ?>
			</div><!-- /.table-responsive -->
		</div><!-- /.table -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_sidebar_football_results_shortcode', 'reendex_sidebar_football_results' );

// Reendex - Football Fixtures and Results.
// usages:[reendex_football_fixtures_shortcode].
if ( ! function_exists( 'reendex_football_fixtures' ) ) {
	/**
	 * Football Fixtures and Results Shortcode
	 *
	 * Displays Football Fixtures and Results.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Football Fixtures and Results.
	 */
	function reendex_football_fixtures( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                => '',
			'football_fixtures'    => '',
			'match_date'           => '',
			'another_date'         => '',
			'match_result_status'  => '',
			'match_start_time'     => '',
			'match_score'          => '',
			'team_label_one'       => '',
			'team_label_two'       => '',
			'team_name_one'        => '',
			'team_name_two'        => '',
			'team_status_one'      => '',
			'team_status_two'      => '',
			'target'               => '_self',
			'up_date'              => '',
			'extra_class'          => '',
		), $atts );
		ob_start();
		?>
		<div class="matches <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<h3 class="matches-header"><?php echo esc_attr( $atts['match_date'] ); ?></h3>
			<ul class="matches-list">
				<?php
					$atts['football_fixtures'] = vc_param_group_parse_atts( $atts['football_fixtures'] );
				foreach ( (array) $atts['football_fixtures'] as $tfixtures ) :
				?>
					<li class="match"> 
						<a class="match-item" href="<?php echo esc_url( $tfixtures['match_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"> 
							<span class="matches-col match-label">
								<?php
								if ( isset( $tfixtures['team_label_one'] ) ) {
									echo esc_attr( $tfixtures['team_label_one'] );
								} ?>
							</span> 
							<span class="matches-col team side-1">
								<?php
								if ( isset( $tfixtures['team_name_one'] ) ) {
									echo esc_attr( $tfixtures['team_name_one'] );
								} ?>
							</span> 
							<span class="matches-col status">
								<?php if ( 1 == $tfixtures['match_result_status'] ) : ?>
									<span class="score"> 
										<span class="score-side">
											<?php
											if ( isset( $tfixtures['team_status_one'] ) ) {
												echo esc_attr( $tfixtures['team_status_one'] );
											} ?>
										</span> 
										<span class="score-side">
											<?php
											if ( isset( $tfixtures['team_status_two'] ) ) {
												echo esc_attr( $tfixtures['team_status_two'] );
											} ?>
										</span>
									</span>
								<?php else : ?>
									<span class="time">
										<?php
										if ( isset( $tfixtures['match_start_time'] ) ) {
											echo esc_attr( $tfixtures['match_start_time'] );
										} ?>
									</span>
								<?php endif; ?>
							</span> 
							<span class="matches-col team side-2">
								<?php
								if ( isset( $tfixtures['team_name_two'] ) ) {
									echo esc_attr( $tfixtures['team_name_two'] );
								} ?>
							</span> 
							<span class="matches-col info">
								<?php
								if ( isset( $tfixtures['team_label_two'] ) ) {
									echo esc_attr( $tfixtures['team_label_two'] );
								} ?>
							</span>
						</a> 
					</li>
				<?php endforeach; ?>
			</ul><!-- /.matches-list -->
			<?php if ( '' != $atts['up_date'] ) : ?>
				<div class="matches-footer"> 
					<span><?php esc_html_e( 'Last updated:', 'reendex' ); ?>
						<span class="date"><?php echo esc_attr( $atts['up_date'] ); ?></span>
					</span> 
				</div>
			<?php endif; ?>			
		</div><!-- /.matches -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_football_fixtures_shortcode', 'reendex_football_fixtures' );

// Reendex - Match Schedule.
// usages:[reendex_match_schedule_shortcode].
if ( ! function_exists( 'reendex_match_schedule' ) ) {
	/**
	 * Match Schedule Shortcode
	 *
	 * Displays Match Schedule.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Match Schedule.
	 */
	function reendex_match_schedule( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'mstyle'                => 1,
			'title'                 => '',
			'match_schedule'        => '',
			'mdate'                 => '',
			'team_name_one'         => '',
			'team_name_two'         => '',
			'team_one_logo'         => '',
			'team_two_logo'         => '',
			'title_margin_bt'       => '',
			'country_name'          => '',
			'match_schedule_time'   => '',
			'match_link'            => '',
			'target'                => '_self',
			'up_date'               => '',
			'live_updates'          => 0,
			'live_updates_link'     => '',
			'updates_target'                => '_self',
			'live_updates_bg_color' => '#d4000e',
			'extra_class'           => '',
		), $atts );
		ob_start();
		?>
		<?php if ( 2 == $atts['mstyle'] ) : ?>
			<div class="panel">
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr class="bg-gray">
								<th>
									<?php
									if ( isset( $atts['country_name'] ) ) {
										echo esc_attr( $atts['country_name'] );
									} ?>
								</th>
								<th><?php esc_html_e( 'TV Times', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'On Air', 'reendex' ); ?></th>
								<th><?php esc_html_e( 'Start', 'reendex' ); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
								$st_2_schedule_time = vc_param_group_parse_atts( $atts['match_schedule_time'] );
							foreach ( $st_2_schedule_time as $stschedule_time ) :
							?>
								<tr>
									<td>
										<?php
										if ( isset( $stschedule_time['smatch_no'] ) ) {
											echo esc_attr( $stschedule_time['smatch_no'] );
										} ?>
									</td>
									<td>
										<?php
										if ( isset( $stschedule_time['tvtime'] ) ) {
											echo esc_attr( $stschedule_time['tvtime'] );
										} ?>
									</td>
									<td>
										<?php
										if ( isset( $stschedule_time['onair_time'] ) ) {
											echo esc_attr( $stschedule_time['onair_time'] );
										} ?>
									</td>
									<td>
										<?php
										if ( isset( $stschedule_time['start_time'] ) ) {
											echo esc_attr( $stschedule_time['start_time'] );
										} ?>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table><!-- /.matches -->
					<?php if ( '' != $atts['up_date'] ) : ?>
						<div class="table-footer"> 
							<span><?php esc_html_e( 'Last updated:', 'reendex' ); ?>
								<span class="date"><?php echo esc_attr( $atts['up_date'] ); ?></span>
							</span> 
							<?php if ( 1 == $atts['live_updates'] ) : ?> 
								<div class="live-updates"> 
									<span>
										<span class="live-title" style="
											<?php
											if ( '' != $atts['live_updates_bg_color'] ) {
												echo 'background-color:' . esc_attr( $atts['live_updates_bg_color'] ) . ';';
											} ?>">
											<?php esc_html_e( 'Live updates', 'reendex' ); ?>
										</span>
										<span class="date">
											<i class="fa fa-play"></i>
											<a href="<?php echo esc_url( $atts['live_updates_link'] ); ?>" target="<?php echo esc_attr( $atts['updates_target'] ); ?>"><?php echo esc_html( date( 'M jS, Y g:ia' ) ); ?></a>
										</span>
									</span>
								</div><!-- /.live-updates -->
							<?php endif; ?>								
						</div><!-- /.table-footer -->
					<?php endif; ?>
				</div><!-- /.table-responsive -->
			</div><!-- /.panel -->
		<?php else : ?>
			<?php if ( '' != $atts['title'] ) : ?>
				<div class="block-title-2" style="
					<?php
					if ( '' != $atts['title_margin_bt'] ) {
						echo 'margin-bottom:' . esc_attr( $atts['title_margin_bt'] ) . 'px;';
					} ?>">
					<h3 class="schedule-title"><?php echo esc_html( $atts['title'] ); ?></h3>
				</div>
			<?php endif; ?>
			<div class="sport-promo <?php echo esc_attr( $atts['extra_class'] ); ?>">
				<?php
					$atts['match_schedule'] = vc_param_group_parse_atts( $atts['match_schedule'] );
				foreach ( $atts['match_schedule'] as $team_schedule ) :
					$image_te_logo1 = wp_get_attachment_image_src( $team_schedule['team_one_logo'],'full' );
					$image_te_logo2 = wp_get_attachment_image_src( $team_schedule['team_two_logo'],'full' );
				?>
					<div class="sport-promo-block"> 
						<a href="<?php echo esc_url( $team_schedule['match_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
							<h4 class="time"><?php echo esc_attr( $team_schedule['mdate'] ); ?></h4>
							<h3 class="sport">
								<?php
								if ( '' != $image_te_logo1 ) {
									echo '<img src=" ' . esc_url( $image_te_logo1[0] ) . ' " alt="' . esc_attr( $team_schedule['team_name_one'] ) . '" class="sport-promo-badge">';
								}
								?>
								<span class="sport-promo-teams"><?php echo esc_attr( $team_schedule['team_name_one'] ); ?> 
									<span class="sport-promo-break"><?php esc_html_e( 'vs', 'reendex' ); ?></span>
									<?php echo esc_attr( $team_schedule['team_name_two'] ); ?>
								</span>
								<?php
								if ( '' != $image_te_logo1 ) {
									echo '<img src=" ' . esc_url( $image_te_logo2[0] ) . ' " alt="' . esc_attr( $team_schedule['team_name_two'] ) . '" class="sport-promo-badge">';
								}
								?>
							</h3>
						</a>
					</div><!-- /.sport-promo-block -->
				<?php endforeach;?>
				<?php if ( '' != $atts['up_date'] ) : ?>
					<div class="table-footer"> 
						<span><?php esc_html_e( 'Last updated:', 'reendex' ); ?>
							<span class="date"><?php echo esc_attr( $atts['up_date'] ); ?></span>
						</span> 
					</div>
				<?php endif; ?>				
				<?php if ( 1 == $atts['live_updates'] ) : ?> 
					<div class="live-updates"> 
						<span>
							<span class="live-title" style="
								<?php
								if ( '' != $atts['live_updates_bg_color'] ) {
									echo 'background-color:' . esc_attr( $atts['live_updates_bg_color'] ) . ';';
								} ?>">
								<?php esc_html_e( 'Live updates', 'reendex' ); ?>
							</span>
							<span class="date">
								<i class="fa fa-play"></i>
								<a href="<?php echo esc_url( $atts['live_updates_link'] ); ?>" target="<?php echo esc_attr( $atts['updates_target'] ); ?>"><?php echo esc_html( date( 'M jS, Y g:ia' ) ); ?></a>
							</span>
						</span>
					</div>
				<?php endif; ?>
			</div><!-- /.sport-promo -->
		<?php endif; ?>
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_match_schedule_shortcode', 'reendex_match_schedule' );

// Reendex - Tennis Tournaments.
// usages:[reendex_tennis_tournaments_shortcode].
if ( ! function_exists( 'reendex_tennis_tournaments' ) ) {
	/**
	 * Tennis Tournaments Shortcode
	 *
	 * Displays Tennis Tournaments.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Tennis Tournaments.
	 */
	function reendex_tennis_tournaments( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                                 => '',
			'title_style'                           => 1,
			'title_margin_bottom'                   => '',
			'tennis_competitions_fixtures'          => '',
			'tennis_competitions_fixtures_other'    => '',
			'match_title'                           => '',
			'match_title_two'                       => '',
			'target'                                => '_self',
			'extra_class'                           => '',
		), $atts );
		ob_start();
		?>
		<div class="<?php if ( '' != $atts['extra_class'] ) { echo esc_attr( $atts['extra_class'] ); } ?>">
			<?php if ( '' != $atts['title'] ) : ?>
				<div class="
					<?php
					if ( 2 == $atts['title_style'] ) {
						echo 'title-style02';
					} elseif ( 3 == $atts['title_style'] ) {
						echo 'block-title-2';
					} else {
						echo 'title-style02-light';
					} ?>"
					style="
						<?php
						if ( '' != $atts['title_margin_bottom'] ) {
							echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
						} ?>"> 
					<h3><?php if ( '' != $atts['title'] ) { echo esc_attr( $atts['title'] ); } ?></h3> 
				</div>
			<?php endif; ?>
			<ul class="category-list">
				<li class="category-list_item">
					<h3 class="category-list_header"><?php echo esc_attr( $atts['match_title'] ); ?></h3>
					<ul class="category-list_sub-links">
						<?php
							$tennis_competitions = vc_param_group_parse_atts( $atts['tennis_competitions_fixtures'] );
						foreach ( $tennis_competitions as $tfixtures ) : ?>
							<li>
								<a href="<?php echo esc_url( $tfixtures['competitions_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>" class="category-list_sub-link"><?php echo esc_attr( $tfixtures['competitions_team'] ); ?></a>
							</li>
						<?php endforeach; ?>
					</ul><!-- /.category-list_sub-links -->
				</li><!-- /.category-list_item -->
				<li class="category-list_item">
					<h3 class="category-list_header"><?php echo esc_attr( $atts['match_title_two'] ); ?></h3>
					<ul class="category-list_sub-links">
						<?php
							$tennis_competitions_ot = vc_param_group_parse_atts( $atts['tennis_competitions_fixtures_other'] );
						foreach ( $tennis_competitions_ot as $ofixtures ) : ?>
							<li>
								<a href="<?php echo esc_url( $ofixtures['ocompetitions_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>" class="category-list_sub-link"><?php echo esc_attr( $ofixtures['ocompetitions_team'] ); ?></a>
							</li>
						<?php endforeach; ?>
					</ul><!-- /.category-list_sub-links -->
				</li>
			</ul><!-- /.category-list -->
		</div>
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_tennis_tournaments_shortcode', 'reendex_tennis_tournaments' );

// Reendex - Tennis Results.
// usages:[reendex_tennis_results_shortcode].
if ( ! function_exists( 'reendex_tennis_results' ) ) {
	/**
	 * Tennis Results Shortcode
	 *
	 * Displays Tennis Results.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Tennis Results.
	 */
	function reendex_tennis_results( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'                         => '',
			'title_style'                   => 1,
			'title_margin_bottom'           => '',
			'match_status'                  => 1,
			'tennis_competitions_result'    => '',
		), $atts );
		ob_start();
		?>
		<?php if ( '' != $atts['title'] ) : ?>
			<div class="
				<?php
				if ( 2 == $atts['title_style'] ) {
					echo 'title-style02-light';
				} elseif ( 3 == $atts['title_style'] ) {
					echo 'block-title-2';
				} else {
					echo 'title-style02';
				} ?>"
				style="
					<?php
					if ( '' != $atts['title_margin_bottom'] ) {
						echo 'margin-bottom:' . esc_attr( $atts['title_margin_bottom'] ) . 'px;';
					} ?>"> 
				<h3><?php if ( '' != $atts['title'] ) { echo esc_attr( $atts['title'] ); } ?></h3> 
			</div>
		<?php endif; ?>		
		<div class="tennis-block">
			<div class="tennis-group">
				<?php
					$tennis_result = vc_param_group_parse_atts( $atts['tennis_competitions_result'] );
				foreach ( $tennis_result as $teresult ) : ?>
					<div class="tennis">
						<div data-winner="side1" class="tennis-players"> 
							<span class="tennis-player">
								<?php
								if ( '' != isset( $teresult['first_player'] ) && $teresult['first_player'] ) {
									echo esc_attr( $teresult['first_player'] );
								} ?>
							</span> 
							<span class="tennis-player">
								<?php
								if ( '' != isset( $teresult['second_player'] ) && $teresult['second_player'] ) {
									echo esc_attr( $teresult['second_player'] );
								} ?>
							</span> 
						</div>
						<?php if ( 1 == $teresult['match_status'] ) : ?>
							<div class="tennis-sets">
								<?php if ( isset( $teresult['first_player_set_one'] ) ) : ?>
									<span class="tennis-col" data-set="1"> 
										<span class="tennis-score">
											<?php
											if ( '' != isset( $teresult['first_player_set_one'] ) && $teresult['first_player_set_one'] ) {
												echo esc_attr( $teresult['first_player_set_one'] );
											} ?>
										</span> 
										<span class="tennis-score">
											<?php
											if ( '' != isset( $teresult['second_player_set_one'] ) && $teresult['second_player_set_one'] ) {
												echo esc_attr( $teresult['second_player_set_one'] );
											} ?>
										</span> 
									</span>
								<?php endif; ?>
								<?php if ( isset( $teresult['first_player_set_two'] ) ) : ?>
									<span class="tennis-col" data-set="2"> 
										<span class="tennis-score">
											<?php
											if ( '' != isset( $teresult['first_player_set_two'] ) && $teresult['first_player_set_two'] ) {
												echo esc_attr( $teresult['first_player_set_two'] );
											} ?>
										</span> 
										<span class="tennis-score">
											<?php
											if ( '' != isset( $teresult['second_player_set_two'] ) && $teresult['second_player_set_two'] ) {
												echo esc_attr( $teresult['second_player_set_two'] );
											} ?>
										</span> 
									</span>
								<?php endif; ?>
								<?php if ( isset( $teresult['first_player_set_three'] ) ) : ?>
									<span class="tennis-col" data-set="3"> 
										<span class="tennis-score">
											<?php
											if ( '' != isset( $teresult['first_player_set_three'] ) && $teresult['first_player_set_three'] ) {
												echo esc_attr( $teresult['first_player_set_three'] );
											} ?>
										</span> 
										<span class="tennis-score">
											<?php
											if ( '' != isset( $teresult['second_player_set_three'] ) && $teresult['second_player_set_three'] ) {
												echo esc_attr( $teresult['second_player_set_three'] );
											} ?>
										</span> 
									</span>
								<?php endif; ?>
								<?php if ( isset( $teresult['first_player_set_four'] ) ) : ?>
									<span class="tennis-col" data-set="4"> 
										<span class="tennis-score">
											<?php
											if ( '' != isset( $teresult['first_player_set_four'] ) && $teresult['first_player_set_four'] ) {
												echo esc_attr( $teresult['first_player_set_four'] );
											} ?>
										</span> 
										<span class="tennis-score">
											<?php
											if ( '' != isset( $teresult['second_player_set_four'] ) && $teresult['second_player_set_four'] ) {
												echo esc_attr( $teresult['second_player_set_four'] );
											} ?>
										</span> 
									</span>
								<?php endif; ?>
								<?php if ( isset( $teresult['first_player_set_five'] ) ) : ?>
									<span class="tennis-col" data-set="5"> 
										<span class="tennis-score">
											<?php
											if ( '' != isset( $teresult['first_player_set_five'] ) && $teresult['first_player_set_five'] ) {
												echo esc_attr( $teresult['first_player_set_five'] );
											} ?>
										</span> 
										<span class="tennis-score">
											<?php
											if ( '' != isset( $teresult['second_player_set_five'] ) && $teresult['second_player_set_five'] ) {
												echo esc_attr( $teresult['second_player_set_five'] );
											} ?>
										</span> 
									</span>
								<?php endif; ?>									
							</div><!-- /.tennis-sets -->
						<?php else : ?>
							<?php if ( isset( $teresult['match_time'] ) ) : ?>
								<div class="tennis-time">
									<span><?php echo esc_attr( $teresult['match_time'] ); ?></span>
								</div>
							<?php endif; ?>
						<?php endif; ?>
					</div><!-- /.tennis -->
				<?php endforeach;?>
			</div><!-- /.tennis-group -->
		</div><!-- /.tennis-block -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_tennis_results_shortcode', 'reendex_tennis_results' );

// Reendex - Sport Shop.
// usages:[reendex_sport_shop_shortcode].
if ( ! function_exists( 'reendex_sport_shop' ) ) {
	/**
	 * Sport Shop Shortcode
	 *
	 * Displays Sport Shop.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Sport Shop.
	 */
	function reendex_sport_shop( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'             => '',
			'shop_image1'       => '',
			'shop_image1_link'  => '',
			'shop_image1_des'   => '',
			'shop_image2'       => '',
			'shop_image2_link'  => '',
			'shop_image2_des'   => '',
			'shop_image3'       => '',
			'shop_image3_link'  => '',
			'shop_image3_des'   => '',
			'shop_image4'       => '',
			'shop_image4_link'  => '',
			'shop_image4_des'   => '',
			'target'            => '_blank',
		), $atts );
		ob_start();
		$image_1_url = wp_get_attachment_image_src( $atts['shop_image1'], 'full' );
		$image_2_url = wp_get_attachment_image_src( $atts['shop_image2'], 'full' );
		$image_3_url = wp_get_attachment_image_src( $atts['shop_image3'], 'full' );
		$image_4_url = wp_get_attachment_image_src( $atts['shop_image4'], 'full' );
		?>
		<div class="article-list">
			<?php if ( '' !== $atts['title'] ) : ?>
				<div class="title-style01">
					<h3 class="title-top"><?php echo esc_html( $atts['title'] ); ?></h3>
				</div>
			<?php endif; ?>
			<div class="shopping-item-block"> 
				<div class="shopping-image">
					<a class="img-link" href="
						<?php
						if ( '' != $atts['shop_image1_link'] ) {
							echo esc_url( $atts['shop_image1_link'] );
						} ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
						<?php
						if ( '' != $image_1_url ) {
							echo '<img src=" ' . esc_url( $image_1_url[0] ) . ' " alt="' . esc_attr( $atts['shop_image1_des'] ) . '" title="' . esc_attr( $atts['shop_image1_des'] ) . '">';
						} ?>
					</a>
				</div>
				<?php if ( '' != $atts['shop_image1_des'] ) : ?>
					<div class="shopping-title">
						<p><a href="<?php echo esc_url( $atts['shop_image1_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_attr( $atts['shop_image1_des'] ); ?></a></p>
					</div> 
				<?php endif; ?>
			</div><!-- /.shopping-item-block -->
			<div class="shopping-item-block">	
				<div class="shopping-image">
					<a class="img-link" href="
						<?php
						if ( '' != $atts['shop_image2_link'] ) {
							echo esc_url( $atts['shop_image2_link'] );
						} ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
						<?php
						if ( '' != $image_2_url ) {
							echo '<img src=" ' . esc_url( $image_2_url[0] ) . ' " alt="' . esc_attr( $atts['shop_image2_des'] ) . '" title="' . esc_attr( $atts['shop_image2_des'] ) . '">';
						} ?>
					</a>
				</div>
				<?php if ( '' != $atts['shop_image2_des'] ) : ?>
					<div class="shopping-title">
						<p><a href="<?php echo esc_url( $atts['shop_image2_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_attr( $atts['shop_image2_des'] ); ?></a></p>
					</div> 
				<?php endif; ?>					
			</div><!-- /.shopping-item-block -->
			<div class="shopping-item-block">	
				<div class="shopping-image">
					<a class="img-link" href="
						<?php
						if ( '' != $atts['shop_image3_link'] ) {
							echo esc_url( $atts['shop_image3_link'] );
						} ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
						<?php
						if ( '' != $image_3_url ) {
							echo '<img src=" ' . esc_url( $image_3_url[0] ) . ' " alt="' . esc_attr( $atts['shop_image3_des'] ) . '" title="' . esc_attr( $atts['shop_image3_des'] ) . '">';
						} ?>
					</a>
				</div>
				<?php if ( '' != $atts['shop_image3_des'] ) : ?>
					<div class="shopping-title">
						<p><a href="<?php echo esc_url( $atts['shop_image3_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_attr( $atts['shop_image3_des'] ); ?></a></p>
					</div> 
				<?php endif; ?>					
			</div><!-- /.shopping-item-block -->
			<div class="shopping-item-block">	
				<div class="shopping-image">
					<a class="img-link" href="
						<?php
						if ( '' != $atts['shop_image4_link'] ) {
							echo esc_url( $atts['shop_image4_link'] );
						} ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
						<?php
						if ( '' != $image_4_url ) {
							echo '<img src=" ' . esc_url( $image_4_url[0] ) . ' " alt="' . esc_attr( $atts['shop_image4_des'] ) . '" title="' . esc_attr( $atts['shop_image4_des'] ) . '">';
						} ?>
					</a>
				</div>
				<?php if ( '' != $atts['shop_image4_des'] ) : ?>
					<div class="shopping-title">
						<p><a href="<?php echo esc_url( $atts['shop_image4_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_attr( $atts['shop_image4_des'] ); ?></a></p>
					</div> 
				<?php endif; ?>					
			</div><!-- /.shopping-item-block -->				
		</div><!-- /.article-list -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_sport_shop_shortcode', 'reendex_sport_shop' );

// Reendex - Sport Tickets.
// usages:[reendex_sport_tickets_shortcode].
if ( ! function_exists( 'reendex_sport_tickets' ) ) {
	/**
	 * Sport Tickets Shortcode
	 *
	 * Displays Sport Tickets.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Sport Tickets.
	 */
	function reendex_sport_tickets( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'             => '',
			'ticket_des'        => '',
			'button_text'       => '',
			'button_link'       => '',
			'add_image'         => '',
			'add_ticket_link'   => '',
			'target'            => '_blank',
		), $atts );
		ob_start();
		$image_url = wp_get_attachment_image_src( $atts['add_image'], 'full' );
		$image_alt = get_post_meta( $atts['add_image'], '_wp_attachment_image_alt', true );
		$image_title = get_the_title( $atts['add_image'] );
		?>
		<div class="article-list">
			<?php if ( '' !== $atts['title'] ) : ?>
				<div class="title-style01">
					<h3 class="title-top"><?php echo esc_html( $atts['title'] ); ?></h3>
				</div>
			<?php endif; ?>
			<div class="tickets-content"> 
				<div class="tickets-image"> 
					<a class="img-link" href="
						<?php
						if ( '' != $atts['add_ticket_link'] ) {
							echo esc_url( $atts['add_ticket_link'] );
						} ?>" target="<?php echo esc_attr( $atts['target'] ); ?>">
						<?php
						if ( '' != $image_url ) {
							echo '<img src=" ' . esc_url( $image_url[0] ) . ' " alt=" ' . esc_attr( $image_alt ) . ' " title=" ' . esc_attr( $image_title ) . ' ">';
						} ?>
					</a>
				</div>
				<?php if ( '' != $atts['ticket_des'] ) : ?>
					<div class="ticket-content">
						<p><a href="<?php echo esc_url( $atts['add_ticket_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_attr( $atts['ticket_des'] ); ?></a></p>
					</div>
				<?php endif; ?>
				<a href="<?php echo esc_url( $atts['button_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>" class="tickets-button btn btn-default"><?php echo esc_attr( $atts['button_text'] ); ?></a>                                 
			</div><!-- /.tickets-content -->
		</div><!-- /.article-list -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_sport_tickets_shortcode', 'reendex_sport_tickets' );

// Reendex - Promo Module.
// usages:[reendex_promo_module_shortcode].
if ( ! function_exists( 'reendex_promo_module' ) ) {
	/**
	 * Promo Module Shortcode
	 *
	 * Displays Promo Module.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Promo Module.
	 */
	function reendex_promo_module( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'image'             => '',
			'header'            => '',
			'programme_name'    => '',
			'add_link'          => '',
			'link'              => 'link',
			'link_text'         => '',
			'button_link'       => '',
			'target'            => '_self',
			'button_text'       => '',
			'line_height'       => '24',
			'header_color'      => '#ced2d9',
			'header_f_size'     => '18',
			'programme_color'   => '#fff',
			'programme_f_size'  => '28',
			'description_color' => '#ced2d9',
			'desc_f_size'       => '14',
			'read_more_color'   => '#ddd',
			'color'             => '#212126',
			'extra_class'       => '',
		), $atts );
		$image = wp_get_attachment_image_src( $atts['image'], 'full', false, array(
			'class'    => 'img-responsive',
		) );
		$image_alt = get_post_meta( $atts['image'], '_wp_attachment_image_alt', true );
		$image_title = get_the_title( $atts['image'] );
		$link = vc_build_link( $atts['link'] );
		$addlink = '';
		$addlink .= '<a class="read-more" href="' . $link['url'] . '" title="' . esc_html( $link['title'] ) . '" target="' . $link['target'] . '">' . $atts['link_text'] . '</a>';
		ob_start();
		?>
		<div class="promo-item <?php echo esc_attr( $atts['extra_class'] ); ?>">
			<div class="promo-image">
				<img src="<?php echo esc_url( $image[0] ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>" title="<?php echo esc_attr( $image_title ); ?>">
			</div><!-- /.promo-image -->
			<div class="promo-wrapper">
				<span class="section-highlight-inner">
					<a class="section-highlight-link" href="<?php echo esc_url( $atts['button_link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php echo esc_html( $atts['button_text'] ); ?></a>
				</span>
				<div class="promo-content" style="background:<?php echo esc_attr( $atts['color'] ); ?>">
					<div class="programme-header" style="color:<?php echo esc_attr( $atts['header_color'] ); ?>; font-size:<?php echo esc_attr( $atts['header_f_size'] ) ?>px;"><?php echo esc_html( $atts['header'] ); ?></div>
					<div class="programme-name" style="color:<?php echo esc_attr( $atts['programme_color'] ); ?>; font-size:<?php echo esc_attr( $atts['programme_f_size'] ) ?>px;">
						<?php echo esc_html( $atts['programme_name'] ); ?>
					</div>
					<div class="content-wrapper">
						<p style="color:<?php echo esc_attr( $atts['description_color'] ); ?>; font-size:<?php echo esc_attr( $atts['desc_f_size'] ) ?>px; line-height:<?php echo esc_attr( $atts['line_height'] ) ?>px;">
							<?php echo esc_html( wp_strip_all_tags( $content ) ); ?>
						</p>
						<a class="read-more" href="<?php echo esc_url( $link['url'] ); ?>" style="color:<?php echo esc_attr( $atts['read_more_color'] ); ?>" target="<?php echo esc_html( $link['target'] ); ?>"><?php echo esc_html( $link['title'] ); ?></a>
					</div>
				</div><!-- /.promo-content-->
			</div><!-- /.promo-wrapper-->
		</div><!-- /.promo-item-->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_promo_module_shortcode', 'reendex_promo_module' );

// Reendex - Video Promo Module.
// usages:[reendex_video_promo_module_shortcode].
if ( ! function_exists( 'reendex_video_promo_module' ) ) {
	/**
	 * Video Promo Module Shortcode
	 *
	 * Displays Video Promo Module.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Video Promo Module.
	 */
	function reendex_video_promo_module( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'video_link'        => '',
			'header'            => '',
			'programme_name'    => '',
			'button_link'       => '',
			'target'            => '_self',
			'button_text'       => '',
			'line_height'       => '24',
			'header_color'      => '#ced2d9',
			'header_f_size'     => '24',
			'programme_color'   => '#fff',
			'programme_f_size'  => '28',
			'description_color' => '#ced2d9',
			'desc_f_size'       => '14',
			'color'             => '#212126',
			'extra_class'       => '',
			'video_source'      => '',
			'youtube_id'        => '',
			'youtube_autoplay'  => '1',
			'mute'              => '1',
			'youtube_loop'      => '1',
			'showinfo'          => '0',
			'clip_id'           => '',
			'autoplay'          => '1',
			'title'             => '1',
			'byline'            => '1',
			'automute'          => '1',
			'loop'              => '1',
			'width'             => '720',
			'height'            => '406',
		), $atts ) );
			$output  = '';
			$output  .= '<div class="video-promo-item ' . $extra_class . '">';
		if ( 1 == $video_source ) {
			$output .= '
			<div class="video-promo-image">
				<div class="video-full">
					<div class="fitvids-video"><iframe src="https://www.youtube.com/embed/' . $youtube_id . '?autoplay=' . $youtube_autoplay . '&amp;loop=' . $youtube_loop . '&amp;mute=' . $mute . '&amp;showinfo=' . $showinfo . '&amp;playlist=' . $youtube_id . '"></iframe></div>	
				</div>
			</div>';
		} elseif ( 2 == $video_source ) {
			$output .= '
			<div class="video-promo-image">
				<div class="video-full">
					<div class="fitvids-video embed-container">
						<iframe id="embeddedVideo" src="https://player.vimeo.com/video/' . $clip_id . '?api=1&player_id=vimeo_player&autoplay=' . $autoplay . '&loop=' . $loop . '" allow="autoplay; fullscreen"></iframe>
						<input type="hidden" id="video_mute" value="' . $automute . '">
					</div>	
				</div>
			</div>';
		}
			$output .= '
			<div class="video-promo-wrapper">
				<div class="video-promo-content" style="background:' . $color . ';">
					<span class="section-highlight-inner"><a href=" ' . esc_url( $button_link ) . '" target="' . esc_attr( $target ) . '">' . esc_html( $button_text ) . '</a></span>
					<div class="programme-header" style="color:' . $header_color . '; font-size:' . $header_f_size . 'px;">' . $header . '</div>
					<div class="programme-name" style="color:' . $programme_color . '; font-size:' . $programme_f_size . 'px;">' . $programme_name . '</div>
					<p style="color:' . $description_color . '; font-size:' . $desc_f_size . 'px; line-height:' . $line_height . 'px;">' . wp_strip_all_tags( $content ) . '</p>
				</div>
			</div>';
			$output .= '</div>';
		return $output;
	}
} // End if().
add_shortcode( 'reendex_video_promo_module_shortcode', 'reendex_video_promo_module' );

// Reendex - Currency Converter.
// usages:[reendex_currency_converter_shortcode].
if ( ! function_exists( 'reendex_currency_converter' ) ) {
	/**
	 * Currency Converter Shortcode
	 *
	 * Displays Currency Converter.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Currency Converter.
	 */
	function reendex_currency_converter( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'             => '',
			'button_text'       => esc_html__( 'convert', 'reendex' ),
			'amount_text'       => esc_html__( 'Amount:', 'reendex' ),
			'from_text'         => esc_html__( 'From:', 'reendex' ),
			'to_text'           => esc_html__( 'To:', 'reendex' ),
			'title_color'       => '',
			'title_bg_color'    => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		?>
		<div class= "<?php echo esc_attr( $atts['extra_class'] ); ?>">
			<?php if ( '' !== $atts['title'] ) : ?>                                 
				<div class="block-title-3" style="<?php if ( '' !== $atts['title_bg_color'] ) { echo 'background-color:' . esc_attr( $atts['title_bg_color'] ) . ';'; } ?>"> 
					<h3 class="currency-title" style="<?php if ( '' !== $atts['title_color'] ) { echo 'color:' . esc_attr( $atts['title_color'] ) . ';'; } ?>"><?php echo esc_html( $atts['title'] ); ?></h3> 
				</div>
			<?php endif; ?> 
			<div class="conversionForm">
				<div class="conversionForm-amount">
					<label for="fromAmount">
						<?php
						if ( isset( $atts['amount_text'] ) ) {
							echo esc_attr( $atts['amount_text'] );
						} ?>
					</label>
					<input id="fromAmount" type="text" class="form-control" value="1">
				</div>
				<div class="conversionForm__from col-sm-6">
					<label for="from">
						<?php
						if ( isset( $atts['from_text'] ) ) {
							echo esc_attr( $atts['from_text'] );
						} ?>
					</label>
					<select class="form-control currency" id="from">
						<option value="USD">US Dollar(USD)</option>
						<option value="EUR">Euro(EUR)</option>
					</select>  
				</div>
				<div class="conversionForm__to col-sm-6">
					<label for="to">
						<?php
						if ( isset( $atts['to_text'] ) ) {
							echo esc_attr( $atts['to_text'] );
						} ?>
					</label>
					<select class="form-control currency" id="to">
						<option value="EUR">Euro(Euro)</option>
						<option value="USD">US Dollar(USD)</option>
					</select>             
				</div>
				<div class="conversionForm-result">
					<input id="toAmount" type="text" class="form-control" aria-label="...">
				</div>
			</div>
		</div>
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_currency_converter_shortcode', 'reendex_currency_converter' );

// Reendex - Category List.
// usages:[reendex_category_list_shortcode].
if ( ! function_exists( 'reendex_category_list' ) ) {
	/**
	 * Category List Shortcode
	 *
	 * Displays Category List.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Category List.
	 */
	function reendex_category_list( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'title'             => '',
			'post_count_show'   => 1,
			'number'            => '',
			'extra_class'       => '',
		), $atts );
		ob_start();
		?>
		<div class="widget-category-area h-cat
			<?php
			if ( '' !== $atts['extra_class'] ) {
				echo esc_attr( $atts['extra_class'] );
			} ?>">
			<?php if ( '' !== $atts['title'] ) : ?>
				<div class="block-title-1"> 
					<h3 class="title-1"><?php echo esc_html( $atts['title'] ); ?></h3> 
				</div>
			<?php endif; ?>
			<ul class="list list-mark-1"> 
				<?php wp_list_categories( array(
					'orderby'       => 'name',
					'parent'        => 0,
					'show_count'    => $atts['post_count_show'],
					'title_li'      => '',
					'number'        => $atts['number'],
					'hide_empty'    => 0,
					'exclude'       => array( 4,7 ),
				) ); ?>
			</ul><!-- /.list list-mark-1 -->
		</div><!-- /.widget-category-area h-cat -->
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_category_list_shortcode', 'reendex_category_list' );

// Reendex - Breadcrumbs.
// usages:[reendex_breadcrumb_shortcode].
if ( ! function_exists( 'reendex_breadcrumb' ) ) {
	/**
	 * Breadcrumb Shortcode
	 *
	 * Displays Breadcrumb.
	 *
	 * @since 1.0
	 * @param array  $atts Shortcode attributes.
	 * @param string $content Content of Breadcrumb.
	 */
	function reendex_breadcrumb( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'breadcrumbs_margin'    => '',
			'extra_class'           => '',
		), $atts );
		ob_start();
		?>
		<div class="<?php echo esc_attr( $atts['extra_class'] ); ?>" style="
			<?php
			if ( '' != $atts['breadcrumbs_margin'] ) {
				echo 'margin: ' . esc_attr( $atts['breadcrumbs_margin'] ) . 'px;';
			} ?>">
			<?php echo esc_url( reendex_custom_breadcrumbs() ); ?>
		</div>
		<?php return ob_get_clean();
	}
} // End if().
add_shortcode( 'reendex_breadcrumb_shortcode', 'reendex_breadcrumb' );

/**
 * Video Embed Shortcode
 *
 * Displays Video Embed.
 *
 * @since 1.0
 * @param array  $atts Shortcode attributes.
 * @param string $content Content of Video Embed.
 */
function reendex_video_sc( $atts, $content = null ) {
	$atts = shortcode_atts( array(
		'site' => 'youtube',
		'id'   => '',
	), $atts );
	ob_start();
	$allowed_html = array(
		'iframe'    => array(
			'src'           => true,
			'width'         => true,
			'height'        => true,
			'marginwidth'   => true,
			'marginheight'  => true,
			'scrolling'     => true,
			'title'         => false,
		),
	);
	if ( 'youtube' == $atts['site'] ) {
		$src = wp_oembed_get( esc_url( 'http://www.youtube-nocookie.com/embed/' . $atts['id'] ) );
	} elseif ( 'vimeo' == $atts['site'] ) {
		$src = wp_oembed_get( esc_url( 'http://player.vimeo.com/video/' . $atts['id'] ) );
	} elseif ( 'dailymotion' == $atts['site'] ) {
		$src = wp_oembed_get( esc_url( 'http://www.dailymotion.com/embed/video/' . $atts['id'] ) );
	} elseif ( 'yahoo' == $atts['site'] ) {
		$src = wp_oembed_get( esc_url( 'http://d.yimg.com/nl/vyc/site/player.html#vid=' . $atts['id'] ) );
	} elseif ( 'bliptv' == $atts['site'] ) {
		$src = wp_oembed_get( esc_url( 'http://a.blip.tv/scripts/shoggplayer.html#file=http://blip.tv/rss/flash/' . $atts['id'] ) );
	} elseif ( 'veoh' == $atts['site'] ) {
		$src = wp_oembed_get( esc_url( 'http://www.veoh.com/static/swf/veoh/SPL.swf?videoAutoPlay=0&permalinkId=' . $atts['id'] ) );
	} elseif ( 'viddler' == $atts['site'] ) {
		$src = wp_oembed_get( esc_url( 'http://www.viddler.com/simple/' . $atts['id'] ) );
	}
	if ( '' != $atts['id'] ) {
		echo wp_kses( $src, $allowed_html );
	}
	return ob_get_clean();
}
add_shortcode( 'reendex_video', 'reendex_video_sc' );
