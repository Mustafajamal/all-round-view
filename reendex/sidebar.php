<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Reendex
 */

$reendex_sidebar = '';
$page_type = '';
///echo 'Side BAr';
if ( is_search() ) {
	$reendex_sidebar = get_theme_mod( 'reendex_search_page_layout', 'rightsidebar' );
	$page_type = 'search';
	//echo '$page_type';
	//echo $page_type;
} elseif ( is_category() ) {
	$reendex_sidebar = get_theme_mod( 'reendex_category_archive_layout', 'rightsidebar' );
} elseif ( is_tag() ) {
	$reendex_sidebar = get_theme_mod( 'reendex_tag_archive_layout', 'rightsidebar' );
} elseif ( is_archive() ) {
	$reendex_sidebar = get_theme_mod( 'reendex_archive_page_layout', 'rightsidebar' );
} elseif ( is_single() ) {
	$reendex_sidebar = get_theme_mod( 'reendex_single_post_layout', 'rightsidebar' );
} else {
	$reendex_sidebar = get_theme_mod( 'reendex_blog_page_layout','rightsidebar' );
}

if ( ! $reendex_sidebar ) {
	$reendex_sidebar = 'rightsidebar';
}
if ( 'nosidebar' == $reendex_sidebar ) {
	return;
}
//echo $reendex_sidebar;
//echo $page_type;

if ( 'rightsidebar' == $reendex_sidebar && is_active_sidebar( 'sidebar-1' ) ) { 
//echo $page_type;
?>
		<section id="secondaryleft" class="home-left-sidebar widget-area col-xs-12 col-sm-12 col-md-3" role="complementary">
			
			<?php 
			dynamic_sidebar( 'leftsidebar' ); ?>
		</section><!-- #secondaryleft -->
	<?php
}
if ( 'leftsidebar' == $reendex_sidebar && is_active_sidebar( 'leftsidebar' ) ) { 
//echo $page_type;
?>
		<section id="secondaryleft" class="home-left-sidebar widget-area col-xs-12 col-sm-12 col-md-3" role="complementary">
		
			<?php 
			if($page_type == 'search'){
			dynamic_sidebar( 'leftsidebarhome' ); 
			}else{
			?>
			<?php dynamic_sidebar( 'leftsidebar' ); 
			}
			?>
		</section><!-- #secondaryleft -->
	<?php
}

