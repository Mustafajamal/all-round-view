<?php
/**
 * Latest Events widget.
 *
 * @package Reendex
 */

	/**
	 * Register widget.
	 *
	 * Calls 'widgets_init' action after widget has been registered.
	 *
	 * @since 1.0.0
	 */
function register_latest_events_widget() {
	register_widget( 'reendex_latest_events_Widget' );
}
	add_action( 'widgets_init', 'register_latest_events_widget' );

	/**
	 * Core class used to implement the Latest Events widget.
	 *
	 * @since  1.0
	 *
	 * @see WP_Widget
	 */
class Reendex_Latest_Events_Widget extends WP_Widget {
	/**
	 * Constructor.
	 */
	function __construct() {
		$widget_ops = array(
			'classname'     => 'reendex-latest-events-widget',
			'description'   => esc_html__( 'All Round View: Displays Latest Events','reendex' ),
			);
		$control_ops = array(
			'id_base' => 'reendex-latest-events-widget',
			);
		parent::__construct( 'reendex-latest-events-widget', esc_html__( 'All Round View: Latest Events','reendex' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content for the current Latest Events widget instance.
	 *
	 * @param array $args     Display arguments including 'before_widget' and 'after_widget'.
	 * @param array $instance Settings for the current Latest Events widget instance.
	 */
	function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : 0;
		$number = isset( $instance['number'] ) ? $instance['number'] : 0;
		if ( isset( $args['before_widget'] ) ) {
			echo wp_kses( $args['before_widget'], 'li' );
		}
		?>
		<div class="latest-events <?php if ( '' != 'extclass' ) { echo esc_attr( $extclass ); } ?> widget container-wrapper"> 
			<?php
			if ( $title ) {
				echo '<h4 class="widget-title">' . esc_html( $title ) . '</h4>';
			}
			?>
			<?php
				$args = array(
					'post_type'         => 'post',
					'posts_per_page'    => $number,
					'has_password'      => false,
					'tax_query' => array(
						array(
							'taxonomy'  => 'post_format',
							'field'     => 'slug',
							'terms'     => 'post-format-video',
							'operator'  => 'NOT IN',
						),
					),
				);
			$ags = new WP_Query( $args );
			$permalink = get_permalink();
			while ( $ags->have_posts() ) : $ags->the_post();
			?>
			<div class="events">
				<div class="events-date">
					<a href="<?php echo esc_html( get_permalink() ); ?>">
						<?php
							$day   = 'd';
							$month = 'M';
						?>
						<span class="events-day"><?php the_time( $day ); ?></span>
						<span class="events-month"><?php the_time( $month ); ?></span>
					</a>
				</div><!-- /.events-date -->
				<div class="content">
					<p><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( wp_trim_words( get_the_content(), 20, '...' ) ); ?></a></p>
				</div><!-- /.content -->
			</div><!-- /.events -->
			<?php endwhile;
			wp_reset_postdata(); ?>
		</div>
	<?php
	if ( isset( $args['after_widget'] ) ) {
		echo wp_kses( $args['after_widget'], 'li' );
	}
	}

	/**
	 * Handles updating the settings for the current Latest Events widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = absint( $new_instance['number'] );
		$instance['extclass'] = sanitize_text_field( $new_instance['extclass'] );
		return $instance;
	}

	/**
	 * Outputs the settings form for the Latest Events widget.
	 *
	 * @param array $instance Current settings.
	 */
	function form( $instance ) {
		$defaults = array(
			'title'     => esc_html__( 'Latest Events', 'reendex' ),
			'number'    => 3,
			'extclass'  => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : '';
	?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title','reendex' ); ?>:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of Posts to show','reendex' ); ?>:</label>
			<input class="widefat" type="number" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" value="<?php echo esc_attr( $instance['number'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>"><?php esc_html_e( 'Widget area class','reendex' ); ?>:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extclass' ) ); ?>" value="<?php echo esc_attr( $instance['extclass'] ); ?>" />
		</p>	
	<?php
	}
}
?>
