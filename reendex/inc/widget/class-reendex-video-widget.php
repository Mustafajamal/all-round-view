<?php
/**
 * Video widget.
 *
 * @package Reendex
 */

	/**
	 * Register widget.
	 *
	 * Calls 'widgets_init' action after widget has been registered.
	 *
	 * @since 1.0.0
	 */
function reendex_video_widgets() {
	register_widget( 'Reendex_Video_Widget' );
}
	add_action( 'widgets_init', 'reendex_video_widgets' );

if ( ! class_exists( 'Reendex_Video_Widget' ) ) {

	/**
	 * Core class used to implement the Video widget.
	 *
	 * @since  1.0
	 */
	class Reendex_Video_Widget extends WP_Widget {

		/**
		 * Constructor.
		 */
		function __construct() {
			$widget_ops = array(
				'classname'     => 'reendex-video-widget',
				'description'   => esc_html__( 'All Round View: Video Widget','reendex'
				),
			);
			$control_ops = array(
				'id_base' => 'reendex-video-widget',
				);
			parent::__construct( 'reendex-video-widget', esc_html( 'All Round View: Video' ), $widget_ops, $control_ops );
		}

		/**
		 * Outputs the content for the current Mega menu elements widget instance.
		 *
		 * @param array $args     Display arguments including 'before_widget' and 'after_widget'.
		 * @param array $instance Settings for Mega Menu elements widget instance.
		 */
		function widget( $args, $instance ) {
			$title          = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
			$categories     = isset( $instance['categories'] ) ? (array) $instance['categories'] : array();
			$extclass       = isset( $instance['extclass'] ) ? $instance['extclass'] : 0;

			if ( isset( $before_widget ) ) {
				echo wp_kses( $before_widget, 'aside' );
			}

				$args = array(
					'post_type'				=> 'post',
					'ignore_sticky_posts'	=> 1,
					'post_status'			=> 'publish',
					'posts_per_page'		=> '1',
					'tax_query' => array(
						array(
							'taxonomy' => 'post_format',
							'field' => 'slug',
							'terms' => 'post-format-video',
						),
					),
				);
			if ( is_array( $categories ) && count( $categories ) > 0 ) {
				$args['category__in'] = $categories;
			}

			global $post;
			$posts = new WP_Query( $args );
			if ( $posts->have_posts() ) :
				$count = 0;
				$num_posts = $posts->post_count;
				?>
				<li class="reendex-widget <?php echo esc_attr( $extclass ); ?> widget container-wrapper">
					<div>
						<h4 class="widget-title"><?php echo esc_attr( $title );?></h4>
							<?php while ( $posts->have_posts() ) :
								$posts->the_post();
							?>
								<div class="video-container">
									<div class="fitvids-video">	        
										<iframe src="<?php echo esc_url( get_post_meta( get_the_id(), 'reendex_post_formate_vdo', true ) ); ?>"></iframe>
									</div><!-- /.fitvids-video -->
								</div><!-- /.video-container -->
							<?php endwhile; ?>
					</div>
				</li><!-- /.reendex-widget -->
			<?php endif;
			if ( isset( $after_widget ) ) {
				echo wp_kses( $after_widget, 'aside' );
			}
			wp_reset_postdata();
		}
		/**
		 * Handles updating the settings for the current Mega menu elements widget instance.
		 *
		 * @param array $new_instance New settings for this instance as input by the user via
		 *                            WP_Widget::form().
		 * @param array $old_instance Old settings for this instance.
		 * @return array Updated settings to save.
		 */
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['title'] 				= sanitize_text_field( $new_instance['title'] );
			$instance['categories'] 		= reendex_sanitize_multiple_checkbox( $new_instance['categories'] );
			$instance['extclass']         = sanitize_text_field( $new_instance['extclass'] );
			return $instance;
		}
		/**
		 * Outputs the settings form for the Mega menu elements widget.
		 *
		 * @param array $instance Current settings.
		 */
		function form( $instance ) {

			$defaults = array(
				'title' 			=> esc_html__( 'Video News', 'reendex' ),
				'categories'		=> array(),
				'extclass' 	=> '',
			);

			$instance = wp_parse_args( (array) $instance, $defaults );
			$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : '';
			$categories = $this->reendex_get_list_categories( 0 );
			if ( ! is_array( $instance['categories'] ) ) {
				$instance['categories'] = array();
			}

		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Enter your title', 'reendex' ); ?> </label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
			</p>
			<p>
				<label><?php esc_html_e( 'Select categories', 'reendex' ); ?></label>
				<div class="categorydiv">
					<div class="tabs-panel">
						<ul class="categorychecklist">
							<?php foreach ( $categories as $cat ) { ?>
							<li>
								<label>
									<input type="checkbox"<?php checked( in_array( $cat->term_id, $instance['categories'] ) ); ?> name="<?php echo esc_attr( $this->get_field_name( 'categories' ) ); ?>[<?php esc_attr( $cat->term_id ); ?>]" value="<?php echo esc_attr( $cat->term_id ); ?>" />
									<?php echo esc_html( $cat->name ); ?>
								</label>
								<?php $this->reendex_get_list_sub_categories( $cat->term_id, $instance ); ?>
							</li>
							<?php } ?>
						</ul><!-- /.categorychecklist -->
					</div><!-- /.tabs-panel -->
				</div><!-- /.categorydiv -->
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>"><?php esc_html_e( 'Widget area class','reendex' ); ?>:</label>
				<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extclass' ) ); ?>" value="<?php echo esc_attr( $instance['extclass'] ); ?>" />
			</p>			
			<?php
		}
		/**
		 * Returns a list of categories.
		 *
		 * @param string $cat_parent_id The parent term ID.
		 */
		function reendex_get_list_categories( $cat_parent_id ) {
			$args = array(
					'hierarchical'		=> 1,
					'parent'			=> $cat_parent_id,
					'title_li'			=> '',
					'child_of'			=> 0,
				);
			$cats = get_categories( $args );
			return $cats;
		}

		/**
		 * Returns a list of subcategories.
		 *
		 * @param string $cat_parent_id The parent term ID.
		 * @param array  $instance Settings for the current Categories widget instance.
		 */
		function reendex_get_list_sub_categories( $cat_parent_id, $instance ) {
			$sub_categories = $this->reendex_get_list_categories( $cat_parent_id );
			if ( count( $sub_categories ) > 0 ) {
			?>
				<ul class="children">
					<?php foreach ( $sub_categories as $sub_cat ) { ?>
						<li>
							<label>
								<input type="checkbox" name="<?php echo esc_attr( $this->get_field_name( 'categories' ) ); ?>[<?php esc_attr( $sub_cat->term_id ); ?>]" value="<?php echo esc_attr( $sub_cat->term_id ); ?>" <?php checked( in_array( $sub_cat->term_id, $instance['categories'] ) ); ?> />
								<?php echo esc_html( $sub_cat->name ); ?>
							</label>
							<?php $this->reendex_get_list_sub_categories( $sub_cat->term_id, $instance ); ?>
						</li>
					<?php } ?>
				</ul><!-- /.children -->
			<?php }
		}

	}
} // End if().
