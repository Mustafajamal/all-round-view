<?php
/**
 * Related videos by category and tags.
 *
 * @package Reendex
 */

$related_videos = reendex_related_videos_function();
$options = reendex_get_theme_options();
$show_related_video_title = get_theme_mod( 'reendex_related_video_title_show', 'enable' );
$related_video_title = $options['reendex_related_video_title'];
$related_video_subtitle = $options['reendex_related_video_subtitle'];
$related_video_title_bg_color = get_theme_mod( 'reendex_related_video_title_bg_color' );
$related_content_length = $options['reendex_related_content_length'];
?>

<?php if ( $related_videos->have_posts() ) : ?>
	<div class="module-wrapper">
		<?php if ( 'disable' !== $show_related_video_title ) : ?>	    
			<div class="module-title">
				<h4 class="title"><span class="bg-1" style="<?php if ( '' != $related_video_title_bg_color ) { echo 'background-color:' . esc_attr( $related_video_title_bg_color ) . ';'; }?>"><?php echo esc_attr( $related_video_title );?></span></h4>
				<h4 class="subtitle"><?php echo esc_attr( $related_video_subtitle );?></h4>
			</div><!-- /.module-title -->
		<?php endif;?>	
		<div class="media-wrapper"> 
			<?php
			while ( $related_videos->have_posts() ) : $related_videos->the_post(); ?>
			<div class="media-block">
				<div class="media-block-wrapper">
					<div class="item-media">	 
						<div class="related video-full">
							<div class="fitvids-video">	        
								<iframe src="
								<?php
									$related_video_content_length = $options['reendex_related_video_content_length'];
									$reendex_metabox_id = get_the_id();
									echo esc_url( get_post_meta( $reendex_metabox_id, 'reendex_video_link', true ) ); ?>">
								</iframe>
							</div>
						</div><!-- /.related video-full -->
						<div class="media-title">
							<h4><a href="<?php the_permalink(); ?>"><?php reendex_short_title( 20 ); ?>...</a></h4> 
						</div>
					</div><!-- /.item-media -->
				</div><!-- /.media-block-wrapper -->
				<div class="media-content">
					<p><a class="external-link" target="_blank" href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $related_video_content_length ) ); ?></a></p>
				</div>
			</div><!-- /.media-block --> 
			<?php endwhile; ?>
		</div>
	</div><!-- /.module-wrapper -->
<?php endif; ?>
<?php wp_reset_postdata(); ?>
