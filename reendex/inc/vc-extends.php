<?php
/**
 * Reendex Theme vc-extend
 *
 * @package Reendex
 */

if ( function_exists( 'vc_map' ) ) {
	$vc_taxonomies_types = get_taxonomies(
		array(
			'public' => true,
		),
		'objects'
	);
	$vc_taxonomies = get_terms( array_keys( $vc_taxonomies_types ), array(
		'hide_empty' => false,
	) );
	$taxonomies_list = array();
	if ( is_array( $vc_taxonomies ) && ! empty( $vc_taxonomies ) ) {
		foreach ( $vc_taxonomies as $t ) {
			if ( is_object( $t ) ) {
				$taxonomies_list[] = array(
					'label'     => $t->name . ' - [ id: ' . $t->term_id . ' ]',
					'value'     => $t->term_id,
					'group_id'  => $t->taxonomy,
					'group'     =>
						isset( $vc_taxonomies_types[ $t->taxonomy ], $vc_taxonomies_types[ $t->taxonomy ]->labels, $vc_taxonomies_types[ $t->taxonomy ]->labels->name )
						? $vc_taxonomies_types[ $t->taxonomy ]->labels->name
						: esc_html__( 'Taxonomies', 'reendex' ),
				);
			}
		}
	}
	// Ad Sizes.
	$get_ad_size = array(
		esc_html__( 'Auto', 'reendex' )   => 'auto',
		'hide'      => esc_html__( 'Hide', 'reendex' ),
		// Standard Ad Sizes.
		esc_html__( '120 x 240 - Vertical banner', 'reendex' )      => '120x240',
		esc_html__( '120 x 600 - Skyscraper', 'reendex' )           => '120x600',
		esc_html__( '125 x 125 - Square Button', 'reendex' )        => '125x125',
		esc_html__( '160 x 600 - Wide skyscraper', 'reendex' )      => '160x600',
		esc_html__( '180 x 150 - Small rectangle', 'reendex' )      => '180x150',
		esc_html__( '200 x 200 - Small square', 'reendex' )         => '200x200',
		esc_html__( '234 x 60 - Half banner', 'reendex' )           => '234x60',
		esc_html__( '250 x 250 - Square', 'reendex' )               => '250x250',
		esc_html__( '300 x 250 - Medium rectangle', 'reendex' )     => '300x250',
		esc_html__( '300 x 600 - Large Skyscraper', 'reendex' )     => '300x600',
		esc_html__( '320 x 50 - Mobile Leaderboard', 'reendex' )    => '320x50',
		esc_html__( '320 x 100 - Large mobile banner', 'reendex' )  => '320x100',
		esc_html__( '336 x 280 - Large rectangle', 'reendex' )      => '336x280',
		esc_html__( '468 x 60 - Banner', 'reendex' )                => '468x60',
		esc_html__( '728 x 90 - Leaderboard', 'reendex' )           => '728x90',
		esc_html__( '970 x 50 - Billboard', 'reendex' )             => '970x50',
		esc_html__( '970 x 90 - Large leaderboard', 'reendex' )     => '970x90',
		// Regional Ad Sizes.
		esc_html__( '240 x 400 - Vertical rectangle', 'reendex' )   => '240x400',
		esc_html__( '250 x 360 - Triple widescreen', 'reendex' )    => '250x360',
		esc_html__( '580 x 400 - Netboard', 'reendex' )             => '580x400',
		esc_html__( '750 x 100 - Billboard', 'reendex' )            => '750x100',
		esc_html__( '750 x 200 - Double billboard', 'reendex' )     => '750x200',
		esc_html__( '750 x 300 - Triple billboard', 'reendex' )     => '750x300',
		esc_html__( '930 x 180 - Top banner', 'reendex' )           => '930x180',
		esc_html__( '980 x 120 - Panorama', 'reendex' )             => '980x120',
		// Link Unit Ads.
		esc_html__( '120 x 90 - Vertical Small', 'reendex' )        => '120x90',
		esc_html__( '160 x 90 - Vertical Medium', 'reendex' )       => '160x90',
		esc_html__( '180 x 90 - Vertical Large', 'reendex' )        => '180x90',
		esc_html__( '200 x 90 - Vertical X-Large', 'reendex' )      => '200x90',
		esc_html__( '468 x 15 - Horizontal Medium', 'reendex' )     => '468x15',
		esc_html__( '728 x 15 - Horizontal Large', 'reendex' )      => '728x15',
	);

	// All Round View- Parallax.
	 vc_map(array(
		 'name'        => esc_html__( 'All Round View- Parallax', 'reendex' ),
		 'base'        => 'reendex_parallax_shortcode',
		 'icon'        => get_template_directory_uri() . '/img/icons/icon-parallax.png',
		 'category'    => esc_html__( 'Reendex', 'reendex' ),
		 'params'      => array(
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Parallax Image', 'reendex' ),
				'param_name'    => 'slider_image',
				'admin_label'   => true,
				'description'   => esc_html__( 'Select Your Parallax Image.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Parallax Title One', 'reendex' ),
				'param_name'    => 'slider_title_one',
				'admin_label'   => true,
				'value'         => esc_html__( 'Title One', 'reendex' ),
				'description'   => esc_html__( 'Add the first title here', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Parallax Title Two', 'reendex' ),
				'param_name'    => 'slider_title_two',
				'admin_label'   => true,
				'value'         => esc_html__( 'Title Two', 'reendex' ),
				'description'   => esc_html__( 'Add the second title here', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Parallax Title Three', 'reendex' ),
				'param_name'    => 'slider_title_three',
				'value'         => esc_html__( 'Title Three', 'reendex' ),
				'admin_label'   => true,
				'description'   => esc_html__( 'Add the third title here', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Parallax Title Four', 'reendex' ),
				'param_name'    => 'slider_title_four',
				'value'         => esc_html__( 'Title Four', 'reendex' ),
				'admin_label'   => true,
				'description'   => esc_html__( 'Add the fourth title here', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Parallax', 'reendex' ),
				'param_name'    => 'parallax',
				'admin_label'   => true,
				'value'         => array(
						'Yes'       => 1,
						'No'        => 0,
				),
				'description' => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Slider Height', 'reendex' ),
				'param_name'    => 'padding',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'parallax',
					'value'     => array( '0' ),
					),
				'description'   => '',
			),
		 ),
	 ));

	// All Round View- News Ticker.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Ticker', 'reendex' ),
		'base'      => 'reendex_newsticker_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-ticker.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Ticker Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
				'value'         => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Ticker Content Length', 'reendex' ),
				'param_name'    => 'ticker_title_length',
				'admin_label'   => true,
				'value'         => 5,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- RSS News Ticker.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- RSS News Ticker', 'reendex' ),
		'base'      => 'reendex_news_ticker_rss_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-rss-ticker.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Ticker Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
				'value'         => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'RSS Link', 'reendex' ),
				'param_name'    => 'urls',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Ticker Title Length', 'reendex' ),
				'param_name'    => 'strip_title',
				'admin_label'   => true,
				'value'         => 5,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Ticker Content Length', 'reendex' ),
				'param_name'    => 'strip_desc',
				'admin_label'   => true,
				'value'         => 5,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Content', 'reendex' ),
				'param_name'    => 'show_desc',
				'admin_label'   => true,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Date', 'reendex' ),
				'param_name'    => 'show_date',
				'admin_label'   => true,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Open links in new tab', 'reendex' ),
				'param_name'    => 'open_newtab',
				'admin_label'   => true,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Read More Text', 'reendex' ),
				'param_name'    => 'read_more',
				'admin_label'   => true,
				'value'         => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Ticker Speed', 'reendex' ),
				'param_name'    => 'ticker_speed',
				'admin_label'   => true,
				'value'         => 5,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number Of News', 'reendex' ),
				'param_name'    => 'count',
				'admin_label'   => true,
				'value'         => 5,
				'description'   => esc_html__( 'Add here the number of news to show in ticker', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- On-Air Ticker.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- On-Air Ticker', 'reendex' ),
		'base'      => 'reendex_on_air_ticker_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-on-air.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'On-Air Ticker Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
				'value'         => '',
				'description'   => esc_html__( 'Add the Ticker title here', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Ticker Text', 'reendex' ),
				'param_name'    => 'ticker_text',
				'admin_label'   => true,
				'description'   => esc_html__( 'Add the News Ticker Text here', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Live Link', 'reendex' ),
				'param_name'    => 'ticker_link',
				'admin_label'   => true,
				'description'   => esc_html__( 'Add the link here', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- News Slider.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Slider', 'reendex' ),
		'base'      => 'reendex_news_slider_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-slider.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the title length here', 'reendex' ),
				'value'         => '10',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Text to display on label', 'reendex' ),
				'param_name'    => 'category_show',
				'admin_label'   => false,
				'value'         => array(
					'Category'  => 0,
					'Tag'       => 1,
				),
				'description'   => esc_html__( 'Choose whether to display Category or Tag on label.', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- World Slider.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- World Slider', 'reendex' ),
		'base'      => 'reendex_world_slider_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-world-slider.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Category', 'reendex' ),
				'param_name'    => 'category_show',
				'admin_label'   => true,
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Date', 'reendex' ),
				'param_name'    => 'date_show',
				'admin_label'   => true,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- Posts Big Gallery Slider 1.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Posts Big Gallery Slider 1', 'reendex' ),
		'base'      => 'reendex_posts_big_gallery_slider_one_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-big-slider.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the total number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Number of visible items', 'reendex' ),
				'param_name'    => 'number_of_visible_item',
				'description'   => esc_html__( 'Set here the number of visible items', 'reendex' ),
				'value'         => array(
					'None'  => 'None',
					'Three' => 'big-gallery-slider-1',
					'Four'  => 'big-gallery-slider-2',
					'Five'  => 'big-gallery-slider-3',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Carousel Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Carousel Title Color', 'reendex' ),
				'param_name'    => 'title_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Carousel Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Category/Tag Label', 'reendex' ),
				'param_name'    => 'label_show',
				'admin_label'   => true,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Display Category or Tag', 'reendex' ),
				'param_name'    => 'category_show',
				'admin_label'   => false,
				'value'         => array(
					'Category'  => 0,
					'Tag'       => 1,
				),
				'description'   => esc_html__( 'Choose whether to display Category or Tag.', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- Posts Big Gallery Slider 2.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Posts Big Gallery Slider 2', 'reendex' ),
		'base'      => 'reendex_posts_big_gallery_slider_two_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-big-slider2.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Slider Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'News Slider Title Color', 'reendex' ),
				'param_name'    => 'title_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'News Slider Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- Posts Small Gallery Slider.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Posts Small Gallery Slider', 'reendex' ),
		'base'      => 'reendex_posts_small_gallery_slider_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-small-slider.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Carousel Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Carousel Title Color', 'reendex' ),
				'param_name'    => 'title_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Carousel Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Display Category or Tag', 'reendex' ),
				'param_name'    => 'category_show',
				'admin_label'   => false,
				'value'         => array(
					'Category'  => 0,
					'Tag'       => 1,
				),
				'description'   => esc_html__( 'Choose whether to display Category or Tag.', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- Video Gallery Slider.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Video Gallery Slider', 'reendex' ),
		'base'      => 'reendex_video_gallery_slider_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-video-slider.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'reendex_category',
				'heading'       => esc_html__( 'Categories', 'reendex' ),
				'param_name'    => 'categories',
				'admin_label'   => true,
				'value'         => '',
				'description'   => esc_html__( 'Filter categories by ID. Enter here the category IDs separated by commas (ex: 315,144,148).', 'reendex' ),
				'class'         => 'video_cat',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Carousel Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Carousel Title Text Color', 'reendex' ),
				'param_name'    => 'title_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Carousel Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order', 'reendex' ),
				'param_name'    => 'order',
				'admin_label'   => false,
				'value'         => array(
					'Descending'    => 'DESC',
					'Ascending'     => 'ASC',
				),
				'description'   => '',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'param_name'    => 'per_page',
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Number of visible items', 'reendex' ),
				'param_name'    => 'number_of_visible_items',
				'description'   => esc_html__( 'Set the number of items to display', 'reendex' ),
				'value'         => array(
					'None'  => 'None',
					'Three' => 'big-gallery-slider-1',
					'Four'  => 'big-gallery-slider-2',
					'Five'  => 'big-gallery-slider-3',
				),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Logo Showcase Slider.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Logo Showcase Slider', 'reendex' ),
		'base'      => 'reendex_logo_showcase_slider_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-logo-slider.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Logo Showcase Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'param_group',
				'param_name'    => 'partner_logo',
				'heading'       => esc_html__( 'Partners:', 'reendex' ),
				// Note params is mapped inside param-group.
				'params'        => array(
					array(
						'type'          => 'attach_image',
						'holder'        => 'div',
						'heading'       => esc_html__( 'Partner Logo', 'reendex' ),
						'param_name'    => 'partner_logo',
						'description'   => esc_html__( 'Add your partner logo here', 'reendex' ),
					),
					array(
						'type'          => 'textfield',
						'value'         => '#',
						'heading'       => esc_html__( 'Logo Link', 'reendex' ),
						'param_name'    => 'logo_link',
					),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Logo link', 'reendex' ),
				'value'         => array(
					   'Open the link in the same browser window'  => '_self',
						'Open the link in a new browser window' => '_blank',
				   ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- RSS Feed.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- RSS Feed', 'reendex' ),
		'base'      => 'reendex_rss_feed_tabs_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-rss-feed.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Module Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => true,
				'value'         => array(
						'Style One'     => 1,
						'Style Two'     => 2,
						'Style Three'   => 3,
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'RSS URL', 'reendex' ),
				'param_name'    => 'urls',
				'admin_label'   => true,
				'description'   => esc_html__( 'Example: http://feeds.reuters.com/reuters/businessNews', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Feed Title Length', 'reendex' ),
				'param_name'    => 'strip_title',
				'admin_label'   => true,
				'value'         => 5,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Feed Content Length', 'reendex' ),
				'param_name'    => 'strip_desc',
				'admin_label'   => true,
				'value'         => 5,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Content', 'reendex' ),
				'param_name'    => 'show_desc',
				'admin_label'   => true,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Date', 'reendex' ),
				'param_name'    => 'show_date',
				'admin_label'   => true,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Open links in new tab', 'reendex' ),
				'param_name'    => 'open_newtab',
				'admin_label'   => true,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Read More Text', 'reendex' ),
				'param_name'    => 'read_more',
				'admin_label'   => true,
				'value'         => '',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Enable Feed Animation', 'reendex' ),
				'param_name'    => 'enable_ticker',
				'admin_label'   => true,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Feed Speed', 'reendex' ),
				'param_name'    => 'ticker_speed',
				'admin_label'   => true,
				'value'         => 5,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Total number of items to display', 'reendex' ),
				'param_name'    => 'count',
				'admin_label'   => true,
				'value'         => 5,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Visible items (Feed height)', 'reendex' ),
				'param_name'    => 'visible_items',
				'admin_label'   => true,
				'value'         => 5,
				'description'   => esc_html__( 'Set value less than 20 to show visible feed items (Example: 5 items) / Set value greater than 20 for fixed feed height( Example: 400px)', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- News Feed.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Feed', 'reendex' ),
		'base'      => 'reendex_news_feed_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-feed.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Number of visible items', 'reendex' ),
				'description'   => esc_html__( 'Set the number of items to display when the scroll is ON', 'reendex' ),
				'param_name'    => 'number_of_visible',
				'admin_label'   => false,
				'value'         => array(
					'Four(4)'   => 'newsfeed-2',
					'Five(5)'   => 'newsfeed-3',
					'Six(6)'    => 'newsfeed-4',
					'Seven(7)'  => 'newsfeed-5',
					'Eight(8)'  => 'newsfeed-6',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Feed Title Style', 'reendex' ),
				'param_name'    => 'feed_title_style',
				'admin_label'   => true,
				'value'         => array(
						'Style One'     => 1,
						'Style Two'     => 2,
						'Style Three'   => 3,
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Feed Title', 'reendex' ),
				'param_name'    => 'feed_title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Feed Title Margin Bottom', 'reendex' ),
				'param_name'    => 'margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Date', 'reendex' ),
				'param_name'    => 'date_show',
				'admin_label'   => true,
				'value'         => array(
						'No'    => '0',
						'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Scroll', 'reendex' ),
				'param_name'    => 'scroll_off',
				'admin_label'   => false,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
				'description'   => '',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show News Title', 'reendex' ),
				'param_name'    => 'title_show',
				'admin_label'   => false,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
				'description' => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Title Length', 'reendex' ),
				'param_name'    => 'feed_title_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Read More Text', 'reendex' ),
				'param_name'    => 'read_more_text',
				'admin_label'   => true,
				'value'         => 'Read more...',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- News Feed Light.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Feed Light', 'reendex' ),
		'base'      => 'reendex_news_feed_light_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-feed-light.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Module Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => true,
				'value'         => array(
						'Style One'     => 1,
						'Style Two'     => 2,
						'Style Three'   => 3,
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Headlines Title', 'reendex' ),
				'param_name'    => 'title_show',
				'admin_label'   => false,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Headlines Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Headlines Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Date', 'reendex' ),
				'param_name'    => 'date_show',
				'admin_label'   => true,
				'value'         => array(
						'No'    => '0',
						'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Text Numbering', 'reendex' ),
				'param_name'    => 'text_numbering',
				'admin_label'   => false,
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- Small Sidebar Ad Banner.
	vc_map(array(
		'name'     => esc_html__( 'All Round View- Small Sidebar Ad Banner', 'reendex' ),
		'base'      => 'reendex_small_sidebar_ad_banner_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-small-ad.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Ad Type', 'reendex' ),
				'param_name'    => 'ad_type',
				'description'   => esc_html__( 'Select the type of Ad you want to use.', 'reendex' ),
				'value'         => array(
					esc_html__( 'Image Ad', 'reendex' )     => 'image',
					esc_html__( 'Script Code', 'reendex' )   => 'code',
					esc_html__( 'Google Ad', 'reendex' )    => 'googleads',
				),
				'admin_label'   => true,
				'std'           => 'image',
			),
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Ad Image', 'reendex' ),
				'param_name'    => 'ad_image',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Image Alternate Text', 'reendex' ),
				'param_name'    => 'ad_image_alt',
				'description'   => esc_html__( 'Insert alternate text of your Ad image.', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Ad Image Link', 'reendex' ),
				'param_name'    => 'ad_image_link',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Ad Image link', 'reendex' ),
				'value'         => array(
					'Open the link in the same browser window'  => '_self',
					'Open the link in a new browser window' => '_blank',
				),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'textarea',
				'heading'       => esc_html__( 'Ad Code', 'reendex' ),
				'param_name'    => 'content',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'code' ),
				),
			),
			array(
				'type'          => 'textfield',
				'param_name'    => 'google_publisher_id',
				'heading'       => esc_html__( 'Google Ad Publisher ID', 'reendex' ),
				'description'   => esc_html__( 'Insert data-ad-client / google_ad_client content.', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'textfield',
				'param_name'    => 'google_slot_id',
				'heading'       => esc_html__( 'Google Ad Slot ID', 'reendex' ),
				'description'   => esc_html__( 'Insert data-ad-slot / google_ad_slot content.', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Google Ads Desktop Ad Size', 'reendex' ),
				'param_name'    => 'google_desktop',
				'description'   => esc_html__( 'Choose the desktop ad size.', 'reendex' ),
				'value'         => $get_ad_size,
				'std'           => '',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Google Ads Tablet Ad Size', 'reendex' ),
				'param_name'    => 'google_tablet',
				'description'   => esc_html__( 'Choose the tablet ad size.', 'reendex' ),
				'value'         => $get_ad_size,
				'std'           => '',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Google Ads Phone Ad Size', 'reendex' ),
				'param_name'    => 'google_phone',
				'description'   => esc_html__( 'Choose the phone ad size.', 'reendex' ),
				'value'         => $get_ad_size,
				'std'           => '',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Ad Title', 'reendex' ),
				'param_name'    => 'ad_title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Title Color', 'reendex' ),
				'param_name'    => 'title_text_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Ad Bottom Text', 'reendex' ),
				'param_name'    => 'ad_bottom_text',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- Big Sidebar Ad Banner.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Big Sidebar Ad Banner', 'reendex' ),
		'base'      => 'reendex_big_sidebar_ad_banner_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-big-ad.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Ad Type', 'reendex' ),
				'param_name'    => 'ad_type',
				'description'   => esc_html__( 'Select the type of Ad you want to use.', 'reendex' ),
				'value'         => array(
					esc_html__( 'Image Ad', 'reendex' )     => 'image',
					esc_html__( 'Script Code', 'reendex' )   => 'code',
					esc_html__( 'Google Ad', 'reendex' )    => 'googleads',
				),
				'admin_label'   => true,
				'std'           => 'image',
			),
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Ad Image', 'reendex' ),
				'param_name'    => 'ad_image',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Image Alternate Text', 'reendex' ),
				'param_name'    => 'ad_image_alt',
				'description'   => esc_html__( 'Insert alternate text of your Ad image.', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Ad Image Link', 'reendex' ),
				'param_name'    => 'ad_image_link',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Ad Image link', 'reendex' ),
				'value'         => array(
					'Open the link in the same browser window'  => '_self',
					'Open the link in a new browser window' => '_blank',
				),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'textarea',
				'heading'       => esc_html__( 'Ad Code', 'reendex' ),
				'param_name'    => 'content',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'code' ),
				),
			),
			array(
				'type'          => 'textfield',
				'param_name'    => 'google_publisher_id',
				'heading'       => esc_html__( 'Google Ad Publisher ID', 'reendex' ),
				'description'   => esc_html__( 'Insert data-ad-client / google_ad_client content.', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'textfield',
				'param_name'    => 'google_slot_id',
				'heading'       => esc_html__( 'Google Ad Slot ID', 'reendex' ),
				'description'   => esc_html__( 'Insert data-ad-slot / google_ad_slot content.', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Google Ads Desktop Ad Size', 'reendex' ),
				'param_name'    => 'google_desktop',
				'description'   => esc_html__( 'Choose the desktop ad size.', 'reendex' ),
				'value'         => $get_ad_size,
				'std'           => '',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Google Ads Tablet Ad Size', 'reendex' ),
				'param_name'    => 'google_tablet',
				'description'   => esc_html__( 'Choose the tablet ad size.', 'reendex' ),
				'value'         => $get_ad_size,
				'std'           => '',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Google Ads Phone Ad Size', 'reendex' ),
				'param_name'    => 'google_phone',
				'description'   => esc_html__( 'Choose the phone ad size.', 'reendex' ),
				'value'         => $get_ad_size,
				'std'           => '',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Ad Title', 'reendex' ),
				'param_name'    => 'ad_title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Title Color', 'reendex' ),
				'param_name'    => 'title_text_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Ad Bottom Text', 'reendex' ),
				'param_name'    => 'ad_bottom_text',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- Center Ad Banner.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Center Ad Banner', 'reendex' ),
		'base'      => 'reendex_center_ad_banner_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-center-ad.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Ad Type', 'reendex' ),
				'param_name'    => 'ad_type',
				'description'   => esc_html__( 'Select the type of Ad you want to use.', 'reendex' ),
				'value'         => array(
					esc_html__( 'Image Ad', 'reendex' )     => 'image',
					esc_html__( 'Script Code', 'reendex' )   => 'code',
					esc_html__( 'Google Ad', 'reendex' )    => 'googleads',
				),
				'admin_label'   => true,
				'std'           => 'image',
			),
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Ad Image', 'reendex' ),
				'param_name'    => 'ad_image',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Image Alternate Text', 'reendex' ),
				'param_name'    => 'ad_image_alt',
				'description'   => esc_html__( 'Insert alternate text of your Ad image.', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Ad Image Link', 'reendex' ),
				'param_name'    => 'ad_image_link',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Ad Image link', 'reendex' ),
				'value'         => array(
					'Open the link in the same browser window'  => '_self',
					'Open the link in a new browser window' => '_blank',
				),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'image' ),
				),
			),
			array(
				'type'          => 'textarea',
				'heading'       => esc_html__( 'Ad Code', 'reendex' ),
				'param_name'    => 'content',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'code' ),
				),
			),
			array(
				'type'          => 'textfield',
				'param_name'    => 'google_publisher_id',
				'heading'       => esc_html__( 'Google Ad Publisher ID', 'reendex' ),
				'description'   => esc_html__( 'Insert data-ad-client / google_ad_client content.', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'textfield',
				'param_name'    => 'google_slot_id',
				'heading'       => esc_html__( 'Google Ad Slot ID', 'reendex' ),
				'description'   => esc_html__( 'Insert data-ad-slot / google_ad_slot content.', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Google Ads Desktop Ad Size', 'reendex' ),
				'param_name'    => 'google_desktop',
				'description'   => esc_html__( 'Choose the desktop ad size.', 'reendex' ),
				'value'         => $get_ad_size,
				'std'           => '',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Google Ads Tablet Ad Size', 'reendex' ),
				'param_name'    => 'google_tablet',
				'description'   => esc_html__( 'Choose the tablet ad size.', 'reendex' ),
				'value'         => $get_ad_size,
				'std'           => '',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Google Ads Phone Ad Size', 'reendex' ),
				'param_name'    => 'google_phone',
				'description'   => esc_html__( 'Choose the phone ad size.', 'reendex' ),
				'value'         => $get_ad_size,
				'std'           => '',
				'dependency' 	=> array(
					'element'   => 'ad_type',
					'value'     => array( 'googleads' ),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Ad Title', 'reendex' ),
				'param_name'    => 'ad_title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Title Color', 'reendex' ),
				'param_name'    => 'title_text_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Ad Bottom Text', 'reendex' ),
				'param_name'    => 'ad_bottom_text',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- Section Title.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Section Title', 'reendex' ),
		'base'      => 'reendex_section_title_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-section-title.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => false,
				'value'         => array(
					'Style One'     => 1,
					'Style Two'     => 2,
					'Style Three'   => 3,
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Section Title', 'reendex' ),
				'param_name'    => 'section_title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Section Subtitle', 'reendex' ),
				'param_name'    => 'section_sub_title',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'title_style',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Section Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'title_style',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- Module Title 1.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Module Title 1', 'reendex' ),
		'base'      => 'reendex_module_title_one_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-module-title.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'description'   => esc_html__( 'Add the title here', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Highlighted Title', 'reendex' ),
				'param_name'    => 'highlighted_title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Subtitle', 'reendex' ),
				'param_name'    => 'subtitle',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Link', 'reendex' ),
				'param_name'    => 'title_link',
				'description'   => esc_html__( 'Add the title link here', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Title link', 'reendex' ),
				'value'         => array(
					'Open the link in the same browser window'  => '_self',
					'Open the link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Module Title 2.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Module Title 2', 'reendex' ),
		'base'      => 'reendex_module_title_two_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-module-title2.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'description'   => esc_html__( 'Add the title here', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Subtitle', 'reendex' ),
				'param_name'    => 'subtitle',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Module Title 3.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Module Title 3', 'reendex' ),
		'base'      => 'reendex_module_title_three_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-module-title3.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'description'   => esc_html__( 'Add the title here', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Subtitle', 'reendex' ),
				'param_name'    => 'subtitle',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Section Highlights.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Section Highlights', 'reendex' ),
		'base'      => 'reendex_section_highlights_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-section-highlights.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Section Highlights', 'reendex' ),
				'param_name'    => 'section_highlights',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Text', 'reendex' ),
				'param_name'    => 'button_text',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Link', 'reendex' ),
				'param_name'    => 'button_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Button link', 'reendex' ),
				'value'         => array(
					'Open the link in the same browser window'  => '_self',
					'Open the link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- News Module 1.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Module 1', 'reendex' ),
		'base'      => 'reendex_news_module_one_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-module-1.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => false,
				'value'         => array(
					'Style One'     => 1,
					'Style Two'     => 2,
					'Style Three'   => 3,
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title', 'reendex' ),
				'description'   => esc_html__( 'Add the title here', 'reendex' ),
				'param_name'    => 'section_title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Subtitle', 'reendex' ),
				'description'   => esc_html__( 'Add the subtitle here', 'reendex' ),
				'param_name'    => 'section_sub_title',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'title_style',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Module Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the title background color here', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'title_style',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Display the content / the excerpt', 'reendex' ),
				'param_name'    => 'content_show',
				'admin_label'   => true,
				'value'         => array(
					'Display the content'   => '1',
					'Display the excerpt'   => '0',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the news title length here', 'reendex' ),
				'value'         => '5',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the content length here (Limit the number of characters to show on your post content.)', 'reendex' ),
				'value'         => '15',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Rows', 'reendex' ),
				'description'   => esc_html__( 'The number of rows', 'reendex' ),
				'param_name'    => 'rows',
				'admin_label'   => true,
				'value'         => '2',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Column', 'reendex' ),
				'description'   => esc_html__( 'The number of columns', 'reendex' ),
				'param_name'    => 'column',
				'admin_label'   => true,
				'value'         => '2',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Category/Tag Label', 'reendex' ),
				'param_name'    => 'label_show',
				'admin_label'   => true,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Text to display on label', 'reendex' ),
				'param_name'    => 'category_show',
				'admin_label'   => false,
				'value'         => array(
					'Category'  => 0,
					'Tag'       => 1,
				),
				'description'   => esc_html__( 'Choose whether to display Category or Tag on label.', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Date', 'reendex' ),
				'param_name'    => 'date_show',
				'admin_label'   => true,
				'value'         => array(
						'No'    => '0',
						'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Author Meta', 'reendex' ),
				'param_name'    => 'author_meta',
				'admin_label'   => true,
				'value'         => array(
						'No'    => '0',
						'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Read More Text', 'reendex' ),
				'param_name'    => 'readmore_show',
				'admin_label'   => true,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Read More Text', 'reendex' ),
				'param_name'    => 'read_more',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'readmore_show',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Item Image Size', 'reendex' ),
				'description'   => esc_html__( 'Set the item image size', 'reendex' ),
				'param_name'    => 'item_image',
				'admin_label'   => true,
				'value'         => array(
						'Image Size One'    => 'item-image-1',
						'Image Size Two'    => 'item-image-2',
						'Image Size Three'  => 'item-image-3',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- News Module 2.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Module 2', 'reendex' ),
		'base'      => 'reendex_news_module_two_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-module-2.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Display the content / the excerpt', 'reendex' ),
				'param_name'    => 'content_show',
				'admin_label'   => true,
				'value'         => array(
					'Display the content'   => '1',
					'Display the excerpt'   => '0',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Content Length', 'reendex' ),
				'description'   => esc_html__( 'Set the content length here (Limit the number of characters to show on your post content.)', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Text to display on label', 'reendex' ),
				'param_name'    => 'category_show',
				'admin_label'   => false,
				'value'         => array(
					'Category'  => 0,
					'Tag'       => 1,
				),
				'description'   => esc_html__( 'Choose whether to display Category or Tag on label.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'description'   => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- News Module 3.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Module 3', 'reendex' ),
		'base'      => 'reendex_news_module_three_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-module-3.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show the title', 'reendex' ),
				'param_name'    => 'title_show',
				'admin_label'   => false,
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the title length here', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'title_show',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Display the content / the excerpt', 'reendex' ),
				'param_name'    => 'content_show',
				'admin_label'   => true,
				'value'         => array(
					'Display the content'   => '1',
					'Display the excerpt'   => '0',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the content length here (Limit the number of characters to show on your post content.)', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show the date', 'reendex' ),
				'param_name'    => 'date_show',
				'admin_label'   => true,
				'value'         => array(
						'No'    => '0',
						'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- News Module 4.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Module 4', 'reendex' ),
		'base'      => 'reendex_news_module_four_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-module-4.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show the title', 'reendex' ),
				'param_name'    => 'title_show',
				'admin_label'   => false,
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
				'description'   => '',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => true,
				'value'         => array(
					'Style One'     => '1',
					'Style Two'     => '2',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the title length here', 'reendex' ),
				'dependency' 	=> array(
					'element'   => 'title_show',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Display the content / the excerpt', 'reendex' ),
				'param_name'    => 'content_show',
				'admin_label'   => true,
				'value'         => array(
					'Display the content'   => '1',
					'Display the excerpt'   => '0',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the content length here (Limit the number of characters to show on your post content.)', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show the date', 'reendex' ),
				'param_name'    => 'date_show',
				'admin_label'   => true,
				'value'         => array(
						'No'    => '0',
						'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Category/Tag Label', 'reendex' ),
				'param_name'    => 'label_show',
				'admin_label'   => true,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Text to display on label', 'reendex' ),
				'param_name'    => 'category_show',
				'admin_label'   => false,
				'value'         => array(
					'Category'  => 0,
					'Tag'       => 1,
				),
				'description'   => esc_html__( 'Choose whether to display Category or Tag on label.', 'reendex' ),
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Category/Tag Label Background Color', 'reendex' ),
				'param_name'    => 'label_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- News Module 5.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Module 5', 'reendex' ),
		'base'      => 'reendex_news_module_five_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-module-5.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Module Title Style', 'reendex' ),
				'param_name'    => 'module_title_style',
				'admin_label'   => false,
				'value'         => array(
					'Style One'     => 1,
					'Style Two'     => 2,
					'Style Three'   => 3,
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Section Title', 'reendex' ),
				'description'   => esc_html__( 'Add the section title here', 'reendex' ),
				'param_name'    => 'section_title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Section Subtitle', 'reendex' ),
				'description'   => esc_html__( 'Add the section subtitle here', 'reendex' ),
				'param_name'    => 'section_sub_title',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'module_title_style',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Section Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
				'description'   => esc_html__( 'Section Title Background Color', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Display the content / the excerpt', 'reendex' ),
				'param_name'    => 'content_show',
				'admin_label'   => true,
				'value'         => array(
					'Display the content'   => '1',
					'Display the excerpt'   => '0',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the title length here', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the content length here (Limit the number of characters to show on your post content.)', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show News Title', 'reendex' ),
				'param_name'    => 'title_show',
				'admin_label'   => true,
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Category/Tag Label', 'reendex' ),
				'param_name'    => 'label_show',
				'admin_label'   => true,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Text to display on label', 'reendex' ),
				'param_name'    => 'category_show',
				'admin_label'   => false,
				'value'         => array(
					'Category'  => 0,
					'Tag'       => 1,
				),
				'description'   => esc_html__( 'Choose whether to display Category or Tag on label.', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show the Date', 'reendex' ),
				'param_name'    => 'date_show',
				'admin_label'   => true,
				'value'     => array(
					'Yes'   => '1',
					'No'    => '0',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Read More text', 'reendex' ),
				'param_name'    => 'readmore_show',
				'admin_label'   => true,
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Read More Text', 'reendex' ),
				'param_name'    => 'read_more',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'readmore_show',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- News Module 6.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Module 6', 'reendex' ),
		'base'      => 'reendex_news_module_six_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-module-6.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Title', 'reendex' ),
				'param_name'    => 'title_show',
				'admin_label'   => true,
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => true,
				'value'         => array(
					'Style One'     => '1',
					'Style Two'     => '2',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Display the content / the excerpt', 'reendex' ),
				'param_name'    => 'content_show',
				'admin_label'   => true,
				'value'         => array(
					'Display the content'   => '1',
					'Display the excerpt'   => '0',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the content length here (Limit the number of characters to show on your post content.)', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Category/Tag Label', 'reendex' ),
				'param_name'    => 'label_show',
				'admin_label'   => true,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Text to display on label', 'reendex' ),
				'param_name'    => 'category_show',
				'admin_label'   => false,
				'value'         => array(
					'Category'  => 0,
					'Tag'       => 1,
				),
				'description'   => esc_html__( 'Choose whether to display Category or Tag on label.', 'reendex' ),
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Category/Tag Label Background Color', 'reendex' ),
				'param_name'    => 'label_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Date', 'reendex' ),
				'param_name'    => 'date_show',
				'admin_label'   => true,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'News Style', 'reendex' ),
				'param_name'    => 'style_blog',
				'admin_label'   => true,
				'value'         => array(
					'Style One'     => '1',
					'Style Two'     => '2',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- News Module 7.
	vc_map( array(
		'name'      => esc_html__( 'All Round View- News Module 7', 'reendex' ),
		'base'      => 'reendex_news_module_seven_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-module-7.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Background image', 'reendex' ),
				'param_name'    => 'bg_image',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Subtitle', 'reendex' ),
				'param_name'    => 'subtitle',
			),
			array(
				'type'          => 'vc_link',
				'heading'       => esc_html__( 'Url', 'reendex' ),
				'value'         => esc_html__( '#', 'reendex' ),
				'param_name'    => 'link',
			),
		),
	));

	// All Round View- News Module 8.
	vc_map( array(
		'name'      => esc_html__( 'All Round View- News Module 8', 'reendex' ),
		'base'      => 'reendex_news_module_eight_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-module-8.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Background image', 'reendex' ),
				'param_name'    => 'bg_image',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Subtitle', 'reendex' ),
				'param_name'    => 'subtitle',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Date', 'reendex' ),
				'param_name'    => 'date',
			),
			array(
				'type'          => 'vc_link',
				'heading'       => esc_html__( 'Url', 'reendex' ),
				'value'         => esc_html__( '#', 'reendex' ),
				'param_name'    => 'link',
			),
		),
	));

	// All Round View- News Headlines.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Headlines', 'reendex' ),
		'base'      => 'reendex_news_headlines_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-headlines.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Module Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => true,
				'value'         => array(
						'Style One'     => 1,
						'Style Two'     => 2,
						'Style Three'   => 3,
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Headlines Title', 'reendex' ),
				'param_name'    => 'title_show',
				'admin_label'   => false,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Headlines Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Headlines Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Date', 'reendex' ),
				'param_name'    => 'date_show',
				'admin_label'   => true,
				'value'         => array(
						'No'    => '0',
						'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Text Numbering', 'reendex' ),
				'param_name'    => 'text_numbering',
				'admin_label'   => false,
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// Reendex News - Latest Events.
	vc_map(array(
		'name'      => esc_html__( 'Reendex News - Latest Events', 'reendex' ),
		'base'      => 'reendex_latest_events_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-latest.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'description'   => esc_html__( 'Add the title here', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Title Color', 'reendex' ),
				'param_name'    => 'title_text_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- News Single Post.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- News Single Post', 'reendex' ),
		'base'      => 'reendex_news_single_post_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-news-single-post.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Module Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => false,
				'value'         => array(
					'Style One'     => '1',
					'Style Two'     => '2',
					'Style Three'   => '3',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
				'description'   => esc_html__( 'Set the content length here', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show the News Title', 'reendex' ),
				'param_name'    => 'title_show',
				'admin_label'   => false,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
				'description' => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'News Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Date', 'reendex' ),
				'param_name'    => 'date_show',
				'admin_label'   => true,
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- Thumbnails Gallery.
	vc_map(array(
		'name'       => esc_html__( 'ARV - Thumbnails Gallery', 'reendex' ),
		'base'       => 'reendex_thumbnails_gallery_shortcode',
		'icon'       => get_template_directory_uri() . '/img/icons/icon-thumbnails.png',
		'category'   => esc_html__( 'Reendex', 'reendex' ),
		'params'     => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Title', 'reendex' ),
				'param_name'    => 'show_title',
				'admin_label'   => true,
				'value'         => array(
					'No'    => 0,
					'Yes'   => 1,
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'show_title',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Category/Tag Label', 'reendex' ),
				'param_name'    => 'label_show',
				'admin_label'   => false,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Text to display on label', 'reendex' ),
				'param_name'    => 'category_show',
				'admin_label'   => false,
				'value'         => array(
					'Category'  => 0,
					'Tag'       => 1,
				),
				'description'   => esc_html__( 'Choose whether to display Category or Tag on label.', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Date', 'reendex' ),
				'param_name'    => 'show_date',
				'admin_label'   => true,
				'value'         => array(
					'Yes'   => 1,
					'No'    => 0,
				),
				'description'   => '',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Date Format', 'reendex' ),
				'param_name'    => 'date_format',
				'admin_label'   => true,
				'value'         => array(
					'Fri, Sep 01, 2018' => 1,
					'1h ago'            => 2,
					'01 Sep'            => 3,
				),
				'description'   => '',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'description'   => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- Lightbox Gallery.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Lightbox Gallery', 'reendex' ),
		'base'      => 'reendex_lighbox_gallery_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-lightbox.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Module Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => false,
				'value'         => array(
					'Style One'     => '1',
					'Style Two'     => '2',
					'Style Three'   => '3',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Item Title', 'reendex' ),
				'param_name'    => 'title_show',
				'admin_label'   => false,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
				'description'   => '',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Item Content', 'reendex' ),
				'param_name'    => 'show_content',
				'admin_label'   => false,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Item Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'title_show',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Item Date', 'reendex' ),
				'param_name'    => 'date_show',
				'admin_label'   => false,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
				'description'   => '',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Display the content / the excerpt', 'reendex' ),
				'param_name'    => 'content_show',
				'admin_label'   => true,
				'value'         => array(
					'Display the content'   => '1',
					'Display the excerpt'   => '0',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Content Length', 'reendex' ),
				'description'   => esc_html__( 'Set the content length here (Limit the number of characters to show on your post content.)', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the article', 'reendex' ),
				'value'         => array(
					'Open the article in the same browser window'  => '_self',
					'Open the article in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- Stay Connected.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Stay Connected', 'reendex' ),
		'base'      => 'reendex_stay_connected_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-stay-connected.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'description'   => esc_html__( 'Add the title here', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			// params group.
			array(
				'type'          => 'param_group',
				'param_name'    => 'social_media',
				'heading'       => esc_html__( 'Social Media', 'reendex' ),
				// Note params is mapped inside param-group.
				'params'        => array(
					array(
						'type'          => 'textfield',
						'value'         => 'facebook',
						'heading'       => esc_html__( 'Social Media Icon', 'reendex' ),
						'param_name'    => 'social_icon',
					),
					array(
						'type'          => 'textfield',
						'value'         => '#',
						'heading'       => esc_html__( 'Social Media Link', 'reendex' ),
						'param_name'    => 'social_link',
					),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Social Icon Link', 'reendex' ),
				'value'         => array(
					'Open the link in the same browser window'  => '_self',
					'Open the link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Instagram.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Instagram', 'reendex' ),
		'base'      => 'reendex_instagram_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-instagram.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textarea',
				'heading'       => esc_html__( 'Instagram Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textarea',
				'heading'       => esc_html__( 'Instagram Subtitle', 'reendex' ),
				'param_name'    => 'subtitle',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textarea',
				'heading'       => esc_html__( 'Description', 'reendex' ),
				'param_name'    => 'description',
				'description'   => esc_html__( 'Add the text content here', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Text', 'reendex' ),
				'param_name'    => 'button_text',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Link', 'reendex' ),
				'param_name'    => 'button_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'button_target',
				'description'   => esc_html__( 'Here you can specify how to open the Button Link', 'reendex' ),
				'value'         => array(
					'Open the button link in the same browser window'  => '_self',
					'Open the button link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Username', 'reendex' ),
				'param_name'    => 'username',
				'admin_label'   => true,
				'value'         => '',
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number Of Photos', 'reendex' ),
				'param_name'    => 'number',
				'description'   => esc_html__( 'Add here the number of photos to display', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Instagram Photos', 'reendex' ),
				'value'         => array(
					'Open the photos in a new browser window' => '_blank',
					'Open the photos in the same browser window'  => '_self',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- Twitter Feed.
	vc_map(array(
		'name'       => esc_html__( 'All Round View- Twitter Feed', 'reendex' ),
		'base'       => 'reendex_twitter_shortcode',
		'icon'       => get_template_directory_uri() . '/img/icons/icon-twitter.png',
		'category'   => esc_html__( 'Reendex', 'reendex' ),
		'params'     => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
				'description'   => esc_html__( 'Add the title here', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Username', 'reendex' ),
				'param_name'    => 'username',
				'admin_label'   => true,
				'value'         => '',
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Limit', 'reendex' ),
				'description'   => esc_html__( 'Limit', 'reendex' ),
				'param_name'    => 'limit',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Cache time', 'reendex' ),
				'description'   => esc_html__( 'Cache time', 'reendex' ),
				'param_name'    => 'cache_time',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Consumer key', 'reendex' ),
				'description'   => esc_html__( 'Consumer key', 'reendex' ),
				'param_name'    => 'consumer_key',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Consumer Secret', 'reendex' ),
				'description'   => esc_html__( 'Consumer Secret', 'reendex' ),
				'param_name'    => 'consumer_secret',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Access token', 'reendex' ),
				'description'   => esc_html__( 'Access token', 'reendex' ),
				'param_name'    => 'access_token',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Access token secret', 'reendex' ),
				'description'   => esc_html__( 'Access token secret', 'reendex' ),
				'param_name'    => 'access_token_secret',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Exclude replies', 'reendex' ),
				'description'   => esc_html__( 'Exclude replies', 'reendex' ),
				'param_name'    => 'exclude_replies',
				'admin_label'   => true,
				'value'         => array(
					'Yes'   => '1',
					'No'    => '2',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra class', 'reendex' ),
				'description'   => esc_html__( 'Extra class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Weather Dark.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Weather Dark', 'reendex' ),
		'base'      => 'reendex_weather_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-dark-weather.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Weather Widget', 'reendex' ),
				'description'   => esc_html__( 'Please add your Weather API Key in Customizer Weather Options', 'reendex' ),
				'param_name'    => 'weather',
				'admin_label'   => true,
				'value'         => 1,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'description'   => esc_html__( 'Extra Class', 'reendex' ),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Weather Light.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Weather Light', 'reendex' ),
		'base'      => 'reendex_weather_light_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-light-weather.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Weather Widget', 'reendex' ),
				'description'   => esc_html__( 'Please add your Weather API Key in Customizer Weather Options', 'reendex' ),
				'param_name'    => 'weather',
				'admin_label'   => true,
				'value'         => 1,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'description'   => esc_html__( 'Extra Class', 'reendex' ),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Small Weather.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Small Weather', 'reendex' ),
		'base'      => 'reendex_small_weather_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-small-weather.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Local Weather Title', 'reendex' ),
				'description'   => esc_html__( 'Local Weather Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Weather Widget', 'reendex' ),
				'description'   => esc_html__( 'Please add your Weather API Key in Customizer Weather Options', 'reendex' ),
				'param_name'    => 'weather',
				'admin_label'   => true,
				'value'         => 1,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'description'   => esc_html__( 'Extra Class', 'reendex' ),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Video Project.
	vc_map(array(

		'name'      => esc_html__( 'All Round View- Video Project', 'reendex' ),
		'base'      => 'reendex_video_project_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-video-project.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'reendex_category',
				'heading'       => esc_html__( 'Categories', 'reendex' ),
				'param_name'    => 'categories',
				'admin_label'   => true,
				'value'         => '',
				'description'   => esc_html__( 'Filter categories by ID. Enter here the category IDs separated by commas (ex: 315,144,148).', 'reendex' ),
				'class'         => 'video_cat',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order', 'reendex' ),
				'param_name'    => 'order',
				'admin_label'   => false,
				'value'         => array(
					'Descending'    => 'DESC',
					'Ascending'     => 'ASC',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Related Video Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Related Video Content Length', 'reendex' ),
				'param_name'    => 'related_content_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Related Video', 'reendex' ),
				'value'         => array(
					'Open the Related Video in the same browser window'  => '_self',
					'Open the Related Video in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Video News.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Video News', 'reendex' ),
		'base'      => 'reendex_video_news_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-video-news.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'autocomplete',
				'heading'       => esc_html__( 'Data source', 'reendex' ),
				'param_name'    => 'taxonomies',
				'settings'      => array(
					'multiple'          => true,
					'min_length'        => 1,
					'groups'            => true,
					'unique_values'     => true,
					'display_inline'    => true,
					'delay'             => 500,
					'auto_focus'        => true,
					'values'            => $taxonomies_list,
				),
				'description' => esc_html__( 'Enter here the categories / tags you would like to display on this module.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'param_name'    => 'per_page',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => false,
				'value'         => array(
					'Style One'     => '1',
					'Style Two'     => '2',
					'Style Three'   => '3',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Video page', 'reendex' ),
				'value'         => array(
					'Open the Video page in the same browser window'  => '_self',
					'Open the Video page in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			// Data settings.
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order by', 'reendex' ),
				'param_name'    => 'orderby',
				'value'         => array(
					esc_html__( 'Date', 'reendex' )                 => 'date',
					esc_html__( 'Order by post ID', 'reendex' )     => 'ID',
					esc_html__( 'Author', 'reendex' )               => 'author',
					esc_html__( 'Title', 'reendex' )                => 'title',
					esc_html__( 'Last modified date', 'reendex' )   => 'modified',
					esc_html__( 'Meta value', 'reendex' )           => 'meta_value',
					esc_html__( 'Random order', 'reendex' )         => 'rand',
				),
				'description'   => esc_html__( 'Select order type.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Sorting', 'reendex' ),
				'param_name'    => 'order',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => array(
					esc_html__( 'Descending', 'reendex' )   => 'DESC',
					esc_html__( 'Ascending', 'reendex' )    => 'ASC',
				),
				'description' => esc_html__( 'Select sorting order.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Offset', 'reendex' ),
				'param_name'    => 'offset',
				'description'   => esc_html__( 'Number of module elements to displace or pass over.', 'reendex' ),
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'value'         => 0,
				'description'   => esc_html__( 'E.g.: If value is set to 1, it will display posts from the 2nd one', 'reendex' ),
			),
			array(
				'type'          => 'checkbox',
				'heading'       => esc_html__( 'Prevent duplicating posts on page', 'reendex' ),
				'param_name'    => 'duplicate_posts',
				'group'         => esc_html__( 'Data settings', 'reendex' ),
				'admin_label'   => false,
				'value'         => array(
					esc_html__( 'Check this box if you want to prevent duplicating posts that are already shown before in other sections on the page.', 'reendex' ) => 'true',
				),
				'std'           => '',
			),
		),
	));

	// All Round View- Video Post Module.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Video Post Module', 'reendex' ),
		'base'      => 'reendex_video_post_module_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-camera-live.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
				'description'   => esc_html__( 'Add the title here', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => false,
				'value'         => array(
					'Style One'     => '1',
					'Style Two'     => '2',
					'Style Three'   => '3',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Video Link', 'reendex' ),
				'param_name'    => 'video_link',
				'admin_label'   => true,
				'description'   => esc_html__( 'Add the video link here', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			array(
				'type'          => 'vc_link',
				'heading'       => esc_html__( 'Url', 'reendex' ),
				'value'         => esc_html__( '#', 'reendex' ),
				'param_name'    => 'link',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Video Content', 'reendex' ),
				'param_name'    => 'show_content',
				'admin_label'   => false,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
				'description'   => '',
			),
			array(
				'type'        => 'textfield',
				'class'       => '',
				'heading'     => esc_html__( 'Programme Header', 'reendex' ),
				'param_name'  => 'header',
				'value'       => '',
			),
			array(
				'type'        => 'textfield',
				'class'       => '',
				'heading'     => esc_html__( 'Programme Name', 'reendex' ),
				'param_name'  => 'programme_name',
				'value'       => '',
			),
			array(
				'type'          => 'textarea',
				'class'         => '',
				'heading'       => esc_html__( 'Programme Description', 'reendex' ),
				'param_name'    => 'content',
				'value'         => '',
				'description'   => esc_html__( 'Provide the description for this programme.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Text', 'reendex' ),
				'param_name'    => 'button_text',
				'admin_label'   => true,
				'value'         => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Link', 'reendex' ),
				'param_name'    => 'button_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Text Background Color', 'reendex' ),
				'param_name'    => 'color',
				'value'         => '#212126',
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Description Font Size', 'reendex' ),
				'param_name'    => 'desc_f_size',
				'value'         => 14,
				'unit'          => 'px',
				'description'   => esc_html__( 'Description Font Size in pixels. Default is 14px', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Description Color', 'reendex' ),
				'param_name'    => 'description_color',
				'value'         => '#ced2d9',
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Description Line Height', 'reendex' ),
				'param_name'    => 'line_height',
				'value'         => 24,
				'unit'          => 'px',
				'description'   => esc_html__( 'Description line height', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Header Font Size', 'reendex' ),
				'param_name'    => 'header_f_size',
				'value'         => 18,
				'unit'          => 'px',
				'description'   => esc_html__( 'Header Font Size in pixels. Default is 18px', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Header Color', 'reendex' ),
				'param_name'    => 'header_color',
				'value'         => '#ced2d9',
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Programme Font Size', 'reendex' ),
				'param_name'    => 'programme_f_size',
				'value'         => 28,
				'unit'          => 'px',
				'description'   => esc_html__( 'Programme Font Size in pixels. Default is 28px', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Programme Color', 'reendex' ),
				'param_name'    => 'programme_color',
				'value'         => '#fff',
				'group'         => 'Styles',
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Read More Text Color', 'reendex' ),
				'param_name'    => 'read_more_color',
				'value'         => '#ddd',
				'group'         => 'Styles',
			),
		),
	));

	// All Round View- Big Video Module.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Big Video Module', 'reendex' ),
		'base'      => 'reendex_big_video_module_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-big-video.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
				'description'   => esc_html__( 'Add the title here', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => false,
				'value'         => array(
					'Style One'     => '1',
					'Style Two'     => '2',
					'Style Three'   => '3',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Video Link', 'reendex' ),
				'param_name'    => 'video_link',
				'admin_label'   => true,
				'description'   => esc_html__( 'Add the video link here', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			array(
				'type'          => 'vc_link',
				'heading'       => esc_html__( 'Url', 'reendex' ),
				'value'         => esc_html__( '#', 'reendex' ),
				'param_name'    => 'link',
			),
		),
	));

	// All Round View- TV Schedule Banner.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- TV Schedule Banner', 'reendex' ),
		'base'      => 'reendex_tv_schedule_banner_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-schedule-banner.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Schedule Title', 'reendex' ),
				'param_name'    => 'schedule_title',
				'description'   => esc_html__( 'Add the schedule title here', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Show Title', 'reendex' ),
				'param_name'    => 'news_title',
				'description'   => esc_html__( 'Add the show title here', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textarea',
				'heading'       => esc_html__( 'Description', 'reendex' ),
				'param_name'    => 'description',
				'description'   => esc_html__( 'Add the show description here', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Description Link', 'reendex' ),
				'param_name'    => 'description_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the description link', 'reendex' ),
				'value'         => array(
					'Open the link in the same browser window'  => '_self',
					'Open the link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Host Title', 'reendex' ),
				'param_name'    => 'host_title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Host Name', 'reendex' ),
				'param_name'    => 'host_name',
				'admin_label'   => true,
			),
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Host Image', 'reendex' ),
				'param_name'    => 'host_image',
				'description'   => esc_html__( 'Host Image', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Box Animation Hide / Show', 'reendex' ),
				'param_name'    => 'box_animation',
				'admin_label'   => true,
				'value'         => array(
					'Yes'   => 1,
					'No'    => 0,
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- TV Schedule Slider.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- TV Schedule Slider', 'reendex' ),
		'base'      => 'reendex_tv_schedule_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-schedule-slider.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Module Title Text Color', 'reendex' ),
				'param_name'    => 'title_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Module Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'param_name'    => 'per_page',
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Schedule Title Length', 'reendex' ),
				'param_name'    => 'title_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Schedule Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the link', 'reendex' ),
				'value'         => array(
					'Open the link in the same browser window'  => '_self',
					'Open the link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- TV Schedule Tab.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- TV Schedule Tab', 'reendex' ),
		'base'      => 'reendex_tv_schedule_tab_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-schedule-tabs.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of items', 'reendex' ),
				'param_name'    => 'number_of_show',
				'description'   => esc_html__( 'Add here the number of items to display', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Items Content Length', 'reendex' ),
				'param_name'    => 'content_length',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( '"Morning" Text', 'reendex' ),
				'param_name'    => 'time_of_the_day_first',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( '"Afternoon" Text', 'reendex' ),
				'param_name'    => 'time_of_the_day_second',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( '"Evening" Text', 'reendex' ),
				'param_name'    => 'time_of_the_day_third',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( '"Late" Text', 'reendex' ),
				'param_name'    => 'time_of_the_day_fourth',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the show link', 'reendex' ),
				'value'         => array(
					'Open the show link in the same browser window'  => '_self',
					'Open the show link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Call To Action.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Call To Action', 'reendex' ),
		'base'      => 'reendex_call_to_action_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-call-to-action.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textarea',
				'heading'       => esc_html__( 'Action Text', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Text', 'reendex' ),
				'param_name'    => 'button_text',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Link', 'reendex' ),
				'param_name'    => 'button_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the button link', 'reendex' ),
				'value'         => array(
					'Open the link in the same browser window'  => '_self',
					'Open the link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Background Image', 'reendex' ),
				'param_name'    => 'background_image',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- Our Team.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Our Team', 'reendex' ),
		'base'      => 'reendex_our_team_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-team.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Order', 'reendex' ),
				'param_name'    => 'order',
				'admin_label'   => false,
				'value'         => array(
					'Descending'    => 'DESC',
					'Ascending'     => 'ASC',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number Of Members', 'reendex' ),
				'param_name'    => 'per_page',
				'description'   => esc_html__( 'Add here the number of members to show', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the member link', 'reendex' ),
				'value'         => array(
					'Open the link in the same browser window'  => '_self',
					'Open the link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'description'   => esc_html__( 'Extra Class', 'reendex' ),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Sport Results.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Sport Results', 'reendex' ),
		'base'      => 'reendex_sport_results_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-sport-results.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Style', 'reendex' ),
				'param_name'    => 'result_style',
				'admin_label'   => false,
				'value'         => array(
					'Football'  => '1',
					'Racing'    => '2',
					'Tennis'    => '3',
				),
			),
				// params group.
			array(
				'type'          => 'param_group',
				'param_name'    => 'football_team',
				'heading'       => esc_html__( 'Team Result:', 'reendex' ),
				// Note params is mapped inside param-group.
				'params'        => array(
					array(
						'type'          => 'textfield',
						'value'         => 'Team Name',
						'heading'       => esc_html__( 'Team Name', 'reendex' ),
						'param_name'    => 'fteam_name',
					),
					array(
						'type'          => 'textfield',
						'value'         => '#',
						'heading'       => esc_html__( 'Team Link', 'reendex' ),
						'param_name'    => 'fteam_link',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Won Matches', 'reendex' ),
						'param_name'    => 'won_match',
						'value'         => 'Won Match',
						'description'   => esc_html__( 'Number Of Won Matches', 'reendex' ),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Lost Matches', 'reendex' ),
						'param_name'    => 'lost_match',
						'value'         => 'Lost Match',
						'description'   => esc_html__( 'Number Of Lost Matches', 'reendex' ),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Tied Matches', 'reendex' ),
						'param_name'    => 'tied_match',
						'value'         => 'Tied Match',
						'description'   => esc_html__( 'Number Of Tied Matches', 'reendex' ),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Pts For', 'reendex' ),
						'param_name'    => 'ptsfor',
						'value'         => 'Pts For',
						'description'   => esc_html__( 'Points For', 'reendex' ),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Pts Ag', 'reendex' ),
						'param_name'    => 'ptsag',
						'value'         => 'Pts Ag',
						'description'   => esc_html__( 'Points Against', 'reendex' ),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'PCT', 'reendex' ),
						'param_name'    => 'win_per',
						'value'         => 'Winning percentage',
						'description'   => esc_html__( 'Winning percentage', 'reendex' ),
					),
				),
				'dependency' 	=> array(
					'element'   => 'result_style',
					'value'     => array( '1' ),
					),
			),
			// params group For Racing.
			array(
				'type'          => 'param_group',
				'param_name'    => 'racing_result',
				'heading'       => esc_html__( 'Racing Result:', 'reendex' ),
				// param-group.
				'params'    => array(
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Driver', 'reendex' ),
						'param_name'    => 'driv_name',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Driver Details Link', 'reendex' ),
						'param_name'    => 'driv_det',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Nationality', 'reendex' ),
						'param_name'    => 'nat',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Team Name', 'reendex' ),
						'param_name'    => 'team_name',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Points', 'reendex' ),
						'param_name'    => 'pts',
					),
				),
				'dependency' 	=> array(
					'element'   => 'result_style',
					'value'     => array( '2' ),
					),
			),
			// params group For Tennis.
			array(
				'type'          => 'param_group',
				'param_name'    => 'tennis_result',
				'heading'       => esc_html__( 'Tennis Results:', 'reendex' ),
				// param-group.
				'params'    => array(
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Date', 'reendex' ),
						'param_name'    => 'tdate',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Round', 'reendex' ),
						'param_name'    => 'tround',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Competitors', 'reendex' ),
						'param_name'    => 'competitor',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Set 1', 'reendex' ),
						'param_name'    => 'sets1',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Set 2', 'reendex' ),
						'param_name'    => 'sets2',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Set 3', 'reendex' ),
						'param_name'    => 'sets3',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Set 4', 'reendex' ),
						'param_name'    => 'sets4',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Set 5', 'reendex' ),
						'param_name'    => 'sets5',
					),
				),
				'dependency' 	=> array(
					'element'   => 'result_style',
					'value'     => array( '3' ),
					),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the links', 'reendex' ),
				'value'         => array(
					'Open the links in the same browser window'  => '_self',
					'Open the links in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Last Update Date', 'reendex' ),
				'param_name'    => 'up_date',
				'value'         => esc_html__( 'Dec 5, 2018 1:09pm', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- Sidebar Football Results.
	 vc_map(array(
		 'name'      => esc_html__( 'All Round View- Sidebar Football Results', 'reendex' ),
		 'base'      => 'reendex_sidebar_football_results_shortcode',
		 'icon'      => get_template_directory_uri() . '/img/icons/icon-sidebar-football-results.png',
		 'category'  => esc_html__( 'Reendex', 'reendex' ),
		 'params'    => array(
			// params group.
			array(
				'type'          => 'param_group',
				'param_name'    => 'sfootball_team',
				'heading'       => esc_html__( 'Team Result:', 'reendex' ),
				// Note params is mapped inside param-group.
				'params'    => array(
					array(
						'type'          => 'textfield',
						'value'         => 'Team Name',
						'heading'       => esc_html__( 'Team Name', 'reendex' ),
						'param_name'    => 'fteam_name',
					),
					array(
						'type'          => 'textfield',
						'value'         => '#',
						'heading'       => esc_html__( 'Team Link', 'reendex' ),
						'param_name'    => 'fteam_link',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'PL', 'reendex' ),
						'param_name'    => 'pl',
						'description'   => esc_html__( 'Games played', 'reendex' ),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'GD', 'reendex' ),
						'param_name'    => 'gd',
						'description'   => esc_html__( 'Goal Difference', 'reendex' ),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Pts', 'reendex' ),
						'param_name'    => 'pts',
						'description'   => esc_html__( 'Points', 'reendex' ),
					),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Last Update Date', 'reendex' ),
				'param_name'    => 'up_date',
				'value'         => esc_html__( 'Dec 6, 2018 1:09pm', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the team link', 'reendex' ),
				'value'         => array(
					'Open the link in the same browser window'  => '_self',
					'Open the link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		 ),
	 ));

	// All Round View- Football Fixtures and Results.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Football Fixtures and Results', 'reendex' ),
		'base'      => 'reendex_football_fixtures_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-football-fixtures-results.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Match Date', 'reendex' ),
				'param_name'    => 'match_date',
				'admin_label'   => true,
				'description'   => esc_html__( 'Match Date', 'reendex' ),
			),
			// params group.
			array(
				'type'          => 'param_group',
				'param_name'    => 'football_fixtures',
				'heading'       => esc_html__( 'Match Fixtures:', 'reendex' ),
				// Note params is mapped inside param-group.
				'params'    => array(
					array(
						'type'          => 'textfield',
						'value'         => 'Soccer League',
						'heading'       => esc_html__( 'Soccer League', 'reendex' ),
						'param_name'    => 'team_label_one',
					),
					array(
						'type'          => 'textfield',
						'value'         => 'First Team Name',
						'heading'       => esc_html__( 'First Team Name', 'reendex' ),
						'param_name'    => 'team_name_one',
					),
					array(
						'type'          => 'textfield',
						'value'         => 'Time Result',
						'heading'       => esc_html__( 'Time Result', 'reendex' ),
						'param_name'    => 'team_label_two',
					),
					array(
						'type'          => 'textfield',
						'value'         => 'Second Team Name',
						'heading'       => esc_html__( 'Second Team Name', 'reendex' ),
						'param_name'    => 'team_name_two',
					),
					array(
						'type'          => 'dropdown',
						'heading'       => esc_html__( 'Match Result Status', 'reendex' ),
						'param_name'    => 'match_result_status',
						'admin_label'   => true,
						'value'         => array(
							'Score'   => 1,
							'Time'    => 0,
						),
						'description'   => '',
					),
					array(
						'type'          => 'textfield',
						'value'         => 'First Team Score',
						'heading'       => esc_html__( 'First Team Score', 'reendex' ),
						'param_name'    => 'team_status_one',
						'dependency' 	=> array(
							'element'   => 'match_result_status',
							'value'     => array( '1' ),
							),
					),
					array(
						'type'          => 'textfield',
						'value'         => 'Second Team Score',
						'heading'       => esc_html__( 'Second Team Score', 'reendex' ),
						'param_name'    => 'team_status_two',
						'dependency' 	=> array(
							'element'   => 'match_result_status',
							'value'     => array( '1' ),
							),
					),
					array(
						'type'          => 'textfield',
						'value'         => 'Match Start Time',
						'heading'       => esc_html__( 'Match Start Time', 'reendex' ),
						'param_name'    => 'match_start_time',
						'dependency' 	=> array(
							'element'   => 'match_result_status',
							'value'     => array( '0' ),
							),
					),
					array(
						'type'          => 'textfield',
						'value'         => '#',
						'heading'       => esc_html__( 'Match Details Link', 'reendex' ),
						'param_name'    => 'match_link',
					),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the links', 'reendex' ),
				'value'         => array(
					'Open the links in the same browser window'  => '_self',
					'Open the links in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Last Update Date', 'reendex' ),
				'param_name'    => 'up_date',
				'value'         => esc_html__( 'Dec 6, 2018 1:09pm', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- Match Schedule.
	vc_map(array(
		 'name'      => esc_html__( 'All Round View- Match Schedule', 'reendex' ),
		 'base'      => 'reendex_match_schedule_shortcode',
		 'icon'      => get_template_directory_uri() . '/img/icons/icon-match-schedule.png',
		 'category'  => esc_html__( 'Reendex', 'reendex' ),
		 'params'    => array(
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Style', 'reendex' ),
				'param_name'    => 'mstyle',
				'admin_label'   => false,
				'value'         => array(
					'Style One' => '1',
					'Style Two' => '2',
				),
			),
			array(
				'type'          => 'textfield',
				'value'         => 'Title',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'dependency' 	=> array(
					'element'   => 'mstyle',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bt',
				'dependency' 	=> array(
					'element'   => 'mstyle',
					'value'     => array( '1' ),
					),
			),
			array(
				'type'          => 'textfield',
				'value'         => 'Country Name',
				'heading'       => esc_html__( 'Country Name', 'reendex' ),
				'param_name'    => 'country_name',
				'dependency' 	=> array(
					'element'   => 'mstyle',
					'value'     => array( '2' ),
					),
			),
			// params group.
			array(
				'type'          => 'param_group',
				'param_name'    => 'match_schedule',
				'heading'       => esc_html__( 'Match Schedule:', 'reendex' ),
				// Note params is mapped inside param-group.
				'params'    => array(
					array(
						'type'          => 'textfield',
						'value'         => 'Match Date',
						'heading'       => esc_html__( 'Match Date', 'reendex' ),
						'param_name'    => 'mdate',
					),
					array(
						'type'          => 'textfield',
						'value'         => 'First Team Name',
						'heading'       => esc_html__( 'First Team Name', 'reendex' ),
						'param_name'    => 'team_name_one',
					),
					array(
						'type'          => 'attach_image',
						'value'         => 'First Team Logo',
						'heading'       => esc_html__( 'First Team Logo', 'reendex' ),
						'param_name'    => 'team_one_logo',
					),
					array(
						'type'          => 'textfield',
						'value'         => 'Second Team Name',
						'heading'       => esc_html__( 'Second Team Name', 'reendex' ),
						'param_name'    => 'team_name_two',
					),
					array(
						'type'          => 'attach_image',
						'value'         => 'Second Team Logo',
						'heading'       => esc_html__( 'Second Team Logo', 'reendex' ),
						'param_name'    => 'team_two_logo',
					),
					array(
						'type'          => 'textfield',
						'value'         => '#',
						'heading'       => esc_html__( 'Match Details Link', 'reendex' ),
						'param_name'    => 'match_link',
					),
				),
				'dependency' 	=> array(
					'element'   => 'mstyle',
					'value'     => array( '1' ),
					),
			),
			// params group Field for style two.
			array(
				'type'          => 'param_group',
				'param_name'    => 'match_schedule_time',
				'heading'       => esc_html__( 'Match Schedule:', 'reendex' ),

				// Note params is mapped inside param-group.
				'params'    => array(
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Match No.', 'reendex' ),
						'param_name'    => 'smatch_no',
						'description'   => esc_html__( 'Match No', 'reendex' ),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'TV Times', 'reendex' ),
						'param_name'    => 'tvtime',
						'description'   => esc_html__( 'TV Times', 'reendex' ),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'On Air', 'reendex' ),
						'param_name'    => 'onair_time',
						'description'   => esc_html__( 'On Air Time', 'reendex' ),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Start', 'reendex' ),
						'param_name'    => 'start_time',
						'description'   => esc_html__( 'Start Time', 'reendex' ),
					),
				),
				'dependency' 	=> array(
					'element'   => 'mstyle',
					'value'     => array( '2' ),
					),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Live Updates', 'reendex' ),
				'param_name'    => 'live_updates',
				'admin_label'   => true,
				'value'         => array(
					'No'    => '0',
					'Yes'   => '1',
				),
				'description'   => '',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Match link', 'reendex' ),
				'value'         => array(
					'Open the Match link in the same browser window'  => '_self',
					'Open the Match link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'value'         => '#',
				'heading'       => esc_html__( 'Live Updates Link', 'reendex' ),
				'param_name'    => 'live_updates_link',
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'updates_target',
				'description'   => esc_html__( 'Here you can specify how to open Live Updates link', 'reendex' ),
				'value'         => array(
					'Open Live Updates link in the same browser window'  => '_self',
					'Open Live Updates link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Live Updates Background Color', 'reendex' ),
				'param_name'    => 'live_updates_bg_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Last Update Date', 'reendex' ),
				'param_name'    => 'up_date',
				'value'         => esc_html__( 'Dec 5, 2018 1:09pm', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'description'   => esc_html__( 'Extra Class', 'reendex' ),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Tennis Tournaments.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Tennis Tournaments', 'reendex' ),
		'base'      => 'reendex_tennis_tournaments_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-tennis.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Module Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => false,
				'value'         => array(
					'Style One'     => '1',
					'Style Two'     => '2',
					'Style Three'   => '3',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'First Tournaments Title', 'reendex' ),
				'param_name'    => 'match_title',
				'admin_label'   => true,
			),
			// params group.
			array(
				'type'          => 'param_group',
				'param_name'    => 'tennis_competitions_fixtures',
				'heading'       => esc_html__( 'Match Fixtures:', 'reendex' ),
				// Note params is mapped inside param-group.
				'params'    => array(
					array(
						'type'          => 'textfield',
						'value'         => 'Tournament name',
						'heading'       => esc_html__( 'Tournament name', 'reendex' ),
						'param_name'    => 'competitions_team',
					),
					array(
						'type'          => 'textfield',
						'value'         => '#',
						'heading'       => esc_html__( 'Tournament link', 'reendex' ),
						'param_name'    => 'competitions_link',
					),
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Second Tournaments Title', 'reendex' ),
				'param_name'    => 'match_title_two',
				'admin_label'   => true,
			),
			// params group.
			array(
				'type'          => 'param_group',
				'param_name'    => 'tennis_competitions_fixtures_other',
				'heading'       => esc_html__( 'Other Tournaments:', 'reendex' ),
				// Note params is mapped inside param-group.
				'params'    => array(
					array(
						'type'          => 'textfield',
						'value'         => 'Competitions team name',
						'heading'       => esc_html__( 'Tournament name', 'reendex' ),
						'param_name'    => 'ocompetitions_team',
					),
					array(
						'type'          => 'textfield',
						'value'         => '#',
						'heading'       => esc_html__( 'Tournament link', 'reendex' ),
						'param_name'    => 'ocompetitions_link',
					),
				),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Tournament link', 'reendex' ),
				'value'         => array(
					'Open the Tournament link in the same browser window'  => '_self',
					'Open the Tournament link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- Tennis Results.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Tennis Results', 'reendex' ),
		'base'      => 'reendex_tennis_results_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-tennis-results.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Module Title Style', 'reendex' ),
				'param_name'    => 'title_style',
				'admin_label'   => false,
				'value'         => array(
					'Style One'     => '1',
					'Style Two'     => '2',
					'Style Three'   => '3',
				),
				'description'   => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Module Title Margin Bottom', 'reendex' ),
				'param_name'    => 'title_margin_bottom',
				'admin_label'   => true,
			),
			// params group.
			array(
				'type'          => 'param_group',
				'param_name'    => 'tennis_competitions_result',
				'heading'       => esc_html__( 'Tenis Results:', 'reendex' ),
				// Note params is mapped inside param-group.
				'params'    => array(
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'First Player', 'reendex' ),
						'param_name'    => 'first_player',
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Second Player', 'reendex' ),
						'param_name'    => 'second_player',
					),
					array(
						'type'          => 'dropdown',
						'heading'       => esc_html__( 'Match Status', 'reendex' ),
						'param_name'    => 'match_status',
						'admin_label'   => true,
						'value'         => array(
							'Result'    => '1',
							'Time'      => '2',
						),
					),
					array(
						'type'          => 'textfield',
						'value'         => 'Match Time',
						'heading'       => esc_html__( 'Match Time', 'reendex' ),
						'param_name'    => 'match_time',
						'dependency' 	=> array(
							'element'   => 'match_status',
							'value'     => array( '2' ),
							),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'First Player Set 1', 'reendex' ),
						'param_name'    => 'first_player_set_one',
						'dependency' 	=> array(
							'element'   => 'match_status',
							'value'     => array( '1' ),
							),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'First Player Set 2', 'reendex' ),
						'param_name'    => 'first_player_set_two',
						'dependency' 	=> array(
							'element'   => 'match_status',
							'value'     => array( '1' ),
							),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'First Player Set 3', 'reendex' ),
						'param_name'    => 'first_player_set_three',
						'dependency' 	=> array(
							'element'   => 'match_status',
							'value'     => array( '1' ),
							),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'First Player Set 4', 'reendex' ),
						'param_name'    => 'first_player_set_four',
						'dependency' 	=> array(
							'element'   => 'match_status',
							'value'     => array( '1' ),
							),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'First Player Set 5', 'reendex' ),
						'param_name'    => 'first_player_set_five',
						'dependency' 	=> array(
							'element'   => 'match_status',
							'value'     => array( '1' ),
							),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Second Player Set 1', 'reendex' ),
						'param_name'    => 'second_player_set_one',
						'dependency' 	=> array(
							'element'   => 'match_status',
							'value'     => array( '1' ),
							),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Second Player Set 2', 'reendex' ),
						'param_name'    => 'second_player_set_two',
						'dependency' 	=> array(
							'element'   => 'match_status',
							'value'     => array( '1' ),
							),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Second Player Set 3', 'reendex' ),
						'param_name'    => 'second_player_set_three',
						'dependency' 	=> array(
							'element'   => 'match_status',
							'value'     => array( '1' ),
							),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Second Player Set 4', 'reendex' ),
						'param_name'    => 'second_player_set_four',
						'dependency' 	=> array(
							'element'   => 'match_status',
							'value'     => array( '1' ),
							),
					),
					array(
						'type'          => 'textfield',
						'heading'       => esc_html__( 'Second Player Set 5', 'reendex' ),
						'param_name'    => 'second_player_set_five',
						'dependency' 	=> array(
							'element'   => 'match_status',
							'value'     => array( '1' ),
						),
					),
				),
			),
		),
	));

	// All Round View- Sport Shop.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Sport Shop', 'reendex' ),
		'base'      => 'reendex_sport_shop_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-sport-shop.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Image 1', 'reendex' ),
				'param_name'    => 'shop_image1',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Image 1 Link', 'reendex' ),
				'param_name'    => 'shop_image1_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Image 1 Description', 'reendex' ),
				'param_name'    => 'shop_image1_des',
				'admin_label'   => true,
			),
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Image 2', 'reendex' ),
				'param_name'    => 'shop_image2',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Image 2 Link', 'reendex' ),
				'param_name'    => 'shop_image2_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Image 2 Description', 'reendex' ),
				'param_name'    => 'shop_image2_des',
				'admin_label'   => true,
			),
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Image 3', 'reendex' ),
				'param_name'    => 'shop_image3',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Image 3 Link', 'reendex' ),
				'param_name'    => 'shop_image3_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Image 3 Description', 'reendex' ),
				'param_name'    => 'shop_image3_des',
				'admin_label'   => true,
			),
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Image 4', 'reendex' ),
				'param_name'    => 'shop_image4',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Image 4 Link', 'reendex' ),
				'param_name'    => 'shop_image4_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Image 4 Description', 'reendex' ),
				'param_name'    => 'shop_image4_des',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the product link', 'reendex' ),
				'value'         => array(
					'Open the product link in a new browser window' => '_blank',
					'Open the product link in the same browser window'  => '_self',
				),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Sport Tickets.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Sport Tickets', 'reendex' ),
		'base'      => 'reendex_sport_tickets_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-sport-tickets.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Description', 'reendex' ),
				'param_name'    => 'ticket_des',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Text', 'reendex' ),
				'param_name'    => 'button_text',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Link', 'reendex' ),
				'param_name'    => 'button_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Partner Logo', 'reendex' ),
				'param_name'    => 'add_image',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Partner Logo Link', 'reendex' ),
				'param_name'    => 'add_ticket_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Partner link', 'reendex' ),
				'value'         => array(
					'Open the Partner link in a new browser window' => '_blank',
					'Open the Partner link in the same browser window'  => '_self',
				),
				'admin_label'   => true,
			),
		),
	));

	// All Round View- Promo Module.
	vc_map( array(
		'name'        => esc_html__( 'All Round View- Promo Module', 'reendex' ),
		'base'        => 'reendex_promo_module_shortcode',
		'icon'        => get_template_directory_uri() . '/img/icons/icon-image-promo.png',
		'category'    => esc_html__( 'Reendex', 'reendex' ),
		'params'      => array(
			array(
				'type'          => 'attach_image',
				'heading'       => esc_html__( 'Programme Image', 'reendex' ),
				'param_name'    => 'image',
				'value'         => '',
				'description'   => esc_html__( 'Select image from media library.', 'reendex' ),
			),
			array(
				'type'        => 'textfield',
				'class'       => '',
				'heading'     => esc_html__( 'Programme Header', 'reendex' ),
				'param_name'  => 'header',
				'value'       => '',
			),
			array(
				'type'        => 'textfield',
				'class'       => '',
				'heading'     => esc_html__( 'Programme Name', 'reendex' ),
				'param_name'  => 'programme_name',
				'value'       => '',
			),
			array(
				'type'          => 'textarea',
				'class'         => '',
				'heading'       => esc_html__( 'Programme Description', 'reendex' ),
				'param_name'    => 'content',
				'value'         => '',
				'description'   => esc_html__( 'Provide the description for this programme.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Text', 'reendex' ),
				'param_name'    => 'button_text',
				'admin_label'   => true,
				'value'         => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Link', 'reendex' ),
				'param_name'    => 'button_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Button link', 'reendex' ),
				'value'         => array(
					'Open the Button link in the same browser window'  => '_self',
					'Open the Button link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'vc_link',
				'class'         => '',
				'heading'       => esc_html__( 'Add Link', 'reendex' ),
				'param_name'    => 'link',
				'value'         => '',
				'description'   => esc_html__( 'Add a custom link or select existing page. You can remove existing link as well.', 'reendex' ),
				'dependency'    => array(
					'element'   => 'add_link',
					'value'     => 'yes',
				),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class in this field, if you wish to style particular content element differently.', 'reendex' ),
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Text Background Color', 'reendex' ),
				'param_name'    => 'color',
				'value'         => '#212126',
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Description Font Size', 'reendex' ),
				'param_name'    => 'desc_f_size',
				'value'         => 14,
				'unit'          => 'px',
				'description'   => esc_html__( 'Description Font Size in pixels. Default is 14px', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Description Color', 'reendex' ),
				'param_name'    => 'description_color',
				'value'         => '#ced2d9',
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Description Line Height', 'reendex' ),
				'param_name'    => 'line_height',
				'value'         => 24,
				'unit'          => 'px',
				'description'   => esc_html__( 'Description line height', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Header Font Size', 'reendex' ),
				'param_name'    => 'header_f_size',
				'value'         => 18,
				'unit'          => 'px',
				'description'   => esc_html__( 'Header Font Size in pixels. Default is 24px', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Header Color', 'reendex' ),
				'param_name'    => 'header_color',
				'value'         => '#ced2d9',
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Programme Font Size', 'reendex' ),
				'param_name'    => 'programme_f_size',
				'value'         => 28,
				'unit'          => 'px',
				'description'   => esc_html__( 'Programme Font Size in pixels. Default is 28px', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Programme Color', 'reendex' ),
				'param_name'    => 'programme_color',
				'value'         => '#fff',
				'group'         => 'Styles',
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Read More Text Color', 'reendex' ),
				'param_name'    => 'read_more_color',
				'value'         => '#ddd',
				'group'         => 'Styles',
			),
		),
	) );

	// All Round View- Video Promo Module.
	vc_map( array(
		'name'        => esc_html__( 'All Round View- Video Promo Module', 'reendex' ),
		'base'        => 'reendex_video_promo_module_shortcode',
		'icon'        => get_template_directory_uri() . '/img/icons/icon-video-promo.png',
		'category'    => esc_html__( 'Reendex', 'reendex' ),
		'params'      => array(
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Video Source', 'reendex' ),
				'param_name'    => 'video_source',
				'admin_label'   => true,
				'value'         => array(
					'YouTube'   => 1,
					'Vimeo'     => 2,
				),
				'description' => 'Select video source from dropdown, Default is YouTube',
				'tooltip'     => 'Select Video Source',
				),
			// Youtube Video Field.
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'YouTube Video ID', 'reendex' ),
				'param_name'  => 'youtube_id',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'video_source',
					'value'     => array( '1' ),
				),
				'description' => esc_html__( 'Enter your Youtube Video ID Here. Example: k0KcWyZ8II0', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Autoplay video on page load', 'reendex' ),
				'param_name'    => 'youtube_autoplay',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'video_source',
					'value'     => array( '1' ),
				),
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
				'description'   => esc_html__( 'Automatically start playback of the video.', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Loop video', 'reendex' ),
				'param_name'    => 'youtube_loop',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'video_source',
					'value'     => array( '1' ),
				),
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
				'description'   => esc_html__( 'Play the video again when it reaches the end.', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Mute video by default', 'reendex' ),
				'param_name'    => 'mute',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'video_source',
					'value'     => array( '1' ),
				),
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
				'description'   => esc_html__( 'When the player loads, the volume will be muted until the viewer adjusts it.', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Show Info', 'reendex' ),
				'param_name'    => 'showinfo',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'video_source',
					'value'     => array( '1' ),
				),
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
				'description'   => esc_html__( 'Display information like the video title and uploader before the video starts playing.', 'reendex' ),
			),

			// Vimeo Video Field.
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Vimeo Clip ID', 'reendex' ),
				'param_name'  => 'clip_id',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'video_source',
					'value'     => array( '2' ),
				),
				'description' => esc_html__( 'Enter your Vimeo Clip ID here. Example: 102615910', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Autoplay video on page load', 'reendex' ),
				'param_name'    => 'autoplay',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'video_source',
					'value'     => array( '2' ),
				),
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
				'description'   => esc_html__( 'Automatically start playback of the video.', 'reendex' ),
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Loop video', 'reendex' ),
				'param_name'    => 'loop',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'video_source',
					'value'     => array( '2' ),
				),
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
				),
				'description'   => esc_html__( 'Play the video again when it reaches the end.', 'reendex' ),
			),
						array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Mute video by default', 'reendex' ),
				'param_name'    => 'automute',
				'admin_label'   => true,
				'dependency' 	=> array(
					'element'   => 'video_source',
					'value'     => array( '2' ),
						),
				'value'         => array(
					'Yes'   => '1',
					'No'    => '0',
						),
				'description'   => esc_html__( 'When the player loads, the volume will be muted until the viewer adjusts it.', 'reendex' ),
			),
			array(
				'type'        => 'textfield',
				'class'       => '',
				'heading'     => esc_html__( 'Programme Header', 'reendex' ),
				'param_name'  => 'header',
				'value'       => '',
			),
			array(
				'type'        => 'textfield',
				'class'       => '',
				'heading'     => esc_html__( 'Programme Name', 'reendex' ),
				'param_name'  => 'programme_name',
				'value'       => '',
			),
			array(
				'type'          => 'textarea',
				'class'         => '',
				'heading'       => esc_html__( 'Programme Description', 'reendex' ),
				'param_name'    => 'content',
				'value'         => '',
				'description'   => esc_html__( 'Provide the description for this programme.', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Text', 'reendex' ),
				'param_name'    => 'button_text',
				'admin_label'   => true,
				'value'         => '',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Link', 'reendex' ),
				'param_name'    => 'button_link',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Link Target Options', 'reendex' ),
				'param_name'    => 'target',
				'description'   => esc_html__( 'Here you can specify how to open the Button link', 'reendex' ),
				'value'         => array(
					'Open the Button link in the same browser window'  => '_self',
					'Open the Button link in a new browser window' => '_blank',
				),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Text Background Color', 'reendex' ),
				'param_name'    => 'color',
				'value'         => '#212126',
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Description Font Size', 'reendex' ),
				'param_name'    => 'desc_f_size',
				'value'         => 14,
				'unit'          => 'px',
				'description'   => esc_html__( 'Description Font Size in pixels. Default is 14px', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Description Color', 'reendex' ),
				'param_name'    => 'description_color',
				'value'         => '#ced2d9',
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Description Line Height', 'reendex' ),
				'param_name'    => 'line_height',
				'value'         => 24,
				'unit'          => 'px',
				'description'   => esc_html__( 'Description line height', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Header Font Size', 'reendex' ),
				'param_name'    => 'header_f_size',
				'value'         => 24,
				'unit'          => 'px',
				'description'   => esc_html__( 'Header Font Size in pixels. Default is 24px', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Header Color', 'reendex' ),
				'param_name'    => 'header_color',
				'value'         => '#ced2d9',
				'group'         => 'Styles',
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Programme Font Size', 'reendex' ),
				'param_name'    => 'programme_f_size',
				'value'         => 28,
				'unit'          => 'px',
				'description'   => esc_html__( 'Programme Font Size in pixels. Default is 28px', 'reendex' ),
				'group'         => 'Styles',
			),
			array(
				'type'          => 'colorpicker',
				'class'         => '',
				'heading'       => esc_html__( 'Programme Color', 'reendex' ),
				'param_name'    => 'programme_color',
				'value'         => '#fff',
				'group'         => 'Styles',
			),
		),
	) );

	// All Round View- Currency Converter.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Currency Converter', 'reendex' ),
		'base'      => 'reendex_currency_converter_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-currency.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'description'   => esc_html__( 'Add the title here', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Title Text Color', 'reendex' ),
				'param_name'    => 'title_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'colorpicker',
				'heading'       => esc_html__( 'Title Background Color', 'reendex' ),
				'param_name'    => 'title_bg_color',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Button Text', 'reendex' ),
				'param_name'    => 'button_text',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( '"Amount" Text', 'reendex' ),
				'param_name'    => 'amount_text',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( '"From" Text', 'reendex' ),
				'param_name'    => 'from_text',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( '"To" Text', 'reendex' ),
				'param_name'    => 'to_text',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- Category List.
	vc_map(array(
		'name'      => esc_html__( 'All Round View- Category List', 'reendex' ),
		'base'      => 'reendex_category_list_shortcode',
		'icon'      => get_template_directory_uri() . '/img/icons/icon-cat-list.png',
		'category'  => esc_html__( 'Reendex', 'reendex' ),
		'params'    => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Title', 'reendex' ),
				'param_name'    => 'title',
				'admin_label'   => true,
			),
			array(
				'type'          => 'dropdown',
				'heading'       => esc_html__( 'Display posts number', 'reendex' ),
				'param_name'    => 'post_count_show',
				'admin_label'   => false,
				'value'         => array(
						'Yes'   => '1',
						'No'    => '0',
				),
				'description'   => esc_html__( 'Select yes if you want display the number of posts in category', 'reendex' ),
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Number of categories', 'reendex' ),
				'description'   => esc_html__( 'Set here the number of categories to display', 'reendex' ),
				'param_name'    => 'number',
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));

	// All Round View- Breadcrumbs.
	vc_map(array(
		'name'       => esc_html__( 'All Round View- Breadcrumbs', 'reendex' ),
		'base'       => 'reendex_breadcrumb_shortcode',
		'icon'       => get_template_directory_uri() . '/img/icons/icon-breadcrumbs.png',
		'category'   => esc_html__( 'Reendex', 'reendex' ),
		'params'     => array(
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Breadcrumbs Margin', 'reendex' ),
				'param_name'    => 'breadcrumbs_margin',
				'description'   => esc_html__( 'Breadcrumbs Margin', 'reendex' ),
				'admin_label'   => true,
			),
			array(
				'type'          => 'textfield',
				'heading'       => esc_html__( 'Extra Class', 'reendex' ),
				'param_name'    => 'extra_class',
				'admin_label'   => true,
				'description'   => esc_html__( 'You can add an extra class here', 'reendex' ),
			),
		),
	));
	/**
	 * Param Shortcode.
	 *
	 * @param array $settings   Array of param seetings.
	 * @param int   $value      Param value.
	 */
	function reendex_product_categories_shortcode_param( $settings, $value ) {
		$categories = reendex_get_list_categories_shortcode_param( 0, $settings );
		$arr_value = explode( ',', $value );
		ob_start();
	?>
	<div class="reendex_category">
		<div class="wpb_el_type_attach_text">
			<textarea name="<?php echo esc_attr( $settings['param_name'] ); ?>" class="reendex_category_value wpb_vc_param_value wpb-textinput title textfield" placeholder="<?php echo esc_attr( 'Enter cat slug list, divide values with comma (,)' );?>" rows="5"><?php echo esc_textarea( $value ); ?></textarea>
		</div>
	</div><!-- /.reendex_category -->
	<?php
	return ob_get_clean();
	}
	WpbakeryShortcodeParams::addField( 'reendex_category', 'reendex_product_categories_shortcode_param' );
} // End if().


if ( ! function_exists( 'reendex_get_list_categories_shortcode_param' ) ) {
	/**
	 * Returns a list of categories.
	 *
	 * @param string $cat_parent_id The parent term ID.
	 * @param array  $settings Category Settings.
	 */
	function reendex_get_list_categories_shortcode_param( $cat_parent_id, $settings ) {
		$taxonomy = 'product_cat';
		if ( isset( $settings['class'] ) ) {
			if ( 'post_cat' === $settings['class'] ) {
				$taxonomy = 'category';
			} elseif ( 'video_cat' === 'video_cat' ) {
				$taxonomy = 'thcat_taxonomy';
			}
		}
		$args = array(
			'taxonomy' 		=> $taxonomy,
			'hierarchical'  => 1,
			'hide_empty'	=> 0,
			'parent'		=> $cat_parent_id,
			'title_li'		=> '',
			'child_of'		=> 0,
		);
		$cats = get_categories( $args );
		return $cats;
	}
}

if ( ! function_exists( 'reendex_get_list_sub_categories_shortcode_param' ) ) {
	/**
	 * Returns a list of subcategories.
	 *
	 * @param string $cat_parent_id The parent term ID.
	 * @param array  $arr_value Array value.
	 * @param array  $settings Subcategory Settings.
	 */
	function reendex_get_list_sub_categories_shortcode_param( $cat_parent_id, $arr_value, $settings ) {
		$sub_categories = reendex_get_list_categories_shortcode_param( $cat_parent_id, $settings );
		if ( count( $sub_categories ) > 0 ) {
			$j = 0;
		?>
			<ul class="children">
				<?php foreach ( $sub_categories as $sub_cat ) { ?>
					<li>
						<label>
							<input type="checkbox" class="checkbox reendex-select-category" value="<?php echo esc_attr( $sub_cat->term_id ); ?>" <?php checked( in_array( $sub_cat->term_id, $arr_value ) ); ?> />
							<?php echo esc_html( $sub_cat->name ); ?>
						</label>
						<?php reendex_get_list_sub_categories_shortcode_param( $sub_cat->term_id, $arr_value, $settings ); ?>
					</li>
				<?php } ?>
			</ul><!-- /.children -->
		<?php }
	}
}
