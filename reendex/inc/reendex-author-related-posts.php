<?php
/**
 * Related posts by the same author.
 *
 * @package Reendex
 */

$options = reendex_get_theme_options();
$show_author_related_title = get_theme_mod( 'reendex_author_related_title_show', 'enable' );
$author_related_title = $options['reendex_author_related_title'];
$author_related_subtitle = $options['reendex_author_related_subtitle'];
$posts_per_page_option = $options['reendex_author_related_number'];
$author_related_content_length = $options['reendex_author_related_content_length'];
$author_related_content_style = get_theme_mod( 'reendex_author_related_content_style' );
$title_length = 6;
?>

<div class="module-wrapper related-posts">
	<?php if ( 'disable' !== $show_author_related_title ) : ?>
		<div class="module-title">
			<h4 class="title"><span class="bg-1"><?php echo esc_attr( $author_related_title );?></span></h4>
			<h4 class="subtitle"><?php echo esc_attr( $author_related_subtitle );?></h4>
		</div><!-- /.module-title -->
	<?php endif;?>
	<div class="
		<?php
		if ( 1 == $author_related_content_style ) {
			echo 'related-posts-1';
		} elseif ( 2 == $author_related_content_style ) {
			echo 'related-posts-2';
		} else {
			echo 'related-posts-3'; } ?>">  
		<?php
		global $authordata, $post;
		$args = array(
			'author'    => $authordata->ID,
			'post_type'         => 'post',
			'post__not_in'      => array( $post->ID ),
			'orderby'           => 'rand',
			'posts_per_page'    => $posts_per_page_option,
		);
		$query = new WP_Query( $args );
		while ( $query->have_posts() ) {
			$query->the_post();
			$permalink = get_permalink();
		?>
			<div class="related-item-block">
				<div class="single-related-posts">
					<?php if ( has_post_thumbnail() ) :
						$image_id = get_post_thumbnail_id();
						$image_path = wp_get_attachment_image_src( $image_id, 'reendex_news1_thumb', true );
						$image_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
					?>
						<div class="item-image">
							<a class="img-link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<img class="img-responsive img-full" src="<?php echo esc_url( $image_path[0] ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>" title="<?php the_title(); ?>" />
								<?php if ( has_post_format( 'video' ) ) : ?>
									<span class="video-icon-small">
										<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-small.png" alt="video"/>
									</span>
								<?php endif; ?>
							</a>
						</div><!-- /.item-image -->
					<?php endif ?>    
					<div class="item-content">
						<h3 class="entry-title">
							<a href="<?php the_permalink() ?>">
								<?php
								if ( get_the_title( $post->ID ) ) {
									reendex_short_title($title_length,'...');
								} else {
									the_time( get_option( 'date_format' ) );
								}
								?>
							</a>
						</h3><!-- /.entry-title -->
						<div class="post-meta-elements">
							<div class="post-meta-author">
								<i class="fa fa-user"></i>
								<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ) ); ?>"><?php the_author(); ?></a>
							</div>
							<div class="post-meta-date">
								<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
									<i class="fa fa-calendar"></i>
									<a href="<?php echo esc_url( get_day_link( get_post_time( 'Y' ), get_post_time( 'm' ), get_post_time( 'd' ) ) ); ?>"><?php the_time( 'M d, Y','reendex' ); ?></a>
								<?php } else { ?>
									<i class="fa fa-calendar"></i>
									<span class="date updated">
										<a href="<?php echo esc_url( get_day_link( get_the_date( 'Y' ), get_the_date( 'm' ), get_the_date( 'd' ) ) ) ?>"><?php echo esc_html( get_the_date( 'M j, Y' ) ); ?></a>
									</span>
								<?php } ?>
							</div><!-- /.post-meta-date -->
						</div><!-- /.post-meta-elements -->
						<div class="content">
							<p><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( reendex_read_more( get_the_content(), $author_related_content_length ) ); ?></a></p>
						</div>				
					</div><!-- /.item-content -->
				</div><!-- /.single-related-posts -->
			</div><!-- /.item-block -->
		<?php } // End while().
		wp_reset_postdata(); ?>
	</div><!-- /.news-block -->
</div><!-- /.module-wrapper -->
