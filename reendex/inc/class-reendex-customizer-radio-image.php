<?php
/**
 * Control - Radio Images.
 *
 * @package Reendex
 */

if ( class_exists( 'WP_Customize_Control' ) ) {
	/**
	 * Custom Control for Customizer Layout Settings.
	 *
	 * @since Reendex 1.0
	 *
	 * @see WP_Customize_Control
	 */
	class Reendex_Customizer_Radio_Image extends WP_Customize_Control {

		/**
		 * The type of customize control being rendered.
		 *
		 * @since  1.0
		 * @access public
		 * @var    string
		 */
		public $type = 'radio-image';

		 /**
		  * Loads the framework scripts/styles.
		  *
		  * @since  1.0
		  * @access public
		  * @return void
		  */
		public function enqueue() {
			wp_enqueue_style( 'reendex-radio-image', trailingslashit( get_template_directory_uri() ) . 'css/reendex-control-radio-image.css', '', time() );
		}

		/**
		 * Render the control to be displayed in the Customizer.
		 *
		 * @since Reendex 1.0
		 */
		public function render_content() {
			if ( empty( $this->choices ) ) {
				return;
			}
			$name = '_customize-radio-' . $this->id;
			?>
			<span class="customize-control-title">
				<?php echo esc_attr( $this->label ); ?>
			</span>
			<?php if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
			<?php endif; ?>
			<div id="input_<?php echo esc_attr( $this->id ); ?>" class="image">
				<?php foreach ( $this->choices as $value => $label ) : ?>
					<label for="<?php echo esc_attr( $this->id . $value ); ?>">
						<input class="image-select" type="radio"
							value="<?php echo esc_attr( $value ); ?>"
							id="<?php echo esc_attr( $this->id . $value ); ?>"
							name="<?php echo esc_attr( $name ); ?>"
							<?php $this->link();
								checked( $this->value(), $value );
							?>>
							<img src="<?php echo esc_html( $label ); ?>" alt="<?php echo esc_attr( $value ); ?>" title="<?php echo esc_attr( $value ); ?>">
						</input>
					</label>
				<?php endforeach; ?>
			</div><!-- /#input -->
			<?php
		}
	}
} // End if().
