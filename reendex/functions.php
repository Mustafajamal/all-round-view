<?php
/**
 * Reendex functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Reendex
 */

if ( ! function_exists( 'reendex_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function reendex_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Reendex, use a find and replace
		 * to change 'reendex' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'reendex', get_template_directory() . '/languages' );
		// This theme styles the visual editor with editor-style.css to match the theme style.
		add_editor_style( 'css/custom-editor-style.css' );
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'video',
			'audio',
		) );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'reendex_news1_thumb', 576, 288, true );
		add_image_size( 'reendex_news4_thumb', 394, 217, true );
		add_image_size( 'reendex_news6_thumb_1', 820, 440, true );
		add_image_size( 'reendex_news6_thumb_2', 400, 288, true );
		add_image_size( 'reendex_carousel_news_thumb', 108, 108, true );
		add_image_size( 'reendex_video_gallery_thumb', 350, 247, true );
		add_image_size( 'reendex_news_gallery_thumb', 400, 240, true );
		add_image_size( 'reendex_single_post_thumb', 822 );
		add_image_size( 'reendex_feed_thumb', 400, 200, true );
		add_image_size( 'reendex_slider1_thumb_1', 590, 443, true );
		add_image_size( 'reendex_slider1_thumb_2', 275, 443, true );
		add_image_size( 'reendex_slider1_thumb_3', 374, 215, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(array(
			'mainmenu'          => esc_html__( 'Main Menu', 'reendex' ),
			'mainmenubottom'    => esc_html__( ' Main Menu Bottom', 'reendex' ),
			'worldmenu'         => esc_html__( 'World Bottom Menu', 'reendex' ),
			'newsmenu'          => esc_html__( 'News Bottom Menu', 'reendex' ),
			'sportmenu'         => esc_html__( 'Sport Bottom Menu', 'reendex' ),
			'healthmenu'        => esc_html__( 'Health Bottom Menu', 'reendex' ),
			'travelmenu'        => esc_html__( 'Travel Bottom Menu', 'reendex' ),
			'entertainmentmenu' => esc_html__( 'Art & Entertainment Bottom Menu', 'reendex' ),
			'offcanvasmenu'     => esc_html__( 'Off-Canvas Menu', 'reendex' ),
		));

		/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
		add_theme_support('html5', array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		));

		// Set up the WordPress core custom background feature.
		add_theme_support('custom-background', apply_filters('reendex_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		)));

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
add_action( 'after_setup_theme', 'reendex_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function reendex_content_width() {
	if ( ! isset( $content_width ) ) {
		$content_width = 640;
	}
}
add_action( 'after_setup_theme', 'reendex_content_width', 0 );

if ( ! function_exists( 'reendex_fonts_url' ) ) :
	/**
	 * Register Google fonts
	 *
	 * @return string Google fonts URL for the theme.
	 */
	function reendex_fonts_url() {
		$fonts_url = '';
		$fonts     = array();
		$subsets   = 'latin,latin-ext';

		/* translators: If there are characters in your language that are not supported by Roboto, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== esc_html_x( 'on', 'Roboto font: on or off', 'reendex' ) ) {
			$fonts[] = 'Roboto:300,400,500,700,900';
		}

		/* translators: If there are characters in your language that are not supported by Roboto Slab, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== esc_html_x( 'on', 'Roboto Slab: on or off', 'reendex' ) ) {
			$fonts[] = 'Roboto Slab:300,400,700';
		}

		/* translators: If there are characters in your language that are not supported by Roboto Condensed, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== esc_html_x( 'on', 'Roboto Condensed: on or off', 'reendex' ) ) {
			$fonts[] = 'Roboto Condensed:300,400,700';
		}

		if ( $fonts ) {
			$fonts_url = add_query_arg( array(
				'family' => rawurlencode( implode( '|', $fonts ) ),
				'subset' => rawurlencode( $subsets ),
			), '//fonts.googleapis.com/css' );
		}
		return $fonts_url;
	}
endif;

/**
 * Enqueue scripts and styles.
 */
function reendex_scripts() {

	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'reendex-fonts', reendex_fonts_url(), array(), null );

	// LOAD CSS.
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'pe-icon-7-stroke', get_template_directory_uri() . '/css/pe-icon-7-stroke.css' );
	wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/css/flexslider.min.css' );
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.min.css' );
	wp_enqueue_style( 'lightbox', get_template_directory_uri() . '/css/lightbox.min.css' );
	wp_enqueue_style( 'bootstrap-select', get_template_directory_uri() . '/css/bootstrap-select.min.css' );
	wp_enqueue_style( 'jquery-ui', get_template_directory_uri() . '/css/jquery-ui.min.css' );
	wp_enqueue_style( 'weather-icons', get_template_directory_uri() . '/css/weather-icons.min.css' );
	wp_enqueue_style( 'reendex-colors', get_template_directory_uri() . '/css/colors.css' );
	wp_enqueue_style( 'responsive-menu', get_template_directory_uri() . '/css/wprmenu.css', array(), '1.01' );
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css' );
	wp_enqueue_style( 'jquery-magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css', array(), '' );

	// WP style.
	wp_enqueue_style( 'reendex-style', get_stylesheet_uri() );
	wp_style_add_data( 'reendex-style', 'rtl', 'replace' );

	// Responsive Style.
	wp_enqueue_style( 'reendex-responsive', get_template_directory_uri() . '/css/responsive.css' );
	wp_enqueue_style( 'reendex-custom-style', get_template_directory_uri() . '/css/custom-style.css',array(),time() );
	// LOAD JS.
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '3.3.7', true );
	wp_enqueue_script( 'jquery-flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array( 'jquery' ), '2.6.3', true );
	wp_enqueue_script( 'jquery-easing', get_template_directory_uri() . '/js/jquery.easing.min.js', array(), '1.4.0', true );
	wp_enqueue_script( 'jquery-parallax', get_template_directory_uri() . '/js/jquery.parallax.min.js', array(), '1.1.3', true );
	wp_enqueue_script( 'jquery-newsTicker-js', get_template_directory_uri() . '/js/jquery.easy-ticker.min.js', array( 'jquery' ), '2.0', true );
	wp_enqueue_script( 'jquery-ui-totop', get_template_directory_uri() . '/js/jquery.ui.totop.min.js', array( 'jquery' ), '1.2', true );
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), '2.2.1', true );
	wp_enqueue_script( 'jquery-fitvid', get_template_directory_uri() . '/js/jquery.fitvids.min.js', array( 'jquery' ), '1.1', true );
	wp_enqueue_script( 'lightbox', get_template_directory_uri() . '/js/lightbox.min.js', array( 'jquery' ), '2.9.0', true );
	wp_enqueue_script( 'bootstrap-select', get_template_directory_uri() . '/js/bootstrap-select.min.js', array( 'jquery' ), '1.12.4', true );
	wp_enqueue_script( 'headroom', get_template_directory_uri() . '/js/headroom.min.js', array( 'jquery' ), '0.9.4', true );
	wp_enqueue_script( 'jquery-countdown', get_template_directory_uri() . '/js/jquery.countdown.min.js', array( 'jquery' ), '2.0.4', true );
	wp_enqueue_script( 'reendex-main', get_template_directory_uri() . '/js/reendex-main.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'reendex-navigation', get_template_directory_uri() . '/js/navigation.min.js', array(), '20151215', true );
	wp_enqueue_script( 'reendex-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'theiaStickySidebar', get_template_directory_uri() . '/js/theia-sticky-sidebar.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'resizesensor', get_template_directory_uri() . '/js/ResizeSensor.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'player', get_template_directory_uri() . '/js/player.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'jquery-magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array( 'jquery' ), '', true );
	// Responsive menu.
	wp_enqueue_script( 'jquery-transit', get_template_directory_uri() . '/js/jquery.transit.min.js', array( 'jquery' ), '0.9.12', true );
	wp_enqueue_script( 'sidr', get_template_directory_uri() . '/js/jquery.sidr.min.js', array( 'jquery' ), '2.2.1', true );

	wp_enqueue_script( 'wprmenu', get_template_directory_uri() . '/js/wprmenu.js', array( 'jquery' ), '2017-03-16', true );
	$wpr_options = array(
		'zooming' => get_theme_mod( 'zooming', 'enable' ),
		'swipe'   => get_theme_mod( 'swipe', 'disable' ),
	);
	wp_localize_script( 'wprmenu', 'wprmenu', $wpr_options );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'reendex_scripts' );

/**
 * Enqueue AdSense script.
 */
function reendex_adsense() {
	wp_enqueue_script( 'reendex-adsense', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js', false );
}
add_action( 'wp_enqueue_scripts', 'reendex_adsense' );

/**
 * Enqueue Admin style.
 */
function reendex_admin_styles() {
	wp_enqueue_style( 'reendex-custom-wp-admin-css', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
	
}
add_action( 'admin_enqueue_scripts', 'reendex_admin_styles' );

/**
 * Enqueue script for custom customize control.
 */
function reendex_custom_customize_enqueue() {
	wp_enqueue_style( 'reendex-customizer', get_template_directory_uri() . '/css/reendex-customizer.css' );
	wp_enqueue_script( 'reendex-customizer-scripts', get_template_directory_uri() . '/js/reendex-customizer-scripts.js', array( 'jquery', 'customize-controls' ), '20160714', true );
}
add_action( 'customize_controls_enqueue_scripts', 'reendex_custom_customize_enqueue' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom styles
 */
require get_template_directory() . '/inc/styles.php';

/**
 * Load customizer custom functions.
 */
require get_template_directory() . '/inc/sanitize-callback.php';
require get_template_directory() . '/inc/custom-functions.php';
require get_template_directory() . '/inc/class-reendex-customizer-radio-image.php';
require get_template_directory() . '/inc/class-reendex-customizer-switch.php';
require get_template_directory() . '/inc/class-reendex-customizer-section.php';
require get_template_directory() . '/inc/reendex-ads.php';

// Default Options.
require get_template_directory() . '/inc/default-options.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Add structural hooks.
 */
require get_template_directory() . '/inc/structure.php';

/**
 * Widget.
 */
require get_template_directory() . '/inc/widget/register-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-recent-feed-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-recent-post-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-follow-us-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-about-us-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-tag-cloud-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-contact-info-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-latest-events-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-weather-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-news-ticker-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-instagram-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-posts-slider-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-posts-list-widget.php';
require get_template_directory() . '/inc/widget/class-reendex-video-widget.php';

/**
 * TGM Plugin.
 */
require get_template_directory() . '/inc/tgm-plugin/class-tgm-plugin-activation.php';
require get_template_directory() . '/inc/tgm-plugin/required-plugins.php';

/**
 * Custom Meta box 2
 */
if ( file_exists( get_template_directory() . '/inc/reendex-metabox.php' ) ) {
	require_once get_template_directory() . '/inc/reendex-metabox.php';
}

/**
 * Menu.
 */
require_once get_template_directory() . '/inc/class-reendex-bootstrap-navwalker.php';

/**
 * Adding Responsive menu.
 */
include get_template_directory() . '/inc/responsive-menu.php';

/**
 * News Ticker
 */
require get_template_directory() . '/inc/reendex-news-ticker.php';

/**
 * VC-EXTEND.
 */
require get_template_directory() . '/inc/vc-extends.php';

/**
 * Post Format input field display by jquery
 */
function reendex_post_formate_field_script() {
	if ( get_post_type() == 'post ' ) :
		wp_enqueue_script( 'reendex-post-formats', get_template_directory_uri() . '/js/reendex-post-formats.js', array( 'jquery' ), false, true );
endif;
}
add_action( 'admin_enqueue_scripts', 'reendex_post_formate_field_script', 1000 );

/**
 * Content excerpt
 *
 * @param string $content The post content.
 * @param string $max_chars_limit Limit number of characters.
 */
if ( ! function_exists( 'reendex_read_more' ) ) {
	/**
	 * Content characters count
	 *
	 * @param string $str The string being parsed.
	 * @param string $n Limit number of characters.
	 * @param string $end_char End character.
	 */
	function reendex_read_more( $str, $n = 500, $end_char = '&#8230;' ) {
		if ( strlen( $str ) < $n ) {
			return $str;
		}
		$str = strip_tags( preg_replace( '!\s+!', ' ', $str ) );
		if ( strlen( $str ) <= $n ) {
			return $str;
		}
		$out = '';
		foreach ( explode( ' ', trim( $str ) ) as $val ) {
			$out .= $val . ' ';
			if ( strlen( $out ) >= $n ) {
				$out = trim( $out );
				return (strlen( $out ) == strlen( $str ) ) ? $out : $out . $end_char;
			}
		}
	}
}

/**
 * Title words count
 *
 * @param string $limit Limit number of words.
 */
function reendex_short_title( $limit = 8 , $after_title ) {
	$after_title = '...';
	$content = explode( ' ', get_the_title() );
	$count   = array_slice( $content, 0, $limit );
	$word_count = count($content);
	if($word_count > $limit){
		array_push($count,$after_title);
	}
	echo implode( ' ', wp_kses( $count, array() ) );
}

/*
 * Display the related posts
 */
if ( ! function_exists( 'reendex_related_posts_function' ) ) {
	/**
	 * Related Posts by category and tags.
	 */
	function reendex_related_posts_function() {
		wp_reset_postdata();
		global $post;
		$options = reendex_get_theme_options();
		$posts_per_page_option = $options['reendex_related_category_number'];
			// Define shared post arguments.
			$args = array(
			'no_found_rows'            => true,
			'update_post_meta_cache'   => false,
			'update_post_term_cache'   => false,
			'ignore_sticky_posts'      => 1,
			'orderby'                  => 'rand',
			'post__not_in'             => array( $post->ID ),
			'posts_per_page'           => $posts_per_page_option,
			);
		// Related by categories.
		if ( get_theme_mod( 'reendex_related_posts', 'categories' ) == 'categories' ) {
			$cats = get_post_meta( $post->ID, 'related-posts', true );
			if ( ! $cats ) {
				$cats = wp_get_post_categories(
					$post->ID, array(
						'fields' => 'ids',
					)
				);
				$args['category__in'] = $cats;
			} else {
				$args['cat'] = $cats;
			}
		}
		// Related by tags.
		if ( get_theme_mod( 'reendex_related_posts', 'categories' ) == 'tags' ) {
			$tags = get_post_meta( $post->ID, 'related-posts', true );
			if ( ! $tags ) {
				$tags = wp_get_post_tags(
					$post->ID, array(
						'fields' => 'ids',
					)
				);
				$args['tag__in'] = $tags;
			} else {
				$args['tag_slug__in'] = explode( ',', $tags );
			}
			if ( ! $tags ) {
				$break = true;
			}
		}
		$query = ! isset( $break )?new WP_Query( $args ):new WP_Query;
		return $query;
	}
} // End if().

/*
 * Display the related videos
 */
if ( ! function_exists( 'reendex_related_videos_function' ) ) :
	/**
	 * Related Videos by category and tags.
	 */
	function reendex_related_videos_function() {
		$options = reendex_get_theme_options();
		$videos_per_page_option = $options['reendex_video_related_number'];
		global $post;
		// Define shared post arguments.
		$args = array(
			'post_type'                 => 'our-video',
			'no_found_rows'             => true,
			'update_post_meta_cache'    => false,
			'update_post_term_cache'    => false,
			'ignore_sticky_posts'       => 1,
			'orderby'                   => 'rand',
			'post__not_in'              => array( $post->ID ),
			'posts_per_page'            => $videos_per_page_option,
		);

		// Related by categories.
		if ( get_theme_mod( 'reendex_related_videos', 'categories' ) == 'categories' ) {
			$cats = wp_get_post_categories( $post->ID, array(
				'fields' => 'ids',
			) );
			$args['category__in'] = $cats;
		}

		// Related by tags.
		if ( get_theme_mod( 'reendex_related_videos', 'categories' ) == 'tags' ) {
			$post_type = 'our-video';
			$tax = 'thcat_taxonomy';
			$tax_terms = get_the_terms( $post->ID, $tax );
			if ( $tax_terms ) {
				$tax_term_ids = array();
				foreach ( $tax_terms as $tax_term ) {
					$tax_term_ids[] = $tax_term->term_id;
				}
				$args = array(
					'post_type' => $post_type,
					'tax_query' => array(
						array(
							'taxonomy'  => $tax,
							'terms'     => $tax_term_ids,
						),
					),
					'post__not_in'      => array( $post->ID ),
					'post_status'       => 'publish',
					'orderby'           => 'rand',
					'posts_per_page'    => $videos_per_page_option,
				);
			}
		}
		$query = null;
		$query = new WP_Query( $args );
		return $query;
	}
endif;

/**
 * Limit the max number of characteres in the title
 *
 * @param string $before Optional. Content to prepend to the title.
 * @param string $after Optional. Content to append to the title.
 * @param bool   $echo Optional, default to true. Whether to display or return.
 * @param string $length The maximum number of characters.
 */
function reendex_the_title_excerpt( $before = '', $after = '', $echo = true, $length = false ) {
	$title = get_the_title();

	if ( $length && is_numeric( $length ) ) {
		$title = substr( $title, 0, $length );
	}
	if ( strlen( $title ) > 0 ) {
		$title = apply_filters( 'reendex_the_title_excerpt', $before . $title . $after, $before, $after );
		if ( $echo ) {
			echo esc_html( $title );
		} else {
			return $title;
		}
	}
}

if ( ! function_exists( 'reendex_the_excerpt' ) ) :

	/**
	 * Generate excerpt.
	 *
	 * @since 1.0
	 *
	 * @param int     $length Excerpt length in words.
	 * @param WP_Post $post_obj WP_Post instance (Optional).
	 * @return string Excerpt.
	 */
	function reendex_the_excerpt( $length = 0, $post_obj = null ) {
		global $post;
		if ( is_null( $post_obj ) ) {
			$post_obj = $post;
		}
		$length = absint( $length );
		if ( 0 === $length ) {
			return;
		}
		$source_content = $post_obj->post_content;
		if ( ! empty( $post_obj->post_excerpt ) ) {
			$source_content = $post_obj->post_excerpt;
		}
		$source_content  = preg_replace( '`\[[^\]]*\]`', '', $source_content );
		$trimmed_content = wp_trim_words( $source_content, $length, '&hellip;' );
		return $trimmed_content;
	}
endif;

add_filter( 'comment_form_default_fields','reendex_comment_default_fields' );

/**
 * Display the classes for the body element.
 *
 * @param string|array $classes One or more classes to add to the class list.
 */
function reendex_body_class( $classes ) {
	$reendex_time_type = get_theme_mod( 'reendex_time_type', 'client-side' );
	$reendex_time_format = get_theme_mod( 'reendex_time_format', '12' );
	if ( 'client-side' === $reendex_time_type ) {
		$classes[] = 'reendex_live_time';
	}
	if ( '12' === $reendex_time_format ) {
		$classes[] = 'reendex_time_in_twelve_format';
	}
	return $classes;
}
add_filter( 'body_class', 'reendex_body_class' );

/**
 * Add classes to embedded videos to get them responsive
 *
 * @param string $html Oembed html to filter.
 */
function reendex_responsive_video( $html ) {
	// Wrap in div element and return #3 and #4.
	return'<div class="fitvids-video">' . $html . '</div>';
}
add_filter( 'embed_oembed_html','reendex_responsive_video', 10, 1 );

/**
 * Override default vimeo oembed behavior
 */
function reendex_set_vimeo_defaults() {
	// set vimeo oembed args.
	// see full list here: developer.vimeo.com/apis/oembed.
	$args = array(
		'color'     => 'ffffff',
		'title'     => false,
		'portrait'  => false,
		'byline'    => false,
		'api'       => true,
		'player_id' => uniqid(),
	);
	// set regex and oembed url.
	$provider_regex = '#https?://(.+\.)?vimeo\.com/.*#i';
	$oembed_source = 'https://vimeo.com/api/oembed.{format}?' . http_build_query( $args );
	// override the default vimeo configuration.
	return wp_oembed_add_provider( $provider_regex, $oembed_source, true );
}
add_action( 'init', 'reendex_set_vimeo_defaults' );

/**
 * Exclude Pages from Search Results
 */
function reendex_remove_post_type_page_from_search() {
	global $wp_post_types;
	$wp_post_types['page']->exclude_from_search = true;
}
add_action( 'init', 'reendex_remove_post_type_page_from_search' );

/**
 * Comment form
 *
 * @param string $default The default value to use.
 */
function reendex_comment_default_fields( $default ) {
	$name_placeholder = esc_attr__( 'Your Name','reendex' );
	$email_placeholder = esc_attr__( 'Your Email','reendex' );
	$message_placeholder = esc_attr__( 'Comment','reendex' );
	$default['author'] = '<div class="row no-gutter">
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<input name="author" type="text" class="form-control" placeholder="' . $name_placeholder . '">
									</div>
								</div>';
	$default['email'] = '		<div class="col-md-6 col-sm-6 col-xs-12">
									<input  name="email"  type="text" class="form-control" placeholder="' . $email_placeholder . '">
								</div>
							</div>';
	$default['url'] = '';
	$default['message'] = '<div class="row no-gutter">
								<div class="col-md-12">
									<textarea name="comment" class="form-control" rows="5" placeholder="' . $message_placeholder . '" title="' . $message_placeholder . '"></textarea>
								</div>
							</div>';
	return $default;
}

add_filter( 'comment_form_defaults', 'reendex_form_default' );

/**
 * Comment form
 *
 * @param string $default_info The default value to use.
 */
function reendex_form_default( $default_info ) {

	$message_placeholder = esc_attr__( 'Comment','reendex' );
	$comment_button_text = esc_attr__( 'submit','reendex' );

	if ( ! is_user_logged_in() ) {
		$default_info['comment_field'] = '';
	} else {
		$default_info['comment_field'] = '<textarea name="comment" class="form-control" rows="5" placeholder="' . $message_placeholder . '" title="' . $message_placeholder . '"></textarea>';
	}

	$default_info['submit_button']          = '<button type="submit" class="btn btn-primary btn-black">' . $comment_button_text . '</button>';
	$default_info['submit_field']           = '<div class="leave_comment">%1$s %2$s</div>';
	$default_info['comment_notes_before']   = '';
	$default_info['title_reply']            = 'Please Comment';
	$default_info['title_reply_before']     = '<div class="comment-title title-style01"><h3>';
	$default_info['title_reply_after']      = '</h3></div>';

	return $default_info;
}

/**
 * Custom comments list
 *
 * @param string  $comment The comments.
 * @param array   $args The arguments.
 * @param integer $depth The depth of the comments.
 */
function reendex_comment_reply( $comment, $args, $depth ) {
	if ( 'pingback' == get_comment_type() || 'trackback' == get_comment_type() ) { ?>
		<li class="comment-list">
			<div class="comment clearfix">
				<div class="comment-content">
					<div class="comment-title">
						<cite><?php comment_author_link() ?></cite>
							<div class="comment-date"><i class="fa fa-calendar"></i><span class="day"><?php comment_date( 'M d, Y' ); ?></span></div>
							<div class="clear"></div>
					</div><!-- /.comment-title -->
				</div><!-- /.comment-content -->
				<div class="comment-btn btn btn-default">
					<?php
					if ( current_user_can( 'moderate_comments' ) ) {
						edit_comment_link( esc_html__( 'Edit', 'reendex' ) );
					}
					?>
				</div><!-- /.comment-btn btn btn-default -->
			</div><!-- /.comment clearfix -->
		</li><!-- /.comment-list -->
	<?php } elseif ( 'comment' == get_comment_type() ) { ?>
		<li class="comment-list">
			<div class="comment clearfix">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<?php
						if ( 0 != $args['avatar_size'] ) {
							echo get_avatar( $comment, $args['avatar_size'] );
						}
						?>
						<?php printf( wp_kses_post( '%s <span class="says">says:</span>', '%s = comment author link', 'reendex' ),
							sprintf( '<cite class="fn">%s</cite>', get_comment_author_link( $comment ) )
						);
						?>
					</div><!-- /.comment-author vcard -->
					<div class="comment-metadata">
						<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
							<time datetime="<?php comment_time( 'c' ); ?>">
								<?php /* translators: 1: date, 2: time */
									printf( esc_html__( '%1$s at %2$s', 'reendex' ), get_comment_date( 'M d, Y' ), get_comment_time() );
								?>
							</time>
						</a>
					</div><!-- /.comment-metadata -->
					<?php if ( '0' === $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'reendex' ); ?></p>
					<?php endif; ?>
				</footer><!-- /.comment-meta -->
				
				<div class="comment-content">
					<p><?php comment_text(); ?></p>
				</div>
				<div class="comment-btn btn btn-default">
					<?php edit_comment_link( esc_html__( 'Edit', 'reendex' ) ); ?>
				</div>				
				<div class="comment-btn btn btn-default">
					<?php
						comment_reply_link(
							array_merge($args,array(
								'reply_text' => esc_html__( 'Reply', 'reendex' ),
								'depth'      => $depth,
								'max_depth'  => $args['max_depth'],
							))
						);
					?>
				</div><!-- /.comment-btn btn btn-default -->
			</div><!-- /.comment clearfix -->
		</li><!-- /.comment-list -->
		<?php
} // End if().
}

/**
 * Breadcrumbs
 */
function reendex_custom_breadcrumbs() {
	// Settings.
	$separator      = ' / ';
	$breadcrums_id  = esc_attr( 'breadcrumbs' );
	$home_title     = get_bloginfo( 'name' );
	// If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat).
	$custom_taxonomy    = 'product_cat';
	// Get the query & post information.
	global $post,$wp_query;
	// Do not display on the homepage.
	if ( ! is_front_page() ) {
		// Build the breadcrums.
		echo '<div class="breadcrumb">';
		// Home page.
		echo '<a class="bread-link bread-home" href="' . esc_url( home_url() ) . '" title="' . esc_attr( $home_title ) . '">' . esc_html( $home_title ) . '</a>';
		echo ' ' . esc_html( $separator ) . ' ';
		if ( is_archive() && ! is_tax() && ! is_category() && ! is_tag() ) {
			echo '<span class="bread-current bread-archive">' . esc_html__( 'Archive','reendex' ) . '</span>';
		} elseif ( is_archive() && is_tax() && ! is_category() && ! is_tag() ) {
			// If post is a custom post type.
			$post_type = get_post_type();
			// If it is a custom post type display name and link.
			if ( 'post' != $post_type ) {
				$post_type_object = get_post_type_object( $post_type );
				$post_type_archive = get_post_type_archive_link( $post_type );
				echo '<a class="bread-cat bread-custom-post-type-' . esc_attr( $post_type ) . '" href="' . esc_url( $post_type_archive ) . '" title="' . esc_attr( $post_type_object->labels->name ) . '">' . esc_html( $post_type_object->labels->name ) . '</a>';
				echo ' ' . esc_html( $separator );
			}
			$custom_tax_name = get_queried_object()->name;
			echo '<span class="bread-current bread-archive">' . esc_html( $custom_tax_name ) . '</span>';
		} elseif ( is_single() ) {
			// If post is a custom post type.
			$post_type = get_post_type();
			// If it is a custom post type display name and link.
			if ( 'post' != $post_type && 'attachment' != $post_type ) {
				$post_type_object = get_post_type_object( $post_type );
				$post_type_archive = get_post_type_archive_link( $post_type );
				echo '<a class="bread-cat bread-custom-post-type-' . esc_attr( $post_type ) . '" href="' . esc_url( $post_type_archive ) . '" title="' . esc_attr( $post_type_object->labels->name ) . '">' . esc_html( $post_type_object->labels->name ) . '</a>';
				echo esc_html( $separator );
			}
			// Get post category info.
			$category = get_the_category();
			if ( ! empty( $category ) ) {
				// Get last category post is in.
				$last_category = end( $category );
				// Get parent any categories and create array.
				$get_cat_parents = rtrim( get_category_parents( $last_category->term_id, true, ',' ),',' );
				$cat_parents = explode( ',',$get_cat_parents );
				// Loop through parent categories and store in variable $cat_display.
				$cat_display = '';
				foreach ( $cat_parents as $parents ) {
					$cat_display .= $parents;
					$cat_display .= $separator;
				}
			}
			// If it's a custom post type within a custom taxonomy.
			$taxonomy_exists = taxonomy_exists( $custom_taxonomy );
			if ( empty( $last_category ) && ! empty( $custom_taxonomy ) && $taxonomy_exists ) {
				$taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
				$cat_id = $taxonomy_terms[0]->term_id;
				$cat_nicename = $taxonomy_terms[0]->slug;
				$cat_link = get_term_link( $taxonomy_terms[0]->term_id, $custom_taxonomy );
				$cat_name = $taxonomy_terms[0]->name;
			}
			// Check if the post is in a category.
			if ( ! empty( $last_category ) ) {

				$allowed_html = array(
					'li' => array(
						'class' => array(),
					),
					'a' => array(
						'href' => array(),
					),
				);

				echo wp_kses( $cat_display , $allowed_html );
				echo '<span class="bread-current bread-' . esc_attr( $post->ID ) . '" title="' . get_the_title() . '">' . get_the_title() . '</span>';
				// Else if post is in a custom taxonomy.
			} elseif ( ! empty( $cat_id ) ) {
				echo '<a class="bread-cat bread-cat-' . esc_attr( $cat_id ) . ' bread-cat-' . esc_attr( $cat_nicename ) . '" href="' . esc_url( $cat_link ) . '" title="' . esc_attr( $cat_name ) . '">' . esc_html( $cat_name ) . '</a></li>';
				echo esc_html( $separator );
				echo '<span class="bread-current bread-' . esc_attr( $post->ID ) . '" title="' . get_the_title() . '">' . get_the_title() . '</span>';
			} else {
				echo '<span class="bread-current bread-' . esc_attr( $post->ID ) . '" title="' . get_the_title() . '">' . get_the_title() . '</span>';
			}
		} elseif ( is_category() ) {
			// Category page.
			echo '<span class="item-current item-cat"><span class="bread-current bread-cat">' . single_cat_title( '', false ) . '</span></span>';
		} elseif ( is_page() ) {
			// Standard page.
			if ( $post->post_parent ) {
				// If child page, get parents.
				$anc = get_post_ancestors( $post->ID );
				// Get parents in the right order.
				$anc = array_reverse( $anc );
				$parents = '';
				// Parent page loop.
				foreach ( $anc as $ancestor ) {
					$parents .= '<a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink( $ancestor ) . '" title="' . get_the_title( $ancestor ) . '">' . get_the_title( $ancestor ) . '</a>';
					$parents .= $separator;
				}
				// Display parent pages.
				echo wp_kses(
					$parents,
					array(
						'li' => array(
							'class' => array(),
						),
						'a' => array(
							'class' => array(),
							'href'  => array(),
							'title' => array(),
						),
					)
				);
				// Current page.
				echo '<span title="' . get_the_title() . '"> ' . get_the_title() . '</span>';
			} else {
				// Just display current page if not parents.
				echo '<span class="bread-current bread-' . esc_attr( $post->ID ) . '"> ' . get_the_title() . '</span>';
			}
		} elseif ( is_tag() ) {
			echo '<span class="bread-current bread-tag-">';
				single_tag_title();
			echo'</span>';
		} elseif ( is_day() ) {
			// Day archive.
			// Year link.
			echo '<a class="bread-year bread-year-' . esc_attr( get_the_time( 'Y' ) ) . '" href="' . esc_url( get_year_link( get_the_time( 'Y' ) ) ) . '" title="' . esc_attr( get_the_time( 'Y' ) ) . '">' . esc_html( get_the_time( 'Y' ) ) . esc_html__( 'Archives','reendex' ) . '</a>';
			echo esc_html( $separator );
			// Month link.
			echo '<a class="bread-month bread-month-' . esc_attr( get_the_time( 'm' ) ) . '" href="' . esc_url( get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ) ) . '" title="' . esc_attr( get_the_time( 'M' ) ) . '">' . esc_html( get_the_time( 'M' ) ) . esc_html__( 'Archives','reendex' ) . '</a>';
			echo esc_html( $separator );
			// Day display.
			echo '<span class="bread-current bread-' . esc_attr( get_the_time( 'j' ) ) . '"> ' . esc_html( get_the_time( 'jS' ) ) . ' ' . esc_html( get_the_time( 'M' ) ) . esc_html__( 'Archives','reendex' ) . '</span>';
		} elseif ( is_month() ) {
			// Month Archive.
			// Year link.
			echo '<a class="bread-year bread-year-' . esc_attr( get_the_time( 'Y' ) ) . '" href="' . esc_url( get_year_link( get_the_time( 'Y' ) ) ) . '" title="' . esc_attr( get_the_time( 'Y' ) ) . '">' . esc_html( get_the_time( 'Y' ) ) . esc_html__( 'Archives','reendex' ) . '</a>';
			echo esc_html( $separator );
			// Month display.
			echo '<span class="bread-month bread-month-' . esc_attr( get_the_time( 'm' ) ) . '" title="' . esc_attr( get_the_time( 'M' ) ) . '">' . esc_html( get_the_time( 'M' ) ) . esc_html__( 'Archives','reendex' ) . '</span>';
		} elseif ( is_year() ) {
			// Display year archive.
			echo '<span class="bread-current bread-current-' . esc_attr( get_the_time( 'Y' ) ) . '" title="' . esc_attr( get_the_time( 'Y' ) ) . '">' . esc_html( get_the_time( 'Y' ) ) . esc_html__( 'Archives','reendex' ) . '</span>';
		} elseif ( is_author() ) {
			// Auhor archive.
			// Get the author information.
			global $author;
			$userdata = get_userdata( $author );
			// Display author name.
			echo '<span class="bread-current bread-current-' . esc_attr( $userdata->user_nicename ) . '" title="' . esc_attr( $userdata->display_name ) . '">' . esc_html__( 'Author: ','reendex' ) . esc_html( $userdata->display_name ) . '</span>';
		} elseif ( get_query_var( 'paged' ) ) {
			// Paginated archives.
			echo '<span class="bread-current bread-current-' . esc_attr( get_query_var( 'paged' ) ) . '" title="' . esc_attr__( 'Page ','reendex' ) . esc_attr( get_query_var( 'paged' ) ) . '">' . esc_html__( 'Page','reendex' ) . ' ' . esc_html( get_query_var( 'paged' ) ) . '</span>';
		} elseif ( is_search() ) {
			// Search results page.
			echo '<span class="bread-current bread-current-' . get_search_query() . '" title="' . esc_attr__( 'Search results for: ','reendex' ) . get_search_query() . '">' . esc_html__( 'Search results for : ','reendex' ) . get_search_query() . '</span>';
		} elseif ( is_404() ) {
			// 404 page.
			esc_html_e( 'Error 404','reendex' );
		} // End if().
		echo '</div>';
	} // End if().
}

/**
 * Pagination sanitization.
 *
 * @param string $content Pagination content.
 */
function reendex_sanitize_pagination( $content ) {
	// Remove role attribute.
	$content = str_replace( 'role="navigation"', '', $content );

	// Remove h2 tag.
	$content = preg_replace( '#<h2.*?>(.*?)<\/h2>#si', '', $content );

	return $content;
}

add_action( 'navigation_markup_template', 'reendex_sanitize_pagination' );

/**
 * Inline Script for Weather.
 */
function reendex_weather_script() {
	$options = reendex_get_theme_options();
	$reendex_weather_access_key = $options['reendex_weather_access_key'];
	$reendex_wea_api = $options['reendex_wea_api'];
	$reendex_weather_location = $options['reendex_weather_location'];
	wp_localize_script('reendex-main', 'reendex_weather_settings',
		array(
			'reendex_wea_api' 		        => $reendex_wea_api,
			'reendex_weather_location'      => $reendex_weather_location,
			'reendex_weather_access_key'    => $reendex_weather_access_key,
		)
	);
}
add_action( 'wp_enqueue_scripts', 'reendex_weather_script' );

/*
 * Google Map API key.
 */
if ( ! function_exists( 'reendex_google_maps_api_key' ) ) :
	/**
	 * This function adds API key ( if provided in settings ) to google maps.
	 */
	function reendex_google_maps_api_key() {
		/* Get Google Maps API Key if available */
		$google_maps_api_key = get_theme_mod( 'reendex_google_maps_api_key' );
	}
endif;

/**
 * Import demo content.
 */
function reendex_import_files() {
		return array(
			array(
				'import_file_name'              => esc_html__( 'Theme Demo Content', 'reendex' ),
				'local_import_file'             => trailingslashit( get_template_directory() ) . 'democontent/all_content.xml',
				'local_import_widget_file'      => trailingslashit( get_template_directory() ) . 'democontent/widgets.wie',
				'local_import_customizer_file'  => trailingslashit( get_template_directory() ) . 'democontent/customizer.dat',
				'import_preview_image_url'      => get_template_directory_uri() . '/screenshot.jpg',
				'import_notice'                 => esc_html__( 'Please do not close the window or refresh the page until the data is imported.', 'reendex' ),
			),
		);
}
	add_filter( 'pt-ocdi/import_files', 'reendex_import_files' );

// Disable PT branding.
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

/**
 * Define actions that happen after import
 */
function reendex_after_import_setup() {
		// Assign menus to their locations.
		$main_menu_name = esc_html__( 'Main Menu', 'reendex' );
		$main_bottom_menu_name = esc_html__( 'Main Menu Bottom', 'reendex' );
		$world_menu_name = esc_html__( 'World Bottom Menu', 'reendex' );
		$news_menu_name = esc_html__( 'News Bottom Menu', 'reendex' );
		$sport_menu_name = esc_html__( 'Sport Bottom Menu', 'reendex' );
		$health_menu_name = esc_html__( 'Health Bottom Menu', 'reendex' );
		$travel_menu_name = esc_html__( 'Travel Bottom Menu', 'reendex' );
		$entertainment_menu_name = esc_html__( 'Art & Entertainment Bottom Menu', 'reendex' );
		$offcanvas_menu_name = esc_html__( 'Off-Canvas Menu', 'reendex' );

		$main_menu = get_term_by( 'name', $main_menu_name, 'nav_menu' );
		$main_bottom_menu = get_term_by( 'name', $main_bottom_menu_name, 'nav_menu' );
		$world_menu = get_term_by( 'name', $world_menu_name, 'nav_menu' );
		$news_menu = get_term_by( 'name', $news_menu_name, 'nav_menu' );
		$sport_menu = get_term_by( 'name', $sport_menu_name, 'nav_menu' );
		$health_menu = get_term_by( 'name', $health_menu_name, 'nav_menu' );
		$travel_menu = get_term_by( 'name', $travel_menu_name, 'nav_menu' );
		$entertainment_menu = get_term_by( 'name', $entertainment_menu_name, 'nav_menu' );
		$offcanvas_menu = get_term_by( 'name', $offcanvas_menu_name, 'nav_menu' );

		set_theme_mod( 'nav_menu_locations', array(
			'mainmenu'          => $main_menu->term_id,
			'mainmenubottom'    => $main_bottom_menu->term_id,
			'worldmenu'         => $world_menu->term_id,
			'newsmenu'          => $news_menu->term_id,
			'sportmenu'         => $sport_menu->term_id,
			'healthmenu'        => $health_menu->term_id,
			'travelmenu'        => $travel_menu->term_id,
			'entertainmentmenu' => $entertainment_menu->term_id,
			'offcanvasmenu'     => $offcanvas_menu->term_id,
		));
		// Assign front page and posts page (blog page).
		$front_page_id = get_page_by_title( 'Home' );
		$blog_page_id  = get_page_by_title( 'Blog' );
		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', $front_page_id->ID );
		update_option( 'page_for_posts', $blog_page_id->ID );
}
	add_action( 'pt-ocdi/after_import', 'reendex_after_import_setup' );


/**
 * Register our sidebars and widgetized areas.
 *
 */
function innerpage_init() {

	/* register_sidebar( array(
		'name'          => 'Common (Main) Left Panel',
		'id'            => 'innerpage_1',
		'before_widget' => "<div class='innerpage_left home'>",
		'after_widget'  => '</div>',
	) );	 */
	register_sidebar( array(
		'name'          => 'Home (Main) Left Panel',
		'id'            => 'leftsidebarhome',
		'before_widget' => "<div class='home-left-sidebar reendex-widget  widget container-wrapper'>",
		'after_widget'  => '</div>',
	) );
	
	register_sidebar( array(
		'name'          => 'News Updates (Main) Left Panel',
		'id'            => 'leftsidebarnews_updates',
		'before_widget' => "<div class='innerpage_left news_updates'>",
		'after_widget'  => '</div>',
	) );
	
	register_sidebar( array(
		'name'          => 'News Updates (Detail) Left Panel',
		'id'            => 'leftsidebarnews_updates_single',
		'before_widget' => "<div class='mainpage_left news_updates'>",
		'after_widget'  => '</div>',
	) );
	
	register_sidebar( array(
		'name'          => 'Articles (Main) Left Panel',
		'id'            => 'innerpage_articles',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );	
	register_sidebar( array(
		'name'          => 'Articles (Detail) Left Panel',
		'id'            => 'articles_single_page',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );

	
	register_sidebar( array(
		'name'          => 'Reports (Main) Left Panel',
		'id'            => 'innerpage_reports',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => 'Reports (Detail) Left Panel',
		'id'            => 'reports_single_page',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => 'Features (Main) Left Panel',
		'id'            => 'leftsidebarfeaturesmain',
		'before_widget' => "<div class='mainpage_left features'>",
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => 'Features (Detail) Left Panel',
		'id'            => 'leftsidebarfeaturessingle',
		'before_widget' => "<div class='innerpage_left features'>",
		'after_widget'  => '</div>',
	) );
		register_sidebar( array(
		'name'          => 'Interviews (Main) Left Panel',
		'id'            => 'innerpage_interviews',
		'before_widget' => "<div class='mainpage_left interviews '>",
		'after_widget'  => '</div>',
	) );
	
	
	register_sidebar( array(
		'name'          => 'Interviews (Detail) Left Panel',
		'id'            => 'interviews_single_page',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );

/* 	register_sidebar( array(
		'name'          => 'Library (Main) Left Panel',
		'id'            => 'leftsidebarlibrarymain',
		'before_widget' => "<div class='mainpage_left library'>",
		'after_widget'  => '</div>',
	) );



	register_sidebar( array(
		'name'          => 'Library (Detail) Left Panel',
		'id'            => 'leftsidebarlibrarysingle',
		'before_widget' => "<div class='innerpage_left library'>",
		'after_widget'  => '</div>',
	) ); */
	
	register_sidebar( array(
		'name'          => 'Blogs (Main) Left Panel',
		'id'            => 'innerpage_blog',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );
	
	register_sidebar( array(
		'name'          => 'Blog (Detail) Left Panel',
		'id'            => 'blog_single_page',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );	
	
/* 	register_sidebar( array(
		'name'          => 'Magazine (Main) Left Panel',
		'id'            => 'leftsidebarmagazinemain',
		'before_widget' => "<div class='mainpage_left magazine'>",
		'after_widget'  => '</div>',
	) );


	register_sidebar( array(
		'name'          => 'Magazine (Detail) Left Panel',
		'id'            => 'leftsidebarmagazinesingle',
		'before_widget' => "<div class='innerpage_left magazine'>",
		'after_widget'  => '</div>',
	) ); */

	
	register_sidebar( array(
		'name'          => 'Editorial (Main) Left Panel',
		'id'            => 'innerpage_editorial',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => 'Editorial (Detail) Left Panel',
		'id'            => 'editorial_single_page',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => 'In Pictures (Main) Left Panel',
		'id'            => 'innerpage_inpictures',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );
	
	register_sidebar( array(
		'name'          => 'In Pictures (Detail) Left Panel',
		'id'            => 'inpictures_single_page',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );	
	register_sidebar( array(
		'name'          => 'In Videos (Main) Left Panel',
		'id'            => 'innerpage_invideos',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => 'In Videos (Detail) Left Panel',
		'id'            => 'invideos_single_page',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => 'Add-ins (Main) Left Panel',
		'id'            => 'innerpage_addins',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );
	
	register_sidebar( array(
		'name'          => 'Add-ins (Detail) Left Panel',
		'id'            => 'add_ins_single_page',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );
		register_sidebar( array(
		'name'          => 'Left Panel Widgets Control',
		'id'            => 'leftsidebar',
		'before_widget' => "<div class='home-left-sidebar-single-post'>",
		'after_widget'  => '</div>',
	) );
}
add_action( 'widgets_init', 'innerpage_init' );


function headerad_init() {

	register_sidebar( array(
		'name'          => 'Header Ad',
		'id'            => 'header_ad_logo',
		'before_widget' => "<div class='header_ad_logo'>",
		'after_widget'  => '</div>',
	) );

}
add_action( 'widgets_init', 'headerad_init' );

function midnight_portal_init() {

	register_sidebar( array(
		'name'          => 'Allrounview Portal (Detail) left panel',
		'id'            => 'innerpage_midnight_portal',
		'before_widget' => "<div class='innerpage_left'>",
		'after_widget'  => '</div>',
	) );

}
add_action( 'widgets_init', 'midnight_portal_init' );


function wpb_custom_new_menu() {
	  register_nav_menu('header-top-menu',__( 'Header Top Menu' ));
	}
add_action( 'init', 'wpb_custom_new_menu' );

function rudr_posts_taxonomy_filter() {
	global $typenow; // this variable stores the current custom post type
	if( $typenow == 'post' ){ // choose one or more post types to apply taxonomy filter for them if( in_array( $typenow  array('post','games') )
		$taxonomy_names = array('platform', 'device');
		foreach ($taxonomy_names as $single_taxonomy) {
			$current_taxonomy = isset( $_GET[$single_taxonomy] ) ? $_GET[$single_taxonomy] : '';
			$taxonomy_object = get_taxonomy( $single_taxonomy );
			$taxonomy_name = strtolower( $taxonomy_object->labels->name );
			$taxonomy_terms = get_terms( $single_taxonomy );
			if(count($taxonomy_terms) > 0) {
				echo "<select name='$single_taxonomy' id='$single_taxonomy' class='postform'>";
				echo "<option value=''>All $taxonomy_name</option>";
				foreach ($taxonomy_terms as $single_term) {
					echo '<option value='. $single_term->slug, $current_taxonomy == $single_term->slug ? ' selected="selected"' : '','>' . $single_term->name .' (' . $single_term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
	}
}
 
add_action( 'restrict_manage_posts', 'rudr_posts_taxonomy_filter' );

function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
    $return = array();

    if (class_exists('WPSEO_Primary_Term')){
        // Show Primary category by Yoast if it is enabled & set
        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
        $primary_term = get_term($wpseo_primary_term->get_primary_term());

        if (!is_wp_error($primary_term)){
            $return['primary_category'] = $primary_term;
        }
    }

    if (empty($return['primary_category']) || $return_all_categories){
        $categories_list = get_the_terms($post_id, $term);

        if (empty($return['primary_category']) && !empty($categories_list)){
            $return['primary_category'] = $categories_list[0];  //get the first category
        }
        if ($return_all_categories){
            $return['all_categories'] = array();

            if (!empty($categories_list)){
                foreach($categories_list as &$category){
                    $return['all_categories'][] = $category->term_id;
                }
            }
        }
    }

    return $return;
}

add_action( 'load-widgets.php', 'my_custom_load' );

function my_custom_load() {    
    wp_enqueue_style( 'wp-color-picker' );        
    wp_enqueue_script( 'wp-color-picker' );    
}

function pagination($pages = '', $range = 4)
{  
     $showitems = ($range * 2)+1;  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
       
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Neddxt &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</div>\n";
     }
}


add_filter('wp_link_pages_args', 'wp_link_pages_args_prevnext_add');
/**
 * Add prev and next links to a numbered link list
 */
function wp_link_pages_args_prevnext_add($args)
{
    global $page, $numpages, $more, $pagenow;

    if (!$args['next_or_number'] == 'next_and_number') 
        return $args; # exit early

    $args['next_or_number'] = 'number'; # keep numbering for the main part
    if (!$more)
        return $args; # exit early

    if($page-1) # there is a previous page
        $args['before'] .= _wp_link_page($page-1)
            . $args['link_before']. $args['previouspagelink'] . $args['link_after'] . '</a>'
        ;

    if ($page<$numpages) # there is a next page
        $args['after'] = _wp_link_page($page+1)
            . $args['link_before'] . $args['nextpagelink'] . $args['link_after'] . '</a>'
            . $args['after']
        ;

    return $args;
}

function custom_wp_link_pages( $args = '' ) {
	$defaults = array(
		'before' => '<div class="page-links-row"><div class="page-links">' . __(), 
		'after' => '</div></div>',
		'text_before' => '<span>',
		'text_after' => '</span>',
		'next_or_number' => 'number', 
		'nextpagelink' => __( '<span class="page-next-pre">Next Page</span>' ),
		'previouspagelink' => __( '<span class="page-next-pre">Previous Page</span>' ),
		'pagelink' => '%',
		'echo' => 1
	);

	$r = wp_parse_args( $args, $defaults );
	$r = apply_filters( 'wp_link_pages_args', $r );
	extract( $r, EXTR_SKIP );

	global $page, $numpages, $multipage, $more, $pagenow;
	$page_link = get_permalink();
	/* ?><pre><?php print_r($numpages); ?></pre><?php
	?><pre><?php print_r($multipage); ?></pre><?php
	?><pre><?php print_r($page); ?></pre><?php
	?><pre><?php print_r($pagenow); ?></pre><?php
	?><pre><?php print_r($more); ?></pre><?php */
	$starting_point = 1;
	if($page >= 7){
		$starting_point = $page - 4;
	}
	$end_limit = $numpages;
	$start_limit_w = 1;
	$output = '';
	if ( $multipage ) {
		
		if ( 'number' == $next_or_number ) {
			$output .= $before;
			if($page >= 7){
				$output .= '<a href="'.$page_link.'"><span>1</span></a><span class="page_range">....</span>';
			}
			for ( $i = $starting_point; $i < ( $numpages + 1 ); $i = $i + 1 ) {
				
				if($numpages >= 8 ){
					if($page <= 2 ){
						$start_limit = $page + 7;
					}elseif($page >= 3 ){
						$start_limit = $page + 4;
					}elseif($page > 4 ){
						$start_limit = $page + 4;
					}
					$start_limit_range = $page + 1;
					
					
					if($i <= $start_limit && $i != $numpages){
						
						$j = str_replace( '%', $i, $pagelink );
						$output .= ' ';
						if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
							//$output .= _wp_link_page( 1 );
							$output .= _wp_link_page( $i );
						else
							$output .= '<span class="current-post-page">';

						$output .= $text_before . $j . $text_after;
						if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
							$output .= '</a>';
						else
							$output .= '</span>';
						if($i == $start_limit){
							$output .= '<span class="page_range">....</span>';
						}
					}
					
					if($i >= $end_limit){
							$j = str_replace( '%', $i, $pagelink );
							$output .= ' ';
							if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
								$output .= _wp_link_page( $i );
							else
								$output .= '<span class="current-post-page">';

							$output .= $text_before . $j . $text_after;
							if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
								$output .= '</a>';
							else
								$output .= '</span>';
					}
				}else{
					$j = str_replace( '%', $i, $pagelink );
					$output .= ' ';
					if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
						$output .= _wp_link_page( $i );
					else
						$output .= '<span class="current-post-page">';

					$output .= $text_before . $j . $text_after;
					if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
						$output .= '</a>';
					else
						$output .= '</span>';
				}
			}
			$output .= $after;
		} else {
			if ( $more ) {
				$output .= $before;
				$i = $page - 1;
				if ( $i && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before .'dsds'. $previouspagelink .'dsds'. $text_after . '</a>';
				}
				$i = $page + 1;
				if ( $i <= $numpages && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before . $nextpagelink . $text_after . '</a>';
				}
				$output .= $after;
			}
		}
	}

	if ( $echo )
		echo $output;

	return $output;
}