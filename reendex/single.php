<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Reendex
 */

get_header();
$reendex_sidebar = esc_attr( get_post_meta( $post->ID, 'reendex_sidebar_layout', true ) );
if ( empty( $reendex_sidebar ) ) {
	$reendex_sidebar = esc_attr( get_theme_mod( 'reendex_single_post_layout','rightsidebar' ) );
}
if ( 'rightsidebar' == $reendex_sidebar ) {
	$reendex_sidebar = 'left';
} elseif ( 'leftsidebar' == $reendex_sidebar ) {
	$reendex_sidebar = 'left';
} else {
	$reendex_sidebar = 'no';
}

$options = reendex_get_theme_options();
$post_template_value = get_theme_mod( 'global_post_template', 'single' );
$reendex_post_template = get_post_meta( $post->ID, 'reendex_post_template_layout', true );
if ( 'single' == $reendex_post_template || empty( $reendex_post_template ) ) {
	$content_value = $post_template_value;
} else {
	$content_value = $reendex_post_template;
}
?>
<?php
global $post;

// grab categories of current post
$categories = get_the_category($post->ID);
//print_r($categories[0]); 
$parent_cat = $categories[0]->category_parent;
$cat_name = get_cat_name( $parent_cat );
if($parent_cat == 0){
	$category_name = $categories[0]->name;
}else{
	$category_name = get_cat_name( $parent_cat );
}
$term = get_term_by('name', $category_name.' single', 'wplss_logo_showcase_cat');

//$title_ad = get_term_by('slug', $category_slug, 'wplss_logo_showcase_cat');
//$title_ad = 'Home';
$cat_id = $term->term_id;
$cat_name = $term->name;

if ( ! current_user_can( 'edit_themes' ) || ! is_user_logged_in() ) {
	$show_comingsoon = get_theme_mod( 'reendex_comingsoon_show', 'disable' );
	if ( 'disable' !== $show_comingsoon ) {
		get_template_part( 'coming', 'soon' );
		exit();
	}
}
?>
	<div class="module">
		<div class="home-<?php echo esc_attr( $reendex_sidebar ); ?>-side">
			<div id="primary" class="content-area">
				<main id="main" class="site-main all-blogs">
					<?php while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content', $content_value );
					endwhile; ?>
				</main><!-- /#main -->
			</div><!-- /#primary -->
		</div><!-- /.home -->
	</div><!-- /.module -->
<?php
$tags = wp_get_post_tags( $post->ID );
$show_random_posts = get_theme_mod( 'reendex_random_posts_show', 'enable' );
if ('disable' !== $show_random_posts ) :
	get_template_part( 'inc/reendex-random-posts' );
endif;
?>
<div id="fb-root"></div>
<div id="footer_ad_section">
<?php
/* <pre><?php print_r($cat_id); ?></pre>
<pre><?php print_r($cat_name); ?></pre>  */
 echo do_shortcode('[logoshowcase cat_id="'.$cat_id.'" cat_name="'.$cat_name.'" slides_column="6"]'); ?>
</div>
<?php get_footer(); ?>
