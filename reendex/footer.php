<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Reendex
 */

/**
 * Reendex_content_end_action hook
 *
 * @hooked reendex_content_end -  10
 */

do_action( 'reendex_content_end_action' );

/**
 * Reendex_footer_start hook
 *
 * @hooked reendex_footer_start -  10
 */

do_action( 'reendex_footer_start' );

/**
 * Reendex_footer hook
 *
 * @hooked reendex_footer_bottom_start - 11
 * @hooked reendex_copyright -  20
 */

do_action( 'reendex_footer' );

/**
 * Reendex_footer_end hook
 *
 * @hooked reendex_footer_end -  100
 */

do_action( 'reendex_footer_end' );

/**
 * Reendex_page_end_action hook
 *
 * @hooked reendex_page_end -  10
 */

do_action( 'reendex_page_end_action' );

?>
<?php wp_footer(); ?>
</body>
</html>
