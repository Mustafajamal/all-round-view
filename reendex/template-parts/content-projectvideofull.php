<?php
/**
 * The template for displaying posts in the Video post format
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

$options = reendex_get_theme_options();
// Meta box data.
$reendex_metabox_id = get_the_id();
$video_completed = get_post_meta( $reendex_metabox_id,'reendex_video_completed',true );
$client_name = get_post_meta( $reendex_metabox_id,'reendex_client_name',true );
$client_link = get_post_meta( $reendex_metabox_id,'reendex_client_link',true );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container clearfix">
		<div class="video-full">
			<div class="fitvids-video">	        
				<iframe src="<?php echo esc_url( get_post_meta( $reendex_metabox_id, 'reendex_video_link', true ) ); ?>"></iframe>
			</div><!-- /.fitvids-video -->
		</div><!-- /.video-full --> 
		<div class="video-post">
			<div class="col-sm-4 col-md-4">
				<div class="item-content">
					<div class="video-post_content">
						<?php
						$show_video_info = get_theme_mod( 'reendex_show_video_info', 'enable' );
						if ( 'enable' === $show_video_info ) :
						?>						
							<div class="title-left title-style04 underline04">
								<h3><?php esc_html_e( 'Video Info','reendex' );?></h3>
							</div>
							<ul class="meta-data">
								<?php
								$show_video_author = get_theme_mod( 'reendex_show_video_author', 'enable' );
								if ( 'enable' === $show_video_author ) :
								?>
									<li><i class="fa fa-user"></i> <span><?php esc_html_e( 'Created by:','reendex' );?></span> <?php the_author();?></li>
								<?php endif;?>
								<?php
								$show_video_date = get_theme_mod( 'reendex_show_video_date', 'enable' );
								if ( 'enable' === $show_video_date ) :
								?>                             
									<li><i class="fa fa-calendar"></i> <span><?php esc_html_e( 'Completed on:','reendex' );?></span> <?php echo esc_attr( $video_completed );?></li>
								<?php endif;?>
								<?php
								$show_video_skills = get_theme_mod( 'reendex_show_video_skills', 'enable' );
								if ( 'enable' === $show_video_skills ) : ?>                            
									<li><i class="fa fa-lightbulb-o"></i> 
										<span><?php esc_html_e( 'Skills:','reendex' );?></span> 
										<?php
										$skills = get_post_meta( $reendex_metabox_id,'reendex_video_group',true );
										$count = count( $skills );
										$i = 0;
										if ( is_array( $skills ) || is_object( $skills ) ) {
											foreach ( $skills as $skill ) {
												$i++;
												echo esc_html( $skill['reendex_skill_name'] );
												if ( $count === $i ) {
													echo ' ';
												} else { echo ' / ';}
											}
										}
										?>
									</li>
								<?php endif;?>
								<?php
								$show_video_client = get_theme_mod( 'reendex_show_video_client', 'enable' );
								if ( 'enable' === $show_video_client ) : ?>							 
									<li><i class="fa fa-link"></i> <span><?php esc_html_e( 'Client:','reendex' )?></span> <a href="<?php echo esc_url( $client_link );?>"><?php echo esc_attr( $client_name );?></a></li>
								<?php endif;?>							    
							</ul><!-- /.meta-data --> 
							<ul class="social-links list-inline">
								<?php
								$social_media = get_post_meta( get_the_id(),'reendex_social_group',true );
								if ( is_array( $social_media ) ) {
									foreach ( $social_media as $icons ) :?>
										<li>
											<a class="<?php echo esc_html( $icons['reendex_social_icon'] );?>" href="<?php echo esc_url( $icons['reendex_social_link'] );?>">
												<i class="fa fa-<?php echo esc_html( $icons['reendex_social_icon'] );?>"></i>
											</a>
										</li>
									<?php
									endforeach;
								}
								?>
							</ul><!-- /.social-links --> 
						<?php endif;?>						
					</div><!-- /.video-post_content --> 
				</div><!-- /.item-content --> 
			</div><!-- /.col --> 
			<?php if ( 'enable' === $show_video_info ) : ?>
				<div class="col-sm-8 col-md-8">
					<div class="video-post_content">
						<div class="title-left title-style04 underline04">
							<h3><?php the_title();?></h3>
						</div>
						<div class="content">
							<?php the_content();?>
						</div>
					</div>
				</div><!-- /.col --> 
			<?php else : ?>
				<div class="col-sm-12 col-md-12">
					<div class="video-post_content">
						<div class="title-left title-style04 underline04">
							<h3><?php the_title();?></h3>
						</div>
						<div class="content">
							<?php the_content();?>
						</div>
					</div>
				</div><!-- /.col --> 
			<?php endif;?>
		</div><!--/.video-post-->
	</div><!--/.container-->
</article><!-- #post-## -->
