<?php
/**
 * Template part for displaying post style 5.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

$options = reendex_get_theme_options();
$reendex_id = get_the_ID();
$banner_single_image_link = reendex_get_default_single_banner();
$reendex_header_image = get_post_meta( $reendex_id, 'reendex_header_image', true );
$reendex_post_banner = get_post_meta( get_the_ID(), 'reendex_post_banner', true );
$reendex_post_banner_title_show = get_post_meta( get_the_ID(), 'reendex_post_banner_title_show', true );
$reendex_post_banner_title_text = get_post_meta( get_the_ID(), 'reendex_post_banner_title_text', true );
if ( has_post_thumbnail( $post->ID ) && 'yes' == $reendex_header_image ) {
	$banner_image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	$reendex_header_image = $banner_image_array[0];
} else {
	$reendex_header_image = $banner_single_image_link;
}
$reendex_breaking_news = get_post_meta( get_the_ID(), 'reendex_breaking_news', true );
$reendex_share_all = get_post_meta( $reendex_id, 'reendex_share_all', true );
$reendex_display_featured_image = get_post_meta( get_the_ID(), 'reendex_display_featured_image', true );
$reendex_post_meta = get_post_meta( get_the_ID(), 'reendex_post_meta', true );
$disable_post_meta = $options['reendex_single_disable_postmeta'];
$show_avatar = get_theme_mod( 'reendex_avatar_show', 'enable' );
$avatar_size = get_theme_mod( 'reendex_avatar_size', '28' );
$show_date = get_theme_mod( 'reendex_date_show', 'enable' );
$reendex_ext_video = get_post_meta( $post->ID, 'reendex_post_formate_vdo', true );
$reendex_facebook_video = get_post_meta( $post->ID, 'reendex_facebook_video', true );
$allowed_html = array(
	'iframe'    => array(
		'src'           => true,
		'width'         => true,
		'height'        => true,
		'marginwidth'   => true,
		'marginheight'  => true,
		'scrolling'     => true,
		'title'         => false,
	),
);
?>
	<?php
	if ( ( 'show' == $reendex_post_banner ) ) : ?>
		<div class="archive-page-header">
			<div class="container-fluid">
				<div class="archive-nav-inline">
					<div id="parallax-section3">
						<div class="image1 img-overlay3" style="background-image: url('<?php echo esc_url( $reendex_header_image ); ?>'); ">
							<div class="container post-title">
								<?php if ( ( 'hide' != $reendex_post_banner_title_show ) ) : ?>
								<h1 class="page-title"><span><?php echo esc_html( $reendex_post_banner_title_text ); ?></span></h1>
								<?php endif; ?>
								<?php
									$reendex_id = get_the_ID();
									$reendex_page_breadcrumbs = get_post_meta( $reendex_id,'reendex_page_breadcrumbs',true );
								if ( 'show' == $reendex_page_breadcrumbs ) {
									reendex_custom_breadcrumbs();
								}
								?>
							</div><!-- /.container post-title -->
						</div><!-- /.image1 img-overlay3 -->
					</div><!-- /#parallax-section3 -->
				</div><!-- /.archive-nav-inline -->
			</div><!-- /.container-fluid -->
		</div><!-- /.archive-page-header -->
	<?php endif; ?>
<?php
if ( ( 'show' === $reendex_breaking_news ) && ( 'disable' === get_theme_mod( 'reendex_single_breaking_show' ) ) || ! ( 'hide' === $reendex_breaking_news ) && ! ( 'enable' !== get_theme_mod( 'reendex_single_breaking_show' ) ) ) : ?>
	<div class="post-style-5 container breaking-ticker">
	<?php get_template_part( 'template-parts/content','breakingnews' ); ?>
	</div><!-- /.post-style-5 -->
<?php endif;
?>
<div class="post-style-5 container main-container">  
	<article id="post-<?php the_ID(); ?>" class="<?php if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		echo 'home-main-content col-xs-12 col-sm-12 col-md-12';
} else {
	echo 'home-main-content col-xs-12 col-sm-12 col-md-8';
}
	?>">
		<div class="post post-full clearfix">
			<?php if ( 'hide' !== $reendex_display_featured_image ) : ?>
				<div class="entry-media">		
					<?php if ( has_post_format( 'video' ) ) :
						if ( ! empty( $reendex_ext_video ) ) : ?>
						<div class="fitvids-video">
							<?php
								echo wp_kses( wp_oembed_get( $reendex_ext_video ), $allowed_html );
							?>
						</div><!-- /.fitvids-video -->
						<?php endif;
						if ( ! empty( $reendex_facebook_video ) ) : ?>
							<div class="fitvids-video">
								<div class="fb-video" data-href="<?php echo esc_url( $reendex_facebook_video ); ?>"></div>
							</div><!-- /.fitvids-video -->
						<?php endif;
					elseif ( has_post_format( 'audio' ) ) : ?>
						<div class="audio-iframe">
							<?php
							$reendex_audio = get_post_meta( $post->ID, 'reendex_post_formate_audio', true );
							$reendex_audio_str = substr( $reendex_audio, -4 );
							if ( wp_oembed_get( $reendex_audio ) ) :
								echo wp_kses( wp_oembed_get( $reendex_audio ), $allowed_html );
							else :
								echo do_shortcode( '[audio src="' . esc_url( $reendex_audio ) . '"]' );
							endif;
							?>
						</div>
					<?php elseif ( ! has_post_format( 'video' ) && has_post_thumbnail() ) :
						the_post_thumbnail( 'reendex_single_post_thumb' );
					?>
					<?php if ( get_post( get_post_thumbnail_id() )->post_excerpt ) { // Search for if the image has caption added on it. ?>
						<span class="featured-image-caption">
							<?php echo wp_kses_post( get_post( get_post_thumbnail_id() )->post_excerpt ); // Displays the image caption. ?>
						</span>
					<?php }
					endif; ?>
				</div><!-- /.entry-media -->
			<?php endif; ?>
			<div class="entry-main">
			<header class="entry-header">
				<h2 class="entry-title title-left title-style04 underline04"><?php the_title();?></h2>
				<?php
					$subtitle = get_post_meta( $post->ID, 'subtitle', true );
				if ( '' !== $subtitle ) {
					echo '<p class="header-subtitle">' . esc_html( $subtitle ) . '</p>';
				}
				?>
				<?php if ( ( 'show' === $reendex_post_meta ) && ( false === $disable_post_meta ) || ! ( 'hide' === $reendex_post_meta ) && ! ( true !== $disable_post_meta ) ) : ?>
					<div class="entry-meta">
						<div class="post-meta-elements">
							<?php if ( 'enable' === $show_avatar ) : ?>
							<div class="post-meta-author">
								<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) );?>" class="reendex-author-avatar"> <?php echo get_avatar( get_the_author_meta( 'ID' ) , $avatar_size ); ?></a>
								<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ) ); ?>"><?php the_author(); ?></a>
							</div>
							<?php endif;?>
							<?php if ( 'enable' === $show_date ) : ?>
							<div class="post-meta-date">
								<?php get_template_part( 'template-parts/content-post-meta-date' ); ?>
							</div><!-- ./post-meta-date -->
							<?php endif;?>
							<span class="post-meta-comments"> <i class="fa fa-comment-o"></i><?php comments_popup_link( esc_html__( 'Leave a comment', 'reendex' ), esc_html__( '1 Comment', 'reendex' ), esc_html__( '% Comments', 'reendex' ) ); ?></span>
							<div class="post-meta-edit"><?php edit_post_link( esc_html__( 'Edit', 'reendex' ), '<i class="fa fa-pencil-square-o"></i> <span class="edit-link">', '</span>' ); ?></div>
						</div><!-- /.post-meta-elements -->
					</div><!-- /.entry-meta -->
				<?php endif;?>
				<?php if ( ( 'show' === $reendex_share_all ) && ( 'disable' === get_theme_mod( 'reendex_share_display_all' ) ) || ! ( 'hide' === $reendex_share_all ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_display_all' ) ) ) : ?>
				<div class="share-wrapper">
					<?php get_template_part( 'template-parts/content-share' ); ?>
				</div>
				<?php endif; ?>			
			</header><!-- /.entry-header -->
				<div class="entry-content">
					<?php the_content();?>
					<?php wp_link_pages( array(
						'before'       => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'reendex' ) . '</span>',
						'after'        => '</div>',
						'link_before'  => '<span>',
						'link_after'   => '</span>',
						'pagelink'     => '<span class="screen-reader-text">' . esc_html__( 'Page', 'reendex' ) . ' </span>%',
						'separator'    => '<span class="screen-reader-text">, </span>',
					) );
					?>
				</div><!-- /.entry-content -->
				<div class="entry-footer">
					<?php reendex_content_nav(); ?>
					<div class="post-category">
						<span>Posted in:</span><?php echo wp_kses_post( get_the_category_list() ); ?>
					</div>
				<div class="post-tags">
				<?php the_tags( '<ul class="tags clearfix"><li><span>Tags:</span></li><li>', '</li><li>', '</li></ul>' ); ?>
				</div>
				</div><!-- /.entry-footer -->
			<?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			?>
			<?php
				$show_author_related = get_theme_mod( 'reendex_author_related_show', 'enable' );
			if ( 'disable' !== $show_author_related ) :
				get_template_part( 'inc/reendex-author-related-posts' );
			endif;
				$show_related_category = get_theme_mod( 'reendex_related_category_show', 'enable' );
			if ( 'disable' !== $show_related_category ) :
				get_template_part( 'inc/reendex-related-posts' );
			endif;
			?>
			</div><!-- /.entry-main -->
		</div><!-- /.post post-full clearfix -->
	</article><!-- #post-## -->
<?php get_sidebar(); ?><!-- SIDEBAR -->
</div><!-- /.post-style-5 container main-container -->
