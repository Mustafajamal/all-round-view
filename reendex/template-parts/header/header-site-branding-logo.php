<?php
/**
 * Site branding Logo.
 *
 * @package Reendex
 */

?>
<div class="header-logo">
	<?php if ( get_theme_mod( 'reendex_logo' ) ) : ?>
		<div class='site-logo'>
			<a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'reendex_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
		</div><!-- /.site-logo -->
	<?php else : ?>
		<div class="site-branding-text">
			<h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
			<h4 class='site-description'><?php bloginfo( 'description' ); ?></h4>
		</div><!-- /.site-branding-text -->
	<?php endif; ?>
</div><!-- /.header-logo -->
