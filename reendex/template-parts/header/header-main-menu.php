<?php
/**
 * Main Menu.
 *
 * @package Reendex
 */

/**
 * Metabox data.
 */
$reendex_second_menu = get_post_meta( get_the_ID(),'reendex_page_second_menu',true );
$reendex_bottom_menu = get_post_meta( get_the_ID(),'reendex_second_menu',true );
$reendex_clock = get_post_meta( get_the_ID(),'reendex_page_clock',true );
?>

<div id="nav-wrapper">
	<nav class="navbar" id="fixed-navbar"> 
		<?php
		if ( has_nav_menu( 'mainmenu' ) ) { ?>
			<div class="main-menu nav" id="fixed-navbar-toggle"> 
				<div class="container"> 
					<div class="main_menu">
						<div class="container">
							<!-- Collect the nav links from WordPress -->
							<div class="collapse navbar-collapse" id="bootstrap-nav-collapse">         
								<?php $args = array(
									'theme_location'    => 'mainmenu',
									'depth'             => 0,
									'container'         => false,
									'menu_class'        => 'nav navbar-nav mega-main-menu',
									'walker'            => new Reendex_Bootstrap_Navwalker(),
								);
								wp_nav_menu( $args );
								?>
							</div><!-- /.collapse navbar-collapse -->
						</div><!-- /.container -->
					</div><!-- /.main_menu -->                            
				</div><!-- /.container -->                         
			</div><!-- /.main-menu nav -->                     
		<?php
		}
		if ( ( 'hide' != $reendex_second_menu ) || ( 'chide' != $reendex_clock ) ) : ?>
			<div class="second-menu navbar" id="nav-below-main"> 
				<div class="container"> 
					<?php
					//if ( 'show' == $reendex_second_menu || '' == $reendex_second_menu ) { ?>
						<div class="collapse navbar-collapse nav-below-main"> 
							<?php
							if ( 'world' === $reendex_bottom_menu ) {
								reendex_world_bottom_menu();
							} elseif ( 'news' === $reendex_bottom_menu ) {
								reendex_news_bottom_menu();
							} elseif ( 'sport' === $reendex_bottom_menu ) {
								reendex_sport_bottom_menu();
							} elseif ( 'health' === $reendex_bottom_menu ) {
								reendex_health_bottom_menu();
							} elseif ( 'travel' === $reendex_bottom_menu ) {
								reendex_travel_bottom_menu();
							} elseif ( 'entertainment' === $reendex_bottom_menu ) {
								reendex_entertainment_bottom_menu();
							} else {
								reendex_main_menu_bottom();
							}
							?>                                                                  
						</div><!-- /.collapse navbar-collapse -->
					<?php
					//}
					?>                                           
					<?php
					$options = reendex_get_theme_options();
					$reendex_date_format = $options['reendex_date_format'];
					$reendex_time_type = get_theme_mod( 'reendex_time_type', 'client-side' );
					if ( get_theme_mod( 'reendex_date_display', 0 ) == 1 ) :
						if ( 'cshow' == $reendex_clock || '' == $reendex_clock ) { ?>
							<div class="clock second_menu"> 
								<?php if ( 'client-side' === $reendex_time_type ) { ?>
									<div id="time"></div> 
								<?php } else {
									echo '<div id="time">' . date_i18n( get_option( 'time_format' ) ) . '</div>';
} ?>
								<div id="date"><?php echo esc_html( date_i18n( $reendex_date_format ) ); ?></div>                                 
							</div> 
						<?php
					}
					endif; ?>
				</div><!-- /.container -->                         
			</div><!-- /.second-menu navbar -->
		<?php endif;?>                    
	</nav><!-- /#site-navigation -->
</div><!-- /#nav-wrapper -->
