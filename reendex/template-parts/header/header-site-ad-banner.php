<?php
/**
 * Site Ad.
 *
 * @package Reendex
 */

?>	
<div class="container header-style-<?php if ( get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-one' ) {
	echo esc_html( 'one' );
} elseif ( get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-two' ) {
	echo esc_html( 'two' );
} elseif ( get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-three' ) {
	echo esc_html( 'three' );
} elseif ( get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-four' ) {
	echo esc_html( 'four' );
} elseif ( get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-five' ) {
	echo esc_html( 'five' );
} else {
	echo esc_html( 'six' );
}
?>">
	<?php if ( get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-four' || get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-six' ) {
		echo '<div class="logo-ad-wrapper clearfix">';
} else {
	echo '<div>';
}
?>
	<?php //do_action( 'reendex_header_ad' ); ?>
	<?php echo do_shortcode( '[wd_ads advert="36802"]' ); ?>
<?php echo '</div>'; ?>
</div><!-- /.container  -->
