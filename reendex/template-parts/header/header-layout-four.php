<?php
/**
 * Header style 4
 *
 * @package Reendex
 */

if ( ! function_exists( 'reendex_top_menu' ) ) :
	/**
	 * Top Menu codes
	 */
	function reendex_top_menu() {
		?>
		<div class="container-fluid header-style-four">
			<div class="container top-menu header-style-four"> 
				<div class="top-menu-wrapper">
					<?php
					/**
					 * Main top bar.
					 */
					$current_user = wp_get_current_user();
					$options = reendex_get_theme_options();
					?>
					<?php
					$show_social = get_theme_mod( 'reendex_topbar_socialmedia_show', 'enable' );
					if ( 'enable' === $show_social ) :
					?>
						<ul class="left-top-menu">
							<?php
								$facebook = $options['reendex_facebook_link'];
								$twitter = $options['reendex_twitter_link'];
								$pinterest = $options['reendex_pinterest_link'];
								$g = $options['reendex_googleplus_link'];
								$vimeo = $options['reendex_vimeo_link'];
								$youtube = $options['reendex_youtube_link'];
								$linkedin = $options['reendex_linkedin_link'];
								$rss = $options['reendex_rss_link'];
								$instagram = $options['reendex_instagram_link'];
							if ( strlen( $facebook ) > 0 ) {
								echo '<li class="facebook"><a class="facebook" href="' . esc_url( $facebook ) . '"><i class="fa fa-facebook"></i></a></li>';
							}
							if ( strlen( $twitter ) > 0 ) {
								echo '<li class="twitter"><a class="twitter" href="' . esc_url( $twitter ) . '"><i class="fa fa-twitter"></i></a></li>';
							}
							if ( strlen( $pinterest ) > 0 ) {
								echo '<li class="pinterest"><a class="pinterest" href="' . esc_url( $pinterest ) . '"><i class="fa fa-pinterest-p"></i></a></li>';
							}
							if ( strlen( $g ) > 0 ) {
								echo '<li class="gplus"><a class="google-plus" href="' . esc_url( $g ) . '"><i class="fa fa-google-plus"></i></a></li>';
							}
							if ( strlen( $vimeo ) > 0 ) {
								echo '<li class="vimeo"><a class="vimeo" href="' . esc_url( $vimeo ) . '"><i class="fa fa-vimeo-square"></i></a></li>';
							}
							if ( strlen( $youtube ) > 0 ) {
								echo '<li class="youtube"><a class="youtube" href="' . esc_url( $youtube ) . '"><i class="fa fa-youtube"></i></a></li>';
							}
							if ( strlen( $linkedin ) > 0 ) {
								echo '<li class="linkedin"><a class="linkedin" href="' . esc_url( $linkedin ) . '"><i class="fa fa-linkedin"></i></a></li>';
							}
							if ( strlen( $rss ) > 0 ) {
								echo '<li class="rss"><a class="rss" href="' . esc_url( $rss ) . '"><i class="fa fa-rss"></i></a></li>';
							}
							if ( strlen( $instagram ) > 0 ) {
								echo '<li class="instagram"><a class="instagram" href="' . esc_url( $instagram ) . '"><i class="fa fa-instagram"></i></a></li>';
							}
							?>
						</ul><!-- /.left-top-menu -->
					<?php endif;
					?>
					<div class="logo-ad-wrapper clearfix">
						<?php get_template_part( 'template-parts/header/header', 'site-branding-logo' ); ?>
					</div>
					<ul class="right-top-menu pull-right"> 
						<?php $location_link = get_theme_mod( 'reendex_top_address' ); if ( ! empty( $location_link ) ) : ?>
							<li class="contact">
								<a><i class="fa fa-map-marker fa-i"></i> <?php echo esc_html( $location_link ); ?></a>
							</li>
						<?php endif; ?>
						<?php
						if ( is_user_logged_in() ) {
							echo '<li class="about">
								<a href="' . esc_url( get_author_posts_url( $current_user->ID ) ) . '"><i class="fa fa-user fa-i"></i></a> 
							</li> ';
						} else {
							echo '<li class="about">
								<a href="' . esc_url( wp_login_url( get_permalink() ) ) . '"><i class="fa fa-sign-in"></i></a> 
							</li> ';
						}
						?>
						<li> 
							<?php
							$show_search_in_header = $options['reendex_show_search_in_header'];
							if ( true === $show_search_in_header ) : ?>
								<div class="header-search-box">
									<a href="#" class="search-icon"><i class="fa fa-search"></i></a>
									<div class="search-box-wrap">
										<div class="search-close-btn"><i class="pe-7s-close"></i></div>
										<?php get_search_form(); ?>
									</div>
								</div><!-- /.header-search-box -->
							<?php endif; ?>                              
						</li>                             
					</ul><!-- /.right-top-menu -->                         
				</div><!-- /.top-menu-wrapper -->                     
			</div><!-- /.container top-menu -->                 
		</div><!-- /.container-fluid -->
		<?php
	}
endif;
add_action( 'reendex_header_action', 'reendex_top_menu', 20 );

if ( ! function_exists( 'reendex_site_branding' ) ) :
	/**
	 * Site branding Logo
	 */
	function reendex_site_branding() {
		get_template_part( 'template-parts/header/header', 'site-ad-banner' );
	}
endif;
add_action( 'reendex_header_action', 'reendex_site_branding', 40 );

if ( ! function_exists( 'reendex_site_navigation' ) ) :
	/**
	 * Site navigation codes
	 */
	function reendex_site_navigation() {
		get_template_part( 'template-parts/header/header', 'main-menu' );
	}
endif;
add_action( 'reendex_header_action', 'reendex_site_navigation', 40 );
