<?php
/**
 * Template Name: left In Videos
 *
 * @package Reendex
 */

get_header();
$options = reendex_get_theme_options();
?>
<?php
if ( ! current_user_can( 'edit_themes' ) || ! is_user_logged_in() ) {
	$show_comingsoon = get_theme_mod( 'reendex_comingsoon_show', 'disable' );
	if ( 'disable' !== $show_comingsoon ) {
		get_template_part( 'coming', 'soon' );
		exit();
	}
}
?>
<div class="main-container">
	<div class="page-content">
		<div class="container innerpage">
			<?php
				$reendex_id = get_the_ID();
				$reendex_page_breadcrumbs = get_post_meta( $reendex_id,'reendex_page_breadcrumbs',true );
			if ( 'show' == $reendex_page_breadcrumbs ) {
				reendex_custom_breadcrumbs();
			}
			?>
			<div class="left-sidebar">
				<?php if ( is_active_sidebar( 'innerpage_invideos' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_invideos' ); ?>
					</div><!-- #primary-sidebar -->
				<?php endif; ?>
				<?php if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif; ?>
			</div>         
			<div class="content_main">
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
			</div>         
		</div><!-- ..container -->
	</div><!-- /.page-content -->
</div><!-- /.main-container -->
<?php get_footer(); ?>