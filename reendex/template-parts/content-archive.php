<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

$options = reendex_get_theme_options();
$excerpt_length = $options['reendex_excerpt_length'];
$readmore_text = $options['reendex_readmore_text'];
$disable_post_footer = $options['reendex_disable_post_footer'];
$reendex_image_archives = get_theme_mod( 'reendex_image_archives', 'enable' );
$reendex_archive_show_category = get_theme_mod( 'reendex_archive_show_category', 'enable' );
$disable_post_meta = $options['reendex_disable_postmeta'];
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="archive-gallery">
		<?php if ( 'disable' != $reendex_image_archives && has_post_thumbnail() || 'disable' != $reendex_image_archives && has_post_format( 'video' ) && has_post_thumbnail() ) { ?>
			<div class="entry-image">
				<a class="img-link" href="<?php the_permalink(); ?>" rel="bookmark">
					<?php the_post_thumbnail( 'full', array(
						'class' => 'img-responsive img-full',
						'alt' => get_the_title(),
					) ); ?>
				</a>
				<?php if ( has_post_format( 'video' ) ) : ?>
					<span class="play-icon"></span>
				<?php endif; ?>
				<?php if ( 'disable' !== $reendex_archive_show_category ) :
					$i = 0;
					foreach ( ( get_the_category() ) as $category ) {
						$i++;
						$category_id = get_cat_ID( $category->cat_name );
						$category_link = get_category_link( $category->term_id );
						if ( 1 == $i ) {
							?>
								<span><a class="label-14" href="<?php echo esc_url( $category_link ); ?>"><?php echo esc_html( $category->cat_name ); ?></a></span>
							<?php
						}
					}
				endif; ?>
			</div><!-- /.entry-image -->
		<?php } ?>
		<div class="entry-content">
			<header class="entry-header">
				<h2 class="entry-post-title">
					<a href="<?php the_permalink() ?>">
						<?php
						if ( get_the_title( $post->ID ) ) {
							the_title();
						} else {
							the_time( get_option( 'date_format' ) );
						}
						?>
					</a>
				</h2><!-- /.entry-post-title -->
				<div class="post-meta-elements">
					<?php
					if ( false != $disable_post_meta ) : ?>
						<div class="post-meta-author"> <i class="fa fa-user"></i><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ) ); ?>"><?php the_author(); ?></a></div>
						<div class="post-meta-date">
							<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
								<i class="fa fa-calendar"></i><a href="<?php echo esc_url( get_day_link( get_post_time( 'Y' ), get_post_time( 'm' ), get_post_time( 'd' ) ) ); ?>"><?php the_time( 'F d, Y','reendex' ); ?></a>
							<?php } else { ?>
								<i class="fa fa-calendar"></i><span class="date updated"><a href="<?php echo esc_url( get_day_link( get_the_date( 'Y' ), get_the_date( 'm' ), get_the_date( 'd' ) ) ) ?>"><?php echo esc_html( get_the_modified_date( 'F j, Y' ) ); ?></a></span>
							<?php } ?>
						</div><!-- /.post-meta-date -->
					<?php endif;?>
					<?php if ( is_sticky() ) : ?>
						<span class="sticky-post"><?php esc_html_e( 'Featured', 'reendex' ); ?></span>
					<?php endif; ?>				
				</div><!-- /.post-meta-elements -->
				<h1>asdasdas</h1>
			</header><!-- /.entry-header -->
			<p><?php echo esc_html( reendex_read_more( get_the_excerpt(), $excerpt_length ) ); ?></p>
			<a href="<?php the_permalink();?>" target="_blank"><span class="read-more"><?php echo esc_html( $readmore_text ); ?></span></a>
			<?php
			if ( false != $disable_post_footer ) : ?>
				<footer class="entry-footer">
					<?php reendex_entry_footer(); ?>
				</footer>
			<?php endif;?>			
		</div><!-- /.entry-content -->
	</div><!-- /.archive-gallery -->
</article><!-- #post-## -->
