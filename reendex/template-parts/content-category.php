<?php
/**
 * Template part for displaying Category Archive loop
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

$options = reendex_get_theme_options();
$excerpt_length = $options['reendex_category_excerpt_length'];
$reendex_category_show_category = get_theme_mod( 'reendex_category_show_category', 'enable' );
$reendex_image_category_archives = get_theme_mod( 'reendex_image_category_archives', 'enable' );
$disable_post_meta = $options['reendex_category_disable_postmeta'];
$disable_post_footer = $options['reendex_category_disable_post_footer'];
$title_length =12;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="archive-gallery">
		<?php if ( 'disable' != $reendex_image_category_archives && has_post_thumbnail() || 'disable' != $reendex_image_category_archives && has_post_format( 'video' ) && has_post_thumbnail() ) { ?>
			<div class="entry-image">
				<a class="img-link" href="<?php the_permalink(); ?>" rel="bookmark">
					<?php the_post_thumbnail( 'full', array(
						'class' => 'img-responsive img-full',
						'alt' => get_the_title(),
					) ); ?>
				</a>
				<?php if ( has_post_format( 'video' ) ) : ?>
					<span class="play-icon"></span>
				<?php endif; ?>
				<?php if ( 'disable' !== $reendex_category_show_category ) :
					$i = 0;
					
					$term_list = wp_get_post_terms($post->ID, 'category', ['fields' => 'all']);
						foreach($term_list as $term) {
							 $i++;
						   if( get_post_meta($post->ID, '_yoast_wpseo_primary_category',true) == $term->term_id ) {
							   $category_link = get_category_link( $term->term_id );
							   ?>
								<span><a class="label-14" href="<?php echo esc_url( $category_link ); ?>"><?php echo esc_html( $term->name ); ?></a></span>
													<?php
							// this is a primary category
						   }
						}
					
					/* foreach ( ( get_the_category() ) as $category ) {
						$i++;
						$category_id = get_cat_ID( $category->cat_name );
						$category_link = get_category_link( $category->term_id );
						
						if ( 1 == $i ) {
							
						}
					} */
				endif; ?>
			</div><!-- /.entry-image -->
		<?php } ?>
		<div class="entry-content">
			<header class="entry-header">
				<div class="child_archive1"> 
					<h3 class="title-left title-style04 underline04">
						<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( $atts['target'] ); ?>"><?php reendex_short_title($title_length,'...'); ?></a>
					</h3> 
				</div><!-- /.title-left -->
				<div class="post-meta-elements archive_line">
					<?php
					if ( false != $disable_post_meta ) : ?>
						
						<?php
								$current_id = get_the_ID();
								$author = get_field( 'hide_author',$current_id);
								$post_obj = get_post($current_id); // gets author from post
								$authid = $post_obj->post_author;
								$author_url = get_author_posts_url($authid );		
								$author = $author[0];
								if( $author == 'yes' ) {
										
								}else{
								?>
								<div class="post-meta-author">
								<i class="fa fa-user"></i>
								<a class="author_link" href="<?php echo $author_url; ?>">
								<?php the_author(); ?>
								</a>
								</div>
								<?php
								}
								?>
						<div class="post-meta-date">
							<?php if ( get_theme_mod( 'reendex_updated_date' ) != '1' ) { ?>
								<i class="fa fa-calendar"></i><a href="<?php echo esc_url( get_day_link( get_post_time( 'Y' ), get_post_time( 'm' ), get_post_time( 'd' ) ) ); ?>"><?php the_time( 'F d, Y','reendex' ); ?></a>
							<?php } else { ?>
								<i class="fa fa-calendar"></i><span class="date updated"><a href="<?php echo esc_url( get_day_link( get_the_date( 'Y' ), get_the_date( 'm' ), get_the_date( 'd' ) ) ) ?>"><?php echo esc_html( get_the_date( 'F j, Y' ) ); ?></a></span>
							<?php } ?>
						</div><!-- /.post-meta-date -->
						<span class="archive_view">
							  <?php 
							  $post_id_get = $post->ID; ?>
							  <i class="fa fa-eye"></i>
							  <?php
							  echo do_shortcode( '[views id="'.$post_id_get.'"]' ); ?>
							  
						</span>
					<?php endif;?>
					<?php if ( is_sticky() ) : ?>
						<span class="sticky-post"><?php esc_html_e( 'Featured', 'reendex' ); ?></span>
					<?php endif; ?>				
				</div><!-- /.post-meta-elements -->
			</header><!-- /.entry-header -->
			
			<p><?php 
			$excerpt_length = 240;
			echo esc_html( reendex_read_more( get_the_excerpt(), $excerpt_length ) ); ?></p>
			<?php
				$readmore_text = $options['reendex_category_readmore_text'];
			?>
			<a href="<?php the_permalink();?>" target="_blank"><span class="read-more"><?php echo esc_html( $readmore_text ); ?></span></a>
			<?php
			if ( false != $disable_post_footer ) : ?>
				<footer class="entry-footer">
					<?php reendex_entry_footer(); ?>
				</footer>
			<?php endif;?>
		</div><!-- /.entry-content -->
	</div><!-- /.archive-gallery -->
</article><!-- #post-## -->
