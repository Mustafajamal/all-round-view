<?php
/**
 * Template Name: Page Template Home (100% Width)
 *
 * @package Reendex
 */

get_header();
$options = reendex_get_theme_options();
?>
<?php
if ( ! current_user_can( 'edit_themes' ) || ! is_user_logged_in() ) {
	$show_comingsoon = get_theme_mod( 'reendex_comingsoon_show', 'disable' );
	if ( 'disable' !== $show_comingsoon ) {
		get_template_part( 'coming', 'soon' );
		exit();
	}
}
?>
<div class="main-container front-page">
	<div class="page-content">
		<div class="container">
			<?php
				$reendex_id = get_the_ID();
				$reendex_page_breadcrumbs = get_post_meta( $reendex_id,'reendex_page_breadcrumbs',true );
			if ( 'show' == $reendex_page_breadcrumbs ) {
				reendex_custom_breadcrumbs();
			}
			?>
				
			<?php //while ( have_posts() ) : the_post(); ?>
				<!-- <article id="post-<?php // the_ID(); ?>" <?php //post_class(); ?>>
					<div class="entry-content">
						<?php //the_content(); ?>
					</div>
				</article> -->
			<?php //endwhile;
				$content_post = get_post($reendex_id);
				$content = $content_post->post_content;
				$content = apply_filters('the_content', $content);
				$content = str_replace(']]>', ']]&gt;', $content);
				echo $content; 
			?> 
					
		</div><!-- ..container -->
	</div><!-- /.page-content -->
</div><!-- /.main-container -->
<?php get_footer(); ?>
