/**
 * Select-category.js
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * Copyright: (c) 2018 Via-Theme, http://via-theme.com
 *
 * @package Reendex
 */

	(function($) {
		"use strict";
		jQuery( document ).ready(function() {
			var id = jQuery( 'input[name="post_format"]:checked' ).attr( 'id' );
			if (id == 'post-format-0') {
				jQuery( '#reendex__reendex_post_formats' ).hide();
			} else {
				jQuery( '#reendex__reendex_post_formats' ).show();
			}
			if ( id == 'post-format-video' ) {
				jQuery( '#reendex__reendex_post_formats' ).show();
				jQuery( '.cmb2-id-reendex-post-formate-vdo' ).show();
			} else {
				jQuery( '.cmb2-id-reendex-post-formate-vdo' ).hide();
			}
			jQuery( 'input[name="post_format"]' ).change(function() {
				jQuery( '.cmb2-id-reendex-post-formate-vdo' ).hide();
				var id = jQuery( 'input[name="post_format"]:checked' ).attr( 'id' );
				if ( id == 'post-format-0' ) {
					jQuery( '#reendex__reendex_post_formats' ).hide();
				} else {
					jQuery( '#reendex__reendex_post_formats' ).show();
				}
				if ( id == 'post-format-video') {
					jQuery( '#reendex__reendex_post_formats' ).show();
					jQuery( '.cmb2-id-reendex-post-formate-vdo' ).show();
				} else {
					jQuery( '.cmb2-id-reendex-post-formate-vdo' ).hide();
				}
			});
		})
	})(jQuery);
