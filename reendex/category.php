<?php
/**
 * The template for displaying Category Archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

get_header();

$reendex_sidebar = get_theme_mod( 'reendex_category_archive_layout', 'rightsidebar' );
if ( 'rightsidebar' === $reendex_sidebar ) {
		$reendex_sidebar = 'right';
} elseif ( 'leftsidebar' === $reendex_sidebar ) {
		$reendex_sidebar = 'left';
} else {
		$reendex_sidebar = 'no';
}
$options = reendex_get_theme_options();
$category_banner_image_link = reendex_get_category_default_banner();
$category_content_style = get_theme_mod( 'reendex_category_content_style' );
$categories = get_queried_object();
$category_name = $categories->name;
$term = get_term_by('name', $category_name, 'wplss_logo_showcase_cat');

//$title_ad = get_term_by('slug', $category_slug, 'wplss_logo_showcase_cat');
//$title_ad = 'Home';
$cat_id = $term->term_id;
$cat_name = $term->name;
?>
<?php
if ( ! current_user_can( 'edit_themes' ) || ! is_user_logged_in() ) {
	$show_comingsoon = get_theme_mod( 'reendex_comingsoon_show', 'disable' );
	if ( 'disable' !== $show_comingsoon ) {
		get_template_part( 'coming', 'soon' );
		exit();
	}
}
?>
<div class="home-<?php echo esc_attr( $reendex_sidebar ); ?>-side">
	<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_category_banner_show', 'disable' ) ) ) : ?>
	<div class="archive-page-header">
		<div class="container-fluid">
			<div>
				<div class="archive-nav-inline">
					<div id="parallax-section3">
						<div class="image1 img-overlay3" style="background-image: url('<?php echo esc_url( $category_banner_image_link ); ?>'); ">
						<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_category_banner_title', 'enable' ) ) ) : ?>
					<div class="container archive-menu">
						<?php
							$reendex_id = get_the_ID();
							$reendex_page_breadcrumbs = get_post_meta( $reendex_id, 'reendex_page_breadcrumbs', true );
								reendex_custom_breadcrumbs();
							$title  = esc_html__( 'Category Archives' , 'reendex' );
						?>
						<h1 class="page-title"><span><?php echo esc_html( $title ); ?></span></h1>
					</div><!-- .container archive-menu -->
						<?php endif; ?>
						</div><!-- /.image1 img-overlay3 -->
					</div><!-- /#parallax-section3 -->
				</div><!-- /.archive-nav-inline -->
			</div>
		</div><!-- /.container-fluid -->
	</div><!-- /.archive-page-header -->
	<?php endif; ?>
	<div class="module1"> 
		<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_category_archive_breaking_show', 'enable' ) ) ) :
			get_template_part( 'template-parts/content','breakingnews' );
		endif; ?>				
		<div class="container innerpage">
			<div class="container">
				<div id="primary" class="home-main-content col-xs-12 col-sm-12 col-md-9 content-area">
					<main id="main" class="site-main">
						<header class="page-header">
							<?php
								//the_archive_title( '<h2 class="page-title"><span>', '</span></h2>' );
								echo '<div class="title-style02 red_bg"><h3>';single_cat_title();echo '</h3></div>';
							?>
						</header><!-- /.page-header -->
						<div class="
							<?php
							if ( 2 == $category_content_style ) {
								echo 'content-archive-wrapper-2';
							} elseif ( 3 == $category_content_style ) {
								echo 'content-archive-wrapper-3';
							} elseif ( 4 == $category_content_style ) {
								 echo 'content-archive-wrapper-4';
							} else {
								echo 'content-archive-wrapper-1'; } ?>">
							<?php
							if ( have_posts() ) :
								/* Start the Loop */
								while ( have_posts() ) : the_post();
									get_template_part( 'template-parts/content', 'category' );
								endwhile;
							else :
								get_template_part( 'template-parts/content', 'none' );
							endif;
							?>
						</div>                                
						<div class="navigation">
							<?php
							// Previous/next page navigation.
							the_posts_pagination( array(
								'prev_text'          => esc_html__( 'Previous page', 'reendex' ),
								'next_text'          => esc_html__( 'Next page', 'reendex' ),
								'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'reendex' ) . ' </span>',
							) );
							?>
							<div class="clearfix"></div>
						</div><!-- /.navigation -->                     
					</main><!-- /#main -->
				</div><!-- /#primary -->
			
				<?php 
				get_sidebar(); ?><!-- SIDEBAR -->
			</div><!-- /.container --> 
		</div><!-- /.row -->                      
	</div><!-- /.module --> 
</div><!-- /.home -->
<div id="footer_ad_section"> <?php
/* <pre><?php print_r($cat_id); ?></pre>
<pre><?php print_r($cat_name); ?></pre>  */

	echo do_shortcode('[logoshowcase cat_id="'.$cat_id.'" cat_name="'.$cat_name.'" slides_column="6"]'); ?>
</div>
<?php
get_footer(); ?>
